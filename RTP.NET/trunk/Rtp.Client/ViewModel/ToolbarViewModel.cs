﻿
using GalaSoft.MvvmLight;

namespace Rtp.Client.ViewModel
{
    internal class ToolbarCommandMessage
    {
        public ToolbarCommandType Argument { get; set; }
    }

    public enum ToolbarCommandType
    {
        NewConnection
        , AppInfo
        
    }

    class ToolbarViewModel : ViewModelBase
    {
    }
}
