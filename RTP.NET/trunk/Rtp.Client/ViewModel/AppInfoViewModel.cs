﻿using System;
using System.Reflection;
using System.Security.Policy;
using GalaSoft.MvvmLight;

namespace Rtp.Client.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// </summary>
    public class AppInfoViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the AppInfoViewModel class.
        /// </summary>
        public AppInfoViewModel()
        {
            ////if (IsInDesignMode)
            ////{
            ////    // Code runs in Blend --> create design time data.
            ////}
            ////else
            ////{
            ////    // Code runs "for real": Connect to service, etc...
            ////}
        }

        protected Assembly AssemblyToFetchInfo
        {
            get { return Assembly.GetExecutingAssembly(); }
        }

        public string VersionInfo
        {
            get { return AssemblyToFetchInfo.GetName().Version.ToString(); }
        }

        public string ApplicationName
        {
            get { return AssemblyToFetchInfo.GetName().Name; }
        }

        public string BuildDate
        {
            get
            {
                return ((AssemblyDescriptionAttribute)Attribute.GetCustomAttribute(
                    AssemblyToFetchInfo, typeof(AssemblyDescriptionAttribute))).Description;
            }
        }

        ////public override void Cleanup()
        ////{
        ////    // Clean own resources if needed

        ////    base.Cleanup();
        ////}
    }
}