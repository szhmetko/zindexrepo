﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Practices.ServiceLocation;
using Rtp.Infrastructure.Config;
using Rtp.Model.Line;
using Rtp.Shared;

namespace Rtp.Client.ViewModel
{

    public class MainViewModel : ViewModelBase
    {
        private IMessageService _messageService;
        public MainViewModel(IMessageService messageService)
        {
            _messageService = messageService;
            ToolbarCommand.Execute(ToolbarCommandType.NewConnection);
        }

        private ModuleViewModel _activeModule;

        public ModuleViewModel ActiveModule
        {
            get { return _activeModule; }
            set
            {
                _activeModule = value;
                RaisePropertyChanged("ActiveModule");
            }
        }

        public int? SelIndex { get; set; }

        private string _title;
        public string Title
        {
            get { return _title; }
            set { _title = value;
                RaisePropertyChanged("Title");
            }
        }

        private ICommand _toolbarCommand;
        public ICommand ToolbarCommand
        {
            get
            {
                if (_toolbarCommand == null)
                {
                    _toolbarCommand = new RelayCommand<ToolbarCommandType>(

                        (arg) =>
                            {
                                if(arg == ToolbarCommandType.AppInfo)
                                {
                                    _messageService.ShowApplicationInfo();
                                }
                                if (arg == ToolbarCommandType.NewConnection)
                                {
                                    LineSelect wnd = new LineSelect();
                                    var res = wnd.ShowDialog();

                                    if (res == true)
                                    {
                                        try
                                        {
                                            using (
                                                var modulesRepository =
                                                    ServiceLocator.Current.GetInstance<IModulesRepository>())
                                            {

                                                Modules = (from module in modulesRepository.FindAll()
                                                          select ModulesFactory.CreateModuleViewModel(module)).ToList();
                                            }
                                        }
                                        catch(System.Exception exception)
                                        {
                                            _messageService.ShowError(exception) ;
                                        }

                                        RaisePropertyChanged("Modules");

                                        //ActiveModule = Modules.FirstOrDefault();
                                        //SelIndex = 0;
                                        //RaisePropertyChanged("SelIndex");

                                        Title = AppConfig.Instance.CurrentLineName;
                                    }
                                }
                            }
                        );

                }

                return _toolbarCommand;
            }
        }

        private bool _isLegendVisibleState;
        public bool IsLegendVisibleState
        {
            get { return _isLegendVisibleState; }
            set
            {
                bool oldValue = _isLegendVisibleState;
                _isLegendVisibleState = value;
                RaisePropertyChanged("IsLegendVisibleState", oldValue, _isLegendVisibleState, true);
            }
        }

        private IEnumerable<ModuleViewModel> _modules;
        public IEnumerable<ModuleViewModel> Modules
        {
            get { return _modules; }
            set
            {
                if(_modules != null)
                {
                    foreach (var moduleViewModel in _modules)
                    {
                        moduleViewModel.Cleanup();
                    }
                }

                _modules = value;
            }
        }
    }

}