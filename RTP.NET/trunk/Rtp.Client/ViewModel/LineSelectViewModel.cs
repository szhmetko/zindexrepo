﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Rtp.Infrastructure.Config;
using Rtp.Model.Master;
using Rtp.Shared;


namespace Rtp.Client.ViewModel
{
    public static class DialogCloser
    {
        public static readonly DependencyProperty DialogResultProperty =
            DependencyProperty.RegisterAttached(
                "DialogResult",
                typeof(bool?),
                typeof(DialogCloser),
                new PropertyMetadata(DialogResultChanged));

        private static void DialogResultChanged(
            DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            var window = d as Window;
            if (window != null)
                window.DialogResult = e.NewValue as bool?;
        }
        public static void SetDialogResult(Window target, bool? value)
        {
            target.SetValue(DialogResultProperty, value);
        }
    }

    public class LineSelectViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the LineSelectViewModel class.
        /// </summary>
        public LineSelectViewModel(ILinesRepository linesRepository)
        {

            LinesAvailable = from line in linesRepository.FindAll() select new LineViewModel(line);


            SimpleCommand = new RelayCommand(() =>
                                                 {
                                                     if (SelectedLine != null)
                                                     {
                                                         AppConfig.Instance.CurentLineConnection = SelectedLine.Model;
                                                         AppConfig.Instance.CurrentLineName = SelectedLine.Model.Title;
                                                         DialogResult = true;
                                                     }
                                                 });
        }

        private bool? _dialogResult;
        public bool? DialogResult
        {
            get { return _dialogResult; }
            set
            {
                _dialogResult = value;
                RaisePropertyChanged("DialogResult");
            }
        }

        public RelayCommand SimpleCommand { get; private set; }

        public IEnumerable<LineViewModel> LinesAvailable { get; private set; }

        public LineViewModel SelectedLine { get; set; }
    }
}
