﻿/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocatorTemplate xmlns:vm="clr-namespace:Rtp.Client.ViewModel"
                                   x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"
  
  OR (WPF only):
  
  xmlns:vm="clr-namespace:Rtp.Client.ViewModel"
  DataContext="{Binding Source={x:Static vm:ViewModelLocatorTemplate.ViewModelNameStatic}}"
*/

using System.Configuration;
using Microsoft.Practices.ServiceLocation;
using Rtp.Client.Infrastructure;
using Rtp.Infrastructure;
using Rtp.Infrastructure.Config;
using Rtp.Infrastructure.UnitOfWork;
using Rtp.Model;
using Rtp.Model.Line;
using Rtp.Model.Master;
using Rtp.Reporting.PiecesReportView.Model;
using Rtp.Reporting.PiecesReportView.ViewModel;
using Rtp.Repository;
using Rtp.Repository.Repositories;
using Rtp.Shared;
using StructureMap;
using StructureMap.Configuration.DSL;
using StructureMap.ServiceLocatorAdapter;

namespace Rtp.Client.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// <para>
    /// Use the <strong>mvvmlocatorproperty</strong> snippet to add ViewModels
    /// to this locator.
    /// </para>
    /// <para>
    /// In Silverlight and WPF, place the ViewModelLocatorTemplate in the App.xaml resources:
    /// </para>
    /// <code>
    /// &lt;Application.Resources&gt;
    ///     &lt;vm:ViewModelLocatorTemplate xmlns:vm="clr-namespace:Rtp.Client.ViewModel"
    ///                                  x:Key="Locator" /&gt;
    /// &lt;/Application.Resources&gt;
    /// </code>
    /// <para>
    /// Then use:
    /// </para>
    /// <code>
    /// DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"
    /// </code>
    /// <para>
    /// You can also use Blend to do all this with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm/getstarted
    /// </para>
    /// <para>
    /// In <strong>*WPF only*</strong> (and if databinding in Blend is not relevant), you can delete
    /// the Main property and bind to the ViewModelNameStatic property instead:
    /// </para>
    /// <code>
    /// xmlns:vm="clr-namespace:Rtp.Client.ViewModel"
    /// DataContext="{Binding Source={x:Static vm:ViewModelLocatorTemplate.ViewModelNameStatic}}"
    /// </code>
    /// </summary>
    public class ViewModelLocator
    {
        static ViewModelLocator()
        {
            var ioc = CreateServiceLocator();
            ServiceLocator.SetLocatorProvider(() => ioc);
        }

        /// <summary>
        /// Gets the Main property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public MainViewModel Main
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainViewModel>();
            }
        }

        public LineSelectViewModel LinesList
        {
            get { return ServiceLocator.Current.GetInstance<LineSelectViewModel>(); }
        }

        public DateTimePickerViewModel DtPicker
        {
            get { return ServiceLocator.Current.GetInstance<DateTimePickerViewModel>(); }
        }

        public IConnectionInfo MasterConnection
        {
            get
            {
                return ServiceLocator.Current.GetInstance<IConnectionInfo>();
            }
        }

        public AppInfoViewModel AppInfo
        {
            get
            {
                return ServiceLocator.Current.GetInstance<AppInfoViewModel>();
            }
        }

        /// <summary>
        /// Cleans up all the resources.
        /// </summary>
        public static void Cleanup()
        {
        }


        private static IServiceLocator CreateServiceLocator()
        {
            var x = new Registry();

            //registry.ScanAssemblies().IncludeAssembly(Assembly.GetExecutingAssembly().FullName);
            x.Scan(scan =>
                              {
                                  scan.TheCallingAssembly();
                                  scan.AssemblyContainingType<NHUnitOfWork>();
                                  scan.AddAllTypesOf<IUnitOfWork>();
                                  scan.AddAllTypesOf<IConnectionInfo>();
                                  //scan.WithDefaultConventions();
                              });

            //registry.For<IConnectionInfo>().Use(

            //    (c) =>
            //    {
            //        string srv = ConfigurationSettings.AppSettings["MasterHost"];
            //        string usr = ConfigurationSettings.AppSettings["MasterUser"];
            //        string pwd = ConfigurationSettings.AppSettings["MasterPwd"];
            //        string dbn = ConfigurationSettings.AppSettings["MasterDbName"];

            //        return new ConnectionInfo(dbn, usr, srv, pwd);
            //    });

            x.For<IUnitOfWork>().Use(
                (uow) =>
                    {
                        string srv = ConfigurationSettings.AppSettings["MasterHost"];
                        string usr = ConfigurationSettings.AppSettings["MasterUser"];
                        string pwd = ConfigurationSettings.AppSettings["MasterPwd"];
                        string dbn = ConfigurationSettings.AppSettings["MasterDbName"];

                        return new NHUnitOfWork(new ConnectionInfo(dbn, usr, srv, pwd));
                    }
                );
            x.For<IConnectionInfo>().Use((a) =>
                                                    {
                                                        return AppConfig.Instance.CurentLineConnection;
                                                    }

                );

            x.For<ILineUnitOfWork>().Use<NHUnitOfWork>();

            x.For<ILinesRepository>().Use<LinesRepository>();

            x.For<IModulesRepository>().Use<ModulesRepository>();

            x.For<IQueriesRepository>().Use<QueriesRepository>();
            x.For<IDataValuesRepository>().Use<DataValuesRoRepository>();

            x.For<IMessageService>().Use<ClientMessageService>();

            x.For<IPiecesRepository>().Use<PiecesRepository>();

            x.For<IRtpReport>().Use<PiecesReportViewModel>();

            x.For<ISettingRepository>().Use<SettingRepository>();
            //registry.For<IValuesTable>().Use<PiecesValuesTableViewModel>();

            //registry.For<IPiecesReportViewRepository>().Use<DummyPiecesReportViewRepository>();

            x.For<IRawSqlRepository>().Use<RawSqlRepository>();

            x.For<IChannelRoRepository>().Use<ChannelsRoRepository>();

            //if (ViewModelBase.IsInDesignModeStatic)
            //{
            //    registry.ForRequestedType<IDataService>().TheDefaultIsConcreteType<Design.DesignDataService>();
            //}
            //else
            //{
            //    registry.ForRequestedType<IDataService>().TheDefaultIsConcreteType<DataService>();
            //}

            IContainer container = new Container(x);

            //var test = container.GetInstance<ILineUnitOfWork>();
            //var test1 = container.GetInstance<IConnectionInfo>();
            //var test2 = container.GetInstance<IUnitOfWork>();

            return new StructureMapServiceLocator(container);
        }
    }
}