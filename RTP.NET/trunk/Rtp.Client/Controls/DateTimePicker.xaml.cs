﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace Rtp.Client.Controls
{
    /// <summary>
    /// Interaction logic for DateTimePicker.xaml
    /// </summary>
    public partial class DateTimePicker : UserControl
    {
        public DateTimePicker()
        {
            InitializeComponent();
            //Time.ItemsSource = Hours;
        }

        //public static readonly DependencyProperty SelectedDateTimeProperty =
        //    DependencyProperty.Register("SelectedDateTime", typeof (DateTime?), typeof (DateTimePicker)
        //                                , new UIPropertyMetadata());

        public DateTime? SelectedDateTime
        {
            set {
                //SetValue(SelectedDateTimeProperty, value);

                Date.SelectedDate = value;

                Time.SelectedValue = value.HasValue ? value.Value.Hour : 0;
            }
        }

        //public Dictionary<int, string> Hours
        //{
        //    get { 
        //        var data = new Dictionary<int, string>();

        //        for (int i = 0; i < 24; i++)
        //        {
        //            data.Add(i, string.Format("{0:d2}:00", i));
        //        }

        //        return data;
        //    }
        //}
    }
}
