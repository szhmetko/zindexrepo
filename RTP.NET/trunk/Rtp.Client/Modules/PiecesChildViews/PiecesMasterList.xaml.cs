﻿using System.Windows.Controls;

namespace Rtp.Client.Modules.PiecesChildViews
{
    /// <summary>
    /// Interaction logic for PiecesMasterList.xaml
    /// </summary>
    public partial class PiecesMasterList : UserControl
    {
        public PiecesMasterList()
        {
            InitializeComponent();
        }
        void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            lb.ScrollIntoView(lb.SelectedItem);
        }
    }
}
