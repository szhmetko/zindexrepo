﻿using System;
using System.Collections;
using System.Windows;
using System.Windows.Controls;
using Rtp.Shared;
using Visiblox.Charts;

namespace Rtp.Client.Charts
{
    /// <summary>
    /// Interaction logic for Chart.xaml
    /// </summary>

    public partial class Chart : UserControl
    {

        private const string StrXValueColumnName = "XValue";

        private IChartViewModel _vm;

        private IChartViewModel Vm
        {
            get { return _vm ?? (_vm = DataContext as IChartViewModel); }
        }

        public Chart()
        {
            InitializeComponent();


            DataContextChanged += ChartDataContextChanged;

        }


        void ChartDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Vm.PropertyChanged += VmPropertyChanged;

            SetYAxesLimits();

        }

        void VmPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(e.PropertyName == "BulkChartsData")
                SetChartData(Vm.BulkChartsData);
        }

        private void SetYAxesLimits() 
        {
            if (Vm != null)
            {
                chart.YAxis.Range = new DoubleRange(Vm.PrimaryYAxisRange.Start, Vm.PrimaryYAxisRange.End);

                chart.SecondaryYAxis.Range = new DoubleRange(Vm.SecondaryYAxisRange.Start, Vm.SecondaryYAxisRange.End);

            }
        }


        protected virtual void SetChartData(IEnumerable table)
        {
            SetYAxesLimits();


            var ret = new SeriesCollection<IChartSeries>();

            foreach (ChartCurveViewModel curve in Vm.CurveModels)
            {
                var serie = new LineSeries() { YAxis = curve.Model.bPrimary ? chart.YAxis : chart.SecondaryYAxis, LineStrokeThickness = 3 };

                ret.Add(serie);

                var data = new DataSeries<IComparable, double>(curve.Model.Channel.Title);

                foreach (Hashtable packet in table)
                {
                    object key = curve.Model.Channel.FieldName;

                    if (packet.ContainsKey(key))
                    {
                        var point = new DataPoint<IComparable, double>
                        {
                            X = Vm.CalculateXValue(Convert.ToInt64(packet[StrXValueColumnName])),
                            Y = curve.Model.Channel.CalculateValue(Convert.ToDouble(packet[key]))
                        };

                        data.Add(point);
                    }
                }

                serie.DataSeries = data;
            }

            if (chart.Series != null)
            {
                chart.Series.Clear();
                chart.Series.DeInit();
            }

            chart.Series = ret;
            
        }


    }
}
