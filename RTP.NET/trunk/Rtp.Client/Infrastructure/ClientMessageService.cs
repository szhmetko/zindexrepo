﻿using Microsoft.Practices.ServiceLocation;
using Rtp.Shared;
using System.Windows;

namespace Rtp.Client.Infrastructure
{
    public class ClientMessageService : MessageService 
    {
        public override MessageBoxResult ShowApplicationInfo()
        {
            var wnd = ServiceLocator.Current.GetInstance<AppInfo>();

            var result = wnd.ShowDialog();

            return result.HasValue && result.Value ? MessageBoxResult.OK : MessageBoxResult.Cancel;
        }
    }
}
