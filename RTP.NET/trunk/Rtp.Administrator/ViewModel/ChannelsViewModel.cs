﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Rtp.Model.Line;
using Rtp.Model.Master;
using Rtp.Repository;
using Rtp.Repository.Repositories;
using Rtp.Shared;

namespace Rtp.Administrator.ViewModel
{

    public class ChannelsViewModel : ViewModelBase
    {
        #region Visibilities

        public Visibility FormVisibility
        {
            get { return SelectedChannel != null ? Visibility.Visible : Visibility.Hidden; }
        }
        #endregion

        protected IMessageService MessageService { private set; get; }
        private ILineUnitOfWork _lineUow;
        private IChannelsRepository _channelsRepository;


        private LineViewModel _activeLine;
        public LineViewModel ActiveLine
        {
            get { return _activeLine; }
            protected set
            {
                _activeLine = value;

                Channels = null;

                if (value != null)
                {
                    if (_lineUow != null)
                    {
                        _lineUow.Rollback();
                        _lineUow.Dispose();
                    }

                    _lineUow = new NHUnitOfWork(value.Model);
                    _channelsRepository = new ChannelsRepository(_lineUow);
                }

                LoadContent();

                RaisePropertyChanged("ActiveLine");
                RaisePropertyChanged("AddNewModuleBtnVisibility");
            }
        }

        private ICollection<ChannelViewModel> _channels;
        public ICollection<ChannelViewModel> Channels
        {
            get { return _channels; }
            private set
            {
                _channels = value;
                RaisePropertyChanged("Channels");
            }
        }

        protected void LoadContent()
        {
            if (Channels == null && ActiveLine != null)
            {
                try
                {
                    {
                        Channels =
                            new ObservableCollection<ChannelViewModel>(from channel in _channelsRepository.FindAll()
                                                                       select new ChannelViewModel(channel)
                                                                                  {
                                                                                      ThermoPair =
                                                                                          ActiveLine.Model.ThermoPairs.
                                                                                          SingleOrDefault(
                                                                                              tp =>
                                                                                              tp.Id ==
                                                                                              channel.ThermoPairId)
                                                                                  });
                    }
                }
                catch (Exception exception)
                {
                    MessageService.ShowError(exception);
                }
            }
        }

        private ChannelViewModel _selectedChannel;
        public ChannelViewModel SelectedChannel
        {
            get { return _selectedChannel; }
            
            set { _selectedChannel = value; 
                RaisePropertyChanged("SelectedChannel");
                RaisePropertyChanged("FormVisibility");
                
            }
        }

        public ChannelsViewModel(IMessageService messageService)
        {
            MessageService = messageService;

            Messenger.Default.Register<PropertyChangedMessage<LineViewModel>>(
                this,
                message =>
                    {
                        ActiveLine = null;
                        ActiveLine = message.NewValue;
                    });
        }

        private ICommand _loadedCommand;
        public ICommand LoadedCommand
        {
            get { return _loadedCommand ?? (_loadedCommand = new RelayCommand(OnLoad)); }
        }

        private ICommand _saveCommand;
        public ICommand SaveCommand
        {
            get { return _saveCommand ?? (_saveCommand = new RelayCommand(SaveChanges)); }
        }

        private void SaveChanges()
        {
            try
            {
                foreach (var channelViewModel in Channels)
                {
                    _channelsRepository.Save(channelViewModel.Model);
                }
                _lineUow.Commit();
            }
            catch (Exception exception)
            {
                MessageService.ShowError(exception);
            }
        }

        private void OnLoad()
        {
            LoadContent();
        }

    }
}