﻿
using System;
using System.Collections;
using System.Collections.Generic;
using Rtp.Model;
using Rtp.Shared;

namespace Rtp.Administrator.ViewModel
{
    internal class DummyPiecesReportViewModel : IRtpReport
    {
        public void Cleanup()
        {
        }

        public PieceItemViewModel SelectedPiece { get; set; }
        public PiecesViewModel ParentModule { get; set; }
        public bool IsPopulated { get; set; }
        public PiecesScreenViewMode CurrentMode { set; private get; }
    }

    internal class DummyRawSqlRepository : IRawSqlRepository
    {
        public IEnumerable Query(string rawSql, IDictionary<string, object> @params)
        {
            throw new NotImplementedException();
        }

        public IEnumerable Query(string rawSql)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<T> QueryEntities<T>(string rawSql, IDictionary<string, object> @params)
        {
            throw new NotImplementedException();
        }

        public IEnumerable QueryEntities(string rawSql, IDictionary<string, object> @params, Type entityType)
        {
            throw new NotImplementedException();
        }
    }
}
