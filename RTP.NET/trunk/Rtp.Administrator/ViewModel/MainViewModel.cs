﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.ServiceLocation;
using Rtp.Model.Master;
using Rtp.Shared;

namespace Rtp.Administrator.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm/getstarted
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {

        private IEnumerable<LineViewModel> _lines;
        public IEnumerable<LineViewModel> Lines
        {
            get { return _lines; }
            private set
            {
                _lines = value;
                RaisePropertyChanged("Lines");
            }

        }

        public bool IsDirtyFlag { get; set; }

        private LineViewModel _activeLine;
        public LineViewModel ActiveLine
        {
            get { return _activeLine; }
            set
            {
                var oldValue = _activeLine;
                _activeLine = value;
                RaisePropertyChanged("ActiveLine", oldValue, value, true);
            }
        }


        private ICommand _loadedCommand;
        public ICommand LoadedCommand
        {
            get { return _loadedCommand ?? (_loadedCommand = new RelayCommand(OnLoad)); }
        }

        public Visibility LineSettingsDetailVisibility
        {
            get
            {
                return ActiveLine == null ? Visibility.Collapsed : Visibility.Visible;
            }
        }
        
        private void OnLoad()
        {
            try
            {
                using (var repo = ServiceLocator.Current.GetInstance<ILinesRepository>())
                {
                    IEnumerable<ThermoPair> thermoPairsAvailable;
                    using (var thermoPairsRepository = ServiceLocator.Current.GetInstance<IThermoPairsRepository>())
                    {
                        thermoPairsAvailable = thermoPairsRepository.FindAll();
                    }

                    var linesVms = new List<LineViewModel>();
                    foreach (var line in repo.FindAll())
                    {
                        line.ThermoPairs = thermoPairsAvailable;

                        linesVms.Add(new LineViewModel(line));
                    }
                    Lines = linesVms;

                    ActiveLine = linesVms.FirstOrDefault();

                } 

            }
            catch (Exception)
            {
                throw;
            }

        }

    }
}