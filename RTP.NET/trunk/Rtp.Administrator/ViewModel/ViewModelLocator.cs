﻿using System;
using System.Configuration;
using Microsoft.Practices.ServiceLocation;
using Rtp.Infrastructure;
using Rtp.Infrastructure.Config;
using Rtp.Infrastructure.UnitOfWork;
using Rtp.Model;
using Rtp.Model.Master;
using Rtp.Repository;
using Rtp.Repository.Repositories;
using Rtp.Shared;
using StructureMap;
using StructureMap.Configuration.DSL;
using StructureMap.Graph;
using StructureMap.ServiceLocatorAdapter;
using System.Reflection;

namespace Rtp.Administrator.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// <para>
    /// Use the <strong>mvvmlocatorproperty</strong> snippet to add ViewModels
    /// to this locator.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm/getstarted
    /// </para>
    /// </summary>
    public class ViewModelLocator
    {
        static ViewModelLocator()
        {
            var ioc = CreateServiceLocator();
            ServiceLocator.SetLocatorProvider(() => ioc);
        }

        /// <summary>
        /// Gets the Main property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public MainViewModel Main
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainViewModel>();
            }
        }

        //public ThermoPairMainGridFormViewModel ThermoPairPage
        //{
        //    get
        //    {
        //        return ServiceLocator.Current.GetInstance<ThermoPairMainGridFormViewModel>();
        //    }
        //}

        //public LinesViewModel Lines
        //{
        //    get
        //    {
        //        return ServiceLocator.Current.GetInstance<LinesViewModel>();
        //    }
        //}

        public LineSettingsDetailViewModel LineSettings
        {
            get
            {
                return ServiceLocator.Current.GetInstance<LineSettingsDetailViewModel>();
            }
        }

        public ModulesViewModel ModulesVm
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ModulesViewModel>();
            }
        }

        public ChannelsViewModel Channels
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ChannelsViewModel>();

            }
        }

        /// <summary>
        /// Cleans up all the resources.
        /// </summary>
        public static void Cleanup()
        {
        }


        private static IServiceLocator CreateServiceLocator()
        {
            var x = new Registry();

            //registry.ScanAssemblies().IncludeAssembly(Assembly.GetExecutingAssembly().FullName);
            x.Scan(scan =>
                              {
                                  scan.TheCallingAssembly();
                                  scan.AssemblyContainingType<NHUnitOfWork>();
                                  scan.AddAllTypesOf<IUnitOfWork>();
                                  /*scan.WithDefaultConventions();
                                  scan.AddAllTypesOf<IUnitOfWork>();*/
                              });

            x.For<IUnitOfWork>().Use<NHUnitOfWork>().Ctor<IConnectionInfo>("connectionInfo")
                .Is(c=>
                        {
                            string srv = ConfigurationSettings.AppSettings["MasterHost"];
                            string usr = ConfigurationSettings.AppSettings["MasterUser"];
                            string pwd = ConfigurationSettings.AppSettings["MasterPwd"];
                            string dbn = ConfigurationSettings.AppSettings["MasterDbName"];

                            return new ConnectionInfo(dbn, usr, srv, pwd);
                        }
                    );//.Use<NHUnitOfWork>());
            x.For<IThermoPairsRepository>().Use<ThermoPairsRepository>();
            x.For<ILinesRepository>().Use<LinesRepository>();
            x.For<IMessageService>().Use<MessageService>();
            x.For<IRtpReport>().Use<DummyPiecesReportViewModel>();
            x.For<IRawSqlRepository>().Use<DummyRawSqlRepository>();

            //if (ViewModelBase.IsInDesignModeStatic)
            //{
            //    registry.ForRequestedType<IDataService>().TheDefaultIsConcreteType<Design.DesignDataService>();
            //}
            //else
            //{
            //    registry.ForRequestedType<IDataService>().TheDefaultIsConcreteType<DataService>();
            //}

            IContainer container = new Container(x);

            var test = container.GetInstance<IUnitOfWork>();

            return new StructureMapServiceLocator(container);
        }
    }
}