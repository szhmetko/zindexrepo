﻿using System;
using Rtp.Administrator.ViewModel.Generics;
using Rtp.Infrastructure.UnitOfWork;
using Rtp.Model.Master;

namespace Rtp.Administrator.ViewModel
{
    [Obsolete]
    public class LinesViewModel : GenericGridViewModel<Line, int>
    {
        public LinesViewModel(ILinesRepository repository, IUnitOfWork unitOfWork ) : base(repository, unitOfWork)
        {
        }
    }
}
