﻿using System.Collections.Generic;
using GalaSoft.MvvmLight;

namespace Rtp.Administrator.ViewModel
{
    public class LineSettingsDetailViewModel : ViewModelBase
    {

        public class LineSettingItemViewModel : ViewModelBase
        {
            public string Title { get; set; }
        }

        public IEnumerable<LineSettingItemViewModel> Settings { get; set; }



    }
}