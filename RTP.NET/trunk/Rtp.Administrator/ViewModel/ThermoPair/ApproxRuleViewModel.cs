﻿using Rtp.Model.Master;
using Rtp.Shared;

namespace Rtp.Administrator.ViewModel
{
    public class ApproxRuleViewModel : GenericEntityViewModel<ApproxRule>
    {

        public ApproxRuleViewModel(ApproxRule model)
            : base(model)
        { }

    }
}
