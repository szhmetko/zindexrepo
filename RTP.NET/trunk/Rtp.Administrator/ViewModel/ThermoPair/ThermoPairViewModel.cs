﻿using Rtp.Model.Master;
using Rtp.Shared;

namespace Rtp.Administrator.ViewModel
{
    public class ThermoPairViewModel : GenericEntityViewModel<ThermoPair>
    {

        public ThermoPairViewModel(ThermoPair model) : base(model)
        {}
 
    }
}