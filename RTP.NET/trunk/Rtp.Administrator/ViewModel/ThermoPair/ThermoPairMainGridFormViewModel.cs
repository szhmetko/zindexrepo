﻿using System;
using Rtp.Administrator.ViewModel.Generics;
using Rtp.Infrastructure.UnitOfWork;
using Rtp.Model;
using Rtp.Model.Master;

namespace Rtp.Administrator.ViewModel
{
    //class ThermoPairsMainGrid : GenericGridViewModel<ThermoPair, long>
    //{
    //    public ThermoPairsMainGrid(IRepository<ThermoPair, long> repository, IUnitOfWork unitOfWork) : base(repository, unitOfWork)
    //    {
    //    }


    //}

    public class ThermoPairMainGridFormViewModel : GridFormViewModelBase<ThermoPair, int>
    {
        private IUnitOfWork _unitOfWork;
        
        public ThermoPairMainGridFormViewModel(IThermoPairsRepository repository, IUnitOfWork unitOfWork) 
            : base(repository, unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        /*public override GenericGridViewModel<ThermoPair, long> Grid
        {
            get { return _grid ?? (_grid = new ThermoPairsMainGrid(Repository, _unitOfWork)); }
        }    */

        public Type ChildEntityType { get { return typeof (ApproxRule); }
        }

    }
}
