﻿using System;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Rtp.Infrastructure;
using Rtp.Infrastructure.UnitOfWork;
using Rtp.Model;
using Rtp.Shared;

namespace Rtp.Administrator.ViewModel.Generics
{
    public class GenericGridViewModel<TEntity, TEntityKey> : ViewModelBase
        where TEntity : IAggregateRoot
    {
        public Type EntityType
        {
            get
            {
                return typeof(TEntity);
            }
        }

        public const string IsItemsLoadingPropertyName = "IsItemsLoading";
        private bool _isItemsLoadingPropertyName = false;

        public bool IsItemsLoading
        {
            get
            {
                return _isItemsLoadingPropertyName;
            }

            set
            {
                if (_isItemsLoadingPropertyName == value)
                {
                    return;
                }

                _isItemsLoadingPropertyName = value;
                RaisePropertyChanged(IsItemsLoadingPropertyName);
            }
        }

        virtual protected void Populate()
        {
            IsItemsLoading = true;

            var items = Repository.FindAll();

            Items.Clear();
            foreach (var item in items)
            {
                Items.Add(new GenericEntityViewModel<TEntity>(item));
            }

            //Repository.UnitOfWork.Commit();

            IsItemsLoading = false;

        }

        public IRepository<TEntity, TEntityKey> Repository { get; protected set; }

        private IUnitOfWork _unitOfWork;

        public IUnitOfWork UnitOfWork
        {
            get { return _unitOfWork; }
        }

        public GenericGridViewModel(IRepository<TEntity, TEntityKey> repository, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            Repository = repository;

            Messenger.Default.Register<EntityCommandMessage<TEntity>>(
                this,
                message =>
                {
                    if(message.Argument == FormCommandArgument.Retrieve)
                        Populate();
                });            
        }

        /// <summary>
        /// The <see cref="SelectedItem" /> property's name.
        /// </summary>
        public const string SelectedItemPropertyName = "SelectedItem";

        private GenericEntityViewModel<TEntity> _selectedItem = default(GenericEntityViewModel<TEntity>);
        
        /// <summary>
        /// Gets the SelectedItem property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public GenericEntityViewModel<TEntity> SelectedItem
        {
            get
            {
                return _selectedItem;
            }

            set
            {
                if (_selectedItem == value)
                {
                    return;
                } 

                var oldValue = _selectedItem;
                _selectedItem = value;

                RaisePropertyChanged(SelectedItemPropertyName, oldValue, _selectedItem, true);

            }
        }

        private ObservableCollection<GenericEntityViewModel<TEntity>> _items;
        public ObservableCollection<GenericEntityViewModel<TEntity>> Items
        {
            get
            {
                if (_items == null)
                {
                    _items = new ObservableCollection<GenericEntityViewModel<TEntity>>();
                    Populate();
                }

                return _items;
            }
        }


        ////public override void Cleanup()
        ////{
        ////    // Clean own resources if needed

        ////    base.Cleanup();
        ////}
    }
}