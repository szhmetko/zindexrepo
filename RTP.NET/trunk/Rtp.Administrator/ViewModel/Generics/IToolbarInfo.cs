using System.Windows;

namespace Rtp.Administrator.ViewModel.Generics
{
    public interface IToolbarInfo
    {
        //bool CanUpdate { get; }
        Visibility Create { get; }
        Visibility Retrieve { get; }
        Visibility Update { get; }
        Visibility Delete { get; }
    }
}