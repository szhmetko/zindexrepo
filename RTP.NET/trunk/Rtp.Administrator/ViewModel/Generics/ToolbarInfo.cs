﻿using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Rtp.Infrastructure;
using Rtp.Infrastructure.Config;
using Rtp.Infrastructure.UnitOfWork;
using Rtp.Model;
using Rtp.Repository;
using Rtp.Shared;

namespace Rtp.Administrator.ViewModel.Generics
{
    internal class EntityCommandMessage<TEntity>
    {
        public FormCommandArgument Argument { get; set; }
    }

    public class ToolbarInfoViewModel<TEntity> : ViewModelBase, IToolbarInfo where TEntity : IAggregateRoot
    {
        private EntityPermissionEngine _permissionEngine;

        private GenericEntityViewModel<TEntity> _model;

        protected GenericEntityViewModel<TEntity> Model
        {
            private set { 
                _model = value; 

                RaisePropertyChanged("Model");
                RaisePropertyChanged("Permissions");
                RaisePropertyChanged("Delete");
                RaisePropertyChanged("Update");
            }
            get { return _model; }
        }

        public ToolbarInfoViewModel(IUnitOfWork unitOfWork)
        {


            Messenger.Default.Register<PropertyChangedMessage<GenericEntityViewModel<TEntity>>>(
                this,
                message =>
                {
                    Model = null;
                    Model = message.NewValue;
                });

            _permissionEngine = new EntityPermissionEngine(unitOfWork);

        }

        private ICommand _toolbarCommand;
        public ICommand ToolbarCommand
        {
            get
            {
                if (_toolbarCommand == null)
                {
                    _toolbarCommand = new RelayCommand<FormCommandArgument>(
                        arg => Messenger.Default.Send(new EntityCommandMessage<TEntity>{Argument = arg}));

                }

                return _toolbarCommand;
            }
        }

        #region       Buttons visibility

        protected IPermissionValue Permissions
        {
            get { return _permissionEngine.GetPermissions<TEntity>(AppConfig.Instance.CurrentRole); }
        }

        public Visibility Create
        {
            get { return GetVisibility(Permissions.CanCreate); }
        }

        public Visibility Retrieve
        {
            get { return GetVisibility(Permissions.CanRetrieve); }
        }

        public Visibility Update
        {
            get { return GetVisibility(Permissions.CanUpdate && Model != null); }
        }

        public Visibility Delete
        {
            get { return GetVisibility(Permissions.CanDelete && Model != null); }
        }

        private static Visibility GetVisibility(bool permission)
        {
            return permission ? Visibility.Visible : Visibility.Hidden;
        }

        #endregion

    }
}
