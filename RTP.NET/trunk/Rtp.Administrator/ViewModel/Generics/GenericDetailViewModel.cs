﻿using System.ComponentModel;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Rtp.Infrastructure;
using Rtp.Infrastructure.UnitOfWork;
using Rtp.Model;
using Rtp.Shared;

namespace Rtp.Administrator.ViewModel.Generics
{


    public class GenericDetailViewModel<TEntity, TEntityKey> : ViewModelBase
        where TEntity : IAggregateRoot
    {
        /// <summary>
        /// The <see cref="SelectedItem" /> property's name.
        /// </summary>
        public const string SelectedItemPropertyName = "SelectedItem";


        private GenericEntityViewModel<TEntity> _selectedItem = null;

        /// <summary>
        /// Gets the SelectedItem property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public GenericEntityViewModel<TEntity> SelectedItem
        {
            get
            {
                return _selectedItem;
            }

            set
            {
                if (_selectedItem == value)
                {
                    return;
                }

                _selectedItem = value;
                RaisePropertyChanged(SelectedItemPropertyName);

                RaisePropertyChanged("DetailVisibility");
            }
        }

        protected virtual void Save()
        {
            Repository.Save(SelectedItem.Model);
            _unitOfWork.Commit();
        }

        public Visibility DetailVisibility
        {
            get
            {
                return SelectedItem == null
                    || SelectedItem.Model == null
                    ? Visibility.Collapsed : Visibility.Visible;
            }
        }


        public IRepository<TEntity, TEntityKey> Repository
        {
            get; private set; }

        private IUnitOfWork _unitOfWork;
        
        /// <summary>
        /// Initializes a new instance of the GridViewModel class.
        /// </summary>
        public GenericDetailViewModel(IRepository<TEntity, TEntityKey> repository, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;

            Repository = repository;

            ////if (IsInDesignMode)
            ////{
            ////    // Code runs in Blend --> create design time data.
            ////}
            ////else
            ////{
            ////    // Code runs "for real": Connect to service, etc...
            ////}

            Messenger.Default.Register<PropertyChangedMessage<GenericEntityViewModel<TEntity>>>(
                this,
                message =>
                {
                    SelectedItem = null;
                    SelectedItem = message.NewValue;
                });

            Messenger.Default.Register <EntityCommandMessage<TEntity>>(
                this,
                message =>
                {
                    if(message.Argument==FormCommandArgument.Save)
                        Save();
                });
        }

        ////public override void Cleanup()
        ////{
        ////    // Clean own resources if needed

        ////    base.Cleanup();
        ////}
    }
}
