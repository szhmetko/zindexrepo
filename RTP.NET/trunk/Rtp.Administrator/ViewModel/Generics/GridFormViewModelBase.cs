﻿using GalaSoft.MvvmLight;
using Rtp.Infrastructure;
using Rtp.Infrastructure.UnitOfWork;
using Rtp.Model;

namespace Rtp.Administrator.ViewModel.Generics
{
    internal enum FormCommandArgument
    {
        Create
        , Retrieve
        , Save
        , Delete
        , Cancel
        , Duplicate
    }

    public class GridFormViewModelBase<TEntity, TKey> : ViewModelBase
        where TEntity : IAggregateRoot
    {
        protected IRepository<TEntity, TKey> Repository { get; set; }

        private IUnitOfWork _unitOfWork;
        public GridFormViewModelBase(IRepository<TEntity, TKey> repository, IUnitOfWork unitOfWork)
        {
            Repository = repository;
            _unitOfWork = unitOfWork;
        }

        protected GenericDetailViewModel<TEntity, TKey> _form;

        virtual public GenericDetailViewModel<TEntity, TKey> Form
        {
            get { return _form ?? (_form = new GenericDetailViewModel<TEntity, TKey>(Repository, _unitOfWork)); }
        }

        protected GenericGridViewModel<TEntity, TKey> _grid;

        virtual public GenericGridViewModel<TEntity, TKey> Grid
        {
            get { return _grid ?? (_grid = new GenericGridViewModel<TEntity, TKey>(Repository, _unitOfWork)); }
        }

        protected ToolbarInfoViewModel<TEntity> _toolbar;

        virtual public ToolbarInfoViewModel<TEntity> Toolbar
        {
            get { return _toolbar ?? (_toolbar = new ToolbarInfoViewModel<TEntity>(_unitOfWork)); }
        }
    }
}
