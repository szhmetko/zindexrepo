﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Rtp.Model.Line;
using Rtp.Repository;
using Rtp.Repository.Repositories;
using Rtp.Shared;

namespace Rtp.Administrator.ViewModel
{
    public class ModulesViewModel : ViewModelBase
    {
        #region Visibilities

        public Visibility ChartTbVisibility
        {
            get { return SelectedChart != null ? Visibility.Visible : Visibility.Hidden; }
        }

        public Visibility ModuleTbVisibility
        {
            get { return SelectedModule != null ? Visibility.Visible : Visibility.Hidden; }
        }

        public Visibility AddNewModuleBtnVisibility
        {
            get { return ActiveLine != null ? Visibility.Visible : Visibility.Hidden; }
        }
        #endregion

        private LineViewModel _activeLine;
        private IEnumerable<ChannelViewModel> _allChannels;
        private ILineUnitOfWork _lineUow;
        private IModulesRepository _modulesRepository;
        private ICommand _loadedCommand;
        private ObservableCollection<ModuleViewModel> _modules;
        private ICommand _saveSelectedModuleCommand;
        private IChartViewModel _selectedChart;
        private ChartCurveViewModel _selectedCurve;


        private ModuleViewModel _selectedModule;

        public ModulesViewModel(IMessageService messageService)
        {
            MessageService = messageService;

            Messenger.Default.Register<PropertyChangedMessage<LineViewModel>>(
                this,
                message =>
                    {
                        ActiveLine = null;
                        ActiveLine = message.NewValue;
                    });
        }

        protected IMessageService MessageService { private set; get; }

        public ModuleViewModel SelectedModule
        {
            get { return _selectedModule; }
            set
            {
                _selectedModule = value;

                RaisePropertyChanged("SelectedModule");
                RaisePropertyChanged("ModuleTbVisibility");
            }
        }

        public IChartViewModel SelectedChart
        {
            get { return _selectedChart; }
            set
            {
                _selectedChart = value;

                RaisePropertyChanged("SelectedChart");
                RaisePropertyChanged("AvailableChannels");
                RaisePropertyChanged("ChartTbVisibility");
            }
        }

        public ChartCurveViewModel SelectedCurve
        {
            get { return _selectedCurve; }
            set
            {
                _selectedCurve = value;

                RaisePropertyChanged("SelectedCurve");
                RaisePropertyChanged("CurveTbVisibility");
            }
        }

        protected IEnumerable<ChannelViewModel> AllChannels
        {
            get
            {
                if (_allChannels == null && ActiveLine != null)
                {
                    try
                    {
                        using (var channelsRepository = new ChannelsRoRepository(ActiveLine.Model))
                        {
                            _allChannels = (from channel in channelsRepository.FindAll()
                                            select new ChannelViewModel(channel)).ToList();
                        }
                    }
                    catch (Exception exception)
                    {
                        MessageService.ShowError(exception);
                    }
                }
                return _allChannels;
            }
        }

        public IEnumerable<ChannelViewModel> AvailableChannels
        {
            get
            {
                if (SelectedChart != null)
                {
                    return 
                        
                        AllChannels.Where(channelViewModel => !SelectedChart.CurveModels
                                                                      .Any(
                                                                          cu =>
                                                                          cu.Model.Channel.Id ==
                                                                          channelViewModel.Model.Id)).ToList();
                }


                return null;
            }
        }

        public LineViewModel ActiveLine
        {
            get { return _activeLine; }
            protected set
            {
                _activeLine = value;

                Modules = null;

                if (value != null)
                {
                    if (_lineUow != null)
                    {
                        _lineUow.Rollback();
                        _lineUow.Dispose();
                    }

                    _lineUow = new NHUnitOfWork(value.Model);
                    _modulesRepository = new ModulesRepository(_lineUow);
                }

                LoadContent();

                RaisePropertyChanged("ActiveLine");
                RaisePropertyChanged("AddNewModuleBtnVisibility");
            }
        }


        public ICommand LoadedCommand
        {
            get { return _loadedCommand ?? (_loadedCommand = new RelayCommand(OnLoad)); }
        }

        public ObservableCollection<ModuleViewModel> Modules
        {
            get { return _modules; }
            private set
            {
                _modules = value;
                RaisePropertyChanged("Modules");
            }
        }

        public ICommand SaveSelectedModuleCommand
        {
            get
            {
                return _saveSelectedModuleCommand ??
                       (_saveSelectedModuleCommand = new RelayCommand(SaveSelectedModule));
            }
        }

        private ICommand _addCurveCommand;
        public ICommand AddCurveCommand
        {
            get
            {
                return _addCurveCommand ??
                       (_addCurveCommand = new RelayCommand(
                           ()=>
                               {
                                   if(SelectedChannel != null)
                                   {
                                       var newCurve = new ChartCurve
                                                          {
                                                              bPrimary = true,
                                                              Channel = SelectedChannel.Model,
                                                              Chart = SelectedChart.Model,
                                                              Id = -1

                                                          };
                                       SelectedChart.CurveModels.Add(new ChartCurveViewModel(newCurve));

                                       SelectedChart.Model.Curves.Add(newCurve);

                                       RaisePropertyChanged("AvailableChannels");
                                   }

                               }
                                               ));
                
            }
        }

        private ICommand _removeCurveCommand;
        public ICommand RemoveCurveCommand
        {
            get
            {
                return _removeCurveCommand ??
                       (_removeCurveCommand = new RelayCommand(
                           () =>
                           {
                               if (SelectedCurve != null)
                               {
                                   SelectedChart.Model.Curves.Remove(SelectedCurve.Model);

                                   SelectedChart.CurveModels.Remove(SelectedCurve);

                                   RaisePropertyChanged("AvailableChannels");
                               }

                           }
                                               ));

            }
        }

        private ICommand _addChartCommand;
        public ICommand AddChartCommand
        {
            get
            {
                return _addChartCommand ??
                       (_addChartCommand = new RelayCommand(
                           () =>
                           {
                               var newChart = ModuleChart.Default;

                               var newChartVm = new MonitorChartViewModel { Model = SelectedModule.Model.AddChart(newChart) };

                               SelectedModule.ChartModels.Add(newChartVm);

                               SelectedChart = newChartVm;

                           }
                                               ));

            }
        }

        private ICommand _removeChartCommand;
        public ICommand RemoveChartCommand
        {
            get
            {
                return _removeChartCommand ??(_removeChartCommand = new RelayCommand(RemoveSelectedChart));

            }
        }

        protected void RemoveSelectedChart()
        {
            if (SelectedChart != null)
            {
                SelectedModule.Model.RemoveChart(SelectedChart.Model);

                SelectedModule.ChartModels.Remove(SelectedChart);
            }
        }

        private ICommand _addModuleCommand;
        public ICommand AddModuleCommand
        {
            get
            {
                return _addModuleCommand ??
                       (_addModuleCommand = new RelayCommand(
                           () =>
                           {
                               var newModule =
                               new Module {
                                   Alias = "monitor",
                                   Id = -1, 
                                   QueryName = "MONITOR", 
                                   Title = "Новый",
                                   Charts = new List<ModuleChart>()
                               };

                               _modulesRepository.Add(newModule);

                               var newModuleVm = new MonitorViewModel {Model = newModule
                                   , ChartModels = new ObservableCollection<IChartViewModel>()};
                               Modules.Add(newModuleVm);

                               SelectedModule = newModuleVm;

                           }
                                               ));

            }
        }

        private ICommand _removeModuleCommand;
        public ICommand RemoveModuleCommand
        {
            get
            {
                return _removeModuleCommand ??
                       (_removeModuleCommand = new RelayCommand(
                           () =>
                           {
                               if(SelectedModule != null)
                               {
                                   _modulesRepository.Remove(SelectedModule.Model);

                                   Modules.Remove(SelectedModule);
                               }
                           }
                        ));

            }
        }


        public ChannelViewModel SelectedChannel
        {
            get; set; 
        }

        private void OnLoad()
        {
            LoadContent();
        }

        protected void LoadContent()
        {
            if (Modules == null && ActiveLine != null)
            {
                try
                {
                    {
                        Modules = new ObservableCollection<ModuleViewModel>(from module in _modulesRepository.FindAll()
                                                                            select ModulesFactory.CreateModuleViewModel(module));
                    }
                }
                catch (Exception exception)
                {
                    MessageService.ShowError(exception);
                }
            }
        }

        protected void SaveSelectedModule()
        {
            try
            {
                foreach (var moduleViewModel in Modules)
                {
                    _modulesRepository.Save(moduleViewModel.Model);
                }

                _lineUow.Commit();
            }
            catch (Exception exception)
            {
                MessageService.ShowError(exception);
            }
        }
    }
}