﻿using System.Windows.Controls;

namespace Rtp.Administrator.Controls
{
    /// <summary>
    /// Description for ModulesView.
    /// </summary>
    public partial class ModulesView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the ModulesView class.
        /// </summary>
        public ModulesView()
        {
            InitializeComponent();
        }
    }
}