﻿
using System.Collections.Generic;

namespace Rtp.DataCollector
{
    public interface IRawDataConsumer
    {
        void SendData(byte data);
        void SendData(IEnumerable<byte> data);

    }
}
