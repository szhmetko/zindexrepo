﻿
using System;
using System.Timers;

namespace Rtp.DataCollector.CollectorStates
{
    public class DummyCollector
    {
        private IGenericState _state;
        public IGenericState State
        {
            get { return _state; }
        }

        public DummyCollector()
        {
            _state = StoppedNotInitializedState.Instance;
        }

        public void Stop()
        {
            _state.Stop(this);
        }

        public void Start()
        {
            _state.Start(this);
        }

        public void Initialize()
        {
            _state.Initialize(this);
        }

        public void ChangeState(IGenericState state)
        {
            _state = state;
        }
    }

    public interface IGenericState
    {
        void Stop(DummyCollector collector);

        void Start(DummyCollector collector);

        void Initialize(DummyCollector collector);

        void ChangeState(DummyCollector collector, IGenericState state);
    }

    public abstract class CollectorState<T> : IGenericState where T : IGenericState, new()
    {
        public virtual void Stop(DummyCollector collector)
        { }

        public virtual void Start(DummyCollector collector)
        { }

        public virtual void Initialize(DummyCollector collector)
        { }

        internal static readonly T State = new T();

        public static IGenericState Instance
        {
            get { return State; }
        }

        public void ChangeState(DummyCollector collector, IGenericState state)
        {
            collector.ChangeState(state);
        }
    }

    public class StoppedNotInitializedState : CollectorState<StoppedNotInitializedState>
    {
        System.Timers.Timer _initializationTimer = new Timer(1e4);

        private DummyCollector _collector;

        protected int Trials = 0;

        protected bool AutoStart = false;

        public StoppedNotInitializedState()
        {
            _initializationTimer.Elapsed += new ElapsedEventHandler(_initializationTimer_Elapsed);
        }

        void _initializationTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _Initialize(_collector);
        }

        public override void Stop(DummyCollector collector)
        {
            _initializationTimer.Enabled = false;
            Trials = 0;
        }

        private bool _Initialize(DummyCollector collector)
        {
            _collector = collector;
            //TODO:
            try
            {
                if(Trials++ < 10)
                {
                    var b = 1 / (Math.Atan(1) - Math.Atan(1));
                }

                _initializationTimer.Enabled = false;

                ChangeState(collector, StoppedInitializedState.Instance);

                return true;

            }
            catch (Exception)
            {
                //TODO: log error
                _initializationTimer.Enabled = true;
                
                return false;
            }
        }

        public override void Start(DummyCollector collector)
        {
            _Initialize(collector);


            StoppedInitializedState.Instance.Start(collector);
        }

        public override void Initialize(DummyCollector collector)
        {
            _Initialize(collector);
        }
    }

    public class StoppedInitializedState : CollectorState<StoppedInitializedState>
    {
        public override void Start(DummyCollector collector)
        {
            throw new NotImplementedException();

            ChangeState(collector, RunningState.Instance);
        }

    }

    public class RunningState : CollectorState<RunningState>
    {
        public override void Stop(DummyCollector collector)
        {
            throw new NotImplementedException();

            ChangeState(collector, StoppedInitializedState.Instance);
        }

    }
}
