﻿using System;
using System.Collections.Generic;
using C5;
using Rtp.Model.Data;
using SCG = System.Collections.Generic;

namespace Rtp.DataCollector
{

    public class RawPacket :  IRawPacket
    {
        public Dictionary<int, RawChannel> RawChannelsBag
        {
            get { return _rawChannelsBag; }
        }
        private Dictionary<int, RawChannel> _rawChannelsBag = new Dictionary<int, RawChannel>();

        private Dictionary<int, object> _channelsValues = new Dictionary<int, object>();

        #region ICloneable Members

        object ICloneable.Clone()
        {
            return Clone();
        }

        #endregion

        #region IRawPacket Members

        public string Uid { get; private set; }
        public long Timestamp { get; private set; }

        public PacketKind Kind { get; set; }

        public Dictionary<int, object> ChannelsValues
        {
            get { return _channelsValues; }
        }

        #endregion

        public void Add(RawChannel channelData)
        {
            RawChannelsBag[channelData.Num] = channelData;
        }

        public void ProcessValues(System.Collections.Generic.IDictionary<int, CalculatableChannel> channelInfo,
                                  IQueue<RawPacket> packetHistory, CalculatableChannel coldEndChannel
            , IDataCollectorCallable dataCollectorCallable)
        {
            Uid = Guid.NewGuid().ToString();
            Timestamp = Infrastructure.Utility.DateTimeRoutines.UnixTimeStamp.UtcNow;

            _channelsValues = new Dictionary<int, object>();

            IRtpChannelsApi owner = new ChannelValueEvaluationContext(packetHistory, channelInfo
                , dataCollectorCallable);

            owner.ColdEnd = 0.0;
            try
            {
                if (coldEndChannel != null) owner.ColdEnd = Convert.ToDouble(EnsureChannelValue(coldEndChannel, owner));
            }
            catch(Exception exception)
            {
                log4net.LogManager.GetLogger("DataCollector").Error("Unable to evaluate cold end value.", exception);
            }


            foreach (CalculatableChannel calculatableChannel in channelInfo.Values)
            {
                EnsureChannelValue(calculatableChannel, owner);
            }

            if (owner.AfterProcessCallback != null)
                owner.AfterProcessCallback.Invoke();
        }

        public object EnsureChannelValue(CalculatableChannel calculatableChannel, IRtpChannelsApi context)
        {
            if (!ChannelsValues.ContainsKey(calculatableChannel.ChannelData.Id))
            {
                var value = calculatableChannel.Calculate(context);
                ChannelsValues[calculatableChannel.ChannelData.Id] = value;
                return value;
            }

            return ChannelsValues[calculatableChannel.ChannelData.Id];
        }

        public RawPacket Clone()
        {
            return (RawPacket) MemberwiseClone();
        }

        public void Clean()
        {
            _channelsValues = new Dictionary<int, object>();
            _rawChannelsBag = new Dictionary<int, RawChannel>();
        }
    }
}