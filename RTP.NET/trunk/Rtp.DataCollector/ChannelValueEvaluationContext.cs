﻿using System;
using System.Linq;
using SCG = System.Collections.Generic;
using C5;
using Rtp.Infrastructure.Utility;
using Rtp.Model.Data;

namespace Rtp.DataCollector
{
    internal class ChannelValueEvaluationContext : IRtpChannelsApi
    {
        private readonly IQueue<RawPacket> _history;

        private IDataCollectorCallable _dataCollectorCallable;

        private SilentIndexedCollection<SilentIndexedCollection<int, int>, int> _raw;
        private SCG.IDictionary<int, CalculatableChannel> _channels;

        public ChannelValueEvaluationContext(IQueue<RawPacket> history, SCG.IDictionary<int, CalculatableChannel> channels
            , IDataCollectorCallable dataCollectorCallable)
        {
            _history = history;
            _dataCollectorCallable = dataCollectorCallable;
            _channels = channels;
        }

        #region IRtpChannelsApi Members

        public SilentIndexedCollection<SilentIndexedCollection<int, int>, int> Raw
        {
            get
            {
                if (_raw == null)
                {
                    var del = new FetchDelegate<SilentIndexedCollection<int, int>, int>(
                        packetIndex =>
                            {
                                RawPacket packet = _history.Count > packetIndex
                                    ? _history[_history.Count - packetIndex - 1] : null;

                                var innerDelegate = new FetchDelegate<int, int>(
                                    channelIndex =>
                                        {
                                            return (packet != null && packet.RawChannelsBag.ContainsKey(channelIndex))
                                                       ? packet.RawChannelsBag[channelIndex].GetValue()
                                                       : 0;
                                        }
                                    );

                                var b = new SilentIndexedCollection<int, int>(innerDelegate);

                                return b;
                            });

                    _raw = new SilentIndexedCollection<SilentIndexedCollection<int, int>, int>(del);
                }

                return _raw;
            }
        }

        private SilentIndexedCollection<SilentIndexedCollection<object, int>, int> _calc;
        /// <summary>
        /// Inputs are channelID from Channels table and packet index
        /// If( (raw[0][1] AND 8191) > 0, 36621.7/(raw[0][1] AND 8191), 0.0) - speed
        /// 
        /// MAX(CAST(calculated[1][13], int), 0) + 1  -- packet counter
        /// 
        ///  MAX(0, CAST(CALCULATED[1][1], INT))+ (((MAX(raw[0][2], 0) >> 8) AND 0x3C) >> 2) - meter
        /// </summary>
        public SilentIndexedCollection<SilentIndexedCollection<object, int>, int> Calculated
        {
            get
            {
                //if (_calc == null)
                {
                    var del = new FetchDelegate<SilentIndexedCollection<object, int>, int>(
                        packetIndex =>
                        {
                            RawPacket packet = _history.Count > packetIndex 
                                ? _history[_history.Count - packetIndex - 1] : null;

                            var innerDelegate = new FetchDelegate<object, int>(
                                channelIndex =>
                                {
                                    if(packet != null)
                                    {
                                        if(_channels.ContainsKey(channelIndex))
                                            return packet.EnsureChannelValue(_channels[channelIndex], this);
                                   
                                    }

                                    return 0;

                                }
                                );

                            var b = new SilentIndexedCollection<object, int>(innerDelegate);

                            return b;
                        });

                    _calc = new SilentIndexedCollection<SilentIndexedCollection<object, int>, int>(del);
                }

                return _calc;
            }
        }

        public int AddPiece()
        {
            //AfterProcessCallback = Flush;
            _dataCollectorCallable.Flush();

            return _dataCollectorCallable.AddPiece();
        }

        public int AddByMeter2Cache(IRawPacket rawPacket)
        {
            return _dataCollectorCallable.AddByMeter2Cache(rawPacket);
        }


        public int AddMeters2Cache(int count)
        {
            if (_history.Count > 1 && count > 0)
            {
                //for (int idx = 0; idx < count; ++idx)
                {
                    AddByMeter2Cache(_history[_history.Count - 2]);
                }
            }

            return count;
        }        
        
        public int SetCallback(int metersReceived)
        {
            if (_history.Any() && metersReceived > 0)
            {
                var packet = _history.Last();

                AfterProcessCallback = () => {return AddByMeter2Cache(packet); };
            }

            return 0;
        }

        public int AddByTime2Cache(IRawPacket rawPacket)
        {
            return _dataCollectorCallable.AddByTime2Cache(rawPacket);
        }

        public int AddByTimeToCache()
        {
            if (_history.Count > 1)
                return AddByTime2Cache(_history[_history.Count - 2]);

            return 0;
        }

        public long CurrentTimeStamp
        {
            get { return Infrastructure.Utility.DateTimeRoutines.UnixTimeStamp.UtcNow; }
        }

        public Func<int> AfterProcessCallback
        {
            get; set; 
        }

        public double ColdEnd { get; set; }

        public int Flush()
        {
            return _dataCollectorCallable.Flush();
        }

        #endregion
    }
}