﻿using System;

namespace Rtp.DataCollector
{
    internal static class BitOperations
    {
        public static bool CheckBitIsSet(byte arg, uint pos)
        {
            var mask = (byte) Math.Pow(2, pos);

            return (arg & mask) == mask;
        }

        public static byte SetBitValue(byte arg, uint pos, bool val)
        {
            var mask = (byte) Math.Pow(2, pos);

            return (byte) (val ? (arg | mask) : arg & ~mask);
        }

        public static byte SetBit(byte arg, uint pos)
        {
            return SetBitValue(arg, pos, true);
        }

        public static byte UnSetBit(byte arg, uint pos)
        {
            return SetBitValue(arg, pos, false);
        }
    }
}