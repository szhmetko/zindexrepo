﻿using System;
using System.Collections.Generic;
using log4net;
using Microsoft.Practices.ServiceLocation;
using Rtp.DataCollector.Listeners;
using Rtp.Infrastructure.Config;
using Rtp.Infrastructure.Exceptions;
using Rtp.Infrastructure.Utility;
using Rtp.Model.Data;
using Rtp.Model.Line;
using Rtp.Model.Master;
using Rtp.Repository;
using Rtp.Repository.Repositories;
using System.Linq;

namespace Rtp.DataCollector
{

    public class DataCollector : IRawDataConsumer, IDataCollectorCallable
    {
        private const int ChunkMinLength = 3;

        private const int RecordInCachePeriod = 3;
        private const int FlushCachePeriod = 30;
        private long _lastWrittenInCache = 0;
        private long _lastCacheFlushed = 0;

        private readonly RawPacket _currentPacket = new RawPacket();
        private readonly LimitedQueue<byte> _quReceivedBytes = new LimitedQueue<byte>(1.0E6);
        private readonly LimitedQueue<RawPacket> _quReceivedRawPackets = new LimitedQueue<RawPacket>(8);

        private readonly LimitedQueue<RawPacket> _quRecordsByTime = new LimitedQueue<RawPacket>(1E3);
        private readonly LimitedQueue<RawPacket> _quRecordsByMeter = new LimitedQueue<RawPacket>(1E3);

        private IDictionary<int, CalculatableChannel> _channelsInfo;

        public void Start()
        {
            try
            {
                Listener.Start();
            }
            catch(DataListenerInitializationException e)
            {
                throw new DataCollectorInitializationException("Fail to start datacollector", e.InnerException);
            }
        }

        public void Stop()
        {
            Listener.Stop();
        }

        public DataCollector(Line line)
        {
            LineSettings = line.Clone();

            AppConfig.Instance.CurentLineConnection = LineSettings;

            Listener = ListenerFactory.Create(line.Collector);

            Listener.DataConsumer = this;
        }

        private readonly ILog _logger = LogManager.GetLogger(typeof(DataCollector));

        private CalculatableChannel _coldEndChannelInfo;
        protected CalculatableChannel ColdEndChannelInfo
        {
            get
            {
                if (_coldEndChannelInfo == null)
                {
                    _coldEndChannelInfo =
                        _channelsInfo.Values.FirstOrDefault(
                            channel => channel.ChannelData.FieldName == LineSettings.ColdEndFieldName);

                }
                return _coldEndChannelInfo;
            }
        }


        internal IDictionary<int, CalculatableChannel> ChannelsInfo
        {
            get
            {
                if (_channelsInfo == null)
                {
                    _channelsInfo = new Dictionary<int, CalculatableChannel>();

                    try
                    {
                        using(var repo = new ChannelsRoRepository(LineSettings))
                        {
                            IList<Channel> channelsCollection = repo.FindAll();
                            lock (_channelsInfo)
                            {
                                foreach (Channel channel in channelsCollection)
                                {
                                    _channelsInfo[channel.Id] = new CalculatableChannel(channel.Clone(),
                                                                                        new ChannelValueEvaluationContext
                                                                                            (_quReceivedRawPackets,
                                                                                             ChannelsInfo, this))
                                                                    {
                                                                        ThermoPair =
                                                                            LineSettings.ThermoPairs.SingleOrDefault(
                                                                                tp => tp.Id == channel.ThermoPairId)
                                                                    };

                                }

                            }
                        }
                    }
                    catch (Exception exception)
                    {
                        _logger.Error("Cannot fetch channels information", exception);

                        throw new DataCollectorInitializationException("Cannot fetch channels information", exception.InnerException);
                    }


                }

                return _channelsInfo;
            }
        }

        private Line LineSettings { get; set; }
        private IDataListener Listener { get; set; }

        public event RawChannelEventHandler RawChannelReceived;
        public event RawPacketEventHandler RawPacketReceived;

        protected void ProcessRawPacket(RawPacket rawPacket)
        {

            lock (_quReceivedRawPackets)
            {
                _quReceivedRawPackets.Enqueue(rawPacket);
                _quReceivedRawPackets.Peek().ProcessValues(ChannelsInfo, _quReceivedRawPackets, ColdEndChannelInfo, this);
            }

            long elapsedCache = Infrastructure.Utility.DateTimeRoutines.UnixTimeStamp.UtcNow - _lastWrittenInCache;
            long elapsedFlush = Infrastructure.Utility.DateTimeRoutines.UnixTimeStamp.UtcNow - _lastCacheFlushed;

            if(elapsedCache > RecordInCachePeriod)
            {
                AddByTime2Cache(_quReceivedRawPackets.Peek());
                _lastWrittenInCache = Infrastructure.Utility.DateTimeRoutines.UnixTimeStamp.UtcNow;
            }

            if(elapsedFlush > FlushCachePeriod)
            {
                Flush();
                _lastCacheFlushed = Infrastructure.Utility.DateTimeRoutines.UnixTimeStamp.UtcNow;
            }

        }

        protected void InvokeRawPacketReceived(RawPacketArgs e)
        {
            ProcessRawPacket(e.Packet);

            RawPacketEventHandler handler = RawPacketReceived;
            if (handler != null) handler(this, e);
        }

        //public void _Dump()
        //{
        //    var dvrr = new DataValuesRoRepository(LineSettings);

        //    List<RawPacket> lst;
        //    lock (_quReceivedRawPackets)
        //    {
        //        lst = new List<RawPacket>(_quReceivedRawPackets);
        //    }

        //    int affected = dvrr.BulkInsert(lst);

        //    if (affected > 0)
        //        lock (_quReceivedRawPackets)
        //            _quReceivedRawPackets.Clear();
        //}

        protected void InvokeRawChannelReceived(RawChannelArgs args)
        {
            if (args.Data.Num == 1)
            {
                InvokeRawPacketReceived(new RawPacketArgs(_currentPacket.Clone()));

                _currentPacket.Clean();
            }

            RawChannelEventHandler handler = RawChannelReceived;

            if (handler != null) handler(this, args);

            _currentPacket.Add(args.Data);
        }


        private void TryProcessQueue()
        {
            try
            {
                lock (_quReceivedBytes)
                {
                    while (_quReceivedBytes.Count > ChunkMinLength)
                    {
                        byte bt = _quReceivedBytes.Dequeue();

                        if (BitOperations.CheckBitIsSet(bt, 7))
                        {
                            //channel number calculated
                            int channelNum = BitOperations.UnSetBit(bt, 7);


                            byte hiByte = _quReceivedBytes.Dequeue();
                            bool loBitValue = BitOperations.CheckBitIsSet(hiByte, 0);

                            hiByte = (byte) (hiByte >> 1);

                            byte loByte = _quReceivedBytes.Dequeue();
                            loByte = BitOperations.SetBitValue(loByte, 7, loBitValue);

                            InvokeRawChannelReceived(new RawChannelArgs(channelNum, hiByte, loByte));
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                _logger.Error("ProcessQueue problem", exception);

                throw new DataCollectorProcessQueueException("ProcessQueue problem", exception.InnerException);
            }
        }

        #region IRawDataConsumer members

        void IRawDataConsumer.SendData(byte data)
        {
            lock (_quReceivedBytes)
            {
                _quReceivedBytes.Enqueue(data);
            }

            TryProcessQueue();
        }

        public void SendData(IEnumerable<byte> data)
        {
            lock (_quReceivedBytes)
            {
                foreach (byte b in data)
                    _quReceivedBytes.Enqueue(b);
            }

            TryProcessQueue();
        }

        #endregion

        public int AddPiece()
        {
            int newId = -1;
            try
            {
                using (var uow = ServiceLocator.Current.GetInstance<ILineUnitOfWork>())
                {

                    using (var repo = new PiecesRepository(uow))
                    {

                        try
                        {
                            var newPiece = new Piece {DtOff = DateTime.MinValue, DtOn = DateTime.Now, Length = 0};

                            repo.Add(newPiece);

                            repo.RecalculateCompleted(new TimeSpan(0, 1, 0));

                            uow.Commit();

                            newId = newPiece.Id;
                        }
                        catch(Exception exception)
                        {
                            uow.Rollback();
                            throw new DataCollectorProcessQueueException("AddPiece error ", exception);
                        }
                    }
                }

            }
            catch (Exception exception)
            {
                _logger.Error("Unable add piece", exception.InnerException ?? exception);

            }
            return newId;
        }

        public int AddByMeter2Cache(IRawPacket rawPacket)
        {
            lock (_quRecordsByMeter)
            {
                RawPacket packet = (RawPacket)rawPacket.Clone();
                packet.Kind = PacketKind.ByMeter;

                _quRecordsByMeter.Enqueue(packet);
            }

            return 0;
        }

        public int AddByTime2Cache(IRawPacket rawPacket)
        {
            lock (_quRecordsByTime)
            {
                RawPacket packet = (RawPacket)rawPacket.Clone();
                packet.Kind = PacketKind.ByTime;

                _quRecordsByTime.Enqueue(packet);
            }

            return 0;
        }

        public int Flush()
        {

            //TODO Lock queues on start

            List<RawPacket> lst = new List<RawPacket>();
            lock (_quRecordsByTime)
            {
                lst.AddRange(_quRecordsByTime);
            }
            lock (_quRecordsByMeter)
            {
                lst.AddRange(_quRecordsByMeter);
            }
            try
            {
                using (var dvrr = new DataValuesRoRepository(LineSettings))
                {

                    int affected = dvrr.BulkInsert(lst, ChannelsInfo);
                    if (affected > 0)
                    {
                        lock (_quRecordsByTime)
                            _quRecordsByTime.Clear();

                        lock (_quRecordsByMeter)
                            _quRecordsByMeter.Clear();
                    }
                }
            }
            catch (Exception exception)
            {
                _logger.Error("Unable flush cache", exception.InnerException ?? exception);
                //throw;

            }



            return 0;
        }
    }
}