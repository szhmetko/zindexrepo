﻿using System;
using Rtp.Model.Data;

namespace Rtp.DataCollector
{
    public delegate void RawChannelEventHandler(object sender, RawChannelArgs args);
    public delegate void RawPacketEventHandler(object sender, RawPacketArgs args);

    /// <summary>
    /// encapsulates information about raw channel received
    /// </summary>
    public class RawChannelArgs : EventArgs
    {
        private readonly RawChannel _rawChannel;


        /// <summary>
        /// Default constructor
        /// </summary>
        public RawChannelArgs(int channelNum, byte hiByte, byte loByte, int pid = -1)
        {
            _rawChannel = new RawChannel
                              {
                                  Num = channelNum,
                                  HiByte = hiByte,
                                  LoByte = loByte,
                              };
        }

        public RawChannel Data
        {
            get { return _rawChannel; }
        }
    }

    /// <summary>
    /// encapsulates information about raw packet received
    /// </summary>
    public class RawPacketArgs : EventArgs
    {
        private readonly RawPacket _rawPacket;

        /// <summary>
        /// Default constructor
        /// </summary>
        public RawPacketArgs(RawPacket rawPacket)
        {
            _rawPacket = rawPacket;
        }

        public RawPacket Packet
        {
            get { return _rawPacket; }
        }
    }
}