﻿using System;
using System.Collections;
using System.Collections.Generic;
using Rtp.Model.Data;
using Rtp.Model.Line;

namespace Rtp.DataCollector
{

    public class LazyIntCollection<T> : IList<T>
    {
        private readonly List<T> _innerList = new List<T>();

        #region IList<T> Members

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return _innerList.GetEnumerator();
        }

        public IEnumerator GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(T item)
        {
            _innerList.Add(item);
        }

        void ICollection<T>.Clear()
        {
            throw new NotImplementedException();
        }

        bool ICollection<T>.Contains(T item)
        {
            throw new NotImplementedException();
        }

        void ICollection<T>.CopyTo(T[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        bool ICollection<T>.Remove(T item)
        {
            throw new NotImplementedException();
        }

        int ICollection<T>.Count
        {
            get { throw new NotImplementedException(); }
        }

        bool ICollection<T>.IsReadOnly
        {
            get { throw new NotImplementedException(); }
        }

        int IList<T>.IndexOf(T item)
        {
            throw new NotImplementedException();
        }

        void IList<T>.Insert(int index, T item)
        {
            throw new NotImplementedException();
        }

        void IList<T>.RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        T IList<T>.this[int index]
        {
            get { return _innerList[index]; }
            set { throw new NotImplementedException(); }
        }

        #endregion
    }

    //public class ChannelValueEvaluationContext
    //{
    //    private readonly Channel _channel;
    //    private readonly RawChannel _rawChannel;

    //    public ChannelValueEvaluationContext(Channel channel, RawChannel rawChannel)
    //    {
    //        _channel = channel;
    //        _rawChannel = rawChannel;
    //    }

    //    public IList<List<int>> Raw
    //    {
    //        get { return new List<List<int>> {new List<int> {99, 2, 8}
    //        , new List<int>{15, 22 ,1 }
    //        }; }
    //    }

    //    //public ISimpleIndexed<int> Dummy
    //    //{   get { return  new MyDummyImplementation();}

    //    //}


    //    public int AddNewPiece()
    //    {
    //        return _channel.Id + _rawChannel.Num;
    //    }
    //}
}