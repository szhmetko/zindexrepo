﻿using System;
using System.IO;
using System.IO.Ports;
using System.Threading;
using log4net;
using Rtp.Infrastructure.Exceptions;
using Rtp.Infrastructure.Utility.DatacollectorSettings;

namespace Rtp.DataCollector.Listeners
{

    public class SerialPortDataListener : IDataListener
    {
        ManualResetEvent _shouldStop = new ManualResetEvent(false);
        ManualResetEvent receiveDone = new ManualResetEvent(false);

        private SerialPort _serialPortListener;

        protected Rs232DatacollectorSettings Settings { get; private set; }

        public SerialPortDataListener(Rs232DatacollectorSettings datacollectorSettings, bool bAutoStart = false)
        {
            Settings = datacollectorSettings;

            if (bAutoStart)
                Start();
        }

        private IRawDataConsumer _dataConsumer;

        public IRawDataConsumer DataConsumer
        {
            set { _dataConsumer = value; }
        }


        public void Start()
        {
            try
            {
                _serialPortListener = new SerialPort(Settings.Com, Settings.Baud, Settings.Parity, Settings.DataBits, Settings.StopBits);
                
                _serialPortListener.Open();

                // Begin receiving the data from the remote device.
                _serialPortListener.DataReceived += _serialPortListener_DataReceived;

                _shouldStop.Reset();
                receiveDone.Reset();
            }
            catch (Exception e)
            {
                DisposeListener();

                LogManager.GetLogger(GetType()).Error(e.ToString(), e);
                
                throw new DataListenerInitializationException("Fail to start listener", e);
            }
        }

        private void _serialPortListener_DataReceived(object sender, SerialDataReceivedEventArgs arg)
        {
            try
            {

                //arg.EventType == SerialData.Eof
                // Create a memory stream object that will contain the new data received
                var buffer = new MemoryStream();
                byte[] receivedByte = new byte[1];

                // Read byte by byte the data received and store it in the memory stream object
                while (_serialPortListener.BytesToRead > 0)
                {
                    _serialPortListener.Read(receivedByte, 0, 1);
                    buffer.Write(receivedByte, 0, 1);
                }

                // Once we got all the new bytes, add the bytes to the internal buffer (memory stream object)
                byte[] aux = buffer.ToArray();

                if (aux.Length > 0)
                {
                    // There might be more data, so store the data received so far.
                    //var dataChunk = new List<byte>(buffer.Take(bytesRead));

                    _dataConsumer.SendData(aux);

                    receiveDone.Set();

                    // Get the rest of the data.
                    if (!_shouldStop.WaitOne(5))
                    {
                        receiveDone.Reset();
                    }
                }

            }
            catch (Exception e)
            {
                LogManager.GetLogger(GetType()).Error(e.ToString(), e);
            }
        }

        private void DisposeListener()
        {
            _serialPortListener.Close();

            _serialPortListener = null;
        }


        public void Stop()
        {
            _shouldStop.Set();

            receiveDone.WaitOne(5000);

            DisposeListener();
        }
    }
}
