﻿using System;
using Rtp.Infrastructure.Utility;
using Rtp.Infrastructure.Utility.DatacollectorSettings;

namespace Rtp.DataCollector.Listeners
{
    internal class ListenerFactory
    {
        public static IDataListener Create(string rawData)
        {
            var genSettings = XmlObjectState.XmlTo<GenericDatacollectorSettings>(rawData);

            switch (genSettings.Type)
            {
                case DatacollectorType.Udp:
                    return new UdpDataListener(XmlObjectState.XmlTo<UdpDatacollectorSettings>(rawData));
                case DatacollectorType.Rs232:
                    return new SerialPortDataListener(XmlObjectState.XmlTo<Rs232DatacollectorSettings>(rawData));
                default:
                    throw new NotImplementedException(String.Format(Messages.ErrorCannotResolveDatalistener, rawData));

            }

        }

    }
}
