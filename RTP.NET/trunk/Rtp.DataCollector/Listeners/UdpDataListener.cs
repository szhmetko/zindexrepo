﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using log4net;
using Rtp.Infrastructure.Exceptions;
using Rtp.Infrastructure.Utility.DatacollectorSettings;

namespace Rtp.DataCollector.Listeners
{

    public class UdpDataListener : IDataListener
    {
        const int BufferSize = 255;

        private Socket _listener;

        ManualResetEvent _shouldStop = new ManualResetEvent(false);
        ManualResetEvent receiveDone = new ManualResetEvent(false);

        protected Socket Listener
        {
            get {
                if(_listener == null)
                {
                    _listener = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

                    var endPoint = new IPEndPoint(IPAddress.Any, Settings.Port);

                    Listener.Bind(endPoint);

                }
                return _listener ;
            }
            set { _listener = value; }
        }


        protected UdpDatacollectorSettings Settings { get; private set; }

        public UdpDataListener(UdpDatacollectorSettings datacollectorSettings, bool bAutoStart = false)
        {
            Settings = datacollectorSettings;

            if(bAutoStart)
                Start();
        }

        private IRawDataConsumer _dataConsumer;

        public IRawDataConsumer DataConsumer
        {
            set { _dataConsumer = value; }
        }


        public void Start()
        {
            try
            {
                var buffer = new byte[BufferSize];

                // Begin receiving the data from the remote device.
                Listener.BeginReceive(buffer, 0, BufferSize, 0, new AsyncCallback(ReceiveCallback), buffer);

                _shouldStop.Reset();
                receiveDone.Reset();
            }
            catch (SocketException e)
            {
                DisposeListener();

                LogManager.GetLogger(GetType()).Error(e.ToString(), e);
                
                throw new DataListenerInitializationException("Fail to start listener", e);
            }
        }

        private void DisposeListener()
        {
            Listener.Shutdown(SocketShutdown.Both);
            Listener.Close();
            Listener.Dispose();

            Listener = null;
        }


        public void Stop()
        {
            //Listener.EndReceive();
            _shouldStop.Set();

            receiveDone.WaitOne(5000);

            DisposeListener();
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            try
            {

                var buffer = (byte[])ar.AsyncState;

                // Read data from the remote device.
                int bytesRead = Listener.EndReceive(ar);

                if (bytesRead > 0)
                {
                    // There might be more data, so store the data received so far.
                    //var dataChunk = new List<byte>(buffer.Take(bytesRead));

                    _dataConsumer.SendData(buffer.Take(bytesRead));

                    receiveDone.Set();

                    // Get the rest of the data.
                    if (!_shouldStop.WaitOne(5))
                    {
                        receiveDone.Reset();
                        Listener.BeginReceive(buffer, 0, BufferSize, 0, new AsyncCallback(ReceiveCallback), buffer);
                    }
                }

            }
            catch (SocketException e)
            {
                LogManager.GetLogger(GetType()).Error(e.ToString(), e);
                //throw;
            }
        }
    }
}
