﻿
namespace Rtp.DataCollector.Listeners
{
    public interface IDataListener
    {
        IRawDataConsumer DataConsumer { set; }

        void Start();
        void Stop();

        //void Restart();
    }

}
