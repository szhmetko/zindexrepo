﻿using System;
using System.Collections;
using System.Linq;
using Rtp.Infrastructure.PermissionAttributes;
using Rtp.Infrastructure.UnitOfWork;
using Rtp.Model;
using Rtp.Repository.Repositories;

namespace Rtp.Repository
{
    public class EntityPermissionEngine
    {
        struct PermissionKey
        {
            internal string EntityType { get; set; }
            internal string Role { get; set; }
        }

        public class PermissionValue : IPermissionValue
        {
            public PermissionValue()
            {
                CanRetrieve = true;
            }

            public bool CanCreate { get; internal set; }
            public bool CanRetrieve { get; internal set; }
            public bool CanUpdate { get; internal set; }
            public bool CanDelete { get; internal set; }
        }

        private PermissionsRepository _repository;
        private bool _useCache;

        private readonly Hashtable _entitiesPermissions = new Hashtable();

        public EntityPermissionEngine(IUnitOfWork unitOfWork, bool useCache = true)
        {
            _repository = new PermissionsRepository(unitOfWork);
            _useCache = useCache;
        }

        public PermissionValue GetPermissions<T>(string role)
        {
            var permissionKey = new PermissionKey { EntityType = GetEntitySpecialName(typeof(T)), Role = role };

            if (_entitiesPermissions[permissionKey] as PermissionValue == null)
            {
                var permissions = _repository.GetPermissionsFor(GetEntitySpecialName(typeof(T)), role);

                if (_useCache) 
                    _entitiesPermissions.Add(permissionKey, permissions ?? new PermissionValue());

                return permissions;
            }

            return _entitiesPermissions[permissionKey] as PermissionValue;
        }

        protected string GetEntitySpecialName(Type type)
        {
            var entityNameAttrribute = type.GetCustomAttributes(true).OfType<EntityPermissionAttribute>().FirstOrDefault();

            if (entityNameAttrribute != null)
                return entityNameAttrribute.EntityKey;

            return type.Name;
        }

    }
}
