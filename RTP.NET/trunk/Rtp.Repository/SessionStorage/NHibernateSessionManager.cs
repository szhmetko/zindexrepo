﻿using System;
using System.Collections;
using System.Runtime.Remoting.Messaging;
using System.Web;
using NHibernate;
using NHibernate.Cache;
using NHibernate.Cfg;
using Rtp.Infrastructure;



namespace Rtp.Repository.SessionStorage
{
    /// <summary>
    /// Handles creation and management of sessions and transactions.  It is a singleton because 
    /// building the initial session factory is very expensive. Inspiration for this class came 
    /// from Chapter 8 of Hibernate in Action by Bauer and King.  Although it is a sealed singleton
    /// you can use TypeMock (http://www.typemock.com) for more flexible testing.
    /// </summary>
    public sealed class NHibernateSessionManager
    {
        #region Thread-safe, lazy Singleton

        /// <summary>
        /// This is a thread-safe, lazy singleton.  See http://www.yoda.arachsys.com/csharp/singleton.html
        /// for more details about its implementation.
        /// </summary>
        public static NHibernateSessionManager Instance
        {
            get
            {
                return Nested.NHibernateSessionManager;
            }
        }

        /// <summary>
        /// Private constructor to enforce singleton
        /// </summary>
        private NHibernateSessionManager()
        {
            log4net.Config.XmlConfigurator.Configure();
        }

        /// <summary>
        /// Assists with ensuring thread-safe, lazy singleton
        /// </summary>
        private class Nested
        {
            static Nested() { }
            internal static readonly NHibernateSessionManager NHibernateSessionManager =
                new NHibernateSessionManager();
        }

        #endregion

        /// <summary>
        /// This method attempts to find a session factory stored in <see cref="sessionFactories" />
        /// via its name; if it can't be found it creates a new one and adds it the hashtable.
        /// </summary>
        /// <param name="sessionKey">Path location of the factory config</param>
        private ISessionFactory GetSessionFactoryFor(string sessionKey)
        {
            if (string.IsNullOrEmpty(sessionKey))
                throw new ArgumentNullException("sessionKey", "sessionKey may not be null nor empty");

            //  Attempt to retrieve a stored SessionFactory from the hashtable.
            ISessionFactory sessionFactory = (ISessionFactory)sessionFactories[sessionKey];

            //  Failed to find a matching SessionFactory so make a new one.
            if (sessionFactory == null)
            {
                var config = new Configuration();
                config.AddAssembly("Rtp.Model");
                sessionFactory = config
                    .Configure()
                    .SetProperty("connection.connection_string", sessionKey).BuildSessionFactory();
      
                if (sessionFactory == null)
                {
                    throw new InvalidOperationException("cfg.BuildSessionFactory() returned null.");
                }

                sessionFactories.Add(sessionKey, sessionFactory);
            }

            return sessionFactory;
        }

        /// <summary>
        /// Allows you to register an interceptor on a new session.  This may not be called if there is already
        /// an open session attached to the HttpContext.  If you have an interceptor to be used, modify
        /// the HttpModule to call this before calling BeginTransaction().
        /// </summary>
        public void RegisterInterceptorOn(string sessionKey, IInterceptor interceptor)
        {
            ISession session = GetSessionFrom(sessionKey);

            if (session != null && session.IsOpen)
            {
                throw new CacheException("You cannot register an interceptor once a session has already been opened");
            }

            GetSessionFrom(sessionKey, interceptor);
        }

        public IStatelessSession GetStatelesSessionFrom(IConnectionInfo connectionInfo)
        {
            return GetSessionFactoryFor(connectionInfo.ConnectionString).OpenStatelessSession();
        }
        
        public ISession GetSessionFrom(string sessionKey)
        {
            return GetSessionFrom(sessionKey, null);
        }

        public ISession GetSessionFrom(IConnectionInfo connectionInfo)
        {
            return GetSessionFrom(connectionInfo.ConnectionString, null);
        }

        /// <summary>
        /// Gets a session with or without an interceptor.  This method is not called directly; instead,
        /// it gets invoked from other public methods.
        /// </summary>
        private ISession GetSessionFrom(string sessionKey, IInterceptor interceptor)
        {
            var sesFactory = GetSessionFactoryFor(sessionKey);
            /*
            if (!NHibernate.Context.CurrentSessionContext.HasBind(sesFactory))
            {
                ISession newSession = interceptor != null ? sesFactory.OpenSession(interceptor) : sesFactory.OpenSession();
                NHibernate.Context.CurrentSessionContext.Bind(newSession);
            } */


            ISession session = sesFactory.OpenSession();// GetCurrentSession();

            if(session == null)
                throw  new InvalidOperationException("session was null");

            return session;
        }

        /// <summary>
        /// Flushes anything left in the session and closes the connection.
        /// </summary>
        public void CloseSessionOn(string sessionKey)
        {
            ISession session = GetSessionFrom(sessionKey);

            if (session != null && session.IsOpen)
            {
                session.Flush();
                session.Close();
            }

            NHibernate.Context.CurrentSessionContext.Unbind(GetSessionFactoryFor(sessionKey));
        }

        public ITransaction BeginTransactionOn(string sessionKey)
        {
            ITransaction transaction = (ITransaction)ContextTransactions[sessionKey];

            if (transaction == null)
            {
                transaction = GetSessionFrom(sessionKey).BeginTransaction();
                ContextTransactions.Add(sessionKey, transaction);
            }

            return transaction;
        }

        public void CommitTransactionOn(string sessionKey)
        {
            ITransaction transaction = (ITransaction)ContextTransactions[sessionKey];

            try
            {
                if (HasOpenTransactionOn(sessionKey))
                {
                    transaction.Commit();
                    ContextTransactions.Remove(sessionKey);
                }
            }
            catch (HibernateException)
            {
                RollbackTransactionOn(sessionKey);
                throw;
            }
        }

        public bool HasOpenTransactionOn(string sessionKey)
        {
            ITransaction transaction = (ITransaction)ContextTransactions[sessionKey];

            return transaction != null && !transaction.WasCommitted && !transaction.WasRolledBack;
        }

        public void RollbackTransactionOn(string sessionKey)
        {
            ITransaction transaction = (ITransaction)ContextTransactions[sessionKey];

            try
            {
                if (HasOpenTransactionOn(sessionKey))
                {
                    transaction.Rollback();
                }

                ContextTransactions.Remove(sessionKey);
            }
            finally
            {
                CloseSessionOn(sessionKey);
            }
        }

        /// <summary>
        /// Since multiple databases may be in use, there may be one transaction per database 
        /// persisted at any one time.  The easiest way to store them is via a hashtable
        /// with the key being tied to session factory.  If within a web context, this uses
        /// <see cref="HttpContext" /> instead of the WinForms specific <see cref="CallContext" />.  
        /// Discussion concerning this found at http://forum.springframework.net/showthread.php?t=572
        /// </summary>
        private Hashtable ContextTransactions
        {
            get
            {
                if (IsInWebContext())
                {
                    if (HttpContext.Current.Items[TRANSACTION_KEY] == null)
                        HttpContext.Current.Items[TRANSACTION_KEY] = new Hashtable();

                    return (Hashtable)HttpContext.Current.Items[TRANSACTION_KEY];
                }
                else
                {
                    if (CallContext.GetData(TRANSACTION_KEY) == null)
                        CallContext.SetData(TRANSACTION_KEY, new Hashtable());

                    return (Hashtable)CallContext.GetData(TRANSACTION_KEY);
                }
            }
        }

        private bool IsInWebContext()
        {
            return HttpContext.Current != null;
        }

        private Hashtable sessionFactories = new Hashtable();
        private const string TRANSACTION_KEY = "CONTEXT_TRANSACTIONS";
    }
}
