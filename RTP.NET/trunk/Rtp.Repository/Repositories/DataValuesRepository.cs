﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using NHibernate;
using NHibernate.Transform;
using Rtp.Infrastructure;
using Rtp.Infrastructure.Exceptions;
using Rtp.Model.Data;
using Rtp.Model.Line;
using Rtp.Repository.SessionStorage;

namespace Rtp.Repository.Repositories
{

    public class DataValuesRoRepository : ReadOnlyRepositry<DataValues, long>, IDataValuesRepository
    {
        public DataValuesRoRepository(IConnectionInfo connectionInfo)
            : base(connectionInfo)
        {
        }

        public int BulkInsert(IEnumerable<IRawPacket> packets, IDictionary<int, CalculatableChannel> channels)
        {
            try
            {
                var chanNames = new StringBuilder();
                var chanValues = new StringBuilder();

                if (!packets.Any()) return 0;

                var header = packets.FirstOrDefault();

                var f = new Func<KeyValuePair<int, object>, bool>((c) =>
                                                                  channels.ContainsKey(c.Key) &&
                                                                  !string.IsNullOrEmpty(
                                                                      channels[c.Key].ChannelData.FieldName)
                    );
                var valuesList = header.ChannelsValues.Where(f);

                foreach (var vl in valuesList)
                {
                    if (!string.IsNullOrEmpty(channels[vl.Key].ChannelData.FieldName))
                    {
                        if (chanNames.Length > 0)
                            chanNames.Append(",");

                        chanNames.Append(channels[vl.Key].ChannelData.FieldName);
                    }
                }

                var idx = 0;

                foreach (var rawPacket in packets)
                {
                    if (idx++ > 0)
                        chanValues.Append(",");

                    chanValues.Append("(");

                    var iidx = 0;
                    foreach (var rawChannel in rawPacket.ChannelsValues.Where(f))
                    {
                        if (iidx++ > 0)
                            chanValues.Append(",");

                        if (channels.ContainsKey(rawChannel.Key))
                            chanValues.Append(rawChannel.Value.ToString().Replace(',', '.'));


                        chanValues.Append(System.Environment.NewLine);
                    }
                    chanValues.AppendFormat(", {0}, {1})", rawPacket.Timestamp, Convert.ToInt32(rawPacket.Kind));
                }

                var strQuery = string.Format("INSERT INTO Xvals ({0}, Uxtime, DataType) VALUES {1}"
                                             , chanNames, chanValues);


                using (ISession session = NHibernateSessionManager.Instance.GetSessionFrom(ConnectionInfo))
                {
                    using (ITransaction tx = session.BeginTransaction())
                    {

                        try
                        {
                            ISQLQuery query = session.CreateSQLQuery(strQuery);
                            query.ExecuteUpdate();
                            tx.Commit();
                        }
                        catch (Exception exception)
                        {
                            tx.Rollback();
                            throw new RtpRepositoryException("commit error", exception);
                        }
                    }
                }



            }
            catch (Exception exception)
            {
                throw new RtpRepositoryException("Bulk insert error.", exception);
            }

            return packets.Count();

        }

        public IEnumerable FetchArchiveData(long start, long end, int valuesDesired, string rawSql)
        {
            var sess = NHibernateSessionManager.Instance.GetStatelesSessionFrom(ConnectionInfo);

            var res = sess.CreateSQLQuery(
                    rawSql)
                    .SetInt32("RES", valuesDesired)
                    .SetInt64("DateBegin", start)
                    .SetInt64("DateEnd", end)
                    .SetResultTransformer(Transformers.AliasToEntityMap).List();

            return res;
        }

        public IEnumerable FetchMonitorData(int mins, int valuesDesired, string rawSql)
        {
            var sess = NHibernateSessionManager.Instance.GetStatelesSessionFrom(ConnectionInfo);

            var res = sess.CreateSQLQuery(
                    rawSql)
                    .SetInt32("RES", valuesDesired)
                    .SetInt32("MINS", mins)
                    .SetResultTransformer(Transformers.AliasToEntityMap).List();

            return res;
        }

        public IEnumerable FetchPieceData(int pid, int valuesDesired, string rawSql)
        {
            var sess = NHibernateSessionManager.Instance.GetStatelesSessionFrom(ConnectionInfo);

            var res = sess.CreateSQLQuery(
                    rawSql)
                    .SetInt32("RES", valuesDesired)
                    .SetInt32("PID", pid)
                    .SetResultTransformer(Transformers.AliasToEntityMap).List();

            return res;
        }
    
    }
}