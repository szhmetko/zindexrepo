﻿using Rtp.Model.Line;

namespace Rtp.Repository.Repositories
{
    public class ChannelsRepository : VolatileRepository<Channel, int>, IChannelsRepository
    {
        public ChannelsRepository(ILineUnitOfWork uow)
            : base(uow)
        {
        }

    }
}
