﻿using NHibernate;
using NHibernate.Transform;
using Rtp.Model.Line;

namespace Rtp.Repository.Repositories
{
    public class ModulesRepository : VolatileRepository<Module, int>, IModulesRepository
    {
        public ModulesRepository(ILineUnitOfWork uow) : base(uow)
        {
        }

        public override System.Collections.Generic.IList<Module> FindAll()
        {
            var criteriaQuery = Session.CreateCriteria(typeof(Module))
                .SetFetchMode("Charts", FetchMode.Eager)
                .SetFetchMode("Charts.Curves", FetchMode.Eager)
                .SetResultTransformer(new DistinctRootEntityResultTransformer());

            return criteriaQuery.List<Module>();
        }
    }
}
