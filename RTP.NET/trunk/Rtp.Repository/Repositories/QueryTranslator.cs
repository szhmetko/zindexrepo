﻿using System;
using NHibernate;
using NHibernate.Criterion;
using Rtp.Infrastructure.Query;

namespace Rtp.Repository.Repositories
{
    public static class QueryTranslator
    {
        public static ICriteria TranslateIntoNHQuery<T>(this Query query, ISession curSession)
        {
            ICriteria criteria;

            if (query.IsNamedQuery())
            {
                criteria = FindNHQueryFor(query);
            }
            else
            {
                criteria = curSession.CreateCriteria(typeof(T));

                var defaultOrderClause = query.OrderByProperty;

                foreach (Criterion c in query.Criteria)
                {
                    global::NHibernate.Criterion.ICriterion criterion;

                    switch (c.criteriaOperator)
                    {
                        case CriteriaOperator.Equal:
                            criterion = Expression.Eq(c.PropertyName, c.Value);
                            break;
                        case CriteriaOperator.LesserThanOrEqual:
                            criterion = Expression.Le(c.PropertyName, c.Value);
                            break;
                        default:
                            throw new ApplicationException("No operator defined");
                    }

                    if (query.QueryOperator == QueryOperator.And)
                        criteria.Add(Expression.Conjunction().Add(criterion));
                    else
                        criteria.Add(Expression.Disjunction().Add(criterion));

                    if(defaultOrderClause == null)
                        defaultOrderClause = new OrderByClause{PropertyName = c.PropertyName};
                }

                criteria.AddOrder(new Order(defaultOrderClause.PropertyName, !defaultOrderClause.Desc));
            }
            return criteria;
        }

        private static ICriteria FindNHQueryFor(Query query)
        {
            // No complex queries have been defined in this sample.
            throw new NotImplementedException();
        }
    }

}
