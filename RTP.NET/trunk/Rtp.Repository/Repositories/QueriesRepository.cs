﻿using Rtp.Model.Line;

namespace Rtp.Repository.Repositories
{
    public class QueriesRepository : VolatileRepository<Query, string>, IQueriesRepository
    {
        public QueriesRepository(ILineUnitOfWork uow)
            : base(uow)
        {
        }
    }
}
