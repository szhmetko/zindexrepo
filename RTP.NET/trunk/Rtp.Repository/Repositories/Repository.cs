﻿using System;
using System.Collections.Generic;
using NHibernate;
using Rtp.Infrastructure;
using Rtp.Infrastructure.Query;
using Rtp.Infrastructure.UnitOfWork;
using Rtp.Model;

namespace Rtp.Repository.Repositories
{
    public abstract class Repository<T, EntityKey> : IRepository<T, EntityKey> where T : IAggregateRoot
    {
        private readonly IUnitOfWork _uow;

        protected Repository(IUnitOfWork uow)
        {
            _uow = uow;
            
        }

        private TResult Transact<TResult>(Func<TResult> func)
        {
            if (!Session.Transaction.IsActive)
            {
                // Wrap in transaction
                TResult result;
                using (var tx = Session.BeginTransaction())
                {
                    result = func.Invoke();
                    tx.Commit();
                }
                return result;
            }
            // Don't wrap;
            return func.Invoke();
        }

        public void Delete(T entity)
        {                
            Transact(() =>
                              {
                                  Session.Delete(entity);
                                  return 0;
                              }

                );
        }


        public void Add(T entity)
        {
            Session.Save(entity);
        }

        public void Remove(T entity)
        {
            Session.Delete(entity);
        }

        protected ISession Session
        {
            get { return ((NHUnitOfWork) _uow).Session; }
        }

        public void Save(T entity)
        {
            Session.SaveOrUpdate(entity);
        }

        public T FindBy(EntityKey Id)
        {
            return Session.Get<T>(Id);
        }

        virtual public IList<T> FindAll()
        {
            ICriteria criteriaQuery = Session.CreateCriteria(typeof(T));

            return criteriaQuery.List<T>();
        }

        public IEnumerable<T> FindAll(int index, int count)
        {
            ICriteria criteriaQuery = Session.CreateCriteria(typeof(T));

            return criteriaQuery.SetFetchSize(count).SetFirstResult(index).List<T>();
        }

        public IEnumerable<T> FindBy(Query query)
        {
            ICriteria nhQuery = query.TranslateIntoNHQuery<T>(Session);

            return nhQuery.List<T>();
        }

        public IEnumerable<T> FindBy(Query query, int index, int count)
        {
            ICriteria nhQuery = query.TranslateIntoNHQuery<T>(Session);

            return nhQuery.SetFetchSize(count).SetFirstResult(index).List<T>();
        }

        public void Dispose()
        {
            if(_uow != null)
                _uow.Dispose();
        }

        public void CommitChanges()
        {
            _uow.Commit();
        }
    }
}
