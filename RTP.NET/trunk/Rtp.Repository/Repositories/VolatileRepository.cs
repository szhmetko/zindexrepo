﻿using Rtp.Infrastructure;

namespace Rtp.Repository.Repositories
{
    public class VolatileRepository<T, EntityKey> : Repository<T, EntityKey> where T : IAggregateRoot
    {

        public VolatileRepository(ILineUnitOfWork uow)
            : base(uow)
        {
        }

    }
}
