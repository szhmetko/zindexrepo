﻿using Rtp.Infrastructure;
using Rtp.Infrastructure.UnitOfWork;

namespace Rtp.Repository.Repositories
{
    public class MasterRepository<T, EntityKey> : Repository<T, EntityKey> where T : IAggregateRoot
    {
        protected  MasterRepository(IUnitOfWork uow) : base(uow)
        {
        }

    }
}
