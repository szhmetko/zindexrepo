﻿using System;
using System.Collections.Generic;
using NHibernate.Criterion;
using Rtp.Model.Line;
using System.Linq;

namespace Rtp.Repository.Repositories
{
    public class PiecesRepository : VolatileRepository<Piece, int>, IPiecesRepository
    {
        public PiecesRepository(ILineUnitOfWork uow)
            : base(uow)
        {
        }

        public void RecalculateCompleted(int piecedId)
        {
            throw new NotImplementedException();
        }

        public void RecalculateCompleted(TimeSpan period)
        {
            var lastPiece = Session.CreateCriteria(typeof(Piece))
                .SetProjection(Projections.ProjectionList()
                    .Add(Projections.Max("Id"))
                )
                .List();
            
            var piecesToUpdateCriteria = Session.CreateCriteria(typeof(Piece));

            var complexCriteria = Expression.Disjunction()
                ;//.Add(Expression.Gt("DtOn", DateTime.Now - period));


            if(lastPiece != null)
            {

                long lastPieceId = Convert.ToInt64(lastPiece[0]);

                complexCriteria.Add(Expression.IdEq(lastPieceId));
                complexCriteria.Add(Expression.IdEq(lastPieceId - 1));
            }

            piecesToUpdateCriteria.Add(complexCriteria);

            foreach (var piece in piecesToUpdateCriteria.List<Piece>())
            {
                var res = Session.CreateCriteria(typeof(DataValues))
                    .Add(Expression.Eq("PieceId", piece.Id))
                    .Add(Expression.Eq("DataType", 1))
                    .SetProjection(Projections.ProjectionList()
                        .Add(Projections.Max("Uxtime"))
                        .Add(Projections.Max("Meter"))
                    )
                    .List<object[]>().FirstOrDefault();
                

                if(res != null)
                {
                    piece.Length = Convert.ToInt32(res[1]);
                    piece.DtOff = Infrastructure.Utility.DateTimeRoutines.UnixTimeStamp.UnixTimeStampToDateTime(Convert.ToInt64(res[0]));
                }

                Session.Update(piece);

            }

        }

        public IList<Piece> FindAll(string rawSql)
        {
            return Session.CreateSQLQuery(rawSql).AddEntity("pc", typeof(Piece)).List<Piece>();
        }

        public IEnumerable<Piece> Test(string nativeSql)
        {
            //select g from Group g, GroupMap m Where m.ItemID = '527' and g.ID = m.GroupID
            //var someResult = Session.CreateQuery("select p.Id, p.Length, p.DtOn, p.DtOff, '' as Description from Piece as p, PieceDescription as pd WHERE p.Id=pd.PieceId").List<Piece>();
            //var someResult = Session.CreateQuery("SELECT p FROM Piece AS p").List<Piece>();
            //var someResult = Session.CreateSQLQuery("SELECT p.Id, p.Length, p.dtOn, dtOff, '' as Description FROM Pieces p").AddEntity(typeof (Piece)).List();
            return Session.CreateSQLQuery(nativeSql).AddEntity("pc", typeof(Piece)).List<Piece>();

        }

    }
}