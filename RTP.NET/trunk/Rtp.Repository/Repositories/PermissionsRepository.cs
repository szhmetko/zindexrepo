﻿using System;
using System.Collections.Generic;
using Rtp.Infrastructure.UnitOfWork;
using Rtp.Model.Master;

namespace Rtp.Repository.Repositories
{

    internal class PermissionsRepository : MasterRepository<Permission, string>//, IPermissionsRepository
    {
        public PermissionsRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        { }

        public EntityPermissionEngine.PermissionValue GetPermissionsFor(string tableFor, string roleFor)
        {
            var permConditions = FindBy(tableFor);

            if (permConditions != null)
            {
                var result = Session.CreateSQLQuery(
                    string.Format("SELECT {0}, {1}, {2} FROM DUAL"
                                  , permConditions.CanCreateCondition, permConditions.CanUpdateCondition,
                                  permConditions.CanDeleteCondition))
                    .SetString("ROLE", "ADM")
                    .List();

                if (result != null && result.Count > 0)
                {
                    var permissions = result[0] as IList<object>;
                    if (permissions != null && permissions.Count > 2)
                        return new EntityPermissionEngine.PermissionValue
                                   {
                                       CanCreate = Convert.ToBoolean(permissions[0])
                                       ,
                                       CanUpdate = Convert.ToBoolean(permissions[1])
                                       ,
                                       CanDelete = Convert.ToBoolean(permissions[2])
                                   };

                }
            }

            return new EntityPermissionEngine.PermissionValue();
        }

    }
}
