﻿using Rtp.Model.Line;

namespace Rtp.Repository.Repositories
{
    public class SettingRepository : VolatileRepository<Setting, string>, ISettingRepository
    {
        public SettingRepository(ILineUnitOfWork uow)
            : base(uow)
        {
        }
    }
}
