﻿using System;
using System.Collections.Generic;
using NHibernate;
using Rtp.Infrastructure;
using Rtp.Infrastructure.Query;
using Rtp.Model;
using Rtp.Repository.SessionStorage;

namespace Rtp.Repository.Repositories
{
    public abstract class ReadOnlyRepositry<T, TEntityKey> : IReadOnlyRepository<T, TEntityKey> where T : IAggregateRoot
    {
        protected IConnectionInfo ConnectionInfo { get; private set; }
        protected ReadOnlyRepositry(IConnectionInfo connectionInfo)
        {
            //_session = NHibernateSessionManager.Instance.GetStatelesSessionFrom(connectionInfo);
            ConnectionInfo = connectionInfo;
        }

        public IEnumerable<T> FindAll(int index, int count)
        {
            using (var session = NHibernateSessionManager.Instance.GetStatelesSessionFrom(ConnectionInfo))
            {
                ICriteria criteriaQuery = session.CreateCriteria(typeof(T));

                return criteriaQuery.SetFetchSize(count).SetFirstResult(index).List<T>();
            }
        }

        public IEnumerable<T> FindBy(Query query)
        {
            throw new NotSupportedException("Why session is not used ?");
            using (var session = NHibernateSessionManager.Instance.GetStatelesSessionFrom(ConnectionInfo))
            {
                ICriteria nhQuery = query.TranslateIntoNHQuery<T>(null);
                return nhQuery.List<T>();
            }
        }

        public IEnumerable<T> FindBy(Query query, int index, int count)
        {
            throw new NotSupportedException("Why session is not used ?");
            using (var session = NHibernateSessionManager.Instance.GetStatelesSessionFrom(ConnectionInfo))
            {
                ICriteria nhQuery = query.TranslateIntoNHQuery<T>(null);

                return nhQuery.SetFetchSize(count).SetFirstResult(index).List<T>();
            }
        }


        public T FindBy(TEntityKey id)
        {
            using (var session = NHibernateSessionManager.Instance.GetStatelesSessionFrom(ConnectionInfo))
            {
                return session.Get<T>(id);
            }
        }

        public IList<T> FindAll()
        {
            using (var session = NHibernateSessionManager.Instance.GetStatelesSessionFrom(ConnectionInfo))
            {

                ICriteria criteriaQuery = session.CreateCriteria(typeof(T));

                return criteriaQuery.List<T>();
            }
        }

        public void Dispose()
        {
        }
    }

}
