﻿using System.Linq;
using Rtp.Infrastructure.Query;
using Rtp.Infrastructure.UnitOfWork;
using Rtp.Model.Master;

namespace Rtp.Repository.Repositories
{
    public class LinesRepository : MasterRepository<Line, int>, ILinesRepository
    {
        public LinesRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        { }

        public Line FindyByAlias(string alias)
        {
            var queryObj = new Query();
            queryObj.Add(new Criterion("Alias", alias, CriteriaOperator.Equal));

            return FindBy(queryObj).FirstOrDefault();
        }
    }
}
