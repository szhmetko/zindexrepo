﻿using Rtp.Infrastructure;
using Rtp.Model.Line;

namespace Rtp.Repository.Repositories
{
    public class ChannelsRoRepository : ReadOnlyRepositry<Channel, int>, IChannelRoRepository
    {
        public ChannelsRoRepository(IConnectionInfo connectionInfo)
            : base(connectionInfo)
        { }

    }
}
