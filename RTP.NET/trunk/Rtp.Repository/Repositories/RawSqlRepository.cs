﻿
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using NHibernate.Transform;
using Rtp.Infrastructure;
using Rtp.Model;
using Rtp.Repository.SessionStorage;

namespace Rtp.Repository.Repositories
{
    public class RawSqlRepository : IRawSqlRepository
    {
        private readonly IConnectionInfo _connectionInfo;
        public RawSqlRepository(IConnectionInfo connectionInfo)
        {
            _connectionInfo = connectionInfo;
        }

        #region Implementation of IRawSqlRepository

        public IEnumerable Query(string rawSql, IDictionary<string, object> @params)
        {
            var sess = NHibernateSessionManager.Instance.GetStatelesSessionFrom(_connectionInfo);

            var sqlQuery = sess.CreateSQLQuery(rawSql);

            foreach (var p in @params)
            {
                sqlQuery.SetParameter(p.Key, p.Value);
            }

            return sqlQuery.SetResultTransformer(Transformers.AliasToEntityMap).List();
        }

        public IEnumerable Query(string rawSql)
        {
            return Query(rawSql, new Dictionary<string, object>());
        }

        public IEnumerable<T> QueryEntities<T>(string rawSql, IDictionary<string, object> @params)
        {
            var pre = this.QueryEntities(rawSql, @params, typeof (T));

            return (from object val in pre select (T) val).ToList();
        }

        public IEnumerable QueryEntities(string rawSql, IDictionary<string, object> @params, Type entityType)
        {
            var rawEntities = Query(rawSql, @params);

            var ret = new ArrayList();

            var properties = entityType.GetProperties();

            foreach (Hashtable rawEntity in rawEntities)
            {
                var typedEntity = Activator.CreateInstance(entityType);

                foreach (var fieldName in rawEntity.Keys)
                {
                    string name = fieldName.ToString();
                    if (properties.Any(p => p.Name == name))
                    {
                        properties.First(p => p.Name == name).SetValue(typedEntity, rawEntity[fieldName], new object[] { });
                    }
                }

                ret.Add(typedEntity);
            }

            return ret;
        }

        #endregion
    }
}
