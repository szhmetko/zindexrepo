﻿using System.Collections.Generic;
using NHibernate;
using NHibernate.Transform;
using Rtp.Infrastructure.UnitOfWork;
using Rtp.Model.Master;
using System.Linq;

namespace Rtp.Repository.Repositories
{
    public class ThermoPairsRepository : MasterRepository<ThermoPair, int>, IThermoPairsRepository
    {
        public ThermoPairsRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        { }

        public override IList<ThermoPair> FindAll()
        {
            var criteriaQuery = Session.CreateCriteria(typeof(ThermoPair))
                .SetFetchMode("ApproxRules", FetchMode.Eager)
                .SetResultTransformer(new DistinctRootEntityResultTransformer());

            return criteriaQuery.List<ThermoPair>();
        }

        public IEnumerable<ThermoPair> FindAllValid()
        {
            //TODO: remake with criteria API or Linq2Nhib etc
            return FindAll().Where(tp => tp.Id != 0);
        }
    }
}
