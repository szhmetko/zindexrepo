﻿using System;
using NHibernate.Event;
using NHibernate.Persister.Entity;
using Rtp.Model;

namespace Rtp.Repository
{
    public class AuditListener : IPreUpdateEventListener, IPreInsertEventListener
    {
        public bool OnPreUpdate(PreUpdateEvent evt)
        {
            var audit = evt.Entity as IHasAuditInformation;
            if (audit == null)
                return false;

            var time = DateTime.Now;
            //var name = WindowsIdentity.GetCurrent().Name;

            Set(evt.Persister, evt.State, "dtModified", time);
            //Set(evt.Persister, evt.State, "UpdatedBy", name);

            audit.dtModified = time;
            //audit.UpdatedBy = name;

            return false;
        }

        public bool OnPreInsert(PreInsertEvent evt)
        {
            var audit = evt.Entity as IHasAuditInformation;
            if (audit == null)
                return false;


            var time = DateTime.Now;
            //var name = WindowsIdentity.GetCurrent().Name;

            Set(evt.Persister, evt.State, "dtCreated", time);
            Set(evt.Persister, evt.State, "dtModified", time);
            //Set(evt.Persister, evt.State, "CreatedBy", name);
            //Set(evt.Persister, evt.State, "UpdatedBy", name);

            audit.dtModified = time;
            //audit.CreatedBy = name;
            audit.dtCreated = time;
            //audit.UpdatedBy = name;

            return false;
        }

        private void Set(IEntityPersister persister, object[] state, string propertyName, object value)
        {
            var index = Array.IndexOf(persister.PropertyNames, propertyName);
            if (index == -1)
                return;
            state[index] = value;
        }
    }

}
