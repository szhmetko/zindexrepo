﻿using System;
using System.Data;
using NHibernate;
using Rtp.Infrastructure;
using Rtp.Infrastructure.Exceptions;
using Rtp.Infrastructure.UnitOfWork;
using Rtp.Repository.SessionStorage;

namespace Rtp.Repository
{
    public interface ILineUnitOfWork : IUnitOfWork
    {}

    public class NHUnitOfWork : IUnitOfWork, ILineUnitOfWork
    {
        /*public string SessionKey
        {
            get;
            set;
        }   */

        private ITransaction transaction;

        public NHUnitOfWork(IConnectionInfo connectionInfo)
        {
            Session = NHibernateSessionManager.Instance.GetSessionFrom(connectionInfo.ConnectionString);

            Session.FlushMode = FlushMode.Auto; //default

            transaction = Session.BeginTransaction(IsolationLevel.ReadCommitted);
        }

        internal ISession Session
        {
            get;
            private set;
        }

        void IUnitOfWork.RegisterAmended(IAggregateRoot entity, IUnitOfWorkRepository unitofWorkRepository)
        {
            Session.SaveOrUpdate(entity);
        }

        void IUnitOfWork.RegisterNew(IAggregateRoot entity, IUnitOfWorkRepository unitofWorkRepository)
        {
            Session.Save(entity);
        }

        void IUnitOfWork.RegisterRemoved(IAggregateRoot entity, IUnitOfWorkRepository unitofWorkRepository)
        {
            Session.Delete(entity);
        }

        /// <summary>
        /// Commits this instance.
        /// </summary>
        public void Commit()
        {
            //becuase flushMode is auto, this will automatically commit when disposed
            if (!transaction.IsActive)
                throw new InvalidOperationException("No active transaction");
            transaction.Commit();
            //start a new transaction
            transaction = Session.BeginTransaction(IsolationLevel.ReadCommitted);
        }

        /// <summary>
        /// Rolls back this instance. You should probably close session.
        /// </summary>
        public void Rollback()
        {
            try
            {

                if (transaction.IsActive) transaction.Rollback();
            }

            catch(Exception exception)
            {
                throw new RtpRepositoryException("Rollback in uow error", exception);
            }
        }


        public void Dispose()
        {
            try
            {
                if (Session != null)
                {
                    if (Session.IsOpen)
                        Session.Close();

                    Session.Dispose();
                    Session = null;
                }
            }
            catch (Exception exception)
            {
                throw new RtpRepositoryException("Dispose in uow error", exception);
            }
        }
    }

}
