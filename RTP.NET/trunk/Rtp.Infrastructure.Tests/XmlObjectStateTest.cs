﻿using NUnit.Framework;
using Rtp.Infrastructure.Utility;
using Rtp.Infrastructure.Utility.DatacollectorSettings;

namespace Rtp.Infrastructure.Tests
{
    [TestFixture]
    public class XmlObjectStateTest
    {

        [Test]
        public void CanSerializeDatacollectorSettings()
        {
            var settings = new UdpDatacollectorSettings {Host = "localhost", Port = 12};

            var result = settings.ToXml();

            var genSettings = XmlObjectState.XmlTo<GenericDatacollectorSettings>(result);


            var newSettings = XmlObjectState.XmlTo<UdpDatacollectorSettings>(result);


            //var opt = XmlObjectState.XmlTo<object>(result);
        }


        [Test]
        public void CanSerializeDatacollectorSettings1()
        {
            var settings = new Rs232DatacollectorSettings
                               {
                                   Baud = 1234,
                                   DataBits = 1,
                                   Com = "COM8",
                                   Parity = 1,
                                   StopBits = 1,
                                   Type = DatacollectorType.Tcp
                               };

            var result = settings.ToXml();

            var genSettings = XmlObjectState.XmlTo<GenericDatacollectorSettings>(result);

        }
        
        [Test]
        public void CanDeSerializeDatacollectorSettings()
        {
            string data = "<Datacollector type=\"Udp\" port=\"1111\" />";

            var genSettings = XmlObjectState.XmlTo<GenericDatacollectorSettings>(data);

            var newSettings = XmlObjectState.XmlTo<UdpDatacollectorSettings>(data);
        }

        [Test]
        public void CanDeSerializeIncorrectDatacollectorSettings()
        {
            string data1 = "<Datacollector type=\"Udp\" port=\"1111\" />";
            string data2 = string.Empty;

            var genSettings = XmlObjectState.XmlTo<GenericDatacollectorSettings>(data1);
            genSettings = XmlObjectState.XmlTo<GenericDatacollectorSettings>(data2);

            var newSettings = XmlObjectState.XmlTo<UdpDatacollectorSettings>(data1);
        }
    }
}
