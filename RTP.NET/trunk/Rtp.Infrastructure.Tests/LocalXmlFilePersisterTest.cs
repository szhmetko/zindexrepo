﻿using System;
using System.IO;
using NUnit.Framework;
using Rtp.Infrastructure.Utility;

namespace Rtp.Infrastructure.Tests
{
    [TestFixture]
    public class LocalXmlFilePersisterTest
    {
        [TestCase("first", 12)]
        [TestCase("second", "localhost")]
        [TestCase("type", DateTimeKind.Utc)]
        public void CanStoreValue(string key, object val)
        {

            var xmlPersister = new LocalXmlFilePersister(key + ".rtp");
            xmlPersister.AddItem(key, val);
            xmlPersister.Store();
        }

        [Test]
        public void CanRestoreValue()
        {

            var xmlPersister = new LocalXmlFilePersister("first" + ".rtp");
            xmlPersister.Restore();
        }

        [Test]
        [ExpectedException(typeof(FileNotFoundException))]
        public void TryRestoreValueFromNonExistingFile()
        {

            var xmlPersister = new LocalXmlFilePersister("dsqda.rtp");
            xmlPersister.Restore();
        }

    }
}
