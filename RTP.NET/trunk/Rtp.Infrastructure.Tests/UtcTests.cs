﻿using System;
using NUnit.Framework;

namespace Rtp.Infrastructure.Tests
{
    [TestFixture]
    public class UtcTests
    {
        [Test]
        public void CheckUtcSeconds()
        {
            var dt = Infrastructure.Utility.DateTimeRoutines.UnixTimeStamp.UtcNow;
            var an = (DateTime.Now.ToUniversalTime().Ticks - 621355968000000000)/10000000;
        }

        [Test]
        public void CheckUnixUtcToLocal()
        {
            long it = 1333713500;
            var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

            dtDateTime = dtDateTime.AddSeconds(it).ToLocalTime();

        }
    }
}
