﻿using NUnit.Framework;
using Rtp.Infrastructure.Utility;

namespace Rtp.Infrastructure.Tests
{
    [TestFixture]
    internal class LimitedQueueTest
    {
        [Test]
        public void CanAutoDequeue()
        {
            var queue = new LimitedQueue<int>(5);

            queue.Enqueue(1);
            queue.Enqueue(2);
            queue.Enqueue(3);
            queue.Enqueue(4);
            queue.Enqueue(5);
            queue.Enqueue(6);
            queue.Enqueue(7);

            Assert.IsTrue(queue.Count == 5);

            Assert.AreEqual(queue[0], 3);

        }

        [Test]
        public void CanPopWithoutDecreasingLength()
        {
            var queue = new LimitedQueue<int>(15);

            queue.Enqueue(1);
            queue.Enqueue(2);
            queue.Enqueue(3);
            queue.Enqueue(4);
            queue.Enqueue(5);
            queue.Enqueue(6);
            queue.Enqueue(7);

            var length = queue.Count;
            var b = queue.Peek();

            Assert.AreEqual(b, 7);

            Assert.AreEqual(queue.Count, length);
        }
    }
}