﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;
using NUnit.Framework;

namespace Rtp.DataCollector.Tests
{


    [TestFixture]
    public class UdpServerTest
    {

        [Test]
        public void TestMethod1()
        {
            const int listenPort = 17776;
            bool done = false;

            UdpClient listener = new UdpClient(listenPort);
            IPEndPoint groupEP = new IPEndPoint(IPAddress.Any, listenPort);

            try
            {
                while (!done)
                {
                    //Console.WriteLine("Waiting for broadcast");
                    byte[] bytes = listener.Receive(ref groupEP);

                    var b = 0;

                    //Trace.WriteLine(Encoding.Default.GetString(bytes, 0, bytes.Length));

                    StringBuilder sb = new StringBuilder();
                    foreach (var b1 in bytes)
                    {
                        Trace.Write(string.Format("{0:X2} ", b1));
                    }
                    Trace.WriteLine("");

                    /*Console.WriteLine("Received broadcast from {0} :\n {1}\n",
                        groupEP.ToString(),
                        Encoding.ASCII.GetString(bytes, 0, bytes.Length));*/
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                listener.Close();
            }
        }
    }
}
