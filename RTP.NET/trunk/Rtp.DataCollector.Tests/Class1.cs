﻿using NUnit.Framework;
using Rtp.DataCollector.CollectorStates;

namespace Rtp.DataCollector.Tests
{
    [TestFixture]
    public class DataCollectorStateTests
    {
        [Test]
        public void Test1()
        {
            var collector = new  DummyCollector();

            Assert.IsInstanceOf(typeof(StoppedNotInitializedState), collector.State);

            collector.Start();

            Assert.IsInstanceOf(typeof(RunningState), collector.State);
        }

    }
}
