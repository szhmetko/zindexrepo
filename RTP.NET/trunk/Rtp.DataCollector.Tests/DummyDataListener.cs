﻿using Rtp.DataCollector.Listeners;

namespace Rtp.DataCollector.Tests
{

    public class DummyDataListener  : IDataListener
    {
        #region Implementation of IDataListener

        private IRawDataConsumer _dataConsumer;
        public IRawDataConsumer DataConsumer
        {
            set { _dataConsumer = value; }
        }

        public void Start()
        {
        }

        public void Stop()
        {
        }

        #endregion
    }
}
