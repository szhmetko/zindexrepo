﻿using System.Configuration;
using Microsoft.Practices.ServiceLocation;
using Rtp.Infrastructure;
using Rtp.Infrastructure.UnitOfWork;
using Rtp.Model.Master;
using Rtp.Repository;
using Rtp.Repository.Repositories;
using StructureMap;
using StructureMap.Configuration.DSL;
using StructureMap.ServiceLocatorAdapter;


namespace Rtp.DataCollector.Tests
{
    public class BootStrapper
    {
        public static void Initialize()
        {
            var ioc = CreateServiceLocator();
            ServiceLocator.SetLocatorProvider(() => ioc);
        }


        private static IServiceLocator CreateServiceLocator()
        {
            var registry = new Registry();

            registry.Scan(scan =>
            {
                scan.TheCallingAssembly();
                scan.AddAllTypesOf<IUnitOfWork>();
                scan.AddAllTypesOf<IConnectionInfo>();

            });

            //For<ILinesRepository>().Use<LinesRepository>();

            registry.For<IUnitOfWork>().Use<NHUnitOfWork>().Ctor<ConnectionInfo>("connectionInfo")
                .Is((c) =>
                        {
                            string srv = ConfigurationSettings.AppSettings["MasterHost"];
                            string usr = ConfigurationSettings.AppSettings["MasterUser"];
                            string pwd = ConfigurationSettings.AppSettings["MasterPwd"];
                            string dbn = ConfigurationSettings.AppSettings["MasterDbName"];

                            var masterConnectionInfo = new ConnectionInfo(dbn, usr, srv, pwd);
                            return masterConnectionInfo;
                        }
                );

            registry.For<ILinesRepository>().Use<LinesRepository>();
            registry.For<IThermoPairsRepository>().Use<ThermoPairsRepository>();

            IContainer container = new Container(registry);

            return new StructureMapServiceLocator(container);
        }

    }
}
