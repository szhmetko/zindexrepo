﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.Practices.ServiceLocation;
using NUnit.Framework;
using Rtp.Model.Master;
using Rtp.Repository.Repositories;

namespace Rtp.DataCollector.Tests
{
    [TestFixture]
    public class DummyDataListenerTests
    {

        protected IEnumerable<byte> RawData { get; set; }
        [SetUp]
        public void Startup()
        {
            BootStrapper.Initialize();

            var byteHeap = new List<byte>();

            using (Stream stream = Assembly.GetExecutingAssembly()
                .GetManifestResourceStream("Rtp.DataCollector.Tests.SampleOutput.txt"))
            {
                using (var reader = new StreamReader(stream))
                {
                    string result = reader.ReadToEnd();

                    var chunks = result.Split(new[] {" ", "\n", "\r"}, StringSplitOptions.RemoveEmptyEntries);

                    byteHeap.AddRange(chunks.Select(chunk => (byte) Convert.ToInt32(chunk, 16)));
                }
            }

            RawData = byteHeap;

        }

        [Test]
        public void TestMethod()
        {

            var linesRepo = ServiceLocator.Current.GetInstance<ILinesRepository>();

            var oLine = linesRepo.FindyByAlias("lknv60");

            using (var thermoPairsRepository = ServiceLocator.Current.GetInstance<IThermoPairsRepository>())
            {
                oLine.ThermoPairs = thermoPairsRepository.FindAllValid();
            }            
            
            var dc = new DataCollector(oLine);

            int pos = 0;

            while (pos <= RawData.Count())
            {
                System.Threading.Thread.Sleep(300);
                dc.SendData(RawData.Skip(pos).Take(96));

                pos += 96;
            }
        }
    }
}
