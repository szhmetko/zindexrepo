﻿using Ciloci.Flee;
using NUnit.Framework;
using Rtp.Infrastructure.Utility;
using Rtp.Model.Data;
using Rtp.Model.Line;

namespace Rtp.DataCollector.Tests
{
    [TestFixture]
    internal class EvaluateExpressionTest
    {
        //[Test]
        //public void CanUseCustomIndexer()
        //{
        //    var rand = new ChannelValueEvaluationContext(new Channel
        //                                                     {
        //                                                         AggregationType = ChannelAggregationType.Avg
        //                                                         ,
        //                                                         Id = 1
        //                                                     }, new RawChannel {HiByte = 1, Num = 2, LoByte = 2});

        //    var context = new ExpressionContext(rand);

        //    IDynamicExpression e = context.CompileDynamic("Dummy[1]");
        //    var result = (int) e.Evaluate();
        //}

        [Test]
        public void CanUseDelegates()
        {
            var q = new LimitedQueue<RawPacket>(2);

            q.Enqueue(new RawPacket());
            q.Enqueue(new RawPacket());
            q.Enqueue(new RawPacket());

            var del = new FetchDelegate<SilentIndexedCollection<int, int>, int>(
                packetIndex =>
                    {
                        RawPacket packet = q.Count > packetIndex ? q[packetIndex] : null;

                        var innerDelegate = new FetchDelegate<int, int>(
                            channelIndex =>
                                {
                                    return (packet != null && packet.RawChannelsBag.ContainsKey(channelIndex))
                                               ? packet.RawChannelsBag[channelIndex].GetValue()
                                               : int.MinValue;
                                }
                            );

                        var b = new SilentIndexedCollection<int, int>(innerDelegate);

                        return b;
                    });

            var specCol = new SilentIndexedCollection<SilentIndexedCollection<int, int>, int>(del);

            int res = specCol[1][3];
        }

        //[Test]
        //public void EvaluateExpressionTest1()
        //{
        //    var rand = new ChannelValueEvaluationContext(new Channel
        //                                                     {
        //                                                         AggregationType = ChannelAggregationType.Avg
        //                                                         ,
        //                                                         Id = 1
        //                                                     }, new RawChannel {HiByte = 1, Num = 2, LoByte = 2});
        //    var context = new ExpressionContext(rand);

        //    IDynamicExpression e = context.CompileDynamic("AddNewPiece() + RAW[0][0] + RAW[0][1] + RAW[1][0]");
        //    var result = (int) e.Evaluate();
        //}

        [Test]
        public void EvaluateExpressionTest1()
        {
            var evaluationEnginecontext = new ExpressionContext();
            evaluationEnginecontext.Imports.AddType(typeof(System.Math));


            IDynamicExpression e = evaluationEnginecontext.CompileDynamic("1000,0 * cos(123456789 / 20,0)");


            var result = e.Evaluate();

            var anotherResult = 1000.0f * System.Math.Cos(123456789/20.0);
        }
    }
}