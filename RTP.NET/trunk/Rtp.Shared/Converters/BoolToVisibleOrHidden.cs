﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Rtp.Shared.Converters
{
    public class BoolToVisibleOrHidden : IValueConverter
    {
        #region Constructors

        #endregion

        #region Properties

        public bool Collapse { get; set; }

        #endregion

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var bValue = (bool) value;
            if (bValue)
            {
                return Visibility.Visible;
            }
            if (Collapse)
                return Visibility.Collapsed;

            return Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var visibility = (Visibility) value;

            if (visibility == Visibility.Visible)
                return true;

            return false;
        }

        #endregion
    }
}