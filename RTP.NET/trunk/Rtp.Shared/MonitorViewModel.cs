﻿using System;
using System.Windows.Threading;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.ServiceLocation;
using Rtp.Model.Line;

namespace Rtp.Shared
{
    public class MonitorInteval
    {
        public int RefreshInterval { get; set; }
        public int HistoryInterval { get; set; }
        public string DisplayText { get; set; }

        public static MonitorInteval Default
        {
            get { 
                return new MonitorInteval
                             {
                                 RefreshInterval = 20,
                                 HistoryInterval = 20,
                                 DisplayText = string.Empty
                             }; }
        }
    }

    public class MonitorViewModel : ModuleViewModel
    {
        public override void Cleanup()
        {
            // Clean own resources if needed
            StopMonitor();
            _timer.Tick -= InvalidateSampleData;


            base.Cleanup();
        }

        private void InvalidateSampleData(object state, EventArgs e)
        {
            InvokeCommand.Execute(null);
        }

        readonly DispatcherTimer _timer = new DispatcherTimer();

        public MonitorViewModel()
        {
            InvokeCommand = new RelayCommand(FetchData);

            StartMonitorCommand = new RelayCommand(StartMonitor);
            StopMonitorCommand = new RelayCommand(StopMonitor);

            _timer.Tick += InvalidateSampleData;
        }

        private void StartMonitor()
        {
            _timer.Interval = new TimeSpan(0, 0, 2);
            _timer.Start();
        }

        private void StopMonitor()
        {
            _timer.Stop();
        }


        private MonitorInteval _selectedInterval;
        public MonitorInteval SelectedInterval
        {
            get { return _selectedInterval; }
            set { 
                _selectedInterval = value;
                RaisePropertyChanged(nameof(SelectedInterval));

            }
        }


        protected void FetchData()
        {
            try
            {
                if(SelectedInterval != null)
                {
                    //var mins = 20;// string.IsNullOrEmpty(IntervalLength) ? 20 : Int32.Parse(IntervalLength);

                    _timer.Stop();
                    _timer.Interval = new TimeSpan(0, 0, SelectedInterval.RefreshInterval);
                    using (var queryRepo = ServiceLocator.Current.GetInstance<IQueriesRepository>())
                    {

                        using (var repoData = ServiceLocator.Current.GetInstance<IDataValuesRepository>())
                        {

                            var query = queryRepo.FindBy(Model.QueryName);

                            var res = repoData.FetchMonitorData(SelectedInterval.HistoryInterval, DesiredPoints, query.QueryText);

                            //RaisePropertyChanged("MonitorData", null, res, true);
                            foreach (var chartViewModel in ChartModels)
                            {
                                chartViewModel.BulkChartsData = res;
                            }

                            RaisePropertyChanged("MonitorData"/*, null, res, true*/);
                        }
                    }
                }
                


            }
            catch (Exception exception)
            {

                MessageService.ShowError(exception);

            }

            _timer.Start();
        }


        public RelayCommand InvokeCommand { get; private set; }
        public RelayCommand StartMonitorCommand { get; private set; }
        public RelayCommand StopMonitorCommand { get; private set; }
    }
}