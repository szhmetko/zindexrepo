﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Rtp.Model.Line;
using Rtp.Shared.Chart;
using Visiblox.Charts;

namespace Rtp.Shared
{
    public interface IChartViewModel : INotifyPropertyChanged
    {
        ObservableCollection<ChartCurveViewModel> CurveModels { get; }
        
        ModuleChart Model { get; set; }
        BehaviourManager BehaviourManager { get; }
        IComparable CalculateXValue(long rawValue);
        IEnumerable BulkChartsData { set; get; }

        RangeViewModel<double> PrimaryYAxisRange { get; }

        RangeViewModel<double> SecondaryYAxisRange { get; }

    }
}