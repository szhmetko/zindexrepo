﻿using Visiblox.Charts;

namespace Rtp.Shared
{
    public class ArchiveChartViewModel : TimeHistoryChartViewModel
    {
        public override BehaviourManager BehaviourManager
        {
            get
            {
                var behaviourManager = new BehaviourManager { IsEnabled = true, AllowMultipleEnabled = true};

                behaviourManager.Behaviours.Add(new PanBehaviour{IsEnabled = true, XPanEnabled = true});
                behaviourManager.Behaviours.Add(new ZoomBehaviour { IsEnabled = true });

                return behaviourManager;

            }
        }

    }
}