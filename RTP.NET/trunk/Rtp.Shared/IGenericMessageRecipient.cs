﻿
using System;
using System.Collections;
using GalaSoft.MvvmLight;

namespace Rtp.Shared
{
    public interface IGenericMessageRecipient
    {
        int ParentId { set; }
    }


    public interface IValuesTable : IGenericMessageRecipient
    {
    }

    public interface IRtpReport : IPieceChildViewModel
    {
        //Hashtable InputParameters { set; }
    }
}
