﻿using System;

namespace Rtp.Shared.Chart
{
    public class RangeViewModel<T>
    {
        public T Start { get; set; }
        public T End { get; set; }
    }
}
