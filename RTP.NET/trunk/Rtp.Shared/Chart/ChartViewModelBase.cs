﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using GalaSoft.MvvmLight.Messaging;
using Rtp.Infrastructure.Utility.DateTimeRoutines;
using Rtp.Model.Line;
using Rtp.Shared.Chart;
using Visiblox.Charts;

namespace Rtp.Shared
{
    

    public abstract class ChartViewModelBase<TXValue> : GenericEntityViewModel<ModuleChart>, IChartViewModel where TXValue : IComparable
    {
        private ObservableCollection<ChartCurveViewModel> _curveModels;

        protected ChartViewModelBase(): base()
        {
            
            Messenger.Default.Register<PropertyChangedMessage<bool>>(
                this,
                message =>
                    {
                        LegendVisibility = message.NewValue ? Visibility.Visible : Visibility.Hidden;
                    });
        }

        private Visibility _legendVisibility = Visibility.Hidden;
        public Visibility LegendVisibility
        {
            get
            {
                return _legendVisibility;
            }
            private set
            {
                _legendVisibility = value;
                RaisePropertyChanged(nameof(LegendVisibility));
            }
        }

        public ObservableCollection<ChartCurveViewModel> CurveModels
        {
            get
            {
                if (_curveModels == null)
                    _curveModels = new ObservableCollection<ChartCurveViewModel>
                        (from curve in Model.Curves select new ChartCurveViewModel(curve));

                return _curveModels;
            }
        }

        virtual public BehaviourManager BehaviourManager
        {
            get { return new BehaviourManager(); }
        }

        public IComparable CalculateXValue(long rawValue)
        {
            var type = typeof(TXValue);

            switch (type.Name)
            {
                case "DateTime":
                    return UnixTimeStamp.UnixTimeStampToDateTime(rawValue);
                default:
                    return rawValue;

            }
        }

        private IEnumerable _bulkChartsData;

        public IEnumerable BulkChartsData
        {
            get { return _bulkChartsData; }
            set
            {
                _bulkChartsData = value;

                RaisePropertyChanged(nameof(BulkChartsData));
            }
        }

        private RangeViewModel<double> CalculateAxisRange(string alias, AxisInfo info)
        {
            switch (alias.ToLower()) {
                case "diameter":

                    {
                        double start = Model.PrimaryYAxis.LowLimit;
                        double end = Model.PrimaryYAxis.HighLimit;

                        var array = (ArrayList)_bulkChartsData;

                        if (array != null && array.Count > 0)
                        {
                            Hashtable ht = array[0] as Hashtable;

                            if (ht != null && Model.Curves.Any())
                            {
                                var channel = Model.Curves.First().Channel;

                                if (channel != null)
                                {
                                    double val = channel.CalculateValue(Convert.ToDouble(ht[channel.FieldName]));

                                    start = val - Model.PrimaryYAxis.LowLimit;
                                    end = val + Model.PrimaryYAxis.HighLimit;
                                }

                            }

                        }

                        return new RangeViewModel<double> { Start = start, End = end };
                    }

                default:
                    return new RangeViewModel<double> { Start = Model.PrimaryYAxis.LowLimit, End = Model.PrimaryYAxis.HighLimit };
            }
        }

        virtual public RangeViewModel<double> PrimaryYAxisRange 
        { 
            get
            {
                return CalculateAxisRange(Model.Alias, Model.PrimaryYAxis);
            } 
        }

        virtual public RangeViewModel<double> SecondaryYAxisRange 
        {
            get
            {
                return CalculateAxisRange(Model.Alias, Model.SecondaryYAxis);
            }

        }
    }
}