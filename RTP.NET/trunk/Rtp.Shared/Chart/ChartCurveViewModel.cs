﻿using Rtp.Model.Line;

namespace Rtp.Shared
{
    public class ChartCurveViewModel : GenericEntityViewModel<ChartCurve>
    {
        public ChartCurveViewModel(ChartCurve model) : base(model)
        {
        }
    }
}