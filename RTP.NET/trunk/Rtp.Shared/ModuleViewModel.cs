﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Microsoft.Practices.ServiceLocation;
using Rtp.Model.Line;

namespace Rtp.Shared
{
    public abstract class ModuleViewModel : GenericEntityViewModel<Module>
    {
        private int _desiredPoints = 1000;

        protected ModuleViewModel()
        {
            MessageService = ServiceLocator.Current.GetInstance<IMessageService>();
        }

        protected IMessageService MessageService { private set; get; }


        public ObservableCollection<IChartViewModel> ChartModels { get; set; }

        public ContentControl TabContent { internal set; get; }

        public int DesiredPoints
        {
            get
            {
                if (_desiredPoints == 1000)
                {
                    _desiredPoints = (int)SystemParameters.PrimaryScreenWidth;
                }
                return _desiredPoints;
            }
        }
    }
}