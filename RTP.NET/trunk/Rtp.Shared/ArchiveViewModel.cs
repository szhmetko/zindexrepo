﻿using System;
using System.Collections.Generic;
using System.Windows;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.ServiceLocation;
using Rtp.Model.Line;
using Rtp.Infrastructure.Utility.DateTimeRoutines;

namespace Rtp.Shared
{
    public class ArchiveViewModel : ModuleViewModel
    {
        private DateTime? _beginDate;
        public DateTime? BeginDate
        {
            get { return _beginDate; }
            set
            {
                _beginDate = value;
                RaisePropertyChanged("BeginDate");
            }
        }

        private DateTime? _endDate;
        public DateTime? EndDate
        {
            get { return _endDate; }
            set
            {
                _endDate = value;
                RaisePropertyChanged("EndDate");
            }
        }

        public int BeginHour
        {
            get { return BeginDate.HasValue ? BeginDate.Value.Hour : 0; }
            set
            {
                if (BeginDate.HasValue)
                {
                    var curVal = BeginDate.Value;
                    BeginDate = new DateTime(curVal.Year, curVal.Month, curVal.Day, value , 0, 0);
                }

                RaisePropertyChanged("BeginHour");
            }
        }

        public int EndHour
        {
            get {  return EndDate.HasValue ? EndDate.Value.Hour : 0; }
            set
            {
                if (EndDate.HasValue)
                {
                    var curVal = EndDate.Value;
                    EndDate = new DateTime(curVal.Year, curVal.Month, curVal.Day, value, 0, 0);
                }

                RaisePropertyChanged("EndHour");
            }
        }

        public ArchiveViewModel()
        {
            BeginDate = DateTime.Now.Date;
            EndDate = DateTime.Now;

            InvokeCommand = new RelayCommand(() =>
                                                 {

                                                     if (BeginDate.HasValue && EndDate.HasValue
                                                         && EndDate > BeginDate)
                                                     {
                                                         var queryRepo = ServiceLocator.Current.GetInstance<IQueriesRepository>();

                                                         var repoData = ServiceLocator.Current.GetInstance<IDataValuesRepository>();
                                                         var uxTimeBegin = BeginDate.Value.ToUniversalTime().ToUnixTimeStamp();
                                                         var uxTimeEnd = EndDate.Value.ToUniversalTime().ToUnixTimeStamp();

                                                         var query = queryRepo.FindBy(Model.QueryName);

                                                         var res = repoData.FetchArchiveData(uxTimeBegin, uxTimeEnd, DesiredPoints, query.QueryText);

                                                         RaisePropertyChanged("ArchiveData"/*, null, res, true*/);

                                                         foreach (var chartViewModel in ChartModels)
                                                         {
                                                             chartViewModel.BulkChartsData = res;
                                                         }

                                                     }

                                                 });
        }

        public RelayCommand InvokeCommand { get; private set; }

        public Dictionary<int, string> Hours
        {
            get
            {
                var data = new Dictionary<int, string>();

                for (int i = 0; i < 24; i++)
                {
                    data.Add(i, string.Format("{0:d2}:00", i));
                }

                return data;
            }
        }




    }
}