﻿using System;
using System.Collections.Generic;
using GalaSoft.MvvmLight;

namespace Rtp.Shared
{
    public class DateTimePickerViewModel : ViewModelBase
    {

        public Dictionary<int, string> Hours
        {
            get
            {
                var data = new Dictionary<int, string>();

                for (int i = 0; i < 24; i++)
                {
                    data.Add(i, string.Format("{0:d2}:00", i));
                }

                return data;
            }
        }

        private DateTime? _selectedDate;
        public DateTime? SelectedDate
        {
            get { return _selectedDate; }
            set
            {
                _selectedDate = value;

                SelectedDateTime = SelectedDate.HasValue ? (SelectedDate.Value + TimeSpan.FromHours(SelectedHour)) : SelectedDate;
            }
        }

        private int _selectedHour;
        public int SelectedHour
        {
            get { return _selectedHour; }
            set
            {
                _selectedHour = value;

                SelectedDateTime = SelectedDate.HasValue ? (SelectedDate.Value + TimeSpan.FromHours(SelectedHour)) : SelectedDate;
            }
        }

        private DateTime? _selectedDateTime;
        private DateTime? SelectedDateTime
        {
            set
            {
                var oldValue = _selectedDateTime;
                _selectedDateTime = value;

                RaisePropertyChanged("SelectedDateTime", oldValue, _selectedDateTime, true);
                MessengerInstance.Send(oldValue);
            }
        }

        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
    }
}
