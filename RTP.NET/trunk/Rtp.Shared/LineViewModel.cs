﻿using Rtp.Model.Master;

namespace Rtp.Shared
{
    public class LineViewModel : GenericEntityViewModel<Line>
    {
        public LineViewModel(Line model)
            : base(model)
        {
        }
    }
}
