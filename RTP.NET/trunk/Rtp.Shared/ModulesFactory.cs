﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Windows.Controls;
using Microsoft.Practices.ServiceLocation;

namespace Rtp.Shared
{
    public class ModulesFactory
    {

        static ObservableCollection<IChartViewModel> CreateModuleChartViewModels(ModuleViewModel moduleModel)
        {
            return new ObservableCollection<IChartViewModel>(
                from chart in moduleModel.Model.Charts select CreateSingleChartViewModel(chart));
        }

        static IChartViewModel CreateSingleChartViewModel(Model.Line.ModuleChart moduleChart)
        {
            var moduleType = Type.GetType(string.Format("Rtp.Shared.{0}ChartViewModel", moduleChart.Module.Alias), false, true);

            var viewModel = ServiceLocator.Current.GetInstance(moduleType) as IChartViewModel;

            if(viewModel!= null)
                viewModel.Model = moduleChart;
            else
                throw new InvalidOperationException("Cannot create chartview model  " + moduleChart.Alias);


            return viewModel;

        }

        public static ModuleViewModel CreateModuleViewModel(Model.Line.Module module)
        {
            try
            {
                var moduleType = Type.GetType(string.Format("Rtp.Shared.{0}ViewModel", module.Alias), false, true);

                var viewModel = ServiceLocator.Current.GetInstance(moduleType) as ModuleViewModel;

                if (viewModel != null)
                {
                    viewModel.Model = module;

                    viewModel.ChartModels = CreateModuleChartViewModels(viewModel);
                }

                return viewModel;
            }
            catch(Exception exception)
            {
                throw new InvalidOperationException("Cannot create module " + module.Alias, exception);
            }

        }


    }
}
