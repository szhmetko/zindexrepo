﻿using System;

namespace Rtp.Shared
{
    public class TimeHistoryChartViewModel : ChartViewModelBase<DateTime>
    {
    }

    public class MonitorChartViewModel : TimeHistoryChartViewModel
    {
    }

    public class PiecesChartViewModel : ChartViewModelBase<long>
    {
    }

}