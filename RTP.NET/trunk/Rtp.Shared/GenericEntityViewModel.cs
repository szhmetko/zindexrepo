﻿using GalaSoft.MvvmLight;
using Rtp.Infrastructure;

namespace Rtp.Shared
{
    public class GenericEntityViewModel<TEntity> : ViewModelBase where TEntity : IAggregateRoot
    {


        ////public override void Cleanup()
        ////{
        ////    // Clean own resources if needed

        ////    base.Cleanup();
        ////}

        public TEntity Model
        {
            get;
            set;
        }

        public GenericEntityViewModel(TEntity model)
        {
            Model = model;
        }

        public GenericEntityViewModel()
        {
        }
    }
}
