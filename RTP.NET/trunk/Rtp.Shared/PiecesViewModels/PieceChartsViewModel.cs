using System;
using System.Collections;
using Microsoft.Practices.ServiceLocation;
using Rtp.Model.Line;

namespace Rtp.Shared
{
    public class PieceChartsViewModel : PieceChildViewModel
    {
        protected override PiecesScreenViewMode VisibleMode
        {
            get { return PiecesScreenViewMode.Charts; }
        }

        private readonly IMessageService _messageService;
        public PieceChartsViewModel(IMessageService messageService)
        {
            _messageService = messageService;

        }

        private string Query { get; set; }


        protected override void Populate()
        {
            try
            {

                if (string.IsNullOrEmpty(Query))
                {
                    using (var queryRepo = ServiceLocator.Current.GetInstance<IQueriesRepository>())
                    {
                        Query = queryRepo.FindBy(ParentModule.Model.QueryName).QueryText;
                    }
                }
                using (var repoData = ServiceLocator.Current.GetInstance<IDataValuesRepository>())
                {
                    IEnumerable res = repoData.FetchPieceData(SelectedPiece.Model.Id, ParentModule.DesiredPoints, Query);

                    foreach (var chartViewModel in ParentModule.ChartModels)
                    {
                        chartViewModel.BulkChartsData = res;
                    }

                    RaisePropertyChanged("PieceData"/*, null, res, true*/);
                }
            }
            catch (Exception exception)
            {
                _messageService.ShowError(exception);
            }
        }

    }
}