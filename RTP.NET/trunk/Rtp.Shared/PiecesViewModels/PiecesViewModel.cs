﻿using System.Collections.Generic;
using GalaSoft.MvvmLight.Command;

namespace Rtp.Shared
{
    public class PiecesViewModel : ModuleViewModel
    {
        private PiecesScreenViewMode _viewMode;

        //public RelayCommand LoadedCommand
        //{
        //    get { return _loadedCommand ?? (_loadedCommand = new RelayCommand(Initialize)); }
        //}

        private RelayCommand _loadPiecesCommand;
        public RelayCommand LoadPiecesCommand
        {
            get
            {
                return _loadPiecesCommand ?? (_loadPiecesCommand = new RelayCommand(() => _masterVm.Reload()));
            }
        }

        //private string QueryList { get; set; }
        //private string Query { get; set; }

        //public IEnumerable<PieceItemViewModel> Pieces
        //{
        //    private set
        //    {
        //        _pieces = value;
        //        RaisePropertyChanged("Pieces");
        //    }
        //    get { return _pieces; }
        //}

        private PieceItemViewModel _selectedPiece;
        public PieceItemViewModel SelectedPiece
        {
            get { return _selectedPiece; }
            set
            {
                var oldValue = _selectedPiece;
                _selectedPiece = value;
                RaisePropertyChanged("SelectedPiece", oldValue, value, true);

                if (_selectedPiece != null)
                    foreach (var pieceChildViewModel in ChilViews)
                    {
                        pieceChildViewModel.SelectedPiece = value;
                    }
            }
        }


        protected readonly IEnumerable<IPieceChildViewModel> ChilViews;

        private readonly PieceMasterView _masterVm;
        private readonly IRtpReport _reportVm;
        private readonly PieceValuesTableViewModel _tableVm;
        private readonly PieceChartsViewModel _chartVm;

        public PiecesViewModel(PieceMasterView masterVm, IRtpReport reportVm
            , PieceValuesTableViewModel tableVm, PieceChartsViewModel chartVm)
        {
            _masterVm = masterVm;
            _reportVm = reportVm;
            _tableVm = tableVm;
            _chartVm = chartVm;


            ChilViews = new List<IPieceChildViewModel>{reportVm, tableVm, chartVm};

            foreach (var pieceChildViewModel in ChilViews)
            {
                pieceChildViewModel.ParentModule = this;
            }

            _masterVm.ParentModule = this;
        }

        public bool IsChartViewMode
        {
            get { return ViewMode == PiecesScreenViewMode.Charts; }
            set
            {
                if (value)
                    ViewMode = PiecesScreenViewMode.Charts;
            }
        }

        public bool IsTableViewMode
        {
            get { return ViewMode == PiecesScreenViewMode.ValuesTable; }
            set
            {
                if (value)
                    ViewMode = PiecesScreenViewMode.ValuesTable;
            }
        }

        public bool IsReportViewMode
        {
            get { return ViewMode == PiecesScreenViewMode.Report; }
            set
            {
                if (value)
                    ViewMode = PiecesScreenViewMode.Report;
            }
        }

        public PiecesScreenViewMode ViewMode
        {
            get { return _viewMode; }
            set
            {
                _viewMode = value;

                foreach (var pieceChildViewModel in ChilViews)
                {
                    pieceChildViewModel.CurrentMode = value;
                }

                RaisePropertyChanged("ViewMode");
                RaisePropertyChanged("IsChartViewMode");
                RaisePropertyChanged("IsTableViewMode");
                RaisePropertyChanged("IsReportViewMode");
            }
        }

        public PieceMasterView MasterVm
        {
            get { return _masterVm; }
        }

        public IRtpReport ReportVm
        {
            get { return _reportVm; }
        }

        public PieceValuesTableViewModel TableVm
        {
            get { return _tableVm; }
        }

        public PieceChartsViewModel ChartVm
        {
            get { return _chartVm; }
        }



        public override void Cleanup()
        {
            foreach (var pieceChildViewModel in ChilViews)
            {
                pieceChildViewModel.Cleanup();
            }

            _masterVm.Cleanup();

            base.Cleanup();

        }


        //protected void FetchData()
        //{
        //    try
        //    {
        //        using (var repoData = ServiceLocator.Current.GetInstance<IDataValuesRepository>())
        //        {
        //            IEnumerable res = repoData.FetchPieceData(SelectedPiece.Model.Id, DesiredPoints, Query);

        //            foreach (var chartViewModel in ChartModels)
        //            {
        //                chartViewModel.BulkChartsData = res;
        //            }

        //            RaisePropertyChanged("PieceData"/*, null, res, true*/);
        //        }
        //    }
        //    catch (Exception exception)
        //    {
        //        MessageService.ShowError(exception);
        //    }
        //}

        //private void Initialize()
        //{
        //    try
        //    {

        //        if (string.IsNullOrEmpty(QueryList))
        //        {
        //            using (var queryRepo = ServiceLocator.Current.GetInstance<IQueriesRepository>())
        //            {
        //                QueryList = queryRepo.FindBy("PIECES_LIST").QueryText;
        //                Query = queryRepo.FindBy(Model.QueryName).QueryText;
        //            }
        //        }


        //        if (Pieces == null)
        //        {
        //            using (var piecesRepository = ServiceLocator.Current.GetInstance<IPiecesRepository>())
        //            {
        //                Pieces = (from Piece p in piecesRepository.FindAll(QueryList)
        //                          select new PieceItemViewModel {Model = p}).ToList();

        //                SelectedPiece = Pieces.LastOrDefault();
        //            }

        //            ViewMode = PiecesScreenViewMode.Charts;

        //        }
        //    }
        //    catch (Exception exception)
        //    {
        //        MessageService.ShowError(exception);
        //    }
        //}
    }
}