using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Microsoft.Practices.ServiceLocation;
using Rtp.Model;
using Rtp.Model.Line;
using System.Linq;
using System.Windows;

namespace Rtp.Shared
{

    public class PieceValuesTableViewModel : PieceChildViewModel
    {
        private const string VisibleColumnsKey = "PiecesGrid.VisibleColumns";

        protected override PiecesScreenViewMode VisibleMode
        {
            get { return PiecesScreenViewMode.ValuesTable; }
        }

        private Query  Query { get; set; }
        public List<string> VisibleChannels { get; private set; }

        private IEnumerable<Channel> _channels;
        public IEnumerable<Channel> Channels
        {
            get
            {
                if(_channels == null)
                {
                    try
                    {
                        using (var repo = ServiceLocator.Current.GetInstance<IChannelRoRepository>())
                        {
                            _channels = repo.FindAll().ToList();
                        }

                    }
                    catch (Exception exception)
                    {
                        _messageService.ShowError(exception);
                    }                 
                }
                
                return _channels;
            }
        }

        public void SaveVisibleColumns(List<string> columns)
        {
            using (var repoSettings = ServiceLocator.Current.GetInstance<ISettingRepository>())
            {
                var value = repoSettings.FindAll()
                    .FirstOrDefault(x => x.Name == VisibleColumnsKey) ?? new Setting();

                value.Name = VisibleColumnsKey;
                value.Value = String.Join(",", columns);

                repoSettings.Save(value);

                // TODO: check if this call can be included inside of Save method ?
                repoSettings.CommitChanges();
            }
        }


        protected override void Populate()
        {
            try
            {
                const string queryKey = "PieceValuesTable";
                if (Query == null)
                {
                    using (var queryRepo = ServiceLocator.Current.GetInstance<IQueriesRepository>())
                    {
                        Query = queryRepo.FindBy(queryKey);
                    }
                }

                if (VisibleChannels == null) 
                {
                    using (var repoSettings = ServiceLocator.Current.GetInstance<ISettingRepository>())
                    {
                        var strValue = repoSettings.FindAll()
                            .FirstOrDefault(x => x.Name == VisibleColumnsKey)?.Value ?? string.Empty;

                        VisibleChannels = strValue.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                            .ToList();
                    }
                }



                if (Query != null)
                {

                    var res = _sqlRepository.Query(Query.QueryText,
                                                   new Dictionary<string, object>
                                                       {
                                                           {
                                                               Query.ParameterNames.FirstOrDefault(),
                                                               SelectedPiece.Model.Id
                                                           }
                                                       });
                    Values = GenerateDataTable(res);
                }
                else
                    throw new ApplicationException("Cannot find query: " + queryKey);

            }
            catch (Exception exception)
            {
                _messageService.ShowError(exception);
            }
        }

        private DataTable GenerateDataTable(IEnumerable rawResult)
        {
            var table = new DataTable();

            int rowNumber = 0;

            foreach (Hashtable packet in rawResult)
            {
                if(rowNumber++ == 0)
                {
                    foreach (var key in packet.Keys)
                    {
                        table.Columns.Add(key.ToString());
                    }
                }

                DataRow row = table.NewRow();

                foreach (var key in packet.Keys)
                {
                    var appropriatedChannel = Channels.FirstOrDefault(channel => channel.FieldName == key.ToString());

                    if (appropriatedChannel != null)
                        row[key.ToString()] = appropriatedChannel.CalculateValue(Convert.ToDouble(packet[key])).ToString("F2");
                    else
                        row[key.ToString()] = packet[key];// != null ? packet[key].ToString() : string.Empty;
                }

                table.Rows.Add(row);
            }

            return table;

        }


        private readonly IMessageService _messageService;
        private readonly IRawSqlRepository _sqlRepository;

        /// <summary>
        /// Initializes a new instance of the PieceValuesTableViewModel class.
        /// </summary>
        public PieceValuesTableViewModel(IMessageService messageService, IRawSqlRepository sqlRepository)
        {
            _messageService = messageService;
            _sqlRepository = sqlRepository;
        }

        public int ParentId { set; protected get; }

        private DataTable _values;
        public DataTable Values
        {
            get { return _values; }
            private set
            {
                _values = value;
                RaisePropertyChanged("Values");
            }
        }

        public void ShowMessage(string messageText)
        {
            _messageService.ShowMessage(messageText, string.Empty, MessageBoxButton.OK,
                   MessageBoxImage.Exclamation);            
        }

    }
}