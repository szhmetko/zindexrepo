﻿using GalaSoft.MvvmLight;

namespace Rtp.Shared
{
    public abstract class PieceChildViewModel : ViewModelBase, IPieceChildViewModel
    {
        private bool _isPopulated;
        public bool IsPopulated
        {
            get { return _isPopulated; }
            set
            {
                _isPopulated = value;

                RaisePropertyChanged("IsPopulated");
            }
        }

        protected abstract PiecesScreenViewMode VisibleMode { get; }
        
        public PiecesScreenViewMode CurrentMode
        {
            set { Visible = (value == VisibleMode); }
        }

        protected abstract void Populate();

        protected virtual void OnBecomeVisible()
        {
            if(SelectedPiece != null && !IsPopulated)
            {
                Populate();
                IsPopulated = true;
            }
        }

        protected void OnBecomeInvisible()
        {
        }

        private bool _visible;
        public bool Visible
        {
            get { return _visible; }
            set
            {
                var oldValue = _visible;
                _visible = value;

                if (oldValue != value)
                {
                    if (value)
                        OnBecomeVisible();
                    else
                    {
                        OnBecomeInvisible();
                    }
                }

                RaisePropertyChanged(nameof(Visible));
            }
        }

        private PieceItemViewModel _selectedPiece;
        public PieceItemViewModel SelectedPiece
        {
            get { return _selectedPiece; }
            set
            {
                var oldValue = _selectedPiece;
                _selectedPiece = value;

                if(oldValue != value)
                {
                    IsPopulated = false;

                    if(Visible)
                        OnBecomeVisible();
                }

                RaisePropertyChanged("SelectedPiece");

            }
        }

        public PiecesViewModel ParentModule
        {
            get; set;
        }
    }
}
