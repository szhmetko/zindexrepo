﻿namespace Rtp.Shared
{
    public enum PiecesScreenViewMode
    {
        Charts,
        ValuesTable,
        Report
    }
}