using GalaSoft.MvvmLight;

namespace Rtp.Shared
{
    public interface IPieceChildViewModel:  ICleanup
    {
        PieceItemViewModel SelectedPiece { get; set; }
        PiecesViewModel ParentModule { get; set; }
        bool IsPopulated { get; set; }

        PiecesScreenViewMode CurrentMode { set; }
    }
}