﻿
using System;
using System.Collections.Generic;
using System.Linq;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.ServiceLocation;
using Rtp.Model.Line;

namespace Rtp.Shared
{
    public class PieceMasterView : ViewModelBase, IPieceChildViewModel
    {
        private PieceItemViewModel _selectedPiece;
        public PieceItemViewModel SelectedPiece
        {
            get { return _selectedPiece; }
            set { 
                _selectedPiece = value;

                ParentModule.SelectedPiece = value;

                RaisePropertyChanged("SelectedPiece");

            }
        }

        public PiecesViewModel ParentModule
        {
            get; set;
        }

        public bool IsPopulated
        {
            get; set;
        }

        public PiecesScreenViewMode CurrentMode
        {
            set {}
        }

        private IEnumerable<PieceItemViewModel> _pieces;
        public IEnumerable<PieceItemViewModel> Pieces
        {
            private set
            {
                _pieces = value;
                RaisePropertyChanged("Pieces");
            }
            get { return _pieces; }
        }

        private readonly IMessageService _messageService;
        public PieceMasterView(IMessageService messageService)
        {
            _messageService = messageService;
        }

        private RelayCommand _loadedCommand;
        public RelayCommand LoadedCommand
        {
            get { return _loadedCommand ?? (_loadedCommand = new RelayCommand(OnViewLoaded)); }
        }

        private string QueryList { get; set; }

        public void Reload()
        {
            IsPopulated = false;
            Pieces = null;
            OnViewLoaded();
        }

        private void OnViewLoaded()
        {
            if(!IsPopulated)
            {
                try
                {

                    if (string.IsNullOrEmpty(QueryList))
                    {
                        using (var queryRepo = ServiceLocator.Current.GetInstance<IQueriesRepository>())
                        {
                            QueryList = queryRepo.FindBy("PIECES_LIST").QueryText;
                        }
                    }


                    if (Pieces == null)
                    {
                        using (var piecesRepository = ServiceLocator.Current.GetInstance<IPiecesRepository>())
                        {
                            Pieces = (from Piece p in piecesRepository.FindAll(QueryList)
                                      select new PieceItemViewModel { Model = p }).ToList();

                            SelectedPiece = Pieces.LastOrDefault();
                        }

                        ParentModule.ViewMode = PiecesScreenViewMode.Charts;

                    }

                }
                catch (Exception exception)
                {
                    _messageService.ShowError(exception);
                }

                IsPopulated = true;
            }
        }
    }
}
