﻿using System;
using System.Windows;

namespace Rtp.Shared
{
    public interface IMessageService
    {
        MessageBoxResult ShowMessage(string text, string title, MessageBoxButton buttons, MessageBoxImage images);
        MessageBoxResult ShowError(Exception exception);
        MessageBoxResult ShowApplicationInfo();
    }

    public class MessageService : IMessageService
    {
        #region IMessageService Members

        public MessageBoxResult ShowError(Exception exception)
        {
            return ShowMessage(exception.Message + Environment.NewLine
                               + GetMoreInnerException(exception).Message, string.Empty, MessageBoxButton.OK,
                               MessageBoxImage.Exclamation);
        }

        virtual public MessageBoxResult ShowApplicationInfo()
        {
            throw new NotImplementedException();
        }

        public MessageBoxResult ShowMessage(string text, string title, MessageBoxButton buttons, MessageBoxImage images)
        {
            return MessageBox.Show(text, title, buttons, images);
        }

        #endregion

        private static Exception GetMoreInnerException(Exception exception)
        {
            while (exception.InnerException != null)
                exception = exception.InnerException;

            return exception;
        }
    }
}