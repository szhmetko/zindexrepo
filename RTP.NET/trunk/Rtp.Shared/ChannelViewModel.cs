﻿using Rtp.Model.Line;
using Rtp.Model.Master;

namespace Rtp.Shared
{
    public class ChannelViewModel : GenericEntityViewModel<Channel>
    {
        public ChannelViewModel(Channel model) : base(model)
        {}

        private ThermoPair _thermoPair;
        public ThermoPair ThermoPair
        {
            get { return _thermoPair; }
            set { _thermoPair = value;
            if (value != null)
                Model.ThermoPairId = value.Id;
            }
        }

        public string IdText
        {
            get { return string.Format("Id: {0}", Model.Id); }
        }
    }
}
