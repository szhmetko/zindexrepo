﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace Rtp.Controls.Grids.SimpleGrid
{
    /// <summary>
    /// Grid view column that can sort
    /// </summary>
    public class RtpSimpleGridSortableColumn : GridViewColumn
    {
        /// <summary>
        /// default ctor
        /// </summary>
        public RtpSimpleGridSortableColumn()
        {
            CanSort = true;
            sortDirection = ListSortDirection.Ascending;
        }

        private ListSortDirection sortDirection;

        /// <summary>
        /// Gets or sets the current sort direction
        /// </summary>
        public ListSortDirection SortDirection
        {
            get { return sortDirection; }
        }

        /// <summary>
        /// Switches the sort direction
        /// </summary>
        public void SetSortDirection()
        {
            sortDirection = sortDirection == ListSortDirection.Ascending ?
                ListSortDirection.Descending : ListSortDirection.Ascending;
        }

        /// <summary>
        /// The property name of the column to be sorted
        /// </summary>
        public string SortPropertyName
        {
            get { return (string)GetValue(SortPropertyNameProperty); }
            set { SetValue(SortPropertyNameProperty, value); }
        }

        /// <summary>
        /// Gets or sets a flag indicating if the column can sort
        /// </summary>
        public bool CanSort { get; set; }

        /// <summary>
        /// The property name of the column to be sorted
        /// </summary>
        public static readonly DependencyProperty SortPropertyNameProperty =
            DependencyProperty.Register("SortPropertyName", typeof(string), typeof(RtpSimpleGridColumn), new UIPropertyMetadata(""));
    }
}
