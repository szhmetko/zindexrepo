﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace Rtp.Controls.Grids.SimpleGrid
{

    /// <summary>
    /// Extends the GridViewColumn to include some extra features
    /// </summary>
    public class RtpSimpleGridColumn : RtpSimpleGridSortableColumn
    {
        double _lastWidth;
        const double DefaultWidth = 50;
        private const string StrIsEnabledPropertyName = "IsEnabled";

        /// <summary>
        /// Default ctor
        /// </summary>
        public RtpSimpleGridColumn()
        {
            DefaultPosition = -1;
            SetLastWidth();
        }

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="name">The header name to set</param>
        /// <param name="sortName">The sort property name to set</param>
        public RtpSimpleGridColumn(string name, string sortName)
            : this()
        {
            Header = name;
            SortPropertyName = sortName;
        }

        /// <summary>
        /// Gets or sets the default position(index) where to place the column
        /// </summary>
        public int DefaultPosition { get; set; }

        //sets the last width 
        private void SetLastWidth()
        {
            _lastWidth = (double.IsNaN(Width) || Width == 0.0)
                ? (ActualWidth == 0.0 ? DefaultWidth : ActualWidth) : Width;
        }

        /// <summary>
        /// Gets or sets the tag for this object
        /// </summary>
        public object Tag { get; set; }

        /// <summary>
        /// a dependancy property to know whether to display the column in the ListView
        /// </summary>
        public static readonly DependencyProperty IsEnabledProperty =
            DependencyProperty.Register(StrIsEnabledPropertyName, typeof(bool)
            , typeof(RtpSimpleGridColumn), new PropertyMetadata(true, propertyChanged));

        //raises the property changed event
        static void propertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var column = d as RtpSimpleGridColumn;
            column.HideShowColumn((bool)e.NewValue);
            column.OnPropertyChanged(new PropertyChangedEventArgs(e.Property.Name));
        }

        /// <summary>
        /// Dependancy property to know whether to display the column in the ListView
        /// </summary>
        public bool IsEnabled
        {
            get { return (bool)GetValue(IsEnabledProperty); }
            set
            {
                //don't set the value if it is the same. this is important since on startup it could set the width as 0 for the columns
                if (value == IsEnabled)
                    return;
                SetValue(IsEnabledProperty, value);
                HideShowColumn(value);
                OnPropertyChanged(new PropertyChangedEventArgs(StrIsEnabledPropertyName));
            }
        }

        //hides / shows the gridview column
        private void HideShowColumn(bool enabled)
        {
            if (enabled)
                Width = _lastWidth;
            else
            {
                SetLastWidth();
                Width = 0;
            }
        }

        /// <summary>
        /// Checks if the property changed is the IsEnabled propety
        /// </summary>
        /// <param name="e">The event arguments to verify</param>
        /// <returns>Returns true if the property is the IsEnabled</returns>
        public static bool IsEnabledPropertyChanged(PropertyChangedEventArgs e)
        {
            if (e == null)
                return false;

            return e.PropertyName == StrIsEnabledPropertyName;
        }

        /// <summary>
        /// Returns a ColumnStateChangeAction that describes the property changed event
        /// </summary>
        /// <param name="sender">The DataGridViewColumn that has raised the event</param>
        /// <param name="e">The event argmunets to parse</param>
        /// <returns>Returns a ColumnStateChangeAction to represent the change of state</returns>
        /// <exception cref="ArgumentNullException">Throws ArgumentNullException if the sender or the arguments are not valid</exception>
        public static ColumnStateChangeAction GetActionFromPropertyChanged(RtpSimpleGridColumn sender, PropertyChangedEventArgs e)
        {
            if (sender != null && IsEnabledPropertyChanged(e))
                return sender.IsEnabled ? ColumnStateChangeAction.Enabled : ColumnStateChangeAction.Disabled;

            throw new ArgumentNullException("e", ExceptionStrings.INVALID_PROPETY_CHANGED_EVENT_ARGS);
        }

        /// <summary>
        /// Gets the identifier for a specific column
        /// </summary>
        /// <param name="column">The column to get the identifier from</param>
        /// <returns>Returns an identifier for the column</returns>
        public static string GetColumnIdentifier(GridViewColumn column)
        {
            return column == null ? string.Empty : column.Header.ToString();
        }

        /// <summary>
        /// Gets a flag indicating if the position of the column is valid
        /// </summary>
        /// <param name="position">The position to check</param>
        /// <returns>Returns true if position is valid</returns>
        public static bool IsValidPosition(int position)
        {
            return position > -1;
        }


    }
}
