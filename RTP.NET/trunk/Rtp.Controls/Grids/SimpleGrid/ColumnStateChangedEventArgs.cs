﻿using System;

namespace Rtp.Controls.Grids.SimpleGrid
{
    /// <summary>
    /// Event arguments for the ColumnStateChanged event
    /// </summary>
    public class ColumnStateChangedEventArgs : EventArgs
    {
        private ColumnStateChangeAction _action;

        /// <summary>
        /// The action for the event
        /// </summary>
        public ColumnStateChangeAction Action
        {
            get { return _action; }
            set { _action = value; }
        }

        /// <summary>
        /// The column that has changed
        /// </summary>
        public RtpSimpleGridColumn ColumnChanged { get; set; }

        /// <summary>
        /// default ctor
        /// </summary>
        /// <param name="columnChanged">The column that has changed</param>
        /// <param name="action">The action for the event</param>
        public ColumnStateChangedEventArgs(RtpSimpleGridColumn columnChanged, ColumnStateChangeAction action)
        {
            _action = action;
            ColumnChanged = columnChanged;
        }
    }
}
