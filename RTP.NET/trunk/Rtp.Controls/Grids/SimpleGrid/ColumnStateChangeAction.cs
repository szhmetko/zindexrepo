﻿namespace Rtp.Controls.Grids.SimpleGrid
{

    /// <summary>
    /// Enum to state the action for the Column State
    /// </summary>
    public enum ColumnStateChangeAction
    {
        /// <summary>
        /// columns is enabled
        /// </summary>
        Enabled,
        /// <summary>
        /// columns is disabled
        /// </summary>
        Disabled
    }
}
