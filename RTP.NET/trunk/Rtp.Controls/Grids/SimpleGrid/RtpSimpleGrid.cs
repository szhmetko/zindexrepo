﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Rtp.Infrastructure.UIAttributes;

namespace Rtp.Controls.Grids.SimpleGrid
{


    /// <summary>
    /// DataGridView is a control that can display a list of objects
    /// The data grid view show the data in a grid style. It generates the columns from the properties of the object in the current context of data
    /// You can use the DataGridViewPropertyDescriptorAttribute attribute to make your class properties interact better with the data grid
    /// </summary>
    public class RtpSimpleGrid : ListView
    {
        //delegate used for the change notification of the column
        private PropertyChangedEventHandler gridViewColumnPropertyChanged;

        //flag that marks explicitly tells the grid view to not generate the column by reflection

        /// <summary>
        /// Gets or set the use of a default view
        /// Set this property to true if you want to specify your own GridView
        /// </summary>
        public bool UseDefaultView { get; set; }

        private Style _columnHeaderContainerStyle;
        protected Style ColumnHeaderContainerStyle
        {
            get
            {
                if(_columnHeaderContainerStyle == null)
                {
                    _columnHeaderContainerStyle = new Style(typeof(GridViewColumnHeader));
                    var setter = new Setter(UIElement.VisibilityProperty, 
                        GridSettings.ShowHeader ? Visibility.Visible : Visibility.Collapsed);

                    _columnHeaderContainerStyle.Setters.Add(setter);
                }
                return _columnHeaderContainerStyle;
            }
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public RtpSimpleGrid()
        {
            gridViewColumnPropertyChanged = new PropertyChangedEventHandler(RtpSimpleGridPropertyChanged);
        }

        //event handler for the property changed event for the GridViewColumn
        void RtpSimpleGridPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var column = (RtpSimpleGridColumn)sender;
            if (RtpSimpleGridColumn.IsEnabledPropertyChanged(e))//check if the property changed is the enabled
            {
                OnColumnStateChanged(new ColumnStateChangedEventArgs(
                    column, RtpSimpleGridColumn.GetActionFromPropertyChanged(column, e)
                    ));
            }
        }

        /// <summary>
        /// Returns true if there are multiple items selected 
        /// </summary>
        public bool MultipleItemsSelected
        {
            get { return (bool)GetValue(MultipleItemsSelectedProperty); }
        }

        /// <summary>
        /// Returns true if there are multiple items selected 
        /// </summary>
        public static readonly DependencyProperty MultipleItemsSelectedProperty =
            DependencyProperty.Register("MultipleItemsSelected", typeof(bool), typeof(RtpSimpleGrid), new UIPropertyMetadata(false));

        /// <summary>
        /// override the selection changed to change the multiple select property
        /// </summary>
        /// <param name="e">The event arguments passed</param>
        protected override void OnSelectionChanged(SelectionChangedEventArgs e)
        {
            SetValue(MultipleItemsSelectedProperty, (SelectedItems.Count > 1));
            base.OnSelectionChanged(e);
        }

        /// <summary>
        /// event raised to notify listeners that a column state has changed
        /// </summary>
        public event EventHandler<ColumnStateChangedEventArgs> ColumnStateChanged;

        /// <summary>
        /// raises the ColumnStateChanged event
        /// </summary>
        /// <param name="e">The event arguments for the event</param>
        protected virtual void OnColumnStateChanged(ColumnStateChangedEventArgs e)
        {
            if (ColumnStateChanged != null)
                ColumnStateChanged(this, e);
        }

        //flag to mark the grid view columns as generated for the new item source
        private bool _hasGridViewBeenSet;

        /// <summary>
        /// Check that the grid view columns where generated for this type if not create them
        /// This usually occurs if data comes in late after binding...
        /// </summary>
        /// <param name="e">The arguments for the notification change of the binded collection</param>
        protected override void OnItemsChanged(NotifyCollectionChangedEventArgs e)
        {
            //if the columns were not created, create them
            if (!_hasGridViewBeenSet)
                GenerateGridViewColumnsForNewDataType(e.NewItems);

            base.OnItemsChanged(e);
        }

        /// <summary>
        /// Repopulate the columns for the listview when the item source changes
        /// </summary>
        /// <param name="oldValue">The old items</param>
        /// <param name="newValue">The new items</param>
        protected override void OnItemsSourceChanged(IEnumerable oldValue, IEnumerable newValue)
        {
            var nValue = (IList)newValue;
            GenerateGridViewColumnsForNewDataType(nValue);
            base.OnItemsSourceChanged(oldValue, newValue);
        }

        //generate the columns for the grid view
        private void GenerateGridViewColumnsForNewDataType(IList newValues)
        {
            if (UseDefaultView) // if there is no need to generate the column we can use the default view
                return;

            //check if the data passed is valid
            if (newValues == null || newValues.Count == 0)
            {
                //mark the gridview columns as not 
                _hasGridViewBeenSet = false;
                return;
            }

            _hasGridViewBeenSet = true;

            //Generate the grid view columns if needed
            GenerateGridColumns();
        }

        /// <summary>
        /// Returns true if there are multiple items selected 
        /// </summary>
        public static readonly DependencyProperty EntityTypeProperty =
            DependencyProperty.Register("EntityType", typeof(Type), typeof(RtpSimpleGrid), new UIPropertyMetadata());

        public Type EntityType
        {
            protected get { return (Type)GetValue(EntityTypeProperty); }
            set { SetValue(EntityTypeProperty, value); }
        }

        /// <summary>
        /// Returns true if there are multiple items selected 
        /// </summary>
        public static readonly DependencyProperty BindPrefixProperty =
            DependencyProperty.Register("BindPrefix", typeof(string), typeof(RtpSimpleGrid), new UIPropertyMetadata());

        public string BindPrefix
        {
            protected get { return (string)GetValue(BindPrefixProperty); }
            set { SetValue(BindPrefixProperty, value); }
        }
        
        //generates the column for the view
        private void GenerateGridColumns()
        {
            //unregister to the old columns notifications
            if (!UseDefaultView && View != null)
            {
                GridView oldView = View as GridView;
                if (oldView != null)
                    foreach (INotifyPropertyChanged column in oldView.Columns)
                        column.PropertyChanged -= gridViewColumnPropertyChanged;
            }

            GridView gridView = new GridView();//grid view to display the columns

            foreach (var item in ColumnsInfo)
            {
                var column = new RtpSimpleGridColumn(item.HeaderText, item.PropertyMetadata.Name)
                                 {
                                     DisplayMemberBinding = new Binding(BindPrefix + item.PropertyMetadata.Name)
                                     {Mode = BindingMode.TwoWay}
                                     , Width = item.FieldAttribute.Width
                                 };
                gridView.Columns.Add(column);
            }

            gridView.ColumnHeaderContainerStyle = ColumnHeaderContainerStyle;

            View = gridView;
        }



        #region Grid settings

        private GridSettingsExtractor _extractor;
        private GridSettingsExtractor Extractor
        {
            get { return _extractor ?? (_extractor = new GridSettingsExtractor(EntityType)); }
        }

        public IList<GridFieldInformation> ColumnsInfo
        {
            get { return Extractor.ColumnsInfo; }
        }

        public GridSettingsAttribute GridSettings
        {
            get { return Extractor.GridSettings; }
        }

        #endregion
    }

}
