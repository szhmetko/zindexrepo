﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Rtp.Infrastructure;
using Rtp.Infrastructure.Connection;

namespace Rtp.Controls.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm/getstarted
    /// </para>
    /// </summary>
    public class DatabaseConnectionViewModel : ViewModelBase
    {
        public IConnectionInfo ConnectionInfo { get; private set; }
        /// <summary>
        /// Initializes a new instance of the MasterConnectionViewModel class.
        /// </summary>
        public DatabaseConnectionViewModel(IConnectionInfo connectionInfo)
        {
            ConnectionInfo = connectionInfo;
            if (IsInDesignMode)
            {
                // Code runs in Blend --> create design time data.
            }
            else
            {
                // Code runs "for real"
                ConnectionInfo = new ConnectionInfo("DataBaseName", "UserName", "000.000.000.000", "Password");
            }
        }


        ////public override void Cleanup()
        ////{
        ////    // Clean up if needed

        ////    base.Cleanup();
        ////}
    }
}