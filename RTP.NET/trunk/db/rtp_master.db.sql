CREATE DATABASE `rtp_master` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

GRANT ALL PRIVILEGES ON rtp_master.* TO rtp_master@localhost IDENTIFIED BY 'rtp_m2a0s1t2er';
GRANT ALL PRIVILEGES ON rtp_master.* TO rtp_master@"%" IDENTIFIED BY 'rtp_m2a0s1t2er';

FLUSH PRIVILEGES;