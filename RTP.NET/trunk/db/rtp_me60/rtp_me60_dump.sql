-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Час створення: Трв 03 2012 р., 08:29
-- Версія сервера: 5.5.8
-- Версія PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- БД: `rtp_me60`
--

-- --------------------------------------------------------

--
-- Структура таблиці `Channels`
--

CREATE TABLE IF NOT EXISTS `Channels` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(64) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `rTpairId` int(11) NOT NULL DEFAULT '0',
  `clr` int(11) NOT NULL DEFAULT '0',
  `dt_ins` datetime NOT NULL,
  `k` float NOT NULL DEFAULT '1',
  `fname` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dt_upd` datetime NOT NULL,
  `Expression` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=22 ;

--
-- Дамп даних таблиці `Channels`
--

INSERT INTO `Channels` (`Id`, `Title`, `rTpairId`, `clr`, `dt_ins`, `k`, `fname`, `dt_upd`, `Expression`) VALUES
(1, 'метраж', 0, 16777215, '2012-05-03 08:55:20', 1, 'meter', '2012-05-03 08:55:20', 'IF(CAST(CALCULATED[0][21], INT) = CAST(CALCULATED[1][21], INT), CAST(CALCULATED[1][1], INT) + (((raw[0][2] >> 8) AND 0x3C) >> 2), 0) + AddMeters2Cache(((raw[0][2] >> 8) AND 0x3C) >> 2)*0'),
(2, 'V1', 1, 65280, '2012-05-03 08:55:20', 0.0274, 'c3', '2012-05-03 08:55:20', 'RAW[0][3]'),
(3, 'V2', 1, 14803200, '2012-05-03 08:55:20', 0.0274, 'c4', '2012-05-03 08:55:20', 'RAW[0][4]'),
(4, 'V3', 1, 10253825, '2012-05-03 08:55:20', 0.0274, 'c5', '2012-05-03 08:55:20', 'RAW[0][5]'),
(5, 'V4', 1, 13211393, '2012-05-03 08:55:20', 0.0274, 'c6', '2012-05-03 08:55:20', 'RAW[0][6]'),
(6, 'V5', 1, 698625, '2012-05-03 08:55:20', 0.0274, 'c7', '2012-05-03 08:55:20', 'RAW[0][7]'),
(7, 'V6', 1, 47541, '2012-05-03 08:55:20', 0.0274, 'c8', '2012-05-03 08:55:20', 'RAW[0][8]'),
(8, 'V7', 2, 909823, '2012-05-03 08:55:20', 0.0274, 'c9', '2012-05-03 08:55:20', 'RAW[0][9]'),
(9, 'V8', 2, 897279, '2012-05-03 08:55:20', 0.0274, 'c10', '2012-05-03 08:55:20', 'RAW[0][10]'),
(10, 'V9', 2, 90091, '2012-05-03 08:55:20', 0.0274, 'c11', '2012-05-03 08:55:20', 'RAW[0][11]'),
(11, 'V10', 1, 16319150, '2012-05-03 08:55:20', 0.0274, 'c12', '2012-05-03 08:55:20', 'RAW[0][12]'),
(12, 'V11', 1, 16519413, '2012-05-03 08:55:20', 0.0274, 'c13', '2012-05-03 08:55:20', 'RAW[0][13]'),
(13, 'V12', 1, 10296254, '2012-05-03 08:55:20', 0.0274, 'c14', '2012-05-03 08:55:20', 'RAW[0][14]'),
(14, 'Tхол.спая', 0, 0, '2012-05-03 08:55:20', 0.49, 'c24', '2012-05-03 08:55:20', 'RAW[0][24]'),
(15, 'Vшн.больш', 0, 198, '2012-05-03 08:55:20', 0.195, 'c15', '2012-05-03 08:55:20', 'RAW[0][15]'),
(16, 'Vшн.мал', 0, 8224125, '2012-05-03 08:55:20', 0.097, 'c16', '2012-05-03 08:55:20', 'RAW[0][16]'),
(17, 'Vлинейн', 0, 13763105, '2012-05-03 08:55:20', 1, 'vlin', '2012-05-03 08:55:20', 'If( (raw[0][1] AND 8191) > 0, 36621.7/(raw[0][1] AND 8191), 0.0)'),
(18, 'Диам.эталон', 0, 3831040, '2012-05-03 08:55:20', 0.01, 'c25', '2012-05-03 08:55:20', 'RAW[0][25]'),
(19, 'Диам.измер.', 0, 12615808, '2012-05-03 08:55:20', 0.01, 'c26', '2012-05-03 08:55:20', 'RAW[0][26]'),
(20, 'пробои', 0, 2567676, '2012-05-03 08:55:20', 1, 'c27', '2012-05-03 08:55:20', 'RAW[0][2] AND 0x0080'),
(21, 'Id Отрезка', 0, 0, '2012-04-15 00:00:00', 1, 'pieceId', '2012-04-16 00:00:00', 'IF( ((raw[0][2] AND 0x0002) =0x0002) OR (CAST(calculated[1][21], INT) = 0), AddPiece(), CAST(calculated[1][21], INT))');

-- --------------------------------------------------------

--
-- Структура таблиці `Curves`
--

CREATE TABLE IF NOT EXISTS `Curves` (
  `rec_id` int(11) NOT NULL AUTO_INCREMENT,
  `dchl_num` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL DEFAULT '0',
  `bPrimary` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`rec_id`),
  KEY `FK_CURVES_CHART` (`group_id`),
  KEY `FK_CURVES_CHANNELS` (`dchl_num`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=58 ;

--
-- Дамп даних таблиці `Curves`
--

INSERT INTO `Curves` (`rec_id`, `dchl_num`, `group_id`, `bPrimary`) VALUES
(4, 2, 2, 1),
(5, 18, 9, 1),
(6, 19, 9, 1),
(7, 20, 9, 1),
(8, 3, 2, 1),
(9, 4, 2, 1),
(10, 5, 2, 1),
(11, 6, 2, 1),
(12, 7, 2, 1),
(13, 8, 2, 1),
(14, 9, 2, 1),
(15, 10, 2, 1),
(16, 11, 2, 1),
(17, 12, 2, 1),
(18, 13, 2, 1),
(19, 15, 3, 1),
(20, 16, 3, 1),
(21, 17, 3, 0),
(22, 18, 10, 1),
(23, 19, 10, 1),
(24, 20, 10, 1),
(25, 15, 5, 1),
(26, 16, 5, 1),
(27, 17, 5, 0),
(28, 2, 4, 1),
(29, 3, 4, 1),
(30, 4, 4, 1),
(31, 5, 4, 1),
(32, 6, 4, 1),
(33, 7, 4, 1),
(34, 8, 4, 1),
(35, 9, 4, 1),
(36, 10, 4, 1),
(37, 11, 4, 1),
(38, 12, 4, 1),
(39, 13, 4, 1),
(40, 15, 8, 1),
(41, 16, 8, 1),
(42, 17, 8, 0),
(43, 18, 7, 1),
(44, 19, 7, 1),
(45, 20, 7, 1),
(46, 2, 6, 1),
(47, 3, 6, 1),
(48, 4, 6, 1),
(49, 5, 6, 1),
(50, 6, 6, 1),
(51, 7, 6, 1),
(52, 8, 6, 1),
(53, 9, 6, 1),
(54, 10, 6, 1),
(55, 11, 6, 1),
(56, 12, 6, 1),
(57, 13, 6, 1);

-- --------------------------------------------------------

--
-- Структура таблиці `PieceDescription`
--

CREATE TABLE IF NOT EXISTS `PieceDescription` (
  `rec_id` int(11) NOT NULL AUTO_INCREMENT,
  `piece_id` int(11) NOT NULL DEFAULT '-1',
  `paramName` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `paramVal` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dt_upd` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  PRIMARY KEY (`rec_id`),
  KEY `IX_PieceID_PieceDescription` (`piece_id`),
  KEY `FK_PIECES_DESC_PIECES` (`piece_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Дамп даних таблиці `PieceDescription`
--


-- --------------------------------------------------------

--
-- Структура таблиці `Pieces`
--

CREATE TABLE IF NOT EXISTS `Pieces` (
  `Id` int(11) NOT NULL,
  `dtOn` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  `dtOff` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  `Length` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `IDX_cablePieces_dtm_on` (`dtOn`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `Pieces`
--


-- --------------------------------------------------------

--
-- Структура таблиці `Queries`
--

CREATE TABLE IF NOT EXISTS `Queries` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(16) NOT NULL,
  `query` text NOT NULL,
  `group` varchar(16) NOT NULL COMMENT 'client',
  `lastDt` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `key_UNIQUE` (`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Дамп даних таблиці `Queries`
--

INSERT INTO `Queries` (`Id`, `key`, `query`, `group`, `lastDt`) VALUES
(1, 'ARCHIVE', 'SELECT MIN(Uxtime) as XValue, MAX(dtm_upd) as dtm_upd, AVG(c3) as c3, AVG(c4) as c4, AVG(c5) as c5, AVG(c6) as c6, AVG(c7) as c7, AVG(c8) as c8, AVG(c9) as c9, AVG(c10) as c10, AVG(c11) as c11, AVG(c12) as c12, AVG(c13) as c13, AVG(c14) as c14, AVG(c15) as c15, AVG(c16) as c16, AVG(c17) as c17, AVG(c18) as c18, AVG(c19) as c19, AVG(c20) as c20, AVG(c21) as c21, AVG(c22) as c22, AVG(c23) as c23, AVG(c24) as c24, AVG(c25) as c25, AVG(c26) as c26, AVG(vlin) as vlin FROM  (SELECT * FROM Xvals WHERE UXTIME BETWEEN :DateBegin AND :DateEnd)it GROUP BY FLOOR(:RES * Uxtime/(:DateEnd -:DateBegin))', 'archive', NULL),
(2, 'MONITOR', 'SELECT MIN(Uxtime) as XValue, MAX(dtm_upd) as dtm_upd, AVG(c3) as c3, AVG(c4) as c4, AVG(c5) as c5, AVG(c6) as c6, AVG(c7) as c7\r\n, AVG(c8) as c8, AVG(c9) as c9, AVG(c10) as c10, AVG(c11) as c11, AVG(c12) as c12, \r\nAVG(c13) as c13, AVG(c14) as c14, AVG(c15) as c15, AVG(c16) as c16, AVG(c17) as c17\r\n, AVG(c18) as c18, AVG(c19) as c19, AVG(c20) as c20, AVG(c21) as c21, AVG(c22) as c22\r\n, AVG(c23) as c23, AVG(c24) as c24, AVG(c25) as c25, AVG(c26) as c26, AVG(vlin) as vlin\r\nFROM  (SELECT * FROM Xvals WHERE UXTIME > UNIX_TIMESTAMP() - :MINS * 60 )it GROUP BY FLOOR(:RES * Uxtime/(:MINS * 60))', 'monitor', NULL),
(3, 'PERIODICAL1', '', 'periodical', NULL),
(4, 'PERIODICAL2', '', 'periodical', NULL),
(5, 'PERIODICAL3', '', 'periodical', NULL),
(6, 'PIECES', 'SELECT MIN(Uxtime) as XValue, MAX(dtm_upd) as dtm_upd, AVG(c3) as c3, AVG(c4) as c4, AVG(c5) as c5, AVG(c6) as c6, AVG(c7) as c7\r\n, AVG(c8) as c8, AVG(c9) as c9, AVG(c10) as c10, AVG(c11) as c11, AVG(c12) as c12, \r\nAVG(c13) as c13, AVG(c14) as c14, AVG(c15) as c15, AVG(c16) as c16, AVG(c17) as c17\r\n, AVG(c18) as c18, AVG(c19) as c19, AVG(c20) as c20, AVG(c21) as c21, AVG(c22) as c22\r\n, AVG(c23) as c23, AVG(c24) as c24, AVG(c25) as c25, AVG(c26) as c26, AVG(vlin) as vlin\r\nFROM  (SELECT * FROM Xvals WHERE piece_id=:PID )it GROUP BY FLOOR(:RES * meter / 1)', 'pieces', NULL),
(7, 'PIECES_LIST', 'SELECT p.*, CASE WHEN 1>2 THEN ''1st case'' ELSE ''????'' END as Description FROM Pieces p', 'pieces', NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `TabChannels`
--

CREATE TABLE IF NOT EXISTS `TabChannels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tab_id` int(11) NOT NULL DEFAULT '0',
  `alias` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'velocity',
  `main_title` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No title',
  `primary_units` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sec_units` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bgcolor` int(11) NOT NULL DEFAULT '16777215',
  `prim_low_lim` float NOT NULL DEFAULT '0',
  `sec_low_lim` float NOT NULL DEFAULT '0',
  `prim_high_lim` float NOT NULL DEFAULT '0',
  `sec_high_lim` float NOT NULL DEFAULT '0',
  `num` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_TABS_CHARTS` (`tab_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Дамп даних таблиці `TabChannels`
--

INSERT INTO `TabChannels` (`id`, `tab_id`, `alias`, `main_title`, `primary_units`, `sec_units`, `bgcolor`, `prim_low_lim`, `sec_low_lim`, `prim_high_lim`, `sec_high_lim`, `num`) VALUES
(2, 3, 'temperatures', 'температуры', 'гр.Цельсия', 'гр.Цельсия', 16777215, 0, 0, 300, 300, 0),
(3, 3, 'velocity', 'скорости', 'об/мин,%', 'м/мин', 15532031, 0, 0, 100, 200, 1),
(4, 4, 'temperatures', 'температуры', 'гр.Цельсия', 'гр.Цельсия', 16710655, 0, 0, 300, 300, 0),
(5, 4, 'velocity', 'скорости', 'об/мин,%', 'м/мин', 15399679, 0, 0, 100, 200, 1),
(6, 5, 'temperatures', 'температуры', 'гр.Цельсия', 'гр.Цельсия', 16777215, 0, 0, 300, 300, 0),
(7, 5, 'diameter', 'диаметр, пробои', 'мм', 'мм', 15597537, 0.25, 0.25, 0.25, 0.25, 1),
(8, 5, 'velocity', 'скорости', 'об/мин,%', 'м/мин', 15268863, 0, 0, 100, 200, 2),
(9, 3, 'diameter', 'диаметр, пробои', 'мм', 'мм', 14876618, 0.25, 0.25, 0.25, 0.25, 2),
(10, 4, 'diameter', 'диаметр, пробои', 'мм', 'мм', 15859686, 0.25, 0.25, 0.25, 0.25, 2);

-- --------------------------------------------------------

--
-- Структура таблиці `Tabs`
--

CREATE TABLE IF NOT EXISTS `Tabs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num` int(11) NOT NULL,
  `Title` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'monitor',
  `query_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'MONITOR',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Дамп даних таблиці `Tabs`
--

INSERT INTO `Tabs` (`id`, `num`, `Title`, `alias`, `query_name`) VALUES
(3, 0, 'монитор', 'monitor', 'MONITOR'),
(4, 1, 'архив', 'archive', 'ARCHIVE'),
(5, 2, 'отрезки', 'pieces', 'PIECES');

-- --------------------------------------------------------

--
-- Структура таблиці `Xvals`
--

CREATE TABLE IF NOT EXISTS `Xvals` (
  `Id` bigint(11) NOT NULL AUTO_INCREMENT,
  `uxtime` bigint(20) DEFAULT NULL,
  `meter` int(11) NOT NULL DEFAULT '0',
  `prob` int(11) NOT NULL DEFAULT '0',
  `pieceId` int(11) NOT NULL DEFAULT '0',
  `DataType` int(11) NOT NULL,
  `c1` float DEFAULT NULL,
  `c2` float DEFAULT NULL,
  `c3` float DEFAULT NULL,
  `c4` float DEFAULT NULL,
  `c5` float DEFAULT NULL,
  `c6` float DEFAULT NULL,
  `c7` float DEFAULT NULL,
  `c8` float DEFAULT NULL,
  `c9` float DEFAULT NULL,
  `c10` float DEFAULT NULL,
  `c11` float DEFAULT NULL,
  `c12` float DEFAULT NULL,
  `c13` float DEFAULT NULL,
  `c14` float DEFAULT NULL,
  `c15` float DEFAULT NULL,
  `c16` float DEFAULT NULL,
  `c17` float DEFAULT NULL,
  `c18` float DEFAULT NULL,
  `c19` float DEFAULT NULL,
  `c20` float DEFAULT NULL,
  `c21` float DEFAULT NULL,
  `c22` float DEFAULT NULL,
  `c23` float DEFAULT NULL,
  `c24` float DEFAULT NULL,
  `c25` float DEFAULT NULL,
  `c26` float DEFAULT NULL,
  `c27` float DEFAULT NULL,
  `c28` float DEFAULT NULL,
  `c29` float DEFAULT NULL,
  `c30` float DEFAULT NULL,
  `c31` float DEFAULT NULL,
  `c32` float DEFAULT NULL,
  `vlin` float DEFAULT NULL,
  `dtm_upd` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`),
  KEY `IDX_Xvals_Uxtime` (`uxtime`),
  KEY `IDX_XVals_piece_id` (`pieceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Дамп даних таблиці `Xvals`
--


--
-- Constraints for dumped tables
--

--
-- Constraints for table `Curves`
--
ALTER TABLE `Curves`
  ADD CONSTRAINT `FK_CURVES_CHANNELS` FOREIGN KEY (`dchl_num`) REFERENCES `channels` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_CURVES_CHART` FOREIGN KEY (`group_id`) REFERENCES `tabchannels` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `PieceDescription`
--
ALTER TABLE `PieceDescription`
  ADD CONSTRAINT `FK_PIECES_DESC_PIECES` FOREIGN KEY (`piece_id`) REFERENCES `pieces` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `TabChannels`
--
ALTER TABLE `TabChannels`
  ADD CONSTRAINT `FK_TABS_CHARTS` FOREIGN KEY (`tab_id`) REFERENCES `tabs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
