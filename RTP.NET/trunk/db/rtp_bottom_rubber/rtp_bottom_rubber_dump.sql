-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Час створення: Трв 03 2012 р., 20:14
-- Версія сервера: 5.5.8
-- Версія PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- БД: `rtp_bottom_rubber`
--

-- --------------------------------------------------------

--
-- Структура таблиці `Channels`
--

CREATE TABLE IF NOT EXISTS `Channels` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(64) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `rTpairId` int(11) NOT NULL DEFAULT '0',
  `clr` int(11) NOT NULL DEFAULT '0',
  `dt_ins` datetime NOT NULL,
  `k` float NOT NULL DEFAULT '1',
  `fname` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dt_upd` datetime NOT NULL,
  `Expression` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Дамп даних таблиці `Channels`
--

INSERT INTO `Channels` (`Id`, `Title`, `rTpairId`, `clr`, `dt_ins`, `k`, `fname`, `dt_upd`, `Expression`) VALUES
(1, 'Метраж', 0, 0, '2012-05-03 21:04:13', 1, 'meter', '2012-05-03 21:04:13', 'IF(CAST(CALCULATED[0][16], INT) = CAST(CALCULATED[1][16], INT), CAST(CALCULATED[1][1], INT) + (((raw[0][2] >> 8) AND 0x3C) >> 2), 0) + AddMeters2Cache(((raw[0][2] >> 8) AND 0x3C) >> 2)*0'),
(2, 'пробои', 0, 2032606, '2012-05-03 21:04:13', 1, 'c27', '2012-05-03 21:04:13', 'RAW[0][2] AND 0x0080'),
(3, 'ск.двиг.шн125', 0, 4227327, '2012-05-03 21:04:13', 1, 'c4', '2012-05-03 21:04:13', 'RAW[0][4]'),
(4, 'натяжение', 0, 5329741, '2012-05-03 21:04:13', 0.95, 'c6', '2012-05-03 21:04:13', 'RAW[0][6]'),
(5, 'т-ра червяка', 0, 8454143, '2012-05-03 21:04:13', 1, 'c7', '2012-05-03 21:04:13', 'RAW[0][7]'),
(6, 'т-ра зоны загру', 0, 15204584, '2012-05-03 21:04:13', 1, 'c8', '2012-05-03 21:04:13', 'RAW[0][8]'),
(7, 'т-ра 1-й зоны', 0, 16290582, '2012-05-03 21:04:13', 1, 'c9', '2012-05-03 21:04:13', 'RAW[0][9]'),
(8, 'т-ра 2-й зоны', 0, 4162855, '2012-05-03 21:04:13', 1, 'c10', '2012-05-03 21:04:13', 'RAW[0][10]'),
(9, 'т-ра головки', 0, 255, '2012-05-03 21:04:13', 1, 'c11', '2012-05-03 21:04:13', 'RAW[0][11]'),
(10, 'т-ра в трубе', 0, 108, '2012-05-03 21:04:13', 1, 'c12', '2012-05-03 21:04:13', 'RAW[0][12]'),
(11, 'хол спай', 0, 0, '2012-05-03 21:04:13', 1, 'c17', '2012-05-03 21:04:13', 'RAW[0][17]'),
(12, 'диам. эталонный', 0, 255, '2012-05-03 21:04:13', 0.01, 'c18', '2012-05-03 21:04:13', 'RAW[0][18]'),
(13, 'диам. измеренны', 0, 16711680, '2012-05-03 21:04:13', 0.01, 'c19', '2012-05-03 21:04:13', 'RAW[0][19]'),
(14, 'лин скорость', 0, 12977282, '2012-05-03 21:04:13', 1.37, 'vlin', '2012-05-03 21:04:13', 'If( (raw[0][1] AND 8191) > 0, 36621.7/(raw[0][1] AND 8191), 0.0)'),
(15, 'Ролик1', 0, 0, '2012-05-03 21:04:13', 100, 'c26', '2012-05-03 21:04:13', 'RAW[0][2] AND 0x0040'),
(16, 'Id Отрезка', 0, 0, '2012-04-15 00:00:00', 1, 'pieceId', '2012-04-16 00:00:00', 'IF( ((raw[0][2] AND 0x0002) =0x0002) OR (CAST(calculated[1][16], INT) = 0), AddPiece(), CAST(calculated[1][16], INT))');

-- --------------------------------------------------------

--
-- Структура таблиці `Curves`
--

CREATE TABLE IF NOT EXISTS `Curves` (
  `rec_id` int(11) NOT NULL AUTO_INCREMENT,
  `dchl_num` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL DEFAULT '0',
  `bPrimary` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`rec_id`),
  KEY `FK_CURVES_CHART` (`group_id`),
  KEY `FK_CURVES_CHANNELS` (`dchl_num`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=78 ;

--
-- Дамп даних таблиці `Curves`
--

INSERT INTO `Curves` (`rec_id`, `dchl_num`, `group_id`, `bPrimary`) VALUES
(7, 5, 1, 1),
(8, 6, 1, 1),
(9, 7, 1, 1),
(10, 8, 1, 1),
(14, 3, 3, 1),
(16, 7, 4, 1),
(17, 6, 4, 1),
(18, 8, 4, 1),
(22, 9, 1, 1),
(23, 10, 1, 1),
(28, 9, 4, 1),
(30, 5, 4, 1),
(34, 3, 5, 1),
(46, 12, 2, 1),
(47, 13, 2, 1),
(48, 2, 2, 1),
(51, 4, 9, 1),
(52, 4, 10, 1),
(53, 3, 11, 1),
(54, 14, 11, 0),
(55, 6, 12, 1),
(56, 5, 12, 1),
(57, 7, 12, 1),
(58, 8, 12, 1),
(59, 9, 12, 1),
(60, 10, 1, 1),
(61, 10, 12, 1),
(62, 14, 3, 0),
(63, 14, 5, 0),
(66, 10, 4, 1),
(71, 12, 13, 1),
(72, 13, 13, 1),
(74, 2, 9, 1),
(76, 15, 9, 1),
(77, 15, 10, 1);

-- --------------------------------------------------------

--
-- Структура таблиці `PieceDescription`
--

CREATE TABLE IF NOT EXISTS `PieceDescription` (
  `rec_id` int(11) NOT NULL AUTO_INCREMENT,
  `piece_id` int(11) NOT NULL DEFAULT '-1',
  `paramName` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `paramVal` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dt_upd` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  PRIMARY KEY (`rec_id`),
  KEY `IX_PieceID_PieceDescription` (`piece_id`),
  KEY `FK_PIECES_DESC_PIECES` (`piece_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Дамп даних таблиці `PieceDescription`
--


-- --------------------------------------------------------

--
-- Структура таблиці `Pieces`
--

CREATE TABLE IF NOT EXISTS `Pieces` (
  `Id` int(11) NOT NULL,
  `dtOn` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  `dtOff` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  `Length` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `IDX_cablePieces_dtm_on` (`dtOn`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `Pieces`
--


-- --------------------------------------------------------

--
-- Структура таблиці `Queries`
--

CREATE TABLE IF NOT EXISTS `Queries` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(16) NOT NULL,
  `query` text NOT NULL,
  `group` varchar(16) NOT NULL COMMENT 'client',
  `lastDt` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `key_UNIQUE` (`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Дамп даних таблиці `Queries`
--

INSERT INTO `Queries` (`Id`, `key`, `query`, `group`, `lastDt`) VALUES
(1, 'ARCHIVE', 'SELECT MIN(Uxtime) as XValue, MAX(dtm_upd) as dtm_upd, AVG(c3) as c3, AVG(c4) as c4, AVG(c5) as c5, AVG(c6) as c6, AVG(c7) as c7, AVG(c8) as c8, AVG(c9) as c9, AVG(c10) as c10, AVG(c11) as c11, AVG(c12) as c12, AVG(c13) as c13, AVG(c14) as c14, AVG(c15) as c15, AVG(c16) as c16, AVG(c17) as c17, AVG(c18) as c18, AVG(c19) as c19, AVG(c20) as c20, AVG(c21) as c21, AVG(c22) as c22, AVG(c23) as c23, AVG(c24) as c24, AVG(c25) as c25, AVG(c26) as c26, AVG(vlin) as vlin FROM  (SELECT * FROM Xvals WHERE UXTIME BETWEEN :DateBegin AND :DateEnd)it GROUP BY FLOOR(:RES * Uxtime/(:DateEnd -:DateBegin))', 'archive', NULL),
(2, 'MONITOR', 'SELECT MIN(Uxtime) as XValue, MAX(dtm_upd) as dtm_upd, AVG(c3) as c3, AVG(c4) as c4, AVG(c5) as c5, AVG(c6) as c6, AVG(c7) as c7\r\n, AVG(c8) as c8, AVG(c9) as c9, AVG(c10) as c10, AVG(c11) as c11, AVG(c12) as c12, \r\nAVG(c13) as c13, AVG(c14) as c14, AVG(c15) as c15, AVG(c16) as c16, AVG(c17) as c17\r\n, AVG(c18) as c18, AVG(c19) as c19, AVG(c20) as c20, AVG(c21) as c21, AVG(c22) as c22\r\n, AVG(c23) as c23, AVG(c24) as c24, AVG(c25) as c25, AVG(c26) as c26, AVG(vlin) as vlin\r\nFROM  (SELECT * FROM Xvals WHERE UXTIME > UNIX_TIMESTAMP() - :MINS * 60 )it GROUP BY FLOOR(:RES * Uxtime/(:MINS * 60))', 'monitor', NULL),
(3, 'PERIODICAL1', '', 'periodical', NULL),
(4, 'PERIODICAL2', '', 'periodical', NULL),
(5, 'PERIODICAL3', '', 'periodical', NULL),
(6, 'PIECES', 'SELECT MIN(Uxtime) as XValue, MAX(dtm_upd) as dtm_upd, AVG(c3) as c3, AVG(c4) as c4, AVG(c5) as c5, AVG(c6) as c6, AVG(c7) as c7\r\n, AVG(c8) as c8, AVG(c9) as c9, AVG(c10) as c10, AVG(c11) as c11, AVG(c12) as c12, \r\nAVG(c13) as c13, AVG(c14) as c14, AVG(c15) as c15, AVG(c16) as c16, AVG(c17) as c17\r\n, AVG(c18) as c18, AVG(c19) as c19, AVG(c20) as c20, AVG(c21) as c21, AVG(c22) as c22\r\n, AVG(c23) as c23, AVG(c24) as c24, AVG(c25) as c25, AVG(c26) as c26, AVG(vlin) as vlin\r\nFROM  (SELECT * FROM Xvals WHERE piece_id=:PID )it GROUP BY FLOOR(:RES * meter / 1)', 'pieces', NULL),
(7, 'PIECES_LIST', 'SELECT p.*, CASE WHEN 1>2 THEN ''1st case'' ELSE ''????'' END as Description FROM Pieces p', 'pieces', NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `TabChannels`
--

CREATE TABLE IF NOT EXISTS `TabChannels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tab_id` int(11) NOT NULL DEFAULT '0',
  `alias` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'velocity',
  `main_title` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No title',
  `primary_units` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sec_units` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bgcolor` int(11) NOT NULL DEFAULT '16777215',
  `prim_low_lim` float NOT NULL DEFAULT '0',
  `sec_low_lim` float NOT NULL DEFAULT '0',
  `prim_high_lim` float NOT NULL DEFAULT '0',
  `sec_high_lim` float NOT NULL DEFAULT '0',
  `num` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_TABS_CHARTS` (`tab_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Дамп даних таблиці `TabChannels`
--

INSERT INTO `TabChannels` (`id`, `tab_id`, `alias`, `main_title`, `primary_units`, `sec_units`, `bgcolor`, `prim_low_lim`, `sec_low_lim`, `prim_high_lim`, `sec_high_lim`, `num`) VALUES
(1, 1, 'temperatures', 'температуры', 'гр. Цельсия', 'гр.Цельсия', 14998240, 0, 0, 250, 250, 0),
(2, 1, 'diameter', 'диаметр/пробои', 'мм', 'мм', 13425635, 2, 2, 2, 2, 1),
(3, 2, 'velocity', 'скорости', 'об/мин', 'м/мин', 14602938, 0, 0, 150, 150, 1),
(4, 2, 'temperatures', 'температуры', 'гр.Цельсия', 'гр.Цельсия', 14998240, 0, 0, 250, 250, 0),
(5, 1, 'velocity', 'скорости', 'об/мин', 'м/мин', 15129025, 0, 0, 200, 140, 2),
(9, 2, 'velocity', 'натяжение, пробои', 'кг', 'кг', 12632256, 0, 0, 750, 750, 2),
(10, 4, 'temperatures', 'натяжение', 'кг', 'кг', 14211288, 0, 0, 750, 750, 0),
(11, 4, 'velocity', 'скорости', 'об/мин', 'м/мин', 15196109, 0, 0, 1000, 120, 1),
(12, 4, 'temperatures', 'температуры', 'гр.Цельсия', 'гр.Цельсия', 15195112, 0, 0, 250, 250, 2),
(13, 2, 'diameter', 'Диаметр ', 'мм', 'мм', 16777088, 0, 0, 40, 40, 3);

-- --------------------------------------------------------

--
-- Структура таблиці `Tabs`
--

CREATE TABLE IF NOT EXISTS `Tabs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num` int(11) NOT NULL,
  `Title` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'monitor',
  `query_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'MONITOR',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Дамп даних таблиці `Tabs`
--

INSERT INTO `Tabs` (`id`, `num`, `Title`, `alias`, `query_name`) VALUES
(1, 0, 'отрезки', 'pieces', 'PIECES'),
(2, 1, 'монитор', 'monitor', 'MONITOR'),
(4, 2, 'Архив', 'archive', 'ARCHIVE');

-- --------------------------------------------------------

--
-- Структура таблиці `Xvals`
--

CREATE TABLE IF NOT EXISTS `Xvals` (
  `Id` bigint(11) NOT NULL AUTO_INCREMENT,
  `uxtime` bigint(20) DEFAULT NULL,
  `meter` int(11) NOT NULL DEFAULT '0',
  `prob` int(11) NOT NULL DEFAULT '0',
  `pieceId` int(11) NOT NULL DEFAULT '0',
  `DataType` int(11) NOT NULL,
  `c1` float DEFAULT NULL,
  `c2` float DEFAULT NULL,
  `c3` float DEFAULT NULL,
  `c4` float DEFAULT NULL,
  `c5` float DEFAULT NULL,
  `c6` float DEFAULT NULL,
  `c7` float DEFAULT NULL,
  `c8` float DEFAULT NULL,
  `c9` float DEFAULT NULL,
  `c10` float DEFAULT NULL,
  `c11` float DEFAULT NULL,
  `c12` float DEFAULT NULL,
  `c13` float DEFAULT NULL,
  `c14` float DEFAULT NULL,
  `c15` float DEFAULT NULL,
  `c16` float DEFAULT NULL,
  `c17` float DEFAULT NULL,
  `c18` float DEFAULT NULL,
  `c19` float DEFAULT NULL,
  `c20` float DEFAULT NULL,
  `c21` float DEFAULT NULL,
  `c22` float DEFAULT NULL,
  `c23` float DEFAULT NULL,
  `c24` float DEFAULT NULL,
  `c25` float DEFAULT NULL,
  `c26` float DEFAULT NULL,
  `c27` float DEFAULT NULL,
  `c28` float DEFAULT NULL,
  `c29` float DEFAULT NULL,
  `c30` float DEFAULT NULL,
  `c31` float DEFAULT NULL,
  `c32` float DEFAULT NULL,
  `vlin` float DEFAULT NULL,
  `dtm_upd` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`),
  KEY `IDX_Xvals_Uxtime` (`uxtime`),
  KEY `IDX_XVals_piece_id` (`pieceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Дамп даних таблиці `Xvals`
--


--
-- Constraints for dumped tables
--

--
-- Constraints for table `Curves`
--
ALTER TABLE `Curves`
  ADD CONSTRAINT `FK_CURVES_CHANNELS` FOREIGN KEY (`dchl_num`) REFERENCES `channels` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_CURVES_CHART` FOREIGN KEY (`group_id`) REFERENCES `tabchannels` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `PieceDescription`
--
ALTER TABLE `PieceDescription`
  ADD CONSTRAINT `FK_PIECES_DESC_PIECES` FOREIGN KEY (`piece_id`) REFERENCES `pieces` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `TabChannels`
--
ALTER TABLE `TabChannels`
  ADD CONSTRAINT `FK_TABS_CHARTS` FOREIGN KEY (`tab_id`) REFERENCES `tabs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
