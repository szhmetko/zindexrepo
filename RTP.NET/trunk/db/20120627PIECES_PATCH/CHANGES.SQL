-- Run against each database
ALTER TABLE `Queries` CHANGE COLUMN `key` `key` VARCHAR(64) CHARACTER SET 'latin1' NOT NULL ;

ALTER TABLE `Queries` ADD COLUMN `params` VARCHAR(128) NULL  AFTER `query` ;

ALTER TABLE `Queries` CHARACTER SET = utf8 ;

ALTER TABLE `Queries` CHANGE COLUMN `query` `query` TEXT NOT NULL  ;


INSERT INTO `Queries` (`key`, `query`, `params`, `group`, `lastDt`) VALUES
('DiameterDeviation', 'SELECT ROUND(FLOOR(c26/20)*20*(SELECT IFNULL(SUM(k), 1) FROM Channels WHERE fname=''c26''), 2) AS ValueRangeStart, COUNT(*) AS Occurences, ''Green'' as Color, :NomD, :NomDDev  FROM XVals WHERE pieceId = :PieceId   GROUP BY  FLOOR(c26/20)', 'NomD, NomDDev,PieceId', '', NULL),
('PieceOverall', 'SELECT pc.*, totalsP.Probs, totalsP.RevertedProbs \r\n, ''��� "���� ��"'' as CompanyName, NOW() as SysDate, pc.Length AS Length, pc.dtOn as DtOn, pc.dtOff as DtOff, CAST(TIMEDIFF(pc.dtOff,pc.dtOn) as CHAR(8)) as OffOnDiff\r\n, totals.vlin AS VLinAvg, :NomVlin-totals.vlin AS VLinDev, totals.DEtal AS DEtal, totals.DAvg AS DAvg, CAST(totalsP.prob AS DECIMAL) as ProbsSum\r\nFROM Pieces pc \r\nLEFT JOIN (SELECT AVG(vlin) as vlin, AVG(c25)*(SELECT IFNULL(SUM(k), 1) K FROM Channels WHERE fname=''c25'') as DEtal, AVG(c26) * (SELECT IFNULL(SUM(k), 1) K FROM Channels WHERE fname=''c26'') as DAvg, pieceId FROM XVals WHERE pieceId = :PieceId AND DataType=1)totals \r\n    ON totals.pieceId=pc.Id\r\nLEFT JOIN (SELECT SUM(prob) AS prob, pieceId, GROUP_CONCAT(meter SEPARATOR '', '') AS Probs, GROUP_CONCAT((SELECT p.Length FROM Pieces p WHERE p.Id = :PieceId) - meter SEPARATOR '', '') AS RevertedProbs \r\n                FROM XVals WHERE pieceId = :PieceId AND DataType=1 AND prob>0)totalsP \r\n    ON totalsP.pieceId=pc.Id\r\nWHERE pc.Id=:PieceId', 'PieceId,NomVlin', '', NULL);
