-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Час створення: Квт 17 2012 р., 05:51
-- Версія сервера: 5.5.8
-- Версія PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- БД: `rtp_demo`
--

-- --------------------------------------------------------

--
-- Структура таблиці `Queries`
--

CREATE TABLE IF NOT EXISTS `Queries` (
  `Id` int(11) NOT NULL,
  `key` varchar(16) NOT NULL,
  `query` text NOT NULL,
  `group` varchar(16) NOT NULL COMMENT 'client',
  `lastDt` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `key_UNIQUE` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп даних таблиці `Queries`
--

INSERT INTO `Queries` (`Id`, `key`, `query`, `group`, `lastDt`) VALUES
(1, 'ARCHIVE', 'SELECT MIN(Uxtime) as XValue, MAX(dtm_upd) as dtm_upd, AVG(c3) as c3, AVG(c4) as c4, AVG(c5) as c5, AVG(c6) as c6, AVG(c7) as c7, AVG(c8) as c8, AVG(c9) as c9, AVG(c10) as c10, AVG(c11) as c11, AVG(c12) as c12, AVG(c13) as c13, AVG(c14) as c14, AVG(c15) as c15, AVG(c16) as c16, AVG(c17) as c17, AVG(c18) as c18, AVG(c19) as c19, AVG(c20) as c20, AVG(c21) as c21, AVG(c22) as c22, AVG(c23) as c23, AVG(c24) as c24, AVG(c25) as c25, AVG(c26) as c26, AVG(vlin) as vlin FROM  (SELECT * FROM Xvals WHERE UXTIME BETWEEN :DateBegin AND :DateEnd)it GROUP BY FLOOR(:RES * Uxtime/(:DateEnd -:DateBegin))', 'archive', NULL),
(2, 'MONITOR', 'SELECT MIN(Uxtime) as XValue, MAX(dtm_upd) as dtm_upd, AVG(c3) as c3, AVG(c4) as c4, AVG(c5) as c5, AVG(c6) as c6, AVG(c7) as c7\r\n, AVG(c8) as c8, AVG(c9) as c9, AVG(c10) as c10, AVG(c11) as c11, AVG(c12) as c12, \r\nAVG(c13) as c13, AVG(c14) as c14, AVG(c15) as c15, AVG(c16) as c16, AVG(c17) as c17\r\n, AVG(c18) as c18, AVG(c19) as c19, AVG(c20) as c20, AVG(c21) as c21, AVG(c22) as c22\r\n, AVG(c23) as c23, AVG(c24) as c24, AVG(c25) as c25, AVG(c26) as c26, AVG(vlin) as vlin\r\nFROM  (SELECT * FROM Xvals WHERE UXTIME > UNIX_TIMESTAMP() - :MINS * 60 )it GROUP BY FLOOR(:RES * Uxtime/(:MINS * 60))', 'monitor', NULL),
(3, 'PERIODICAL1', '', 'periodical', NULL),
(4, 'PERIODICAL2', '', 'periodical', NULL),
(5, 'PERIODICAL3', '', 'periodical', NULL),
(6, 'PIECES', 'SELECT MIN(Uxtime) as XValue, MAX(dtm_upd) as dtm_upd, AVG(c3) as c3, AVG(c4) as c4, AVG(c5) as c5, AVG(c6) as c6, AVG(c7) as c7\r\n, AVG(c8) as c8, AVG(c9) as c9, AVG(c10) as c10, AVG(c11) as c11, AVG(c12) as c12, \r\nAVG(c13) as c13, AVG(c14) as c14, AVG(c15) as c15, AVG(c16) as c16, AVG(c17) as c17\r\n, AVG(c18) as c18, AVG(c19) as c19, AVG(c20) as c20, AVG(c21) as c21, AVG(c22) as c22\r\n, AVG(c23) as c23, AVG(c24) as c24, AVG(c25) as c25, AVG(c26) as c26, AVG(vlin) as vlin\r\nFROM  (SELECT * FROM Xvals WHERE piece_id=:PID )it GROUP BY FLOOR(:RES * meter / 1)', 'pieces', NULL),
(7, 'PIECES_LIST', 'SELECT p.*, CASE WHEN 1>2 THEN ''1st case'' ELSE ''????'' END as Description FROM Pieces p', 'pieces', NULL);
