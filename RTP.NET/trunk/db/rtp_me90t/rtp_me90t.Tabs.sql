-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Час створення: Квт 17 2012 р., 06:56
-- Версія сервера: 5.5.8
-- Версія PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- БД: `rtp_demo`
--

-- --------------------------------------------------------

--
-- Структура таблиці `Tabs`
--

CREATE TABLE IF NOT EXISTS `Tabs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num` int(11) NOT NULL,
  `Title` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'monitor',
  `query_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'MONITOR',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Дамп даних таблиці `Tabs`
--

INSERT INTO `Tabs` (`id`, `num`, `Title`, `alias`, `query_name`) VALUES
(2, 0, 'Монитор', 'monitor', 'MONITOR'),
(4, 1, 'отрезки', 'pieces', 'PIECES'),
(5, 2, 'архив', 'archive', 'ARCHIVE');
