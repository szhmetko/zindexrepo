-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Час створення: Квт 17 2012 р., 06:57
-- Версія сервера: 5.5.8
-- Версія PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- БД: `rtp_demo`
--

-- --------------------------------------------------------

--
-- Структура таблиці `Curves`
--

CREATE TABLE IF NOT EXISTS `Curves` (
  `rec_id` int(11) NOT NULL AUTO_INCREMENT,
  `dchl_num` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL DEFAULT '0',
  `bPrimary` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`rec_id`),
  KEY `FK_CURVES_CHART` (`group_id`),
  KEY `FK_CURVES_CHANNELS` (`dchl_num`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=53 ;

--
-- Дамп даних таблиці `Curves`
--

INSERT INTO `Curves` (`rec_id`, `dchl_num`, `group_id`, `bPrimary`) VALUES
(1, 2, 1, 1),
(2, 3, 1, 1),
(3, 4, 1, 1),
(5, 9, 5, 1),
(7, 5, 1, 1),
(8, 6, 1, 1),
(9, 7, 1, 1),
(10, 8, 1, 1),
(11, 10, 5, 0),
(16, 11, 8, 1),
(17, 12, 8, 1),
(19, 15, 8, 1),
(22, 14, 1, 0),
(23, 9, 4, 1),
(24, 10, 4, 0),
(25, 11, 3, 1),
(26, 12, 3, 1),
(27, 15, 3, 1),
(28, 2, 2, 1),
(29, 3, 2, 1),
(30, 4, 2, 1),
(31, 5, 2, 1),
(32, 6, 2, 1),
(33, 7, 2, 1),
(34, 8, 2, 1),
(35, 14, 2, 0),
(36, 9, 7, 1),
(37, 10, 7, 0),
(38, 2, 6, 1),
(39, 3, 6, 1),
(40, 4, 6, 1),
(41, 5, 6, 1),
(42, 6, 6, 1),
(43, 7, 6, 1),
(44, 8, 6, 1),
(46, 14, 6, 1),
(47, 11, 9, 1),
(48, 12, 9, 1),
(49, 15, 9, 1),
(50, 13, 5, 1),
(51, 13, 7, 1),
(52, 13, 4, 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Curves`
--
ALTER TABLE `Curves`
  ADD CONSTRAINT `FK_CURVES_CHANNELS` FOREIGN KEY (`dchl_num`) REFERENCES `channels` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_CURVES_CHART` FOREIGN KEY (`group_id`) REFERENCES `tabchannels` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
