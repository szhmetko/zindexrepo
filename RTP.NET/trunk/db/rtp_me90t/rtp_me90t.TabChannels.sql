-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Час створення: Квт 17 2012 р., 06:56
-- Версія сервера: 5.5.8
-- Версія PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- БД: `rtp_demo`
--

-- --------------------------------------------------------

--
-- Структура таблиці `TabChannels`
--

CREATE TABLE IF NOT EXISTS `TabChannels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tab_id` int(11) NOT NULL DEFAULT '0',
  `alias` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'velocity',
  `main_title` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No title',
  `primary_units` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sec_units` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bgcolor` int(11) NOT NULL DEFAULT '16777215',
  `prim_low_lim` float NOT NULL DEFAULT '0',
  `sec_low_lim` float NOT NULL DEFAULT '0',
  `prim_high_lim` float NOT NULL DEFAULT '0',
  `sec_high_lim` float NOT NULL DEFAULT '0',
  `num` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_TABS_CHARTS` (`tab_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Дамп даних таблиці `TabChannels`
--

INSERT INTO `TabChannels` (`id`, `tab_id`, `alias`, `main_title`, `primary_units`, `sec_units`, `bgcolor`, `prim_low_lim`, `sec_low_lim`, `prim_high_lim`, `sec_high_lim`, `num`) VALUES
(1, 2, 'temperatures', 'Температура', 'гр.Цельсия', 'гр.Цельсия', 16642303, 0, 0, 300, 300, 0),
(2, 4, 'temperatures', 'температуры', 'гр.Цельсия', 'гр.Цельсия', 16575231, 0, 0, 300, 300, 0),
(3, 4, 'diameter', 'диаметр, пробои', 'мм', 'мм', 16575674, 0.5, 0.5, 0.5, 0.5, 1),
(4, 4, 'velocity', 'скорости', 'об/мин,100%', 'м/мин', 12910591, 0, 0, 100, 200, 2),
(5, 2, 'velocity', 'скорости', 'об/мин, А,%', 'м/мин', 12713970, 0, 0, 100, 200, 1),
(6, 5, 'temperatures', 'температуры', 'гр.Цельсия', 'гр.Цельсия', 16575231, 0, 0, 300, 300, 0),
(7, 5, 'velocity', 'скорости', 'об/мин,100%, A', 'м/мин', 13303807, 0, 0, 100, 200, 1),
(8, 2, 'diameter', 'диам', '', '', 15793869, 0.5, 0.5, 0.5, 0.5, 2),
(9, 5, 'diameter', 'диаметр,пробои', 'мм', 'мм', 15263952, 0.5, 0.5, 0.5, 0.5, 2);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `TabChannels`
--
ALTER TABLE `TabChannels`
  ADD CONSTRAINT `FK_TABS_CHARTS` FOREIGN KEY (`tab_id`) REFERENCES `tabs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
