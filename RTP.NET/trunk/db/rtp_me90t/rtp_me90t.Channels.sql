-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Час створення: Квт 17 2012 р., 05:51
-- Версія сервера: 5.5.8
-- Версія PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- БД: `rtp_demo`
--

-- --------------------------------------------------------

--
-- Структура таблиці `Channels`
--

CREATE TABLE IF NOT EXISTS `Channels` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(64) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `rTpairId` int(11) NOT NULL DEFAULT '0',
  `clr` int(11) NOT NULL DEFAULT '0',
  `dt_ins` datetime NOT NULL,
  `k` float NOT NULL DEFAULT '1',
  `fname` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dt_upd` datetime NOT NULL,
  `Expression` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Дамп даних таблиці `Channels`
--

INSERT INTO `Channels` (`Id`, `Title`, `rTpairId`, `clr`, `dt_ins`, `k`, `fname`, `dt_upd`, `Expression`) VALUES
(1, 'метраж', 0, 2928128, '2012-03-22 14:34:45', 1, 'meter', '2012-03-22 14:34:45', 'IF(CAST(CALCULATED[0][28], INT) = CAST(CALCULATED[1][28], INT), CAST(CALCULATED[1][1], INT) + (((raw[0][2] >> 8) AND 0x3C) >> 2), 0) + AddMeters2Cache(((raw[0][2] >> 8) AND 0x3C) >> 2)*0'),
(2, 'зона7', 0, 12566272, '2012-03-22 14:34:45', 0.22, 'c10', '2012-03-22 14:34:45', 'raw[0][1]'),
(3, 'зона6', 0, 4227327, '2012-03-22 14:34:45', 0.22, 'c9', '2012-03-22 14:34:45', 'raw[0][1]'),
(4, 'зона5', 0, 16760767, '2012-03-22 14:34:45', 0.24, 'c8', '2012-03-22 14:34:45', 'raw[0][1]'),
(5, 'зона4', 0, 12687142, '2012-03-22 14:34:45', 0.23, 'c7', '2012-03-22 14:34:45', 'RAW[0][5]'),
(6, 'зона3', 0, 1073805, '2012-03-22 14:34:45', 0.225, 'c6', '2012-03-22 14:34:45', 'raw[0][1]'),
(7, 'зона2', 0, 2992878, '2012-03-22 14:34:45', 0.22, 'c5', '2012-03-22 14:34:45', 'raw[0][1]'),
(8, 'зона1', 0, 55255, '2012-03-22 14:34:45', 0.2, 'c4', '2012-03-22 14:34:45', 'raw[0][1]'),
(9, 'Vшн', 0, 0, '2012-03-22 14:34:45', 0.098, 'c3', '2012-03-22 14:34:45', 'raw[0][1]'),
(10, 'Vлин', 0, 14248960, '2012-03-22 14:34:45', 1, 'vlin', '2012-03-22 14:34:45', 'If( (raw[0][1] AND 8191) > 0, 36621.7/(raw[0][1] AND 8191), 0.0)'),
(11, 'Диам. эталон.', 0, 5878528, '2012-03-22 14:34:45', 0.01, 'c25', '2012-03-22 14:34:45', 'raw[0][1]'),
(12, 'Диам. измерен', 0, 10050598, '2012-03-22 14:34:45', 0.01, 'c26', '2012-03-22 14:34:45', 'raw[0][1]'),
(13, 'Ток шнека', 0, 10946233, '2012-03-22 14:34:45', 1, 'c11', '2012-03-22 14:34:45', 'RAW[0][5]'),
(14, 'Tхол. спая', 0, 0, '2012-03-22 14:34:45', 1, 'c24', '2012-03-22 14:34:45', 'raw[0][1]'),
(15, 'пробои', 0, 475889, '2012-03-22 14:34:45', 1, 'prob', '2012-03-22 14:34:45', 'raw[0][2]'),
(16, 'Id ???????', 0, 0, '2012-04-15 00:00:00', 1, 'pieceId', '2012-04-16 00:00:00', 'IF( (raw[0][2] AND 0x0002) =0x0002, AddPiece(), CAST(calculated[1][28], INT))');
