CREATE DATABASE `rtp_me125` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE  TABLE  `rtp_me125`.`Channels` (  
	`Id` int( 11  )  NOT  NULL  AUTO_INCREMENT,
	`Title` varchar( 64  )  CHARACTER  SET utf8 NOT  NULL DEFAULT  '',
 
	`rTpairId` int( 11  )  NOT  NULL DEFAULT  '0',
	`clr` int( 11  )  NOT  NULL DEFAULT  '0',
	`dt_ins` datetime NOT  NULL,
	`k` float NOT  NULL DEFAULT  '1',

	`fname` varchar( 16  )  COLLATE utf8_unicode_ci NOT  NULL DEFAULT  '',
	`dt_upd` datetime NOT  NULL,
	`Expression` varchar( 1024  )  COLLATE utf8_unicode_ci  DEFAULT NULL,
	PRIMARY  KEY (  `Id`  )  
) ENGINE  = InnoDB  DEFAULT CHARSET  = utf8 COLLATE  = utf8_unicode_ci;



CREATE  TABLE  `rtp_me125`.`Tabs` (
	`id` int( 11  )  NOT  NULL  AUTO_INCREMENT ,

	`num` int( 11  )  NOT  NULL ,
	`Title` varchar( 64  )  COLLATE utf8_unicode_ci NOT  NULL DEFAULT  '',

	`alias` varchar( 16  )  COLLATE utf8_unicode_ci NOT  NULL DEFAULT  'monitor',

	`query_name` varchar( 32  )  COLLATE utf8_unicode_ci NOT  NULL DEFAULT  'MONITOR',
	PRIMARY  KEY (  `id`  ) 
) ENGINE  = InnoDB  DEFAULT CHARSET  = utf8 COLLATE  = utf8_unicode_ci;


CREATE  TABLE  `rtp_me125`.`TabChannels` (
	`id` int( 11  )  NOT  NULL  AUTO_INCREMENT ,

	`tab_id` int( 11  )  NOT  NULL DEFAULT  '0',

	`alias` varchar( 16  )  COLLATE utf8_unicode_ci NOT  NULL DEFAULT  'velocity',
	`main_title` varchar( 32  )  COLLATE utf8_unicode_ci NOT  NULL DEFAULT  'No title',

	`primary_units` varchar( 32  )  COLLATE utf8_unicode_ci NOT  NULL DEFAULT  '',

	`sec_units` varchar( 32  )  COLLATE utf8_unicode_ci NOT  NULL DEFAULT  '',

	`bgcolor` int( 11  )  NOT  NULL DEFAULT  '16777215',

	`prim_low_lim` float NOT  NULL DEFAULT  '0',

	`sec_low_lim` float NOT  NULL DEFAULT  '0',

	`prim_high_lim` float NOT  NULL DEFAULT  '0',

	`sec_high_lim` float NOT  NULL DEFAULT  '0',

	`num` int( 11  )  NOT  NULL DEFAULT  '0',
 
	PRIMARY  KEY (  `id`  )  
) ENGINE  = InnoDB  DEFAULT CHARSET  = utf8 COLLATE  = utf8_unicode_ci;



ALTER TABLE `rtp_me125`.`TabChannels`   ADD CONSTRAINT `FK_TABS_CHARTS`  FOREIGN KEY (`tab_id` )  REFERENCES `rtp_me125`.`Tabs` (`id` )  ON DELETE NO ACTION  ON UPDATE NO ACTION, ADD INDEX `FK_TABS_CHARTS` (`tab_id` ASC) ;

CREATE  TABLE  `rtp_me125`.`Curves` (  
	`rec_id` int( 11  )  NOT  NULL  AUTO_INCREMENT,
	`dchl_num` int( 11  )  NOT  NULL DEFAULT  '0',
	`group_id` int( 11  )  NOT  NULL DEFAULT  '0',
	`bPrimary` tinyint( 1  )  NOT  NULL DEFAULT  '1',
 
	PRIMARY  KEY (  `rec_id`  )  
) ENGINE  = InnoDB  DEFAULT CHARSET  = utf8 COLLATE  = utf8_unicode_ci
;

ALTER TABLE `rtp_me125`.`Curves`   ADD CONSTRAINT `FK_CURVES_CHART`  FOREIGN KEY (`group_id` )  REFERENCES `rtp_me125`.`TabChannels` (`id` )  ON DELETE NO ACTION  ON UPDATE NO ACTION, ADD INDEX `FK_CURVES_CHART` (`group_id` ASC) ;

ALTER TABLE `rtp_me125`.`Curves`   ADD CONSTRAINT `FK_CURVES_CHANNELS`  FOREIGN KEY (`dchl_num` )  REFERENCES `rtp_me125`.`Channels` (`Id` )  ON DELETE NO ACTION  ON UPDATE NO ACTION, ADD INDEX `FK_CURVES_CHANNELS` (`dchl_num` ASC) ;CREATE  TABLE  `rtp_me125`.`Pieces` (  
	`Id` int( 11  )  NOT  NULL ,
	`dtOn` datetime NOT  NULL DEFAULT  '1900-01-01 00:00:00',

	`dtOff` datetime NOT  NULL DEFAULT  '1900-01-01 00:00:00',

	`Length` int( 11  )  NOT  NULL DEFAULT  '0',
 
	PRIMARY  KEY (  `Id`  ) ,

	KEY  `IDX_cablePieces_dtm_on` (  `dtOn`  )  
) ENGINE  = InnoDB  DEFAULT CHARSET  = utf8 COLLATE  = utf8_unicode_ci;


CREATE  TABLE  `rtp_me125`.`PieceDescription` (  
	`rec_id` int( 11  )  NOT  NULL  AUTO_INCREMENT ,

	`piece_id` int( 11  )  NOT  NULL DEFAULT  '-1',
 
	`paramName` varchar( 64  )  COLLATE utf8_unicode_ci NOT  NULL DEFAULT  '',

	`paramVal` varchar( 128  )  COLLATE utf8_unicode_ci NOT  NULL DEFAULT  '',

	`dt_upd` datetime NOT  NULL DEFAULT  '1900-01-01 00:00:00',
	PRIMARY  KEY (  `rec_id`  ) ,

	KEY  `IX_PieceID_PieceDescription` (  `piece_id`  )  
) ENGINE  = InnoDB  DEFAULT CHARSET  = utf8 COLLATE  = utf8_unicode_ci;


ALTER TABLE `rtp_me125`.`PieceDescription`   ADD CONSTRAINT `FK_PIECES_DESC_PIECES`  FOREIGN KEY (`piece_id` )  REFERENCES `rtp_me125`.`Pieces` (`Id` )  ON DELETE NO ACTION  ON UPDATE NO ACTION, ADD INDEX `FK_PIECES_DESC_PIECES` (`piece_id`) ;

CREATE  TABLE  `rtp_me125`.`Queries` (
	`Id` int( 11  )  NOT  NULL ,
	`key` varchar( 16  )  NOT  NULL ,
	`query` text NOT  NULL ,
	`group` varchar( 16  )  NOT  NULL  COMMENT  'client',
	`lastDt` datetime  DEFAULT NULL ,
	PRIMARY  KEY (  `Id`  ) ,
	UNIQUE  KEY  `key_UNIQUE` (  `key`  )  
) ENGINE  = InnoDB  DEFAULT CHARSET  = latin1;

ALTER TABLE `rtp_me125`.`Queries` CHANGE COLUMN `Id` `Id` INT(11) NOT NULL AUTO_INCREMENT  ;

CREATE  TABLE  `rtp_me125`.`Xvals` (
	`Id` bigint( 11  )  NOT  NULL  AUTO_INCREMENT,
	`uxtime` bigint( 20  )  DEFAULT NULL ,
	`meter` int( 11  )  NOT  NULL DEFAULT  '0',
	`prob` int( 11  )  NOT  NULL DEFAULT  '0',
	`pieceId` int( 11  )  NOT  NULL DEFAULT  '0',
	`DataType` int( 11  )  NOT  NULL ,
	`c1` float  DEFAULT NULL ,

	`c2` float  DEFAULT NULL ,
	`c3` float  DEFAULT NULL ,

	`c4` float  DEFAULT NULL ,
	`c5` float  DEFAULT NULL ,
	`c6` float  DEFAULT NULL ,

	`c7` float  DEFAULT NULL ,
	`c8` float  DEFAULT NULL ,
	`c9` float  DEFAULT NULL ,
	`c10` float  DEFAULT NULL ,

	`c11` float  DEFAULT NULL ,
	`c12` float  DEFAULT NULL ,
	`c13` float  DEFAULT NULL ,

	`c14` float  DEFAULT NULL ,
	`c15` float  DEFAULT NULL ,

	`c16` float  DEFAULT NULL ,

	`c17` float  DEFAULT NULL ,
	`c18` float  DEFAULT NULL ,
	`c19` float  DEFAULT NULL ,

	`c20` float  DEFAULT NULL ,
	`c21` float  DEFAULT NULL ,
	`c22` float  DEFAULT NULL ,

	`c23` float  DEFAULT NULL ,
	`c24` float  DEFAULT NULL ,
	`c25` float  DEFAULT NULL ,

	`c26` float  DEFAULT NULL ,
	`c27` float  DEFAULT NULL ,
	`c28` float  DEFAULT NULL ,
	`c29` float  DEFAULT NULL ,
	`c30` float  DEFAULT NULL ,
	`c31` float  DEFAULT NULL ,
	`c32` float  DEFAULT NULL ,
	`vlin` float  DEFAULT NULL ,
	`dtm_upd` timestamp NULL  DEFAULT CURRENT_TIMESTAMP ,

	PRIMARY  KEY (  `Id`  ) ,
	KEY  `IDX_Xvals_Uxtime` (  `uxtime`  ) ,
	KEY  `IDX_XVals_piece_id` (  `pieceId`  )  
) ENGINE  = InnoDB  DEFAULT CHARSET  = utf8;
