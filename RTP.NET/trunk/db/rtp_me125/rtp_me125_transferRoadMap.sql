INSERT INTO rtp_me125.Channels (Id, Title, rTpairId, clr, dt_ins, k, fname, dt_upd, Expression)
select chl_id as Id, `name` as Title, 
(SELECT ct.tpair_id from me125_dbo.chl2TPairs ct WHERE ct.chl_num=c.chl_id) as TPairId,
clr, NOW() as dt_ins, k, fname, NOW() as dt_ins, 'raw[0][1]' as Expression 
from me125_dbo.Channels c;


INSERT INTO rtp_me125.Tabs
SELECT `id`, num, caption as Title, alias, 'MONITOR' as query_name FROM me125_dbo.Tabs;


DELETE FROM me125_dbo.TabChannels WHERE tab_id NOT IN (SELECT id FROM rtp_me125.Tabs);

INSERT INTO rtp_me125.TabChannels
(`id`, tab_id, alias, main_title, primary_units, sec_units, bgcolor
, prim_low_lim, sec_low_lim, prim_high_lim, sec_high_lim, num)
SELECT `id`, tab_id, alias, main_title, IFNULL(primary_units, ''), IFNULL(sec_units, ''), bgcolor
, prim_low_lim, sec_low_lim, prim_high_lim, sec_high_lim, num FROM me125_dbo.TabChannels;

DELETE FROM me125_dbo.Curves WHERE group_id NOT IN (SELECT id FROM rtp_me125.TabChannels);

INSERT INTO rtp_me125.Curves (rec_id, dchl_num, group_id, bPrimary)
SELECT rec_id, dchl_num, group_id, ABS(bPrimary) FROM me125_dbo.Curves;



INSERT INTO rtp_me125.`Queries` (`Id`, `key`, `query`, `group`, `lastDt`) VALUES
(1, 'ARCHIVE', 'SELECT MIN(Uxtime) as XValue, MAX(dtm_upd) as dtm_upd, AVG(c3) as c3, AVG(c4) as c4, AVG(c5) as c5, AVG(c6) as c6, AVG(c7) as c7, AVG(c8) as c8, AVG(c9) as c9, AVG(c10) as c10, AVG(c11) as c11, AVG(c12) as c12, AVG(c13) as c13, AVG(c14) as c14, AVG(c15) as c15, AVG(c16) as c16, AVG(c17) as c17, AVG(c18) as c18, AVG(c19) as c19, AVG(c20) as c20, AVG(c21) as c21, AVG(c22) as c22, AVG(c23) as c23, AVG(c24) as c24, AVG(c25) as c25, AVG(c26) as c26, AVG(vlin) as vlin FROM  (SELECT * FROM Xvals WHERE UXTIME BETWEEN :DateBegin AND :DateEnd)it GROUP BY FLOOR(:RES * Uxtime/(:DateEnd -:DateBegin))', 'archive', NULL),
(2, 'MONITOR', 'SELECT MIN(Uxtime) as XValue, MAX(dtm_upd) as dtm_upd, AVG(c3) as c3, AVG(c4) as c4, AVG(c5) as c5, AVG(c6) as c6, AVG(c7) as c7\r\n, AVG(c8) as c8, AVG(c9) as c9, AVG(c10) as c10, AVG(c11) as c11, AVG(c12) as c12, \r\nAVG(c13) as c13, AVG(c14) as c14, AVG(c15) as c15, AVG(c16) as c16, AVG(c17) as c17\r\n, AVG(c18) as c18, AVG(c19) as c19, AVG(c20) as c20, AVG(c21) as c21, AVG(c22) as c22\r\n, AVG(c23) as c23, AVG(c24) as c24, AVG(c25) as c25, AVG(c26) as c26, AVG(vlin) as vlin\r\nFROM  (SELECT * FROM Xvals WHERE UXTIME > UNIX_TIMESTAMP() - :MINS * 60 )it GROUP BY FLOOR(:RES * Uxtime/(:MINS * 60))', 'monitor', NULL),
(3, 'PERIODICAL1', '', 'periodical', NULL),
(4, 'PERIODICAL2', '', 'periodical', NULL),
(5, 'PERIODICAL3', '', 'periodical', NULL),
(6, 'PIECES', 'SELECT MIN(Uxtime) as XValue, MAX(dtm_upd) as dtm_upd, AVG(c3) as c3, AVG(c4) as c4, AVG(c5) as c5, AVG(c6) as c6, AVG(c7) as c7\r\n, AVG(c8) as c8, AVG(c9) as c9, AVG(c10) as c10, AVG(c11) as c11, AVG(c12) as c12, \r\nAVG(c13) as c13, AVG(c14) as c14, AVG(c15) as c15, AVG(c16) as c16, AVG(c17) as c17\r\n, AVG(c18) as c18, AVG(c19) as c19, AVG(c20) as c20, AVG(c21) as c21, AVG(c22) as c22\r\n, AVG(c23) as c23, AVG(c24) as c24, AVG(c25) as c25, AVG(c26) as c26, AVG(vlin) as vlin\r\nFROM  (SELECT * FROM Xvals WHERE piece_id=:PID )it GROUP BY FLOOR(:RES * meter / 1)', 'pieces', NULL),
(7, 'PIECES_LIST', 'SELECT p.*, CASE WHEN 1>2 THEN ''1st case'' ELSE ''����'' END as Description FROM Pieces p', 'pieces', NULL);



INSERT INTO `Channels` ( `Title`, `rTpairId`, `clr`, `dt_ins`, `k`, `fname`, `dt_upd`, `Expression`) VALUES
( 'Id �������', 0, 0, '2012-04-15 00:00:00', 1, 'pieceId', '2012-04-16 00:00:00', 'IF( ((raw[0][2] AND 0x0002) =0x0002) OR (CAST(calculated[1][27], INT) = 0), AddPiece(), CAST(calculated[1][27], INT))');