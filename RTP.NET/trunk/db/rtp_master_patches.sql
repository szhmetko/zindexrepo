-- #1 START
ALTER TABLE `rtp_master`.`Lines` ADD COLUMN `ColdEndFieldName` VARCHAR(64) NULL COMMENT 'cold end field name'  AFTER `alias` ;
UPDATE `rtp_master`.`Lines` SET ColdEndFieldName='c22' WHERE 1=1;

ALTER TABLE `rtp_master`.`Approx` DROP FOREIGN KEY `FK_THERMOPAIR_ID` ;ALTER TABLE `rtp_master`.`Approx`   ADD CONSTRAINT `FK_THERMOPAIR_ID`  FOREIGN KEY (`rTpairId` )  REFERENCES `rtp_master`.`ThermoPair` (`Id` )  ON DELETE NO ACTION  ON UPDATE CASCADE;

UPDATE `rtp_master`.ThermoPair SET Id=0 WHERE Id=1;
UPDATE `rtp_master`.ThermoPair SET Id=1 WHERE Id=2;

-- #1 END