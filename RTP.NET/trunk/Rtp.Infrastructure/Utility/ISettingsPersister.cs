﻿
namespace Rtp.Infrastructure.Utility
{
    public interface ISettingsPersister
    {
        void AddItem(string key, object val);
        void Store();
        void Restore();
    }
}
