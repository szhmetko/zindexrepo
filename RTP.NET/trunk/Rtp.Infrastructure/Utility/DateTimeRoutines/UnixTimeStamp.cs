﻿using System;


namespace Rtp.Infrastructure.Utility.DateTimeRoutines
{

    public static class UnixTimeStamp
    {
        public static long ToUnixTimeStamp(this DateTime data)
        {
            TimeSpan span = (data - UnixEpochStart.ToUniversalTime());

            return (long)span.TotalSeconds;
        }

        static private DateTime UnixEpochStart
        {
            get { return new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc); }
        }

        public static DateTime UnixTimeStampToDateTime( long unixTimeStamp )
        {
            // Unix timestamp is seconds past epoch
            return UnixEpochStart.AddSeconds(unixTimeStamp).ToLocalTime();
        }


        /// <summary>
        /// Seconds from 01-01-1970 accordingly to UTC
        /// </summary>
        public static long UtcNow
        {
            get
            {
                TimeSpan span = (DateTime.UtcNow - UnixEpochStart.ToUniversalTime());
                
                return (long) span.TotalSeconds;
            }
        }

        /// <summary>
        /// Seconds from 01-01-1970 accordingly to localtime
        /// </summary>
        public static long Now
        {
            get
            {
                TimeSpan span = (DateTime.Now - UnixEpochStart.ToLocalTime());

                return (long)span.TotalSeconds;
            }
        }
    }
}
