﻿using C5;

namespace Rtp.Infrastructure.Utility
{
    public class LimitedQueue<T> : CircularQueue<T>
    {
        public LimitedQueue(int limit)
            : base(limit)
        {
            Limit = limit;
        }

        public LimitedQueue(double limit) : this((int) limit)
        {
        }

        protected int Limit { get; private set; }

        public override void Enqueue(T item)
        {
            if (Count >= Limit)
                Dequeue();

            base.Enqueue(item);
        }

        public void Clear()
        {
            while (Count > 0)
                Dequeue();
        }

        public T Peek()
        {
            return this[Count - 1];
        }
    }
}