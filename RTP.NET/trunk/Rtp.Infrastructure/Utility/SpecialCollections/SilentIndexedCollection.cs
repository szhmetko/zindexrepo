﻿namespace Rtp.Infrastructure.Utility
{
    public delegate T FetchDelegate<T, TKey>(TKey i);

    public class SilentIndexedCollection<TValue, TKey>
    {
        private readonly FetchDelegate<TValue, TKey> _delegate;

        public SilentIndexedCollection(FetchDelegate<TValue, TKey> @delegate)
        {
            _delegate = @delegate;
        }

        public TValue this[TKey i]
        {
            get { return _delegate(i); }
        }
    }
}