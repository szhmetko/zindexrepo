﻿using System;
using System.Xml.Serialization;

namespace Rtp.Infrastructure.Utility.DatacollectorSettings
{
    [XmlRoot(ElementName = "Datacollector")]
    [Serializable]
    public class GenericDatacollectorSettings : XmlObjectState
    {
        [XmlAttributeAttribute(AttributeName = "type")]
        virtual public DatacollectorType Type
        {
            get; set;
        }
    }
}
