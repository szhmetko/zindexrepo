﻿
using System.Xml.Serialization;

namespace Rtp.Infrastructure.Utility.DatacollectorSettings
{
    [XmlRoot(ElementName = "Datacollector")]
    public class TcpDatacollectorSettings : UdpDatacollectorSettings
    {
        [XmlAttribute(AttributeName = "type")]
        public override DatacollectorType Type
        {
            get { return DatacollectorType.Tcp; }
            set { }
        }
    }
}
