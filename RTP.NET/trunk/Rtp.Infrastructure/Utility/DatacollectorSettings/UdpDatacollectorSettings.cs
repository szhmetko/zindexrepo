﻿using System;
using System.Xml.Serialization;

namespace Rtp.Infrastructure.Utility.DatacollectorSettings
{
    [XmlRoot(ElementName = "Datacollector")]
    [Serializable]
    public class UdpDatacollectorSettings : GenericDatacollectorSettings
    {
        #region Fields to be serialized

        [XmlAttributeAttribute(AttributeName = "port")]
        public int Port
        {
            set;
            get;
        }

        [XmlAttributeAttribute(AttributeName = "host")]
        public string Host
        {
            set;
            get;
        }

        #endregion

        [XmlAttributeAttribute(AttributeName = "type")]
        public override DatacollectorType Type
        {
            get { return DatacollectorType.Udp; }
            set {  }
        }
    }
}
