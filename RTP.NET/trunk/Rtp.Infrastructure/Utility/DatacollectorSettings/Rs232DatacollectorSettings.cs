﻿using System;
using System.IO.Ports;
using System.Xml.Serialization;

namespace Rtp.Infrastructure.Utility.DatacollectorSettings
{
    [XmlRoot(ElementName = "Datacollector")]
    [Serializable]
    public class Rs232DatacollectorSettings : GenericDatacollectorSettings
    {
        #region Fields to be serialized

        [XmlAttributeAttribute(AttributeName = "com")]
        public string Com
        {
            set;
            get;
        }

        [XmlAttributeAttribute(AttributeName = "baud")]
        public int Baud
        {
            set;
            get;
        }

        [XmlAttributeAttribute(AttributeName = "parity")]
        public Parity Parity
        {
            set;
            get;
        }

        [XmlAttributeAttribute(AttributeName = "databits")]
        public int DataBits
        {
            set;
            get;
        }

        [XmlAttributeAttribute(AttributeName = "stopbits")]
        public StopBits StopBits
        {
            set;
            get;
        }

        [XmlAttributeAttribute(AttributeName = "type")]
        public override DatacollectorType Type
        {
            get { return DatacollectorType.Rs232; }
            set { }
        }

        #endregion
    }
}
