﻿
namespace Rtp.Infrastructure.Utility.DatacollectorSettings
{
    public enum DatacollectorType
    {
        Udp = 0,

        Tcp = 1,
        
        Rs232 = 2
    }
}
