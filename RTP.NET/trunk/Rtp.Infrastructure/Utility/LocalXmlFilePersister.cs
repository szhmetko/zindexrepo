﻿
using System;
using System.Data;
using System.IO;

namespace Rtp.Infrastructure.Utility
{

    public class LocalXmlFilePersister : ISettingsPersister
    {
        protected readonly string FilePath;

        private DataTable _data = new DataTable("Settings");

        private const string StrValueColumnName = "Value";
        private const string StrKeyColumnName = "Key";

        public LocalXmlFilePersister(string fileName)
        {
            FilePath = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + Path.DirectorySeparatorChar + fileName;

            _data.Columns.Add(StrKeyColumnName, typeof(string));
            _data.Columns.Add(StrValueColumnName);
        }

        public void AddItem(string key, object val)
        {
            var newItem = _data.NewRow();

            newItem[StrKeyColumnName] = key;
            newItem[StrValueColumnName] = val;

            _data.Rows.Add(newItem);
        }

        public void Store()
        {
            var dataSet = new DataSet(ToString());
            dataSet.Tables.Add(_data);
            dataSet.WriteXml(FilePath);
        }

        public void Restore()
        {
            var copy = new DataSet();
            
            copy.ReadXml(FilePath);

            if (copy.Tables.Count > 0)
                _data = copy.Tables[0].Copy();

            else throw new FileLoadException("Config file has no data.");

        }

        public T GetValue<T>(string key)
        {
            var result = _data.Select(string.Format("StrKeyColumnName={0}", key));

            if (result.Length > 0)
                return (T) result[0][StrValueColumnName];

            return default(T);
        }


    }
}
