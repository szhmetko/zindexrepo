﻿using System.IO;
using System.Text;
using System.Xml;

namespace Rtp.Infrastructure.Utility
{
    /// <summary>
    /// 
    /// </summary>
    public class SpecialXmlWriter : XmlTextWriter
    {
        bool m_includeStartDocument = true;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tw"></param>
        /// <param name="includeStartDocument"></param>
        public SpecialXmlWriter(TextWriter tw,
                    bool includeStartDocument)
            : base(tw)
        {
            m_includeStartDocument = includeStartDocument;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="encoding"></param>
        /// <param name="includeStartDocument"></param>
        public SpecialXmlWriter(Stream sw, Encoding encoding,
                     bool includeStartDocument)
            : base(sw, null)
        {
            m_includeStartDocument = includeStartDocument;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="encoding"></param>
        /// <param name="includeStartDocument"></param>
        public SpecialXmlWriter(string filePath, Encoding encoding,
                  bool includeStartDocument)
            : base(filePath, null)
        {
            m_includeStartDocument = includeStartDocument;
        }

        /// <summary>
        /// 
        /// </summary>
        public override void WriteStartDocument()
        {
            if (m_includeStartDocument)
            {
                base.WriteStartDocument();
            }
        }
    }
}
