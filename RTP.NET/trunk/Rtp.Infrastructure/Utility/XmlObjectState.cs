﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace Rtp.Infrastructure.Utility
{
    public class XmlObjectState
    {
        /// <summary>
        /// Makes serialization object to XML string
        /// </summary>
        /// <returns></returns>
        public string ToXml()
        {
            Object obj = this;
            SpecialXmlWriter stWriter = null;
            StringWriter sw = null;
            string buffer;
            try
            {
                var xmlSerializer = new XmlSerializer(obj.GetType());
                sw = new StringWriter();
                stWriter = new SpecialXmlWriter(sw, false);

                var xs = new XmlSerializerNamespaces();

                //To remove namespace and any other inline information tag                      
                xs.Add(string.Empty, string.Empty);
                xmlSerializer.Serialize(stWriter, obj, xs);

                buffer = sw.ToString();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if (stWriter != null) stWriter.Close();
                if (sw != null) sw.Close();
            }
            return buffer.TrimEnd(new[] { '\0' });
        }

        /// <summary>
        /// Performs object restoration from XML string.
        /// </summary>
        /// <param name="xmlString"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static T XmlTo<T>(string xmlString)
        {
            XmlSerializer xmlSerializer;
            //MemoryStream memStream = null;
            StringReader reader = null;

            try
            {
                xmlSerializer = new XmlSerializer(typeof(T));
                reader = new StringReader(xmlString);

                return (T)xmlSerializer.Deserialize(reader);
            }
            catch (InvalidOperationException ex)
            {
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            return default(T);
        }

    }

}
