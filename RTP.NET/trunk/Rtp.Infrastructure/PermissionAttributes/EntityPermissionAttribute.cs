﻿using System;

namespace Rtp.Infrastructure.PermissionAttributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class EntityPermissionAttribute : Attribute
    {
        public string EntityKey {  set; get; }

    }
}
