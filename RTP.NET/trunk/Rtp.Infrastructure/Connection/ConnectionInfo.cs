﻿using Rtp.Infrastructure.Connection;

namespace Rtp.Infrastructure
{
    public class ConnectionInfo : IConnectionInfo
    {
        public ConnectionInfo(string dbName, string userName, string hostName, string password)
        {
            DatabaseName = dbName;
            User = userName;
            Host = hostName;
            Password = password;
        }

        protected string DatabaseName { get; set; }
        protected string User { get; set; }
        protected string Host { get; set; }
        protected string Password { get; set; }

        public string ConnectionString
        {
            get { return string.Format(StrConnectionstringPattern, DatabaseName, Host, User, Password); }
        }

        public virtual void Store(string fileName)
        {}

        public virtual bool IsDefault
        {
            get { return this is DefaultConnection ; }
        }

        private const string StrConnectionstringPattern =
            "database={0};server={1};uid={2};pwd={3};charset=utf8";
    }
}
