﻿
namespace Rtp.Infrastructure
{
    public interface IConnectionInfo
    {
        string ConnectionString { get; }
        void Store(string fileName);
        bool IsDefault { get; }
    }
}
