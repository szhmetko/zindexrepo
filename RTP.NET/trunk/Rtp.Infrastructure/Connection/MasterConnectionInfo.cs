﻿using System.IO;
using Rtp.Infrastructure.Utility;

namespace Rtp.Infrastructure.Connection
{
    public class MasterConnectionInfo : ConnectionInfo
    {
        internal MasterConnectionInfo(string dbName, string userName, string hostName, string password) : base(dbName, userName, hostName, password)
        {
        }

        public static IConnectionInfo Create(string fileName)
        {
            try
            {
                var xmlPersister = new LocalXmlFilePersister(fileName);
                xmlPersister.Restore();

                var host = xmlPersister.GetValue<string>("MasterServer");
                var userName = xmlPersister.GetValue<string>("MasterUser");
                var pwd = xmlPersister.GetValue<string>("MasterPwd");
                var dbName = xmlPersister.GetValue<string>("MasterDb");

                return new MasterConnectionInfo(dbName, userName, host, pwd);
            }
            catch(FileNotFoundException)
            {
            }

            return new DefaultConnection();

        }

        public override void Store(string fileName)
        {
            var xmlPersister = new LocalXmlFilePersister(fileName);
            xmlPersister.Store();
        }
    }
}
