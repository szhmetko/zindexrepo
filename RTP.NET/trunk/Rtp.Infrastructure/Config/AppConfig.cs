﻿
using Rtp.Infrastructure.Connection;

namespace Rtp.Infrastructure.Config
{
    public sealed class AppConfig
    {
        #region Thread-safe, lazy Singleton

        /// <summary>
        /// This is a thread-safe, lazy singleton.  See http://www.yoda.arachsys.com/csharp/singleton.html
        /// for more details about its implementation.
        /// </summary>
        public static AppConfig Instance
        {
            get
            {
                return Nested.AppConfig;
            }
        }

        /// <summary>
        /// Private constructor to enforce singleton
        /// </summary>
        private AppConfig()
        {
        }

        /// <summary>
        /// Assists with ensuring thread-safe, lazy singleton
        /// </summary>
        private class Nested
        {
            // Explicit static constructor to tell C# compiler
            // not to mark type as before field init
            static Nested() { }
            internal static readonly AppConfig AppConfig =
                new AppConfig();
        }

        private IConnectionInfo _masterConnection;
        public IConnectionInfo MasterConnection
        { 
            get { return _masterConnection ?? (_masterConnection = MasterConnectionInfo.Create("Settings.rpt")); }
            set { _masterConnection = value;
                _masterConnection.Store("Settings.rpt");
            }
        }
        public IConnectionInfo CurentLineConnection { get; set; }

        public string CurrentRole
        {
            get { return System.Configuration.ConfigurationSettings.AppSettings["Role"];}
        }

        public string CurrentLineName { get; set; }

        #endregion

    }
}
