﻿using System;

namespace Rtp.Infrastructure.Exceptions
{
    public class DataCollectorInitializationException : RtpBaseException
    {
        public DataCollectorInitializationException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}