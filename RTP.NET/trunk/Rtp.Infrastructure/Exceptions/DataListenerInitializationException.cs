﻿using System;

namespace Rtp.Infrastructure.Exceptions
{
    public  class DataListenerInitializationException : RtpBaseException
    {
        public DataListenerInitializationException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}