﻿using System;

namespace Rtp.Infrastructure.Exceptions
{
    public class ChannelValueEvaluationException : RtpBaseException
    {
        public ChannelValueEvaluationException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}