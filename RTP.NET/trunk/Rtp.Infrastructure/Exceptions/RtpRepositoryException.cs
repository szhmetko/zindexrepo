﻿using System;

namespace Rtp.Infrastructure.Exceptions
{
    public class RtpRepositoryException : RtpBaseException
    {
        public RtpRepositoryException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}