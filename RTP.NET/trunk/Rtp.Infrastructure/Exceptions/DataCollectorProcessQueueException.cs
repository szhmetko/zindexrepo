﻿using System;

namespace Rtp.Infrastructure.Exceptions
{
    public class DataCollectorProcessQueueException : RtpBaseException
    {
        public DataCollectorProcessQueueException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}