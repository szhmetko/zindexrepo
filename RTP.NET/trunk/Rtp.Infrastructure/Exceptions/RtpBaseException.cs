﻿using System;

namespace Rtp.Infrastructure.Exceptions
{
    public class RtpBaseException : Exception
    {
        public RtpBaseException(string message, Exception inner)  : base(message, inner)
        {
        }
    }
}