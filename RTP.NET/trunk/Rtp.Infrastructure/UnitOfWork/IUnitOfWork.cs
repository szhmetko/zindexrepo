﻿
using System;

namespace Rtp.Infrastructure.UnitOfWork
{
    public interface IUnitOfWork    : IDisposable
    {
        void RegisterAmended(IAggregateRoot entity, IUnitOfWorkRepository unitofWorkRepository);
        void RegisterNew(IAggregateRoot entity, IUnitOfWorkRepository unitofWorkRepository);
        void RegisterRemoved(IAggregateRoot entity, IUnitOfWorkRepository unitofWorkRepository);
        void Commit();

        void Rollback();

        //string SessionKey { get; set; }
    }
}
