﻿using System;

namespace Rtp.Infrastructure.UIAttributes
{

    [AttributeUsage(AttributeTargets.Class)]
    public class GridSettingsAttribute : Attribute
    {
        public GridType GridType { get; private set; }
        public bool ShowHeader { get; private set; }

        public GridSettingsAttribute(GridType gridType, bool showHeader = true)
        {
            GridType = gridType;

            ShowHeader = showHeader;
        }

    }
}
