﻿using System;

namespace Rtp.Infrastructure.UIAttributes
{


    [AttributeUsage(AttributeTargets.Property)]
    public class FormFieldAttribute : Attribute
    {
        public FormFieldAttribute(FormFieldBounds fieldBounds, FormFieldBounds labelBounds, string labelText)
        {
        }


    }
}
