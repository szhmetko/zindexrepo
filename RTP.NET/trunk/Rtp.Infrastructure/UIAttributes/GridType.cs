﻿namespace Rtp.Infrastructure.UIAttributes
{
    public enum GridType
    {
        DataGrid,
        ListView,
        Custom
    }

}
