﻿using System;

namespace Rtp.Infrastructure.UIAttributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class GridFieldAttribute : Attribute
    {
        public int Position
        {
            private set; get; 
        }

        public int Width
        {
            private set;
            get;
        }

        public string HeaderText
        {
            private set;
            get;
        }

        public GridFieldAttribute() : this(null)
        {
        }

        public GridFieldAttribute(string headerText = null, int position = 0, int width = 90)
        {
            HeaderText = headerText;
            Position = position;
            Width = width;

        }
    }
}
