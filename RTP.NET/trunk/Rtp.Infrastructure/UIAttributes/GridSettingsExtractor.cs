﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Rtp.Infrastructure.UIAttributes
{
    /// <summary>
    /// TODO: make it more generic
    /// </summary>
    public class GridSettingsExtractor
    {
        private Type EntityType { get; set; }

        public GridSettingsExtractor(Type type)
        {
            EntityType = type;
        }
        private IList<GridFieldInformation> _columnsInfo;

        public IList<GridFieldInformation> ColumnsInfo
        {
            get
            {
                if (_columnsInfo == null)
                {
                    var list = (from propertyInfo in EntityType.GetProperties()
                                from attr in propertyInfo.GetCustomAttributes(true)
                                where attr is GridFieldAttribute
                                select new GridFieldInformation(attr as GridFieldAttribute, propertyInfo)).ToList();

                    _columnsInfo = list;
                }

                return _columnsInfo;
            }
        }

        private GridSettingsAttribute _gridSettings;
        public GridSettingsAttribute GridSettings
        {
            get
            {
                if (_gridSettings == null)
                {
                    _gridSettings = EntityType.GetCustomAttributes(true).OfType<GridSettingsAttribute>().FirstOrDefault();
                }

                return _gridSettings;
            }
        }
    }
}
