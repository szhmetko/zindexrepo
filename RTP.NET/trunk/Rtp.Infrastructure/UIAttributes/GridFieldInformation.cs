﻿using System.Reflection;

namespace Rtp.Infrastructure.UIAttributes
{
    public class GridFieldInformation
    {
        public GridFieldAttribute FieldAttribute
        {
            get;
            private set;
        }

        public PropertyInfo PropertyMetadata
        {
            get;
            private set;
        }

        public string HeaderText
        {
            get
            {
                return string.IsNullOrEmpty(FieldAttribute.HeaderText)
                           ? PropertyMetadata.Name
                           : FieldAttribute.HeaderText;
            }
        }

        public GridFieldInformation(GridFieldAttribute sourceAttribute, PropertyInfo propertyInfo)
        {
            FieldAttribute = sourceAttribute;

            PropertyMetadata = propertyInfo;
        }
    }
}
