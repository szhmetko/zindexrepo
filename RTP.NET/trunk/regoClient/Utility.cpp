// $Header: /rtpClient/Utility.cpp 5     11.07.08 2:01 Sergey Zhmetko $ 
// Utlity.cpp: implementation of the CUtlity class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Utility.h"
#include <sstream>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CUtility::CUtility(){}

CUtility::CUtility(const char* pattern) : 
	m_channelID(-1)
	, m_digits(8)
	, m_scale(2)
	, m_value(0.0)
{
	ATLASSERT(pattern);
	Parse(pattern);
}

CUtility::CUtility(short int cID, short int scale) : 
	m_channelID(cID)
	, m_digits(8)
	, m_scale(scale)
	, m_value(0.0)
{}

void CUtility::Parse(const char* buffer)
{
	std::istringstream istr(buffer);
	char ch = 0;
	
	while(/*istr && */istr.get(ch))
	{
		if(ch == '#')	break;	//����� �� #
		m_strPrefix += ch;
	}
	istr >> m_channelID;

	while(istr.get(ch))
	{
		switch(ch)
		{
		case ':':
			istr >> m_digits;
			break;
		case '.':
			istr >> m_scale;
			break;
		default:
			m_strPostfix += ch;
			
		}
	}
	
}
std::vector<std::string> CUtility::explode(char* pBuffer, const char* pDelims)
{
	std::vector<std::string> retVector;
	int idx = 0;
	char *token = strtok( pBuffer, pDelims );

	while( token != NULL )
	{
		retVector.push_back(token);
		OutputDebugString(token);	OutputDebugString("\n");
		token = strtok( NULL, pDelims );
		idx++;
	}
	return retVector;
}
std::string CUtility::FormatString() const
{
	char buffer[64]= {0};
	char mask[64]= {0};
//	sprintf(mask, "%%%d.%df", m_digits, m_scale);
	sprintf(mask, "%%.%df", m_scale);
	sprintf(buffer, mask, m_value);
	return m_channelID == -1 ? "" : m_strPrefix + buffer + m_strPostfix;
}


std::string CUtility::FormatDBString() const
{
	using namespace std;
	string ret;
	char buffer[64]= {0};
	sprintf(buffer, "#%d:%d.%d", m_channelID, m_digits, m_scale);
	ret = m_strPrefix + buffer + m_strPostfix;
	return ret;
}