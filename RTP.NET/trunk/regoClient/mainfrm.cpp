// $Header: /rtpClient/mainfrm.cpp 10    12.10.08 20:40 Sergey Zhmetko $ 
// MainFrm.cpp : implmentation of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"

#include "aboutdlg.h"
#include "ChildView.h"
#include "LegendWnd.h"
#include "MainFrm.h"
#include "ChannelsDlg.h"
#include "constructorDlg.h"
#include "GenericTab.h"
//#include "WFDesignerDlg.h"

BOOL CMainFrame::PreTranslateMessage(MSG* pMsg)
{
	if(CFrameWindowImpl<CMainFrame>::PreTranslateMessage(pMsg))
		return TRUE;

	return m_view.PreTranslateMessage(pMsg);
}

BOOL CMainFrame::OnIdle()
{
	UIUpdateToolBar();
	return FALSE;
}

LRESULT CMainFrame::OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	// create command bar window
	HWND hWndCmdBar = m_CmdBar.Create(m_hWnd, rcDefault, NULL, ATL_SIMPLE_CMDBAR_PANE_STYLE);
	// attach menu
	m_CmdBar.AttachMenu(GetMenu());
	// load command bar images
	m_CmdBar.LoadImages(IDR_MAINFRAME);
	// remove old menu
	SetMenu(NULL);

	HWND hWndToolBar = CreateSimpleToolBarCtrl(m_hWnd, IDR_MAINFRAME, FALSE, ATL_SIMPLE_TOOLBAR_PANE_STYLE);

	CreateSimpleReBar(ATL_SIMPLE_REBAR_NOBORDER_STYLE);
	AddSimpleReBarBand(hWndCmdBar);
	AddSimpleReBarBand(hWndToolBar, NULL, TRUE);

	CreateSimpleStatusBar();

	m_view.SetListener(this);
	m_hWndClient = m_view.Create( m_hWnd, rcDefault, NULL, WS_CHILD | WS_VISIBLE, NULL );

	UIAddToolBar(hWndToolBar);
	UISetCheck(ID_VIEW_TOOLBAR, 1);
	UISetCheck(ID_VIEW_STATUS_BAR, 1);

	// register object for message filtering and idle updates
	CMessageLoop* pLoop = _Module.GetMessageLoop();
	ATLASSERT(pLoop != NULL);
	pLoop->AddMessageFilter(this);
	pLoop->AddIdleHandler(this);

	m_legend.CreateAndDisplay(m_hWnd);
	m_legend.ShowWindow(SW_HIDE);
	
	SendMessage(WM_COMMAND, ID_NEWCONNECTION, 0);	//show new connection dialog


	//UIEnable(ID_VIEW_LEGEND, FALSE);
	//UISetText(ID_VIEW_LEGEND, "4444");
	return 0;
}

LRESULT CMainFrame::OnGetMinMaxInfo(UINT, WPARAM, LPARAM lParam, BOOL&)
{
	LPMINMAXINFO lpMinMax = (LPMINMAXINFO)lParam;
	if(lpMinMax)
		lpMinMax->ptMinTrackSize = CPoint(800, 600);
	return 0;
}

LRESULT CMainFrame::OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if(wParam == SC_CLOSE)
		SendMessage(WM_COMMAND, ID_APP_EXIT, 0);
	else
		bHandled = FALSE;
	return 0;
}

LRESULT CMainFrame::OnCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	switch(LOWORD(wParam))
	{
	case ID_APP_EXIT:
		PostMessage(WM_CLOSE);	break;

	case ID_VIEW_TOOLBAR:
		{
			static BOOL bVisible = TRUE;	// initially visible
			bVisible = !bVisible;
			CReBarCtrl rebar = m_hWndToolBar;
			int nBandIndex = rebar.IdToIndex(ATL_IDW_BAND_FIRST + 1);	// toolbar is 2nd added band
			rebar.ShowBand(nBandIndex, bVisible);
			UISetCheck(ID_VIEW_TOOLBAR, bVisible);
			UpdateLayout();
		}
		break;

	case ID_VIEW_STATUS_BAR:
		{
			BOOL bVisible = !::IsWindowVisible(m_hWndStatusBar);
			::ShowWindow(m_hWndStatusBar, bVisible ? SW_SHOWNOACTIVATE : SW_HIDE);
			UISetCheck(ID_VIEW_STATUS_BAR, bVisible);
			UpdateLayout();
		}
		break;

	case ID_APP_ABOUT:
		{	
			CAboutDlg dlg;
			dlg.DoModal();
		}
		break;

	case ID_NEWCONNECTION:
		m_view.ConnectTo();	
		UISetCheck(ID_ZOOM_HONLY, 0);
		UISetCheck(ID_ZOOM_VONLY, 0);
		UISetCheck(ID_ZOOM_BOTH, 0);
		UpdateLayout();
		
		break;

	case ID_VIEW_LEGEND:
		{
			BOOL bVisible = m_legend.IsWindowVisible();
			m_legend.ShowWindow(bVisible ? SW_HIDE : SW_SHOW);
			UISetCheck(ID_VIEW_LEGEND, !bVisible);
		}
		break;
	case ID_MCHANNELSETTINGS:
		{
			CChannelsDlg dlg; dlg.DoModal();
		}
		break;
	case ID_MCONSTRUCT:
		{
			CConstructorDlg dlg; dlg.DoModal();
		}
		break;
	case ID_ZOOM_HONLY:
	case ID_ZOOM_VONLY:
	case ID_ZOOM_BOTH:
		{
			WORD wID = LOWORD(wParam);
			DWORD dwState = UIGetState(wID);
			bool bNotChecked =  ( (UPDUI_CHECKED & dwState) != UPDUI_CHECKED );
			UISetCheck(ID_ZOOM_HONLY, bNotChecked && (wID == ID_ZOOM_HONLY));
			UISetCheck(ID_ZOOM_VONLY, bNotChecked && (wID == ID_ZOOM_VONLY));
			UISetCheck(ID_ZOOM_BOTH, bNotChecked && (wID == ID_ZOOM_BOTH));
			OscZoomMode zm;
			if(wID == ID_ZOOM_HONLY)zm = ZoomHorz;
			if(wID == ID_ZOOM_VONLY)zm = ZoomVert;
			if(wID == ID_ZOOM_BOTH)zm = ZoomRect;
			m_view.SetTabsZoomMode(zm, bNotChecked);
			UpdateLayout();
		}
		break;
	case ID_FILE_SAVE_AS:
		{
			TCHAR szFilters[] =
		//			_T("Excel Files\0*.xls\0")
				_T("Text files (*.csv)\0*.csv\0")
				_T("All Files \0*.*\0");


			CFileDialog fdlg(FALSE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilters);
			if( fdlg.DoModal() == IDOK )
			{
				fdlg.m_szFileName[fdlg.m_ofn.nFileExtension-1] = 0;	//to remove extension

				CGenericTab* paTab = m_view.GetActiveViewTab();
				if(paTab)paTab->ResultSaveAs(fdlg.m_szFileName, fdlg.m_ofn.nFilterIndex);

			}
			break;
		}
		case ID_FILE_PRINT:
			{
				CGenericTab* paTab = m_view.GetActiveViewTab();
				if(paTab)paTab->PlotPrint();
			}
			break;
		case IDM_WFDESIGNER_TEST:
			{
				//CWFDesignerDlg dlg; dlg.DoModal();
			}
			break;

	}
	return 0;
}