//$Header: /rtpClient/WFDCell.cpp 7     11.07.08 2:00 Sergey Zhmetko $
// WFCell.cpp: implementation of the CWFCell class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "WFDCell.h"
#include "WFDRow.h"
#include "WFDesigner.h"
#include "resource.h"
#include "config.h"
#include <typeinfo>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

LRESULT CGenericWFCell::OnKeyUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if(wParam == VK_DELETE && m_hWnd)
	{	
		HWND hParent = GetParent();
		DestroyWindow();
		::PostMessage(hParent, WM_CHILDWND_DESTROYED, WPARAM(this), 0);
	}
	return 0;
}

LRESULT CGenericWFCell::OnActivate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	Invalidate();
	bHandled = FALSE;
	return 0;
}

LRESULT CGenericWFCell::OnEraseBgnd(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if(wParam)
	{
		CRect rcClient;
		GetClientRect(&rcClient);
		CDCHandle dc = HDC(wParam);
		if(GetActiveWindow() == m_hWnd)
		{
			//dc.FillRect(&rcClient, COLOR_HIGHLIGHT);
			dc.FillSolidRect(&rcClient, RGB(148, 166, 192));
			rcClient.InflateRect(-4, -4);
			dc.FillRect(&rcClient, COLOR_INACTIVEBORDER);
		}
		else
			dc.FillRect(&rcClient, COLOR_INACTIVEBORDER);
	}
	//bHandled = FALSE;
	return 1;
}

LRESULT CGenericWFCell::OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = TRUE;
	return 0;
}

LRESULT CValuesWFCell::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	m_cmds.push_back(IDS_WFD_CMD_ADDCHL2CELL);	
	bHandled = FALSE;
	return 0;
}


int CValuesWFCell::Save()
{
	using namespace dbutils;
	using namespace std;
	CDBSession	locSession;
	HResult hres;
	try
	{
		hres = locSession.Open();
		CCommand<CAccessor<CWorkFlowCellAccessor> > rst;
		hres = rst.Open(locSession, CConfig::GetValue("workflow/insert").c_str()
			, locSession.GetPropRowset(DBPROPVAL_UP_INSERT));
		rst.ClearRecord();
		hres = rst.MoveFirst();

		strcpy(rst.m_content, "bmp");
		rst.m_parent_id = m_parent.m_item_id;
		rst.m_type = 1;
		rst.m_subtype = 1;


		list<CUtility>::const_iterator iter = begin();
		string contents;
		char buffer[16] = {0};
		while(iter != end())
		{
			const CUtility& cut = *iter;
			if(!contents.empty())contents += "|";
			contents += cut.FormatDBString();
			iter++;
		}
		strncpy(rst.m_content, contents.c_str(), sizeof(rst.m_content));

		hres = rst.Insert(1);
		rst.Close();
		locSession.Close();

		return size();
	}
	catch(HresError he)
	{
		::MessageBox(0, he.what().c_str(), "", MB_OK);
		return 0;
	}
}

void CValuesWFCell::_DrawText(CDCHandle& dc)
{
	const int iHeight = 12;	//height or row in pixels
	COLORREF oldClr = dc.GetTextColor();
	dc.SetTextColor( 0 );
	HFONT oldFont = dc.SelectFont(AtlGetDefaultGuiFont());

	CRect rcItem;
	GetClientRect(&rcItem);
	rcItem.bottom -= 2;
	rcItem.top = rcItem.bottom - iHeight;

	for(const_iterator citer = begin(); citer != end(); citer++)
	{
		const CUtility& util = *citer;
		std::string strText = util.FormatString();
		dc.DrawText(strText.c_str(), -1, &rcItem, 
			DT_EDITCONTROL | DT_CENTER | DT_SINGLELINE );
		rcItem.OffsetRect(0, -iHeight - 2);
	}
	if (oldFont) dc.SelectFont(oldFont);
	if (oldClr) dc.SetTextColor( oldClr );
}


LRESULT CValuesWFCell::OnCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL&)
{
	WORD wID = LOWORD(wParam);
	switch(wID)
	{
	case IDS_WFD_CMD_ADDCHL2CELL:
		CChlsLibDlg dlg(*this);
		if(IDOK == dlg.DoModal())
		{
			clear();
			insert(end(), dlg.begin(), dlg.end());
		}
		break;
	}
	return 0;
}

LRESULT CValuesWFCell::OnEraseBkgnd(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if(wParam)
	{
		CRect rcClient;
		GetClientRect(&rcClient);
		CDCHandle dc = HDC(wParam);
		if(GetActiveWindow() == m_hWnd)
		{
			dc.FillRect(&rcClient, COLOR_HIGHLIGHT);
			rcClient.InflateRect(-4, -4);
			dc.FillRect(&rcClient, RGB(255, 255, 255));
		}
		else
			dc.FillRect(&rcClient, RGB(255, 255, 255));
		/*HFONT oldFont = dc.SelectFont(AtlGetDefaultGuiFont());
		dc.DrawText(typeid( *this ).name()+6, -1, &rcClient, DT_CENTER | DT_SINGLELINE | DT_VCENTER);
		dc.SelectFont(oldFont);*/
		_DrawText(dc);

	}
	//bHandled = FALSE;
	return 1;
}


LRESULT CBMPWFCell::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	m_cmds.push_back(IDS_WFD_CMD_ADDBMP2CELL);	

	CPaintDC dc(m_hWnd); 
	m_VirtScreenDC.CreateCompatibleDC(dc);
	m_VirtScreenBitmap.CreateCompatibleBitmap(dc, 2, 2);
	
//	AddImage(10);
	bHandled = FALSE;
	return 0;
}

LRESULT CBMPWFCell::OnEraseBkgnd(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CRect rc;
	GetClientRect(&rc);
	PAINTSTRUCT ps;
	CDC dc = BeginPaint(&ps);
	LRESULT ret = 0;
	if(!empty())
	{
		m_VirtScreenDC.SelectBitmap(front());
		dc.BitBlt(0, 0, rc.right, rc.bottom, m_VirtScreenDC, 0, 0, SRCCOPY);
	}
	else
	{
		dc.FillSolidRect(&rc, RGB(188, 206, 232));
		dc.DrawText(_T("�����"), -1, &rc, DT_CENTER | DT_SINGLELINE | DT_VCENTER );	
		ret = 1;
	}
	
	EndPaint(&ps);
	bHandled = TRUE;
	return ret;
}

CBMPWFCell::~CBMPWFCell()	{	clear();	}

void CBMPWFCell::clear()	
{	
	for(iterator iter = begin(); iter != end(); ++iter)
	{
		CBitmapHandle bmp = *iter;
		bmp.DeleteObject();
	}
	baseContainer::clear();	
}

int CBMPWFCell::Save()
{
	using namespace dbutils;
	using namespace std;
	CDBSession	locSession;
	HResult hres;
	try
	{
		hres = locSession.Open();
		CCommand<CAccessor<CWorkFlowCellAccessor> > rst;
		hres = rst.Open(locSession, CConfig::GetValue("workflow/insert").c_str()
			, locSession.GetPropRowset(DBPROPVAL_UP_INSERT));
		rst.ClearRecord();
		hres = rst.MoveFirst();

		strcpy(rst.m_content, "bmp");
		rst.m_parent_id = m_parent.m_item_id;
		rst.m_type = 1;
		rst.m_subtype = 0;


		list<long>::const_iterator iter = m_imgIDsList.begin();
		string contents;
		char buffer[16] = {0};
		while(iter != m_imgIDsList.end())
		{
			sprintf(buffer, "%d", *iter);
			if(!contents.empty())contents += "|";
			contents += buffer;
			iter++;
		}
		strncpy(rst.m_content, contents.c_str(), sizeof(rst.m_content));

		hres = rst.Insert(1);
		rst.Close();
		//m_item_id = locSession.getScalar(CConfig::GetValue("workflow/identity").c_str());
		locSession.Close();

		return size();
	}
	catch(HresError he)
	{
		::MessageBox(0, he.what().c_str(), "", MB_OK);
		return 0;
	}
}

void CBMPWFCell::AddImage(long l2add)
{
	CWFImageCache::const_iterator citer = m_parent.m_parent.m_imgCache.find(l2add);
	if(citer != m_parent.m_parent.m_imgCache.end())
	{
		CRect rcDesired; 
		GetClientRect(&rcDesired);
		CBitmapHandle resizedBMP = (HBITMAP)::CopyImage(citer->second, IMAGE_BITMAP
			, rcDesired.Width(), rcDesired.Height(), LR_COPYRETURNORG);
		push_back(resizedBMP);
		m_imgIDsList.push_back(l2add);
	}
}

CBMPWFCell::CBMPWFCell(CWFRow& r) : m_parent(r)
{}

void CBMPWFCell::RemoveCurrentImage(){pop_front();Invalidate();}

LRESULT CBMPWFCell::OnCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	WORD wID = LOWORD(wParam);
	switch(wID)
	{
	case IDS_WFD_CMD_ADDBMP2CELL:
		CImageLibDlg dlg(m_parent.m_parent.m_imgCache);
		if(IDOK == dlg.DoModal())
		{
			CImageLibDlg::const_iterator citer = dlg.begin();
			while(citer != dlg.end())
			{
				AddImage(*citer);
				citer++;
			}
		}
		break;
	}
	return 0;
}

LRESULT CBMPWFCell::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{

	
/*	CPaintDC dc(m_hWnd); 
*/
	bHandled = TRUE;
	return 0;
}

HWND CWFCellToolBar::Construct(HWND hParent, RECT& rc, const std::list<UINT>& cmds)
{
	m_commands = cmds;
	return Create(hParent, rc);
}

CWFCellToolBar::CWFCellToolBar()
{
}

LRESULT CWFCellToolBar::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	using namespace std;
	CRect rc(0, 0, 18, 18);
	list<UINT>::const_iterator citer = m_commands.begin();
	CButton btn;
	while(citer != m_commands.end())
	{
		push_back(btn);
		back().Create(m_hWnd, &rc, CCommandSupplier<CWindow>::LoadStringPair(*citer).second.c_str()
			, WS_CHILD | WS_VISIBLE, 0, *citer);
		back().SetFont(AtlGetDefaultGuiFont());
		rc.OffsetRect(rc.Width(), 0);
		citer++;
	}
	return 0;
}

LRESULT CWFCellToolBar::OnEraseBgnd(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	//bHandled = FALSE;
	return 1;
}

LRESULT CWFCellToolBar::OnCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL&)
{
	::SendMessage(GetParent(), uMsg, wParam, lParam);
	return 0;
}

