// $Header: /rtpClient/ApproxRanges.h 4     13.04.08 17:22 Sergey Zhmetko $ 
#if !defined(AFX_APPROX_RANGES_H_INCLUDED_)
#define AFX_APPROX_RANGES_H_INCLUDED_

class CRange
{
public:
	CRange();
	~CRange();
	bool ValInMe(long val);
	void Fill(CApproxAccessor& apac, long Uto);
	double Calculate(double preVal);
private:
	BYTE m_num;
	double	m_offset;
	double	m_k;
	long	m_u_from;
	long	m_u_to;
};

class CRangesGroup : public CTPairsAccessor
{
public:
	CRangesGroup();
	CRangesGroup(const CTPairsAccessor& tac );
	~CRangesGroup();
	double Calculate(double preVal);
private:
	CRange m_arRanges[5];
};

typedef  std::map<int, CRangesGroup> THERMOPAIRS_MAP;	//key is tpair_id
typedef  THERMOPAIRS_MAP::iterator THERMOPAIRS_ITER;

#endif // !defined(AFX_APPROX_RANGES_H_INCLUDED_)
