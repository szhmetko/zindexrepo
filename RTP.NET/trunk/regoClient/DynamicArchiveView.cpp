// $Header: /rtpClient/DynamicArchiveView.cpp 4     13.04.08 17:15 Sergey Zhmetko $ 
// DynamicArchiveView.cpp: implementation of the CDynamicArchiveView class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DynamicArchiveView.h"

#define ARCHIVE_MAX_HOURS	12

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDynamicArchiveView::CDynamicArchiveView( const CTabsAccessor& tac)
				: CGenericRTPView<CDynamicArchiveView>(tac)
{}

CDynamicArchiveView::~CDynamicArchiveView()
{}

void CDynamicArchiveView::InitializeTab()
{
	CGenericRTPView<CDynamicArchiveView>::InitializeTab();

	char szFormat[32] = {0};
	CComboBox m_ctlComboFrom = GetDlgItem(IDC_HOURS_FROM);
	CComboBox m_ctlComboTo = GetDlgItem(IDC_HOURS_TO);
	for( int i = 0; i < 24; ++i )
	{
		sprintf(szFormat, "%02d-00", i);
		m_ctlComboFrom.AddString(szFormat);
		m_ctlComboTo.AddString(szFormat);
	}
	SYSTEMTIME ltNow;
	GetLocalTime(&ltNow);
	m_ctlComboFrom.SetCurSel(0);
	m_ctlComboTo.SetCurSel(ltNow.wHour);
	SYSTEMTIME ltFrom = ltNow;

	SYSTEMTIME stRange[2];
	ltFrom.wYear -= 5;
	stRange[0] = ltFrom;	
	stRange[1] = ltNow;

	CDateTimePickerCtrl ctlDTP = (CDateTimePickerCtrl)GetDlgItem(IDC_DTP_FROM);
	ctlDTP.SetMonthCalColor(MCSC_MONTHBK, RGB(220,255,220));
	ctlDTP.SetRange(GDTR_MIN | GDTR_MAX, stRange);

	CDateTimePickerCtrl ctlDTPto = (CDateTimePickerCtrl)GetDlgItem(IDC_DTP_TO);
	ctlDTPto.SetMonthCalColor(MCSC_MONTHBK , RGB(255,220,220));
	ctlDTPto.SetRange(GDTR_MIN | GDTR_MAX, stRange);
}

LRESULT CDynamicArchiveView::OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CSize sz(lParam);
	CRect plotRect;
	m_plot.GetWindowRect(&plotRect);
	ScreenToClient(plotRect);
	plotRect.bottom = sz.cy;
	plotRect.right = sz.cx;
	m_plot.MoveWindow(&plotRect);
	for(size_t i = 0; i < m_TabChlsVector.size(); i++)
		m_plot.ShowChannel(i);
	return 0;
}	

LRESULT CDynamicArchiveView::OnButtonRefresh(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	using namespace dbutils;
	CDateTimePickerCtrl ctlDTP = (CDateTimePickerCtrl)GetDlgItem(IDC_DTP_FROM);
	CComboBox m_ctlHourCombo = GetDlgItem(IDC_HOURS_FROM);

	SYSTEMTIME dtFrom, dtTo;
	ctlDTP.GetSystemTime(&dtFrom);
	dtFrom.wHour = m_ctlHourCombo.GetCurSel();

	ctlDTP = (CDateTimePickerCtrl)GetDlgItem(IDC_DTP_TO);
	m_ctlHourCombo = GetDlgItem(IDC_HOURS_TO);

	ctlDTP.GetSystemTime(&dtTo);
	dtTo.wHour = m_ctlHourCombo.GetCurSel();

	time_t unxFrom = CDBSession::SystemTimeToUnixTime(dtFrom);
	time_t unxTo = CDBSession::SystemTimeToUnixTime(dtTo);
	if(unxTo - unxFrom > ARCHIVE_MAX_HOURS*3600 )
	{
		if(IDNO == ::MessageBox(m_hWnd,
			_T("��������� �������� ��������� ���������� ��������\n ����������?"), 
			_T("����������"), MB_YESNO | MB_ICONEXCLAMATION) )
		return 0;
	}
	CString strFilter = "";

	strFilter.Format( " where dtm_upd between "
				 "convert(datetime, '%4d-%02d-%02d %02d:00:00', 102) and "
				 "convert(datetime, '%4d-%02d-%02d %02d:00:00', 102) ",
		dtFrom.wMonth, dtFrom.wDay, dtFrom.wYear, dtFrom.wHour,
		dtTo.wMonth, dtTo.wDay, dtTo.wYear, dtTo.wHour);

	ShowChart(strFilter);
	return 0;
}
