# Microsoft Developer Studio Project File - Name="rtpClient" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=rtpClient - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "rtpClient.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "rtpClient.mak" CFG="rtpClient - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "rtpClient - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "rtpClient - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/rtpClient", MRAAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "rtpClient - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GR /GX /O1 /I "include/" /I "gdiplus/" /I "Lib/wtl/" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "STRICT" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x419 /d "NDEBUG"
# ADD RSC /l 0x419 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386 /libpath:"lib/"

!ELSEIF  "$(CFG)" == "rtpClient - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GR /GX /ZI /Od /I "..\Lib\wtl" /I "..\Lib\wtl\\" /I "include/" /I "gdiplus/" /I "Lib/wtl/" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "STRICT" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x419 /d "_DEBUG"
# ADD RSC /l 0x419 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept /libpath:"lib/"

!ENDIF 

# Begin Target

# Name "rtpClient - Win32 Release"
# Name "rtpClient - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\aboutdlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ApproxRanges.cpp
# End Source File
# Begin Source File

SOURCE=.\cell.cpp
# End Source File
# Begin Source File

SOURCE=.\ChannelsDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ChildView.cpp
# End Source File
# Begin Source File

SOURCE=.\config.cpp
# End Source File
# Begin Source File

SOURCE=.\ConstructorDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DataContainer.cpp
# End Source File
# Begin Source File

SOURCE=.\dbutils.cpp
# End Source File
# Begin Source File

SOURCE=.\DynamicArchiveView.cpp
# End Source File
# Begin Source File

SOURCE=.\DynamicMonitorView.cpp
# End Source File
# Begin Source File

SOURCE=.\DynamicPiecesView.cpp
# End Source File
# Begin Source File

SOURCE=.\DynamicWFlowView.cpp
# End Source File
# Begin Source File

SOURCE=.\GenericTab.cpp
# End Source File
# Begin Source File

SOURCE=.\IssHelper.cpp
# End Source File
# Begin Source File

SOURCE=.\LegendWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\LineSelectCombo.cpp
# End Source File
# Begin Source File

SOURCE=.\mainfrm.cpp
# End Source File
# Begin Source File

SOURCE=.\PiecesLists.cpp
# End Source File
# Begin Source File

SOURCE=.\row.cpp
# End Source File
# Begin Source File

SOURCE=.\rtpClient.cpp
# End Source File
# Begin Source File

SOURCE=.\rtpClient.rc
# End Source File
# Begin Source File

SOURCE=.\StatReportWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\stdafx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\TabChannelsDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\Utility.cpp
# End Source File
# Begin Source File

SOURCE=.\WFDCell.cpp
# End Source File
# Begin Source File

SOURCE=.\WFDesigner.cpp
# End Source File
# Begin Source File

SOURCE=.\WFDesignerDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\WFDRow.cpp
# End Source File
# Begin Source File

SOURCE=.\WFImageLibrary.cpp
# End Source File
# Begin Source File

SOURCE=.\wflow.cpp
# End Source File
# Begin Source File

SOURCE=.\WorfFlowBuilder.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\aboutdlg.h
# End Source File
# Begin Source File

SOURCE=.\accessors.h
# End Source File
# Begin Source File

SOURCE=.\ApproxRanges.h
# End Source File
# Begin Source File

SOURCE=.\include\cell.h
# End Source File
# Begin Source File

SOURCE=.\ChannelsDlg.h
# End Source File
# Begin Source File

SOURCE=.\ChildView.h
# End Source File
# Begin Source File

SOURCE=.\config.h
# End Source File
# Begin Source File

SOURCE=.\ConstructorDlg.h
# End Source File
# Begin Source File

SOURCE=.\DataContainer.h
# End Source File
# Begin Source File

SOURCE=.\dbutils.h
# End Source File
# Begin Source File

SOURCE=.\definitions.h
# End Source File
# Begin Source File

SOURCE=.\DynamicArchiveView.h
# End Source File
# Begin Source File

SOURCE=.\DynamicMonitorView.h
# End Source File
# Begin Source File

SOURCE=.\DynamicPiecesView.h
# End Source File
# Begin Source File

SOURCE=.\DynamicWFlowView.h
# End Source File
# Begin Source File

SOURCE=.\GenericTab.h
# End Source File
# Begin Source File

SOURCE=.\IssHelper.h
# End Source File
# Begin Source File

SOURCE=.\LegendWnd.h
# End Source File
# Begin Source File

SOURCE=.\mainfrm.h
# End Source File
# Begin Source File

SOURCE=.\Oscilloscope.h
# End Source File
# Begin Source File

SOURCE=.\PiecesLists.h
# End Source File
# Begin Source File

SOURCE=.\resource.h
# End Source File
# Begin Source File

SOURCE=.\include\row.h
# End Source File
# Begin Source File

SOURCE=.\StatReportWnd.h
# End Source File
# Begin Source File

SOURCE=.\stdafx.h
# End Source File
# Begin Source File

SOURCE=.\TabChannelsDlg.h
# End Source File
# Begin Source File

SOURCE=.\include\Utility.h
# End Source File
# Begin Source File

SOURCE=.\webro.h
# End Source File
# Begin Source File

SOURCE=.\include\WFDCell.h
# End Source File
# Begin Source File

SOURCE=.\include\WFDesigner.h
# End Source File
# Begin Source File

SOURCE=.\WFDesignerDlg.h
# End Source File
# Begin Source File

SOURCE=.\include\WFDRow.h
# End Source File
# Begin Source File

SOURCE=.\WFImageLibrary.h
# End Source File
# Begin Source File

SOURCE=.\include\wflow.h
# End Source File
# Begin Source File

SOURCE=.\include\WorfFlowBuilder.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\100656.ico
# End Source File
# Begin Source File

SOURCE=.\res\56.ico
# End Source File
# Begin Source File

SOURCE=.\res\57.ico
# End Source File
# Begin Source File

SOURCE=.\res\76.ico
# End Source File
# Begin Source File

SOURCE=.\res\FOLDRS02.ICO
# End Source File
# Begin Source File

SOURCE=.\res\GRAPH04.ICO
# End Source File
# Begin Source File

SOURCE=.\res\ico00001.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00002.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\res\idr_main.ico
# End Source File
# Begin Source File

SOURCE=.\res\mainfram.bmp
# End Source File
# Begin Source File

SOURCE=.\res\rtpClient.exe.manifest
# End Source File
# Begin Source File

SOURCE=.\res\rtpClient.ico
# End Source File
# Begin Source File

SOURCE=.\res\toolbar1.bmp
# End Source File
# End Group
# Begin Source File

SOURCE=.\res\html_err.htm
# End Source File
# Begin Source File

SOURCE=.\misc\lines.dat
# End Source File
# End Target
# End Project
