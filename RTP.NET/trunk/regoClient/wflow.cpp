// $Header: /rtpClient/wflow.cpp 3     13.04.08 17:22 Sergey Zhmetko $ 
#include "stdafx.h"
#include "wflow.h"
#include "row.h"
#include "IssHelper.h"

/*BOOL CWorkFlowCtrl::SubclassWindow(HWND hWnd)
{
	ATLASSERT(m_hWnd == NULL);
	ATLASSERT(::IsWindow(hWnd));

#ifdef _DEBUG
	// Check class
	char szBuffer[16] = {0};
	if( ::GetClassName(hWnd, szBuffer, (sizeof(szBuffer)/sizeof(TCHAR)) - 1) ) 
		ATLASSERT(::lstrcmpi(szBuffer, CStatic::GetWndClassName()) == 0);
#endif

	BOOL bRet = CWindowImpl<CWorkFlowCtrl, CStatic, CControlWinTraits>::SubclassWindow(hWnd);
	if( bRet ) _Init();
	return bRet;
}
void CWorkFlowCtrl::_Init()
{
	ATLASSERT(::IsWindow(m_hWnd));
	Layout();
}
void CWorkFlowCtrl::Layout()
{
	ATLASSERT(::IsWindow(m_hWnd));

	CRect rcItem;
	GetClientRect(&rcItem);
	ROWS_VECTOR::iterator riter = m_rows.begin();
	while( riter != m_rows.end() )
	{
		CWorkFlowRow* pRow = *riter;
		if(pRow)
		{
			rcItem.bottom = rcItem.top + pRow->Height();
			pRow->MoveWindow(&rcItem);
			rcItem.bottom = rcItem.top;
		}
		riter++;
	}
}
CWorkFlowCtrl::~CWorkFlowCtrl()
{
	ROWS_VECTOR::iterator riter = m_rows.begin();
	while( riter != m_rows.end() )
	{
		CWorkFlowRow* pRow = *riter;
		if(pRow)
			delete pRow;
		riter++;
	}
}
size_t CWorkFlowCtrl::AddRow(unsigned short uHeight = 0)
{
	if( !m_rows.empty() )
	{
		CRect rcItem;
		if(uHeight = 0)
		{
			GetClientRect(&rcItem);
			uHeight = rcItem.Width() / (m_rows.size() + 1);
		}
		CWorkFlowRow* pNewRow = new CWorkFlowRow(uHeight);
		m_rows.push_back(pNewRow);
		Layout();
		return	m_rows.size() - 1;
	}
	return 0;
}*/

CWorkFlowCtrl::CWorkFlowCtrl(CSession& session) : m_session(session)
{
	ReadConfig();
	DownLoadPictures();
}

void CWorkFlowCtrl::DownLoadPictures()
{
	using namespace std;
	using namespace dbutils;
	ULONG ulBytesRead = 4;

	void*   pData = NULL;		// Data field parameter.
	CCommand<CAccessor<CImagesAccessor> >	rs;

	try
	{

		HResult hr = rs.Open(m_session, CConfig::GetValue("images/select").c_str());
		CISSHelper ISSHelper;
		rs.ClearRecord();
		while( S_OK == rs.MoveNext() )
		{
			//hr = rs.MoveFirst();
			BYTE buffer[4096] = {0};

			if ( DBSTATUS_S_OK != rs.m_BLOBDATA_STATUS )
			{
				::MessageBox(0,  _T("Unable to retrieve data from BLOB field, field may be NULL."), "", MB_OK );
				return;
			}

			char szName[32] = {0};
			sprintf(szName, "%08d", rs.m_id);
			m_pictures.push_back(szName);
			string strFile = m_strImageDir + szName;
			FILE* pFile = fopen(strFile.c_str(), "ab");
			if(pFile != 0)
			{

				do
				{
					// Call ISequentialStream::Read using provider's pointer.
					hr = rs.m_data->Read( buffer, sizeof(buffer), &ulBytesRead );
					size_t lRet = fwrite(buffer, sizeof(BYTE), ulBytesRead, pFile);

					// Stuff data into helper class which is used as a dynamic buffer here.
	//					hr = ISSHelper.Write( buffer, sizeof(buffer), NULL );
				}
				while ( sizeof(buffer) == ulBytesRead );
				fclose(pFile);
			}
			rs.ClearRecord();
		}



		// Set ISequentialStream pointer to our implementation.
	}
	catch(HresError he)
	{
		::MessageBox(0, he.what().c_str(), "", MB_OK);
	}

}

CSession&	CWorkFlowCtrl::GetDBSession()
{	return m_session;	}

CWorkFlowCtrl::~CWorkFlowCtrl()
{
	using namespace std;
	list<string>::const_iterator citer = m_pictures.begin();
	while(citer != m_pictures.end())
	{
		string sFileName = m_strImageDir + *citer;
		::DeleteFile(sFileName.c_str());
		citer++;
	}
}
