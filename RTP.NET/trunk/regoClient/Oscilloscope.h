// $Header: /rtpClient/Oscilloscope.h 6     13.04.08 22:10 Sergey Zhmetko $ 
#ifndef _WTL_OSCILLOSCOPE_H_
#define _WTL_OSCILLOSCOPE_H_

#include <math.h>
#include <vector>
#include <time.h>

/////////////////////////////////////////////////////////////////////////////

#define OSC_X_AXIS_HEIGHT		25
#define OSC_Y_AXIS_WIDTH		50

#define OSC_EPSILON				1e-15
#define OSC_ZERO(x)				(fabs(x) < OSC_EPSILON)

#define OSC_LABEL_FORMAT		"%g"
#define OSC_TIME_LABEL_FORMAT		"%H:%M:%S"

#define OSC_OBJ_OFFSET_X		4
#define OSC_OBJ_OFFSET_Y		4
#define OSC_BIG_TIC_LEN			10
#define OSC_SMALL_TIC_LEN		5

#define OSC_MIN_TIC1_DIST		15
#define OSC_MIN_TIC3_DIST		25
#define OSC_MIN_TIC4_DIST		35

#define OSC_DEF_LAST_POINTS		100
#define OSC_MIN_LAST_POINTS		10
#define OSC_MAX_LAST_POINTS		2000
/////////////////////////////////////////////////////////////////////////////
enum OscZoomDir  { ZoomOut  = 0, ZoomIn,   ZoomOutFull };
typedef struct _OscZoomRect {
	double xMin, xMax;
	double yMin, yMax;
} OscZoomRect;
/////////////////////////////////////////////////////////////////////////////
enum OscPointStyle  { None = 0, UTriangle, DTriangle, Diamond, Circle, Cross, XCross };
/////////////////////////////////////////////////////////////////////////////
class OscZoomStack
{
	std::vector<OscZoomRect>	m_zoomIn[OSC_MAX_CHANNELS];
	std::vector<OscZoomRect>	m_zoomOut[OSC_MAX_CHANNELS];

public:
	OscZoomStack()  {}
	~OscZoomStack() {}

	size_t ZoomInSize()  { return m_zoomIn[0].size();  }
	size_t ZoomOutSize() { return m_zoomOut[0].size(); }
	
	void Clear()
	{
		for(size_t i=0; i<OSC_MAX_CHANNELS; i++) {
			m_zoomIn[i].clear();
			m_zoomOut[i].clear();
		}
	}

	void AddZoom(OscZoomRect* zoom)
	{
		for(size_t i=0; i<OSC_MAX_CHANNELS; i++) {
			m_zoomIn[i].clear();
			m_zoomOut[i].push_back(zoom[i]);
		}
	}

	bool ZoomIn(OscZoomRect* zoom)
	{
		if ( ZoomInSize() == 0 )
			return false;

		for(size_t i=0; i<OSC_MAX_CHANNELS; i++) {
			m_zoomOut[i].push_back(zoom[i]);
			zoom[i] = *(m_zoomIn[i].end() - 1);
			m_zoomIn[i].pop_back();
		}

		return true;
	}

	bool ZoomOut(OscZoomRect* zoom)
	{
		if ( ZoomOutSize() == 0 )
			return false;

		for(size_t i=0; i<OSC_MAX_CHANNELS; i++) {
			m_zoomIn[i].push_back(zoom[i]);
			zoom[i] = *(m_zoomOut[i].end() - 1);
			m_zoomOut[i].pop_back();
		}

		return true;
	}

	bool ZoomOutFull(OscZoomRect* zoom)
	{
		if ( ZoomOutSize() == 0 )
			return false;
	
		while( ZoomOut(zoom) );

		return true;
	}
};
/////////////////////////////////////////////////////////////////////////////
class OscHandler
{
public:
	virtual bool UseGrid() = 0;
	virtual bool UseNewMinMax() = 0;
	virtual bool UsePadding() = 0;
	virtual double PaddingCoef() = 0;
	virtual bool EnableZoom() = 0;
	virtual void Zoom(POINT* start, POINT* end) = 0;
	virtual void Zoom(OscZoomDir dir) = 0;
	virtual OscZoomMode ZoomMode() = 0;
	virtual OscPointStyle PointStyle() = 0;
};
/////////////////////////////////////////////////////////////////////////////
template< class T, class TBase = CStatic, class TWinTraits = CControlWinTraits >
class ATL_NO_VTABLE OscilloscopeChannelImpl : public CWindowImpl< T, TBase, TWinTraits >
{
	double		m_xMin, m_xMax;
	double		m_yMin, m_yMax;
	double		m_yMinPad, m_yMaxPad;

	double		m_XK, m_YK;
	size_t		m_time;
	double*		m_values;
	
	POINT*		m_points;
	size_t		m_pointsCnt;

	COLORREF	m_bgColor;	// background color
	COLORREF	m_lnColor;	// curve color
	COLORREF	m_grColor;	// grid color
	COLORREF	m_brColor;	// brush color (for simbols)

	bool		m_zooming;
	POINT		m_startZoomPnt;
	POINT		m_endZoomPnt;

	OscHandler* m_handler;
	std::string		m_strTitle;
	std::string	m_strYRightTitle;
	std::string	m_strYLeftTitle;

	std::vector<double*> m_ValuesVector;
	std::vector<double> m_KoefVector;	//��� ��������� ��� = 1.0
	std::vector<POINT*> m_PointsVector;
	std::vector<COLORREF> m_LnColorVector;

/*****************************************************************************/
	std::vector<POINT*> m_DisPointsVector;
	POINT*		m_Dispoints;
	short int	m_iStepTruncPts;
public:
	size_t		m_DispointsCnt;

/*****************************************************************************/

///virtual screen section
	CBitmap   m_VirtScreenBitmap;
	CDC       m_VirtScreenDC;
public:
	typedef OscilloscopeChannelImpl< T, TBase, TWinTraits > thisClass;

	DECLARE_WND_SUPERCLASS(NULL, TBase::GetWndClassName())

	// Operations

	BOOL SubclassWindow(HWND hWnd)
	{
		ATLASSERT(m_hWnd==NULL);
		ATLASSERT(::IsWindow(hWnd));
#ifdef _DEBUG
		// Check class
		TCHAR szBuffer[16];
		if( ::GetClassName(hWnd, szBuffer, (sizeof(szBuffer)/sizeof(TCHAR))-1) ) {
			ATLASSERT(::lstrcmpi(szBuffer, TBase::GetWndClassName())==0);
		}
#endif
		BOOL bRet = CWindowImpl< T, TBase, TWinTraits >::SubclassWindow(hWnd);
		if( bRet ) _Init();
		return bRet;
	}

	// Interface
	int AddCurve(double* vals, COLORREF clr = RGB(0,0,0) , double koef = 1.0 )
	{
		ATLASSERT(vals);
		if(vals)
		{
			m_KoefVector.push_back(koef);
			m_ValuesVector.push_back(vals);
			m_LnColorVector.push_back(clr);
			POINT* pts = new POINT[m_pointsCnt];
			m_PointsVector.push_back(pts);
			
			if(m_PointsVector.size() > m_DisPointsVector.size() )
				m_DisPointsVector.push_back(0);

//			_UpdateYRange();
//			_Recalc();

		}
		return m_ValuesVector.size();
	}
	void LayoutCurves()
	{_Recalc();}
	void RemoveAllCurves()
	{
		m_ValuesVector.erase( m_ValuesVector.begin(), m_ValuesVector.end() );
		std::vector<POINT*>::iterator theIterator;
		for(theIterator = m_PointsVector.begin(); 
			theIterator != m_PointsVector.end(); ++theIterator )
		{
			POINT* pts = *theIterator;
			if (pts)
			{
				delete [] pts;
				pts = 0;
			}
		}
		/**/

		m_PointsVector.erase( m_PointsVector.begin(), m_PointsVector.end() );
		m_KoefVector.erase( m_KoefVector.begin(), m_KoefVector.end() );
		m_LnColorVector.erase( m_LnColorVector.begin(), m_LnColorVector.end() );
		m_values = 0;

	}

	void SetTitleText(char* newText) 
	{ 
		m_strTitle = newText; Invalidate(FALSE);	
	}
	std::string& GetTitleText()             { return m_strTitle;	}

	void SetYAxisesText(char* leftText, char* rightText) 
	{ 
		m_strYRightTitle = rightText; 
		m_strYLeftTitle = leftText;
		Invalidate(FALSE);	
	}

	void SetBgColor(COLORREF bgColor) { m_bgColor = bgColor; Invalidate(FALSE);	}
	COLORREF GetBgColor()             { return m_bgColor;                       }

	void SetLnColor(COLORREF lnColor) { m_lnColor = lnColor; Invalidate(FALSE); }
	COLORREF GetLnColor()             { return m_lnColor;                       }

	void SetGrColor(COLORREF grColor) { m_grColor = grColor; Invalidate(FALSE); }
	COLORREF GetGrColor()             { return m_grColor;                       }

	void SetBrColor(COLORREF brColor) { m_brColor = brColor; Invalidate(FALSE); }
	COLORREF GetBrColor()             { return m_brColor;                       }

	void UseNewMinMax(bool use) { m_useNewMinMax = use; }
	bool UseNewMinMax(void)     { return m_useNewMinMax }

	void UsePadding(bool use) { m_usePadding = use; }
	bool UsePadding(void)     { return m_usePadding; }

	void SetXRange(double min, double max)
	{
		m_xMin = min;
		m_xMax = max;
	}
	void GetXRange(double& min, double& max)
	{ 
		min = m_xMin;
		max = m_xMax;
	}

	void SetYRange(double min, double max)
	{
		m_yMin = m_yMinPad = min;
		m_yMax = m_yMaxPad = max;
	}
	void GetYRange(double& min, double& max)
	{ 
		min = m_yMinPad;
		max = m_yMaxPad;
	}

	void SetPoints(size_t time, double* values, size_t count)
	{
		m_time = time;
		m_values = values;
		m_pointsCnt = count;

		if (m_points) delete [] m_points;
		m_points = new POINT[m_pointsCnt];
		
//		_UpdateYRange();
//		_Recalc();
	}

	void SetHandler(OscHandler* handler) { m_handler = handler; }

	// Message map and handlers

	BEGIN_MSG_MAP(thisClass)
		MESSAGE_HANDLER(WM_CREATE, OnCreate)
		MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_SIZE, OnSize)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnLMouseDown)
		MESSAGE_HANDLER(WM_RBUTTONDOWN, OnRMouseDown)
		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnLMouseUp)
		DEFAULT_REFLECTION_HANDLER()
	END_MSG_MAP()

	LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& /*bHandled*/)
	{
		LRESULT lRes = DefWindowProc(uMsg, wParam, lParam);
		CPaintDC dc(m_hWnd); 
		m_VirtScreenDC.CreateCompatibleDC(dc);
		m_VirtScreenBitmap.CreateCompatibleBitmap(dc, 2, 2);
		_Init();
		return lRes;
	}
	bool _UpdateVirtualScreen()
	{
		CPaintDC dc(m_hWnd); 
		BITMAP BMStruct; 
		BMStruct.bmWidth = BMStruct.bmHeight = 0;

		// ������� ������� � �������� 
		int scrw=dc.GetDeviceCaps(HORZRES);
		int scrh=dc.GetDeviceCaps(VERTRES);
		// ������� ��������� ������ � ���������
		// ��� ��������� m_VirtScreenBitmap
		// (���� ��� ���� ����� ������� � ���������)
		// � ���� ����������� ������� �� 
		CBitmap TempBM; TempBM.CreateCompatibleBitmap(dc,1,1);
		m_VirtScreenDC.SelectBitmap(TempBM);
		// �������� ����� �������������� ������
		m_VirtScreenBitmap.DeleteObject();

		// � �� �� ����� �������� ����� �� �������� �����������,
		// �� ������ �������� �������
		if(!m_VirtScreenBitmap.CreateCompatibleBitmap(dc, scrw, scrh))
			return false;
		// ����� ������ ������� � ��������� ������������ ������
		m_VirtScreenDC.SelectBitmap(m_VirtScreenBitmap);		

		// ������� ����������� �����
		CBrush FonBrush(HBRUSH(RGB(127, 127, 127)) ); // ����� ��� ������� ����
		m_VirtScreenBitmap.GetBitmap(&BMStruct); // ������ ������� ������
		m_VirtScreenDC.FillRect(&CRect(0,0,
						BMStruct.bmWidth, BMStruct.bmHeight), FonBrush);
		
		// ������� �� ����������� ����� ��������
		{
			CWaitCursor wc;
			_DrawBackground(&m_VirtScreenDC);
			_DrawGrid(&m_VirtScreenDC);
//			::MessageBox(*this, "", _T("before draw curves"), MB_OK);
			_DrawCurve(&m_VirtScreenDC);
			_DrawCurves(&m_VirtScreenDC);
			_DrawPoints(&m_VirtScreenDC);
			_DrawTitle(&m_VirtScreenDC);/**/
		}

		// ������� ����������� �� ������
		Invalidate();
		return true;
	}

	bool GetBoundRect(LPRECT lpRect, HDC hdcRef = 0)
	{
		ATLASSERT( lpRect );
		if( !hdcRef )
			hdcRef = GetDC();

		// Determine the picture frame dimensions. 
		// iWidthMM is the display width in millimeters. 
		// iHeightMM is the display height in millimeters. 
		// iWidthPels is the display width in pixels. 
		// iHeightPels is the display height in pixels 
		int iWidthMM = GetDeviceCaps(hdcRef, HORZSIZE); 
		int iHeightMM = GetDeviceCaps(hdcRef, VERTSIZE); 
		int iWidthPels = GetDeviceCaps(hdcRef, HORZRES); 
		int iHeightPels = GetDeviceCaps(hdcRef, VERTRES); 		
		
		GetClientRect( lpRect );

		// Convert client coordinates to .01-mm units. 
		// Use iWidthMM, iWidthPels, iHeightMM, and 
		// iHeightPels to determine the number of 
		// .01-millimeter units per pixel in the x- 
		//  and y-directions.
		lpRect->left = (lpRect->left * iWidthMM * 100) / iWidthPels; 
		lpRect->top = (lpRect->top * iHeightMM * 100) / iHeightPels; 
		lpRect->right = (lpRect->right * iWidthMM * 100) / iWidthPels; 
		lpRect->bottom = (lpRect->bottom * iHeightMM * 100) / iHeightPels;
		////////////////////////////////////////////////////////////////////////

		
		return true;
	}
	bool RecordToEMF(std::string strPath, LPRECT lpBoundRect, HDC dcRef = 0)
	{
		ATLASSERT(lpBoundRect);

		GetBoundRect(lpBoundRect, dcRef);

		HDC dcEMF = ::CreateEnhMetaFile(/*hdcRef*/0, strPath.c_str(), 
				lpBoundRect, _T("emfpic") );

		CDC dc(dcEMF);
//		_DrawBackground(&dc);
		_DrawGrid(&dc);
		_DrawCurve(&dc);
		_DrawCurves(&dc);
		_DrawPoints(&dc);
		_DrawTitle(&dc);

		GetClientRect( lpBoundRect );
		HENHMETAFILE hEMHF = ::CloseEnhMetaFile(dcEMF);
		::DeleteEnhMetaFile(hEMHF);


		return true;
	}


	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& /*bHandled*/)
	{
		LRESULT lRes = DefWindowProc(uMsg, wParam, lParam);
		_Close();
		return lRes;
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
	
		RECT rc;
		GetClientRect(&rc);
		
		CPaintDC dc(m_hWnd);
		dc.BitBlt(0, 0, rc.right, rc.bottom, m_VirtScreenDC, 0, 0, SRCCOPY);

		bHandled = TRUE;
		return 1;
	}

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& /*bHandled*/)
	{
		_Recalc();
		Invalidate(FALSE);
		return 0;
	}

	LRESULT OnLMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& /*bHandled*/)
	{
		if ( m_zooming || !m_handler->EnableZoom() ) return 0;

		m_zooming = true;
		::SetCapture(m_hWnd);

		m_startZoomPnt.x = GET_X_LPARAM(lParam);
		m_startZoomPnt.y = GET_Y_LPARAM(lParam);
		m_endZoomPnt.x   = GET_X_LPARAM(lParam);
		m_endZoomPnt.y   = GET_Y_LPARAM(lParam);
		_DrawZoom();

		return 0;
	}

	LRESULT OnRMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& /*bHandled*/)
	{
		if ( m_zooming ) {
			_DrawZoom();
			
			m_zooming = false;
			::ReleaseCapture();
		} else if ( m_handler->EnableZoom() )
			m_handler->Zoom( (wParam & MK_CONTROL) ? ZoomOutFull : ZoomOut );

		return 0;
	}
	
	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& /*bHandled*/)
	{
		if ( !m_zooming ) return 0;
		
		_DrawZoom();
		m_endZoomPnt.x = GET_X_LPARAM(lParam);
		m_endZoomPnt.y = GET_Y_LPARAM(lParam);
		_DrawZoom();

		return 0;
	}

	LRESULT OnLMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& /*bHandled*/)
	{
		if ( !m_zooming ) return 0;

		_DrawZoom();
		
		m_zooming = false;
		::ReleaseCapture();

		m_handler->Zoom(&m_startZoomPnt, &m_endZoomPnt);

		return 0;
	}

	void Zoom(POINT* start, POINT* end)
	{
		RECT rc;
		GetClientRect(&rc);

		double xMin = _XCoord2Value(rc.left,   min(start->x, end->x));
		double yMin = _YCoord2Value(rc.bottom, max(start->y, end->y));
		double xMax = _XCoord2Value(rc.left,   max(start->x, end->x));
		double yMax = _YCoord2Value(rc.bottom, min(start->y, end->y));

		_InflateEpsilon(xMin, xMax);
		_InflateEpsilon(yMin, yMax);

		double xPad = _FindPadding(xMin, xMax) / 4;
		double yPad = _FindPadding(yMin, yMax) / 4;

		switch( m_handler->ZoomMode() )
		{
		case ZoomRect:
			SetXRange(xMin-xPad, xMax+xPad);
			SetYRange(yMin-yPad, yMax+yPad);
			break;
		case ZoomHorz:
			SetXRange(xMin-xPad, xMax+xPad);
			break;
		case ZoomVert:
			SetYRange(yMin-yPad, yMax+yPad);
			break;
		}
		_Recalc();
	}

	void Zoom(OscZoomRect* zoom)
	{
		SetXRange(zoom->xMin, zoom->xMax);
		SetYRange(zoom->yMin, zoom->yMax);
		_Recalc();
	}

private:

	// Implementation

	void _Init()
	{
		ATLASSERT(::IsWindow(m_hWnd));

		m_bgColor = RGB(  0,   0,   0);
		m_lnColor = RGB(255, 255, 255);
		m_grColor = RGB(128, 128, 128);
		m_brColor = RGB(255, 255,   0);
		
		m_time = 0;
		m_values = NULL;

		m_points = NULL;
		m_pointsCnt = 0;
		m_Dispoints = 0;
		m_DispointsCnt = 0;

		SetXRange(0.0, 1.0);
		SetYRange(0.0, 1.0);

		m_zooming = false;

		m_handler = NULL;
		m_strTitle = "Plot Caption";
		m_strYLeftTitle = _T("");
		m_strYRightTitle = _T("");
	}

	void _Close()
	{
		if (m_points)
		{
			delete [] m_points;
			m_points = 0;
		}
		if (m_Dispoints) 
		{
//			free(m_Dispoints);
			m_Dispoints = 0;
		}
/**/		RemoveAllCurves();

		std::vector<POINT*>::iterator theIterator;
		for(theIterator = m_DisPointsVector.begin(); 
			theIterator != m_DisPointsVector.end(); ++theIterator )
		{
			POINT* pts = *theIterator;
/*			if (pts) free( pts );
			pts = 0;*/
		}
		m_DisPointsVector.erase( m_DisPointsVector.begin(), m_DisPointsVector.end() );
	}

	void _UpdateYRange()
	{
		if ( !m_values )
			return;
		
		if (m_handler->UseNewMinMax() || m_pointsCnt == 1)
			m_yMin = m_yMax = m_values[0];

		double min, max;
		_FindMinMax(min, max, m_values, m_pointsCnt);
		if ( min < m_yMin )	m_yMin = min;
		if ( max > m_yMax )	m_yMax = max;

		double pad = m_handler->UsePadding() ? _FindPadding(min, max) / m_handler->PaddingCoef() : 0.0;
		m_yMinPad = m_yMin - pad;
		m_yMaxPad = m_yMax + pad;
	}

	void _Recalc()
	{
		size_t ms = _get_sbh_threshold();
		_set_sbh_threshold(16);
		RECT rc;
		GetClientRect(&rc);
		// recalc koefs
		m_XK = (rc.right - rc.left) / (m_xMax - m_xMin);
		m_YK = (rc.bottom - rc.top) / (m_yMaxPad - m_yMinPad);


		if (!m_points || m_pointsCnt < 1 || !m_values)
			return;
		
/*******************************speed optimization section***************************/
		short int iStep = 1;
		if( m_points && (m_pointsCnt > 1) )
		{
			size_t msize = 0;
			int iScale =  m_pointsCnt / (m_xMax - m_xMin);
			iScale = max(1, iScale);
			iStep = m_pointsCnt / max(1, rc.right - rc.left) / iScale;
			iStep = max(1, iStep);
			m_DispointsCnt = m_pointsCnt / iStep;
			m_DispointsCnt = min(m_DispointsCnt, m_pointsCnt);
			m_DispointsCnt = max(m_DispointsCnt, 2);
/*			if (m_Dispoints) 
				msize  = _msize(m_Dispoints);*/
//			m_Dispoints = new POINT[m_DispointsCnt];
			m_Dispoints = (POINT*)realloc(m_Dispoints, m_DispointsCnt*sizeof(POINT));
			if(m_Dispoints == NULL)
			{
				::MessageBox(*this, "m_Dispoints == NULL", "", MB_OK);
				return;
			}
/*			if (m_Dispoints) 
				msize  = _msize(m_Dispoints);*/

			std::vector<POINT*>::iterator theIterator;

			long sz = m_DisPointsVector.size();

			for(theIterator = m_DisPointsVector.begin(); 
				theIterator != m_DisPointsVector.end(); ++theIterator )
			{
				POINT* pts = *theIterator;
//				if (pts) delete [] pts;
				if (*theIterator) 
					msize  = _msize(*theIterator);
				*theIterator = (POINT*)realloc(*theIterator, m_DispointsCnt*sizeof(POINT));

				if(*theIterator == NULL)
				{
					::MessageBox(*this, "*theIterator == NULL", "", MB_OK);
					return;
				}
/*				if (*theIterator) 
					msize  = _msize(*theIterator);*/

			}

		}
/*******************************speed optimization section***************************/

		// recalc points
		if( int(m_values) == 0xfdfdfdfd  || int(m_values) == 0xdddddddd ||
			int(m_values) == 0x000000dc)	return;
		POINT ptVisible;
		memset(&ptVisible, 0, sizeof(POINT) );
		for(size_t i=0; i<m_pointsCnt; i++) 
		{
			m_points[i].x = _XValue2Coord(rc.left,   m_time + i);
			m_points[i].y = _YValue2Coord(rc.bottom, m_values[i]);
			if( (i % iStep) == 0 && i)
			{
				m_Dispoints[i / iStep].x = ptVisible.x;//m_points[i];
				m_Dispoints[i / iStep].y = ptVisible.y / iStep;//m_points[i];
				memset(&ptVisible, 0, sizeof(POINT) );
			}
			ptVisible.y += m_points[i].y;
			ptVisible.x = m_points[i].x;

		}
/////secondary curves manipulation
		for(i=0; i<m_PointsVector.size(); i++) 
		{
			POINT* pts = m_PointsVector[i];
			POINT* dpts = m_DisPointsVector[i];
			double* vls = m_ValuesVector[i];
			if( pts == 0 || vls == 0 || dpts == 0)
				continue;
			memset(&ptVisible, 0, sizeof(POINT) );

			dpts[0].x = 0;
			dpts[0].y =  0;
			for(size_t idx=0; idx<m_pointsCnt; idx++) 
			{
				pts[idx].x = _XValue2Coord(rc.left,   m_time + idx);
				pts[idx].y = _YValue2Coord(rc.bottom, vls[idx]*m_KoefVector[i]);
				pts[idx].y = max( pts[idx].y, -1e6);
				pts[idx].y = min( pts[idx].y, 1e6 );
//				dpts[idx / iStep] = pts[idx];

				if( idx == 0 )
				{
					dpts[idx].x = pts[idx].x;
					dpts[idx].y =  pts[idx].y;
				}
				if( (idx % iStep) == 0 && idx)
				{
					dpts[idx / iStep].x = ptVisible.x;//m_points[i];
					dpts[idx / iStep].y = ptVisible.y / iStep;//m_points[i];
					memset(&ptVisible, 0, sizeof(POINT) );
				}
				ptVisible.y += pts[idx].y;
				ptVisible.x = pts[idx].x;
			}
			dpts[(idx-1) / iStep].x = ptVisible.x;//
			dpts[(idx-1) / iStep].y = ptVisible.y / iStep;//
		}
/////secondary curves manipulation EOF
		_UpdateVirtualScreen();
	}

	int _XValue2Coord(int offset, double value)
	{
		return offset + int((value - m_xMin) * m_XK);
	}

	int _YValue2Coord(int offset, double value)
	{
		return offset - int((value - m_yMinPad) * m_YK) - 1;
	}

	double _XCoord2Value(int offset, int coord)
	{
		return m_xMin + double(coord - offset) / m_XK;
	}

	double _YCoord2Value(int offset, int coord)
	{
		return m_yMinPad + double(offset - coord - 1) / m_YK;
	}

	void _FindMinMax(double& min, double& max, double* values, size_t cnt)
	{
		if ( !values )
			return;

		min = max = values[0];
		for(size_t i=0; i<cnt; i++) {
			if ( values[i] < min )	min = values[i];
			if ( values[i] > max )	max = values[i];
		}

		_InflateEpsilon(min, max);
	}

	void _InflateEpsilon(double& min, double& max)
	{
		if ( OSC_ZERO(max - min) ) {
			min -= OSC_EPSILON;
			max += OSC_EPSILON;
		}
	}

	double _FindPadding(double min, double max)
	{
		double diff = fabs( (max - min)/10 );
		int h = OSC_ZERO(diff) ? 0 : (int)floor( log10(diff) );
		double step1 = 1.0 * pow(10.0, h);
		double step2 = 2.0 * pow(10.0, h);
		double step5 = 5.0 * pow(10.0, h);
		if (diff == step1)
				return step1;
		else if (diff <= step2)
				return step2;
		else if (diff <= step5)
				return step5;
		else	return pow(10, h + 1);
	}

	void _DrawBackground(CDC* dc)
	{
		RECT rc;
		GetClientRect(&rc);
		dc->FillSolidRect(rc.left, rc.top, rc.right, rc.bottom, m_bgColor);
	}
	
	void _DrawGrid(CDC* dc)
	{
		if ( !m_handler->UseGrid() )
			return;
		
		RECT rc;
		GetClientRect(&rc);

		POINT pnts[2];
		CPen pen;
		pen.CreatePen(PS_DOT, 1, m_grColor);
		HPEN oldPen = dc->SelectPen(pen);

		// draw vertivcal lines
		double xStep = _FindPadding(m_xMin, m_xMax);
		double xStart = m_xMin - fmod(m_xMin, xStep) - xStep;
		for(; xStart <= m_xMax; xStart += xStep) {
			pnts[0].x = _XValue2Coord(rc.left, xStart);
			pnts[0].y = rc.top;
			pnts[1].x = pnts[0].x;
			pnts[1].y = rc.bottom;
			dc->Polyline(pnts, 2);
		}
		// draw horizontal lines
		double yStep = _FindPadding(m_yMinPad, m_yMaxPad);
		double yStart = m_yMinPad - fmod(m_yMinPad, yStep) - yStep;
		for(; yStart <= m_yMaxPad; yStart += yStep) {
			pnts[0].x = rc.left;
			pnts[0].y = _YValue2Coord(rc.bottom, yStart);
			pnts[1].x = rc.right;
			pnts[1].y = pnts[0].y;
			dc->Polyline(pnts, 2);
		}
		
		if (oldPen) dc->SelectPen(oldPen);
	}

	void _DrawCurve(CDC* dc)
	{
		CPen pen;
		pen.CreatePen(PS_SOLID, 1, m_lnColor);
		HPEN oldPen = dc->SelectPen(pen);

/*		if (m_points && m_pointsCnt > 1)
			dc->Polyline(m_points, m_pointsCnt);*/
		if (m_Dispoints && m_DispointsCnt > 1)
			dc->Polyline(m_Dispoints, m_DispointsCnt);
		
	
		if (oldPen) dc->SelectPen(oldPen);
	}
	void _DrawCurves(CDC* dc)
	{
		CPen pen;
		pen.CreatePen(PS_SOLID, 1, m_lnColor);
		HPEN oldPen = dc->SelectPen(pen);


		for(int i=0; i<m_PointsVector.size(); i++) 
		{
			CPen pn;
			pn.CreatePen(PS_SOLID, 1, m_LnColorVector[i]);
			dc->SelectPen(pn);

/*			POINT* pts = m_PointsVector[i];
			if (pts && m_pointsCnt > 1)
				dc->Polyline(pts, m_pointsCnt);*/
			POINT* dpts = m_DisPointsVector[i];
			if (dpts && m_pointsCnt > 1)
				dc->Polyline(dpts, m_DispointsCnt);

		}
/////secondary curves manipulation EOF

	
		if (oldPen) dc->SelectPen(oldPen);
	}

	void _DrawPoints(CDC* dc)
	{
		OscPointStyle style = m_handler->PointStyle();
		if ( style == None )
			return;
		
		CPen pen;
		pen.CreatePen(PS_SOLID, 1, m_lnColor);
		HPEN oldPen = dc->SelectPen(pen);
		CBrush brush;
		brush.CreateSolidBrush( m_brColor );
		HBRUSH oldBrush = dc->SelectBrush(brush);

		POINT pnts[4];
		for(size_t i=0; i<m_pointsCnt; i++)
			switch( style )
			{
			case UTriangle:
				pnts[0].x = m_points[i].x - 3; pnts[0].y = m_points[i].y  + 3;
				pnts[1].x = m_points[i].x + 0; pnts[1].y = m_points[i].y  - 4;
				pnts[2].x = m_points[i].x + 3; pnts[2].y = m_points[i].y  + 3;
				dc->Polygon(pnts, 3);
				break;
			case DTriangle:
				pnts[0].x = m_points[i].x - 3; pnts[0].y = m_points[i].y  - 3;
				pnts[1].x = m_points[i].x + 0; pnts[1].y = m_points[i].y  + 4;
				pnts[2].x = m_points[i].x + 3; pnts[2].y = m_points[i].y  - 3;
				dc->Polygon(pnts, 3);
				break;
			case Diamond:
				pnts[0].x = m_points[i].x - 3; pnts[0].y = m_points[i].y  - 0;
				pnts[1].x = m_points[i].x + 0; pnts[1].y = m_points[i].y  + 5;
				pnts[2].x = m_points[i].x + 3; pnts[2].y = m_points[i].y  - 0;
				pnts[3].x = m_points[i].x + 0; pnts[3].y = m_points[i].y  - 5;
				dc->Polygon(pnts, 4);
				break;
			case Circle:
				dc->Ellipse( m_points[i].x - 3, m_points[i].y - 3,
							 m_points[i].x + 4, m_points[i].y + 4 );
				break;
			case Cross:
				dc->MoveTo(m_points[i].x - 3, m_points[i].y - 0);
				dc->LineTo(m_points[i].x + 4, m_points[i].y + 0);
				dc->MoveTo(m_points[i].x - 0, m_points[i].y + 3);
				dc->LineTo(m_points[i].x + 0, m_points[i].y - 4);
				break;
			case XCross:
				dc->MoveTo(m_points[i].x - 3, m_points[i].y - 3);
				dc->LineTo(m_points[i].x + 4, m_points[i].y + 4);
				dc->MoveTo(m_points[i].x - 3, m_points[i].y + 3);
				dc->LineTo(m_points[i].x + 4, m_points[i].y - 4);
				break;
			}
		
		if (oldPen)		dc->SelectPen(oldPen);
		if (oldBrush)	dc->SelectBrush(oldBrush);
	}
	void _DrawTitle(CDC* dc)	// my func :-)
	{
		unsigned int uHeight = 12, uWidth = m_strTitle.length()*5 + 5;
		LOGFONT lf;
		memset(&lf, 0, sizeof(LOGFONT));
		lf.lfHeight = 12;
		strcpy(lf.lfFaceName, "Arial CYR");
	
		CFont		font;
		font.CreateFontIndirect(&lf);
		HFONT oldFont = dc->SelectFont(font);

		RECT rc, ltrc, rtrc;
		GetClientRect(&rc);
		ltrc = rtrc = rc;
		ltrc.right = ltrc.left + m_strYLeftTitle.length()*5;
		ltrc.top = ltrc.top+uHeight/2;
		ltrc.bottom = ltrc.top + uHeight;
		
		rtrc.left = rtrc.right - m_strYRightTitle.length()*5;
		rtrc.top = rtrc.top+uHeight/2;
		rtrc.bottom = rtrc.top + uHeight;

		rc.left = rc.left+(rc.right-rc.left)/2 - uWidth/2;
		rc.top = rc.top+uHeight/2;
		rc.bottom = rc.top + uHeight;
		rc.right = rc.left + uWidth;
		dc->FillSolidRect( &rc, RGB(255,255,255) );

//		dc->FillSolidRect( &rtrc, RGB(255,255,255) );
//		dc->FillSolidRect( &ltrc, RGB(255,255,255) );

		COLORREF oldClr = dc->GetTextColor();
		dc->SetTextColor( RGB(255, 0, 155) );
		
		dc->DrawText(m_strTitle.c_str(), -1, &rc, DT_CENTER | DT_SINGLELINE);

		dc->DrawText(m_strYRightTitle.c_str(), -1, &rtrc, DT_CENTER | DT_SINGLELINE);
		dc->DrawText(m_strYLeftTitle.c_str(), -1, &ltrc, DT_CENTER | DT_SINGLELINE);

		if (oldFont) dc->SelectFont(oldFont);
		if (oldClr) dc->SetTextColor( oldClr );
	}


	void _DrawZoom()
	{
		CDC dc = GetDC();

		RECT rc;
		GetClientRect(&rc);
		RECT rc_zoom = { max(min(m_startZoomPnt.x, m_endZoomPnt.x), 0),
						 max(min(m_startZoomPnt.y, m_endZoomPnt.y), 0),
						 min(max(m_startZoomPnt.x, m_endZoomPnt.x), rc.right),
						 min(max(m_startZoomPnt.y, m_endZoomPnt.y), rc.bottom) };

		switch( m_handler->ZoomMode() )
		{
		case ZoomHorz:
			rc_zoom.top = rc.top;
			rc_zoom.bottom = rc.bottom;
			break;
		case ZoomVert:
			rc_zoom.left = rc.left;
			rc_zoom.right = rc.right;
			break;
		}
	
		dc.DrawFocusRect(&rc_zoom);
		ReleaseDC(dc);
	}
};
/////////////////////////////////////////////////////////////////////////////
class OscilloscopeChannel : public OscilloscopeChannelImpl<OscilloscopeChannel>
{
public:
	DECLARE_WND_SUPERCLASS(_T("WTL_OscilloscopeChannel"), GetWndClassName())  
};
/////////////////////////////////////////////////////////////////////////////
template< class T, class TBase = CStatic, class TWinTraits = CControlWinTraits >
class ATL_NO_VTABLE OscilloscopeAxisImpl : public CWindowImpl< T, TBase, TWinTraits >
{
	bool		m_horiz;
	bool		m_invert;
	bool		m_bTimeAxis;

	COLORREF	m_bgColor;
	COLORREF	m_frColor;
	CFont		m_font;

	double		m_min, m_max;
	double		m_k;

	double		m_step;
	int			m_ticks;

public:
	typedef OscilloscopeAxisImpl< T, TBase, TWinTraits > thisClass;

	DECLARE_WND_SUPERCLASS(NULL, TBase::GetWndClassName())

	// Interface

	void SetBgColor(COLORREF bgColor)
	{
		m_bgColor = bgColor;
		Invalidate(FALSE);
	}
	COLORREF GetBgColor() { return m_bgColor; }

	void SetFrColor(COLORREF frColor)
	{
		m_frColor = frColor;
		Invalidate(FALSE);
	}
	COLORREF GetFrColor() { return m_frColor; }

	void SetHorizontal()
	{
		m_horiz = true;
		_Recalc();
	}
	void SetTimeAxis()
	{
		m_bTimeAxis = true;
		_Recalc();
	}
	void SetVertical()
	{ 
		m_horiz = false;
		_Recalc();
	}
	bool IsHorizontal() { return m_horiz; }
	bool IsTimeAxis() { return m_bTimeAxis; }

	void SetInvert(bool invert)
	{ 
		m_invert = invert;
		Invalidate(FALSE);
	}
	bool IsInvert() { return m_invert; }

	void GetRange(double& min, double& max)
	{
		min = m_min; max = m_max;
	}
	bool SetRange(double min, double max)
	{
		if (m_min == min && m_max == max)
			return false;

		m_min = min;
		m_max = max;
		_Recalc();

		return true;
	}

	double GetStep() { return m_step; }
		
	// Message map and handlers

	BEGIN_MSG_MAP(thisClass)
		MESSAGE_HANDLER(WM_CREATE, OnCreate)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_SIZE, OnSize)
		DEFAULT_REFLECTION_HANDLER()
	END_MSG_MAP()

	LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& /*bHandled*/)
	{
		LRESULT lRes = DefWindowProc(uMsg, wParam, lParam);
		_Init();
		return lRes;
	}

	void _Draw(CDC& dc, bool bDrawBg = true)
	{
		RECT rc;
		GetClientRect(&rc);
		dc.FillSolidRect(rc.left, rc.top, rc.right, rc.bottom, 
			bDrawBg ? m_bgColor : RGB(255, 255, 255) );

		CPen pen;
		pen.CreatePen(PS_SOLID, 1, m_frColor);
		HPEN oldPen = dc.SelectPen(pen);

		HFONT oldFont = dc.SelectFont(m_font);

		::InflateRect(&rc, -OSC_OBJ_OFFSET_X, -OSC_OBJ_OFFSET_Y);

		// draw axis line
		POINT pnts[2];
		if (m_horiz) {
			pnts[0].x = rc.left;  pnts[0].y = m_invert ? rc.bottom : rc.top;
			pnts[1].x = rc.right; pnts[1].y = m_invert ? rc.bottom : rc.top;
		} else {
			pnts[0].x = m_invert ? rc.left : rc.right; pnts[0].y = rc.top;
			pnts[1].x = m_invert ? rc.left : rc.right; pnts[1].y = rc.bottom;
		}
		dc.Polyline(pnts, 2);
		
		// draw axis ticks and labels
		double start = m_min - fmod(m_min, m_step) - m_step;
		double stepPart = (m_step / (double)(m_ticks + 1));
		for(; start <= m_max; start += m_step) {
			// draw big tick
			if (m_horiz) 
			{
				if(m_bTimeAxis)
				{
					pnts[0].x = _Value2Coord(start);
					if (pnts[0].x >= rc.left && pnts[0].x <= rc.right) 
					{
						pnts[0].y = m_invert ? rc.bottom : rc.top;
						pnts[1].x = pnts[0].x;
						pnts[1].y = m_invert ? pnts[0].y - OSC_BIG_TIC_LEN : pnts[0].y + OSC_BIG_TIC_LEN;
						dc.Polyline(pnts, 2);
						// draw label
						char buf[128] = {'\0'};;
//						sprintf(buf, OSC_LABEL_FORMAT, OSC_ZERO(start) ? 0.0 : start);
						time_t tstart = time_t(max(0, start));
						struct tm* tm_start = localtime( &tstart );
						if(tm_start)	strftime(buf, 128, OSC_TIME_LABEL_FORMAT, tm_start);
						RECT text_rc = { pnts[1].x - 1, pnts[1].y, pnts[1].x + 1,  pnts[1].y};
						dc.DrawText(buf, -1, &text_rc, DT_CENTER | (m_invert ? DT_BOTTOM : DT_TOP) | DT_NOCLIP| DT_SINGLELINE);
					}
				}
				else
				{
					pnts[0].x = _Value2Coord(start);
					if (pnts[0].x >= rc.left && pnts[0].x <= rc.right) 
					{
						pnts[0].y = m_invert ? rc.bottom : rc.top;
						pnts[1].x = pnts[0].x;
						pnts[1].y = m_invert ? pnts[0].y - OSC_BIG_TIC_LEN : pnts[0].y + OSC_BIG_TIC_LEN;
						dc.Polyline(pnts, 2);
						// draw label
						char buf[128];
						sprintf(buf, OSC_LABEL_FORMAT, OSC_ZERO(start) ? 0.0 : start);
						RECT text_rc = { pnts[1].x - 1, pnts[1].y, pnts[1].x + 1,  pnts[1].y};
						dc.DrawText(buf, -1, &text_rc, DT_CENTER | (m_invert ? DT_BOTTOM : DT_TOP) | DT_NOCLIP| DT_SINGLELINE);
					}
				}

			} 
			else {
				pnts[0].y = _Value2Coord(start);
				if (pnts[0].y >= rc.top && pnts[0].y <= rc.bottom) {
					pnts[0].x = m_invert ? rc.left : rc.right;                   
					pnts[1].x = m_invert ? rc.left + OSC_BIG_TIC_LEN : rc.right - OSC_BIG_TIC_LEN;
					pnts[1].y = pnts[0].y;
					dc.Polyline(pnts, 2);
					// draw label
					char buf[128];
					sprintf(buf, OSC_LABEL_FORMAT, OSC_ZERO(start) ? 0.0 : start);
					RECT text_rc = { pnts[1].x + 3, pnts[1].y, pnts[1].x - 2, pnts[1].y};
					dc.DrawText(buf, -1, &text_rc, DT_VCENTER | (m_invert ? DT_LEFT : DT_RIGHT) | DT_NOCLIP| DT_SINGLELINE);
				}
			}
			// draw small ticks
			for(int i=0;i<m_ticks;i++) {
				double x = start + (double)(i + 1) * stepPart;
				if (m_horiz) 
				{
					pnts[0].x = _Value2Coord(x);
					if (pnts[0].x >= rc.left && pnts[0].x <= rc.right) {
						pnts[0].y = m_invert ? rc.bottom : rc.top;
						pnts[1].x = pnts[0].x;
						pnts[1].y = m_invert ? pnts[0].y - OSC_SMALL_TIC_LEN : rc.top + OSC_SMALL_TIC_LEN;
						dc.Polyline(pnts, 2);
					}
				} else {
					pnts[0].y = _Value2Coord(x);
					if (pnts[0].y >= rc.top && pnts[0].y <= rc.bottom) {
						pnts[0].x = m_invert ? rc.left : rc.right;
						pnts[1].x = m_invert ? rc.left + OSC_SMALL_TIC_LEN : rc.right - OSC_SMALL_TIC_LEN;
						pnts[1].y = pnts[0].y;
						dc.Polyline(pnts, 2);
					}
				}
			}
		}

		if (oldFont) dc.SelectFont(oldFont);
		if (oldPen) dc.SelectPen(oldPen);
	}
	bool GetBoundRect(LPRECT lpRect, HDC hdcRef = 0)
	{
		ATLASSERT( lpRect );
		if( !hdcRef )
			hdcRef = GetDC();

		// Determine the picture frame dimensions. 
		// iWidthMM is the display width in millimeters. 
		// iHeightMM is the display height in millimeters. 
		// iWidthPels is the display width in pixels. 
		// iHeightPels is the display height in pixels 
		int iWidthMM = GetDeviceCaps(hdcRef, HORZSIZE); 
		int iHeightMM = GetDeviceCaps(hdcRef, VERTSIZE); 
		int iWidthPels = GetDeviceCaps(hdcRef, HORZRES); 
		int iHeightPels = GetDeviceCaps(hdcRef, VERTRES); 		
		
		GetClientRect( lpRect );

		// Convert client coordinates to .01-mm units. 
		// Use iWidthMM, iWidthPels, iHeightMM, and 
		// iHeightPels to determine the number of 
		// .01-millimeter units per pixel in the x- 
		//  and y-directions.

		lpRect->left = (lpRect->left * iWidthMM * 100) / iWidthPels; 
		lpRect->top = (lpRect->top * iHeightMM * 100) / iHeightPels; 
		lpRect->right = (lpRect->right * iWidthMM * 100) / iWidthPels; 
		lpRect->bottom = (lpRect->bottom * iHeightMM * 100) / iHeightPels;
		////////////////////////////////////////////////////////////////////////
		
		return true;
	}
	bool RecordToEMF(std::string strPath, LPRECT lpBoundRect, HDC dcRef = 0)
	{
		ATLASSERT(lpBoundRect);

		GetBoundRect(lpBoundRect, dcRef);

		HDC dcEMF = ::CreateEnhMetaFile(/*hdcRef*/0, strPath.c_str(), lpBoundRect, _T("axis") );

		CDC dc(dcEMF);

		_Draw( dc, false );

		GetClientRect( lpBoundRect );
		HENHMETAFILE hEMHF = ::CloseEnhMetaFile(dcEMF);
		::DeleteEnhMetaFile(hEMHF);
		return true;
	}


	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		PAINTSTRUCT ps;
		CDC dc = BeginPaint(&ps);
		
		_Draw( dc );
		EndPaint(&ps);
		
		bHandled = TRUE;
		return 0;
	}

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& /*bHandled*/)
	{
		_Recalc();
		Invalidate(FALSE);
		
		return 0;
	}

private:

	// Implementation

	void _Init()
	{
		ATLASSERT(::IsWindow(m_hWnd));

		m_horiz   = true;
		m_invert  = false;
		m_bTimeAxis = false;
		m_bgColor = RGB(250, 250, 255);//::GetSysColor(CTLCOLOR_DLG);
		m_frColor = RGB(0, 0, 0);

		LOGFONT lf;
		memset(&lf, 0, sizeof(LOGFONT));
		lf.lfHeight = 12;
		strcpy(lf.lfFaceName, "Arial");
		m_font.CreateFontIndirect(&lf);

		SetRange(0.0, 1.0);
	}

	void _Recalc()
	{
		RECT rc;
		GetClientRect(&rc);
		::InflateRect(&rc, -OSC_OBJ_OFFSET_X, -OSC_OBJ_OFFSET_Y);

		if (m_horiz)
				m_k = (rc.right - rc.left) / (m_max - m_min);
		else	m_k = (rc.bottom - rc.top) / (m_max - m_min);

		bool halfStep;
		m_step = _FindStep(m_min, m_max, halfStep);

		int coord1 = _Value2Coord(m_min);
		int coord2 = _Value2Coord(m_max + m_step);
		int coord_dif;
		if (m_horiz)
				coord_dif = coord2 - coord1;
		else	coord_dif = coord1 - coord2;

		if (coord_dif >= OSC_MIN_TIC1_DIST)
			if (halfStep)
					m_ticks = (coord_dif >= OSC_MIN_TIC4_DIST) ? 4 : 1;
			else	m_ticks = (coord_dif >= OSC_MIN_TIC3_DIST) ? 3 : 1;
		else		m_ticks = 0;
	}

	double _FindStep(double min, double max, bool& halfStep)
	{
//		if(m_bTimeAxis)	return _FindTimeStep(min, max, halfStep);
		halfStep = false;
		
		double diff = fabs( (max - min)/10 );
		int h = OSC_ZERO(diff) ? 0 : (int)floor( log10(diff) );
		double step1 = 1.0 * pow(10.0, h);
		double step2 = 2.0 * pow(10.0, h);
		double step5 = 5.0 * pow(10.0, h);
		if (diff == step1)
				return step1;
		else if (diff <= step2)
				return step2;
		else if (diff <= step5) 
		{
				halfStep = true;
				return step5;
		}
		else	
			return pow(10, h + 1);
	}
	double _FindTimeStep(double min, double max, bool& halfStep)
	{
		halfStep = false;
		
		double diff = fabs( (max - min)/60.0 );
		int h = OSC_ZERO(diff) ? 0 : (int)floor( log(diff)/log(60.0) );
		double step1 = 1.0 * pow(60.0, h);
		double step2 = 1.5 * pow(60.0, h);
		double step3 = 2.5 * pow(60.0, h);
		double step4 = 3.0 * pow(60.0, h);
		double step5 = 30.0 * pow(60.0, h);
/*		if(m_bTimeAxis)
		{
			step1 = 1.0 * pow(10.0, h);
			step2 = 10.0 * pow(10.0, h);
			step5 = 60.0 * pow(10.0, h);
		}
*/		if (diff == step1)
				return step1;
		else if (diff <= step2)
				return step2;
		else if (diff <= step3)
				return step3;
		else if (diff <= step4)
				return step4;
		else if (diff <= step5) 
		{
				halfStep = true;
				return step5;
		}
		else	
			return pow(60.0, h + 1);
	}

	int _Value2Coord(double value)
	{
		RECT rc;
		GetClientRect(&rc);
		::InflateRect(&rc, -OSC_OBJ_OFFSET_X, -OSC_OBJ_OFFSET_Y);

		if (m_horiz)
				return rc.left + int((value - m_min) * m_k);
		else	return rc.bottom - int((value - m_min) * m_k) - 1;
	}
};
/////////////////////////////////////////////////////////////////////////////
class OscilloscopeAxis : public OscilloscopeAxisImpl<OscilloscopeAxis>
{
public:
	DECLARE_WND_SUPERCLASS(_T("WTL_OscilloscopeAxis"), GetWndClassName())  
};
/////////////////////////////////////////////////////////////////////////////
template< class T, class TBase = CStatic, class TWinTraits = CControlWinTraits >
class ATL_NO_VTABLE OscilloscopeCtrlImpl : public CWindowImpl< T, TBase, TWinTraits >,
										   public OscHandler
{
	OscilloscopeChannel	m_channel[OSC_MAX_CHANNELS];
	OscilloscopeAxis	m_topXAxis, m_bottomXAxis;
	OscilloscopeAxis	m_leftYAxis[OSC_MAX_CHANNELS], m_rightYAxis[OSC_MAX_CHANNELS];
	bool				m_visible[OSC_MAX_CHANNELS];
	bool				m_hasTopXAxis, m_hasBottomXAxis;
	bool				m_hasLeftYAxis, m_hasRightYAxis;
	bool				m_hasGrid;
	bool				m_useNewMinMax;
	bool				m_usePadding;
	double				m_paddingCoef;
	bool				m_enableZoom;
	OscZoomMode			m_zoomMode;
	OscZoomStack		m_zoomStack;
	OscPointStyle		m_pointStyle;
	size_t				m_lastPnts;
	int					m_iChannelsNum;

public:
	typedef OscilloscopeCtrlImpl< T, TBase, TWinTraits > thisClass;

	DECLARE_WND_SUPERCLASS(NULL, TBase::GetWndClassName())

	// Operations

	BOOL SubclassWindow(HWND hWnd, int iChannels = 0)
	{
		ATLASSERT(m_hWnd==NULL);
		ATLASSERT(::IsWindow(hWnd));
#ifdef _DEBUG
		// Check class
		TCHAR szBuffer[16];
		if( ::GetClassName(hWnd, szBuffer, (sizeof(szBuffer)/sizeof(TCHAR))-1) ) {
			ATLASSERT(::lstrcmpi(szBuffer, TBase::GetWndClassName())==0);
		}
#endif
		m_iChannelsNum = (iChannels > 0)?iChannels:OSC_MAX_CHANNELS;
		BOOL bRet = CWindowImpl< T, TBase, TWinTraits >::SubclassWindow(hWnd);
		if( bRet ) _Init();
		return bRet;
	}

	// Interface

	OscilloscopeChannel* GetChannel(size_t index)    { return &m_channel[index];    }
	OscilloscopeAxis*    GetTopXAxis()               { return &m_topXAxis;          }
	OscilloscopeAxis*    GetBottomXAxis()            { return &m_bottomXAxis;       }
	OscilloscopeAxis*    GetLeftYAxis(size_t index)  { return &m_leftYAxis[index];  }
	OscilloscopeAxis*    GetRightYAxis(size_t index) { return &m_rightYAxis[index]; }
	OscZoomStack*        GetZoomStack()              { return &m_zoomStack;         }

	void ShowChannel(size_t index)    { m_visible[index] = true;  _Layout(); }
	void HideChannel(size_t index)    { m_visible[index] = false; _Layout(); }
	bool ChannelVisible(size_t index) { return m_visible[index];             }

	void TopXAxis(bool show)    { m_hasTopXAxis = show; _Layout();    }
	bool TopXAxis()             { return m_hasTopXAxis;               }

	void BottomXAxis(bool show) { m_hasBottomXAxis = show; _Layout(); }
	bool BottomXAxis()          { return m_hasBottomXAxis;            }

	void LeftYAxis(bool show)   { m_hasLeftYAxis = show; _Layout();   }
	bool LeftYAxis()            { return m_hasLeftYAxis;              }

	void RightYAxis(bool show)  { m_hasRightYAxis = show; _Layout();  }
	bool RightYAxis()           { return m_hasRightYAxis;             }

	void UseGrid(bool grid)                { m_hasGrid = grid;            }
	virtual bool UseGrid()                 { return m_hasGrid;            }

	void UseNewMinMax(bool use)            { m_useNewMinMax = use;        }
	virtual bool UseNewMinMax(void)        { return m_useNewMinMax;       }

	void UsePadding(bool use)              { m_usePadding = use;          }
	virtual bool UsePadding(void)          { return m_usePadding;         }

	void PaddingCoef(double paddingCoef)   { m_paddingCoef = paddingCoef; }
	virtual double PaddingCoef(void)       { return m_paddingCoef;        }

	void EnableZoom(bool enable)           { m_enableZoom = enable;       }
	virtual bool EnableZoom(void)          { return m_enableZoom;         }

	void ZoomMode(OscZoomMode mode)        { m_zoomMode = mode;           }
	virtual OscZoomMode ZoomMode(void)     { return m_zoomMode;           }

	void PointStyle(OscPointStyle style)   { m_pointStyle = style;        }
	virtual OscPointStyle PointStyle(void) { return m_pointStyle;         }

	void LastPoints(size_t lastPnts)       { m_lastPnts = lastPnts;       }
	virtual size_t LastPoints(void)        { return m_lastPnts;           }

	virtual void Zoom(POINT* start, POINT* end)
	{
		//�������� �� ��������� ���� �����
		const int iZoomLowLimit = 8;	//px
		UINT dx = abs(start->x - end->x);
		UINT dy = abs(start->y - end->y);
		if( max(dx, dy) < iZoomLowLimit )	
			return;
		//�������� �� ��������� ���� �����

		OscZoomRect zoom[OSC_MAX_CHANNELS];
		for(size_t i=0; i<m_iChannelsNum; i++) 
		{
			m_channel[i].GetXRange(zoom[i].xMin, zoom[i].xMax);
//			m_bottomXAxis.GetRange(zoom[i].xMin, zoom[i].xMax);
			m_channel[i].GetYRange(zoom[i].yMin, zoom[i].yMax);
		}
		m_zoomStack.AddZoom(zoom);		
		
		double xMinOld,xMaxOld,xRatio;
		m_bottomXAxis.GetRange(xMinOld, xMaxOld);

		double yMin,yMax;
		double yMinSec,yMaxSec, yRatio;
		double leftBase;
		for(i=0; i<m_iChannelsNum; i++) {
			m_rightYAxis[i].GetRange(yMinSec, yMaxSec);
			m_leftYAxis[i].GetRange(yMin, yMax);
			leftBase = yMin;
			yRatio = (yMaxSec-yMinSec) / (yMax-yMin);
			m_channel[i].Zoom(start, end);
			m_channel[i].Invalidate(FALSE);
			m_channel[i].GetYRange(yMin, yMax);
			m_leftYAxis[i].SetRange(yMin, yMax);
			m_leftYAxis[i].Invalidate(FALSE);
			m_rightYAxis[i].SetRange(yMinSec + yRatio*(yMin - leftBase), yMinSec + yRatio*(yMax - leftBase));
			m_rightYAxis[i].Invalidate(FALSE);
		}

		double xMin,xMax;
		for(i=0; i<m_iChannelsNum; i++)
			if ( m_visible[i] ) {
				xRatio = (xMaxOld-xMinOld) / (zoom[i].xMax-zoom[i].xMin);
				m_channel[i].GetXRange(xMin, xMax);
				m_topXAxis.SetRange(xMin, xMax);
				m_topXAxis.Invalidate(FALSE);
//				m_bottomXAxis.SetRange(xMin, xMax);
				m_bottomXAxis.SetRange(xMinOld + (xMin - zoom[i].xMin)* xRatio
					, xMinOld + (xMax - zoom[i].xMin)* xRatio);
				m_bottomXAxis.Invalidate(FALSE);
				break;
			}
	}

	virtual void Zoom(OscZoomDir dir)
	{
		double xMinOld,xMaxOld,xRatio;
		double xMinOld1,xMaxOld1;
		m_bottomXAxis.GetRange(xMinOld, xMaxOld);
		OscZoomRect zoom[OSC_MAX_CHANNELS];
		for(size_t i=0; i<m_iChannelsNum; i++) {
			m_channel[i].GetXRange(zoom[i].xMin, zoom[i].xMax);
			m_channel[i].GetYRange(zoom[i].yMin, zoom[i].yMax);
			m_channel[i].GetXRange(xMinOld1,xMaxOld1);
		}
		bool ret;
		switch( dir )
		{
		case ZoomIn:
			ret = m_zoomStack.ZoomIn(zoom);
			break;
		case ZoomOut:
			ret = m_zoomStack.ZoomOut(zoom);
			break;
		case ZoomOutFull:
			ret = m_zoomStack.ZoomOutFull(zoom);
			break;
		}
		if ( !ret )
			return;

		double yMin,yMax;
		double yMinSec,yMaxSec, yRatio;
		double leftBase;
		for(i=0; i<m_iChannelsNum; i++) {
			m_rightYAxis[i].GetRange(yMinSec, yMaxSec);
			m_leftYAxis[i].GetRange(yMin, yMax);
			leftBase = yMin;
			yRatio = (yMaxSec-yMinSec) / (yMax-yMin);
			m_channel[i].Zoom( &zoom[i] );
			m_channel[i].Invalidate(FALSE);
			m_leftYAxis[i].SetRange(zoom[i].yMin, zoom[i].yMax);
			m_leftYAxis[i].Invalidate(FALSE);
			//m_rightYAxis[i].SetRange(zoom[i].yMin, zoom[i].yMax);
			m_rightYAxis[i].SetRange(yMinSec + yRatio*(zoom[i].yMin - leftBase)
				, yMinSec + yRatio*(zoom[i].yMax - leftBase));
			m_rightYAxis[i].Invalidate(FALSE);
		}

//		double xMin, xMax;
		for(i=0; i<m_iChannelsNum; i++)
			if ( m_visible[i] ) {
				xRatio = (xMaxOld-xMinOld) / (xMaxOld1-xMinOld1);
//				m_channel[i].GetXRange(xMin, xMax);
				m_topXAxis.SetRange(zoom[i].xMin, zoom[i].xMax);
				m_topXAxis.Invalidate(FALSE);
//				m_bottomXAxis.SetRange(zoom[i].xMin, zoom[i].xMax);
				m_bottomXAxis.SetRange(xMinOld + (zoom[i].xMin - xMinOld1) * xRatio
					, xMinOld + (zoom[i].xMax - xMinOld1)* xRatio);
				m_bottomXAxis.Invalidate(FALSE);
				break;
			}
	}

	void mySetPoints(size_t timeF, size_t timeL, double** values, size_t cnt)
	{
//		size_t timeF = time - cnt;
//		size_t timeL = time - cnt + m_lastPnts - 1;
		
		// set X Range to all channels and X axis		
		for(size_t i=0; i<m_iChannelsNum; i++)
			m_channel[i].SetXRange((double)timeF, (double)timeL);
		bool updateTopXaxis    = m_topXAxis.SetRange((double)timeF, (double)timeL);
		bool updateBottomXaxis = m_bottomXAxis.SetRange((double)timeF, (double)timeL);
		
		// set points to all channels and then set Y range to all Y axises
		double yMin,yMax;
		bool updateLeftYaxis[OSC_MAX_CHANNELS];
		bool updateRightYaxis[OSC_MAX_CHANNELS];
		for(i=0; i<m_iChannelsNum; i++) {
			m_channel[i].SetPoints(timeF, values[i], cnt);
			m_channel[i].GetYRange(yMin, yMax);
			updateLeftYaxis[i]  = m_leftYAxis[i].SetRange(yMin, yMax);
			updateRightYaxis[i] = m_rightYAxis[i].SetRange(yMin, yMax);
		}

		// redraw axises and channels
		for(i=0; i<m_iChannelsNum; i++) {
			m_channel[i].Invalidate(FALSE);
			if (updateLeftYaxis[i]) m_leftYAxis[i].Invalidate(FALSE);
			if (updateRightYaxis[i]) m_rightYAxis[i].Invalidate(FALSE);
		}
		if (updateTopXaxis) m_topXAxis.Invalidate(FALSE);
		if (updateBottomXaxis) m_bottomXAxis.Invalidate(FALSE);
	}		
	void SetPoints(size_t time, double** values, size_t cnt)
	{
		size_t timeF = time - cnt;
		size_t timeL = time - cnt + m_lastPnts - 1;
		
		// set X Range to all channels and X axis		
		for(size_t i=0; i<m_iChannelsNum; i++)
			m_channel[i].SetXRange((double)timeF, (double)timeL);
		bool updateTopXaxis    = m_topXAxis.SetRange((double)timeF, (double)timeL);
		bool updateBottomXaxis = m_bottomXAxis.SetRange((double)timeF, (double)timeL);
		
		// set points to all channels and then set Y range to all Y axises
		double yMin,yMax;
		bool updateLeftYaxis[OSC_MAX_CHANNELS];
		bool updateRightYaxis[OSC_MAX_CHANNELS];
		for(i=0; i<m_iChannelsNum; i++) {
			m_channel[i].SetPoints(timeF, values[i], cnt);
			m_channel[i].GetYRange(yMin, yMax);
			updateLeftYaxis[i]  = m_leftYAxis[i].SetRange(yMin, yMax);
			updateRightYaxis[i] = m_rightYAxis[i].SetRange(yMin, yMax);
		}

		// redraw axises and channels
		for(i=0; i<m_iChannelsNum; i++) {
			m_channel[i].Invalidate(FALSE);
			if (updateLeftYaxis[i]) m_leftYAxis[i].Invalidate(FALSE);
			if (updateRightYaxis[i]) m_rightYAxis[i].Invalidate(FALSE);
		}
		if (updateTopXaxis) m_topXAxis.Invalidate(FALSE);
		if (updateBottomXaxis) m_bottomXAxis.Invalidate(FALSE);
	}		

	// Message map and handlers

	BEGIN_MSG_MAP(thisClass)
		MESSAGE_HANDLER(WM_CREATE, OnCreate)
		REFLECT_NOTIFICATIONS()
	END_MSG_MAP()

	LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& /*bHandled*/)
	{
		LRESULT lRes = DefWindowProc(uMsg, wParam, lParam);
		_Init();
		return lRes;
	}

private:

	// Implementation

	void _Init()
	{
		ATLASSERT(::IsWindow(m_hWnd));

		m_hasTopXAxis    = false;
		m_hasBottomXAxis = true;
		m_hasLeftYAxis   = true;
		m_hasRightYAxis  = false;
		m_hasGrid        = true;
		m_useNewMinMax   = false;
		m_usePadding     = true;
		m_enableZoom     = true;
		m_paddingCoef    = 1.0;
		m_zoomMode       = ZoomRect;
		m_pointStyle     = None;
		m_lastPnts       = OSC_DEF_LAST_POINTS;

		RECT rc = {0, 0, 0, 0};
		m_topXAxis.Create(m_hWnd, rc, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, 0);
		m_topXAxis.SetHorizontal();
		m_topXAxis.SetInvert(true);
		m_bottomXAxis.Create(m_hWnd, rc, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, 0);
		m_bottomXAxis.SetHorizontal();
		for(size_t i=0; i<m_iChannelsNum; i++) {
			m_leftYAxis[i].Create(m_hWnd, rc, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, 0);
			m_leftYAxis[i].SetVertical();
			m_rightYAxis[i].Create(m_hWnd, rc, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, 0);
			m_rightYAxis[i].SetVertical();
			m_rightYAxis[i].SetInvert(true);
			m_channel[i].Create(m_hWnd, rc, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | SS_NOTIFY, 0);
			m_channel[i].SetHandler(this);
			m_visible[i] = false;
		}

		_Layout();
	}
	
	void _Layout()
	{
		ATLASSERT(::IsWindow(m_hWnd));
	
		RECT rc;
		GetClientRect(&rc);

		size_t channelCnt = 0;
		for(size_t i=0; i<m_iChannelsNum; i++)
			if (m_visible[i]) channelCnt++;
		
		int xAxisesHeight = (m_hasTopXAxis ? OSC_X_AXIS_HEIGHT : 0) + (m_hasBottomXAxis ? OSC_X_AXIS_HEIGHT : 0);
		int ch_height = 0;
		if ( channelCnt )
			ch_height = (rc.bottom - rc.top - xAxisesHeight) / channelCnt;

		size_t offset = 0;
		for(i=0; i<m_iChannelsNum; i++) {
			RECT rc_channel    = {0, 0, 0, 0};
			RECT rc_leftYAxis  = {0, 0, 0, 0};
			RECT rc_rightYAxis = {0, 0, 0, 0};
			if (m_visible[i]) {
				rc_channel = rc;
				if (m_hasTopXAxis)		rc_channel.top += OSC_X_AXIS_HEIGHT;
				if (m_hasBottomXAxis)	rc_channel.bottom -= OSC_X_AXIS_HEIGHT;
				if (m_hasLeftYAxis)		rc_channel.left += OSC_Y_AXIS_WIDTH;
				if (m_hasRightYAxis)	rc_channel.right -= OSC_Y_AXIS_WIDTH;
				rc_channel.top += ch_height * offset++;
				rc_channel.bottom = rc_channel.top + ch_height;
				if (m_hasLeftYAxis) {
					rc_leftYAxis = rc;
					rc_leftYAxis.right  = rc_leftYAxis.left  + OSC_Y_AXIS_WIDTH;
					rc_leftYAxis.top    = rc_channel.top;
					rc_leftYAxis.bottom = rc_channel.bottom;
				}
				if (m_hasRightYAxis) {
					rc_rightYAxis = rc;
					rc_rightYAxis.left   = rc_rightYAxis.right - OSC_Y_AXIS_WIDTH;
					rc_rightYAxis.top    = rc_channel.top;
					rc_rightYAxis.bottom = rc_channel.bottom;
				}
			}
			::InflateRect(&rc_channel, -OSC_OBJ_OFFSET_X, -OSC_OBJ_OFFSET_Y);
			m_channel[i].MoveWindow(&rc_channel);
			m_leftYAxis[i].MoveWindow(&rc_leftYAxis);
			m_rightYAxis[i].MoveWindow(&rc_rightYAxis);
		}

		RECT rc_topXAxis    = {0, 0, 0, 0};
		if (m_hasTopXAxis && channelCnt > 0) {
			rc_topXAxis = rc;
			if (m_hasLeftYAxis)		rc_topXAxis.left += OSC_Y_AXIS_WIDTH;
			if (m_hasRightYAxis)	rc_topXAxis.right -= OSC_Y_AXIS_WIDTH;
			rc_topXAxis.top = rc.top;
			rc_topXAxis.bottom = rc.top + OSC_X_AXIS_HEIGHT; 
		}
		m_topXAxis.MoveWindow(&rc_topXAxis);

		RECT rc_bottomXAxis = {0, 0, 0, 0};
		if (m_hasBottomXAxis && channelCnt > 0) {
			rc_bottomXAxis = rc;
			if (m_hasLeftYAxis)		rc_bottomXAxis.left += OSC_Y_AXIS_WIDTH;
			if (m_hasRightYAxis)	rc_bottomXAxis.right -= OSC_Y_AXIS_WIDTH;
			rc_bottomXAxis.top = ch_height * channelCnt + (m_hasTopXAxis ? OSC_X_AXIS_HEIGHT : 0);
			rc_bottomXAxis.bottom = ch_height * channelCnt + (m_hasTopXAxis ? 2 * OSC_X_AXIS_HEIGHT : OSC_X_AXIS_HEIGHT); 
		}
		m_bottomXAxis.MoveWindow(&rc_bottomXAxis);
	}
};
/////////////////////////////////////////////////////////////////////////////
class OscilloscopeCtrl : public OscilloscopeCtrlImpl<OscilloscopeCtrl>
{
public:
	DECLARE_WND_SUPERCLASS(_T("WTL_Oscilloscope"), GetWndClassName())  
};
/////////////////////////////////////////////////////////////////////////////

#endif	// _WTL_OSCILLOSCOPE_H_