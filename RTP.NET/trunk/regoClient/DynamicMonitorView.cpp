// $Header: /rtpClient/DynamicMonitorView.cpp 4     13.04.08 17:15 Sergey Zhmetko $ 
// DynamicMonitorView.cpp: implementation of the CDynamicMonitorView class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DynamicMonitorView.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDynamicMonitorView::CDynamicMonitorView( const CTabsAccessor& tac)
				: CGenericRTPView<CDynamicMonitorView>(tac, false, true)
{
	m_spansList.push_back(CTimeSpanData(_T("10 ���"),	5000,	-10));
	m_spansList.push_back(CTimeSpanData(_T("30 ���"),	7000,	-30));
	m_spansList.push_back(CTimeSpanData(_T("1 ���"),	10000,	-60));
	m_spansList.push_back(CTimeSpanData(_T("3 ����"),	20000,	-180));
	m_spansList.push_back(CTimeSpanData(_T("6 �����"),	30000,	-360));
	// for monitor speed optimization I use helper table which filled by ValsByTimeRow.onInsert trigger
	strcpy(m_tbl_name, "CurVal");
}

CDynamicMonitorView::~CDynamicMonitorView()
{}

void CDynamicMonitorView::InitializeTab()
{
	CGenericRTPView<CDynamicMonitorView>::InitializeTab();
	CComboBox ctlComboRange = GetDlgItem(IDC_RANGE_COMBO);

	std::list<CTimeSpanData>::const_iterator iter = m_spansList.begin();
	while(iter != m_spansList.end())
	{
		const CTimeSpanData& sd = *iter;
		int nAdded = ctlComboRange.AddString(sd.m_szDisplayText);
		ctlComboRange.SetItemData(nAdded, DWORD(&sd));
		iter++;
	}
	ctlComboRange.SetCurSel(0);
	SetTimer(1, 10000);
}

LRESULT CDynamicMonitorView::OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CSize sz(lParam);
	CRect plotRect;
	m_plot.GetWindowRect(&plotRect);
	ScreenToClient(plotRect);
	plotRect.bottom = sz.cy;
	plotRect.right = sz.cx;
	m_plot.MoveWindow(&plotRect);
	for(size_t i = 0; i < m_TabChlsVector.size(); i++)
		m_plot.ShowChannel(i);
	return 0;
}	

LRESULT CDynamicMonitorView::OnRangeSelChanged(WORD, WORD, HWND, BOOL&)
{
	KillTimer(1);
	CComboBox ctlComboRange = GetDlgItem(IDC_RANGE_COMBO);
	DWORD dwData = ctlComboRange.GetItemData(ctlComboRange.GetCurSel());
	if(dwData)
	{
		const CTimeSpanData& sd = *(CTimeSpanData*)dwData;
		SetTimer(1, sd.m_dwRefreshPeriod);
	}
	return 0;
}

LRESULT CDynamicMonitorView::OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if( 1 == wParam  && IsWindowVisible())
	{
		CComboBox ctlComboRange = GetDlgItem(IDC_RANGE_COMBO);
		DWORD dwData = ctlComboRange.GetItemData(ctlComboRange.GetCurSel());
		if(dwData)
		{
			const CTimeSpanData& sd = *(CTimeSpanData*)dwData;
			CString strFilter;
			strFilter.Format(" where dtm_upd between DateAdd(n,%ld,getdate()) and getdate()", sd.m_dwMinutes);
			ShowChart(strFilter, "order by dtm_upd asc");
		}
	}
	return 0;
}
