// $Header: /rtpClient/WorfFlowBuilder.cpp 5     1.05.08 11:14 Sergey Zhmetko $ 
// WorfFlowBuilder.cpp: implementation of the CWorkFlowBuilder class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "WorfFlowBuilder.h"
#include "wflow.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CWorkFlowBuilder::CWorkFlowBuilder(CSession& ses, HWND hWnd) : 
		  m_pCtrl(0)
		, m_hWnd(0)
{

	m_pCtrl = new CWorkFlowCtrl(ses);
	m_hWnd  = hWnd;
	ATLASSERT(m_hWnd);
	if(m_pCtrl)
	{
		m_pCtrl->SubclassWindow(m_hWnd);

		using namespace dbutils;
		try
		{
			CCommand<CAccessor<CWorkFlowRowAccessor> >	crst;
			HResult hres = S_OK;
			hres = crst.Open( m_pCtrl->GetDBSession(), CConfig::GetValue("wflowrow/select").c_str());

			while(crst.MoveNext() == S_OK)
			{
				ATLTRACE("row founded\n");
				CWorkFlowRow* pRow = 0;
				pRow = m_pCtrl->AddRow(crst, 200);
				//*pRow = crst;
				pRow->Fill(ses);
				crst.ClearRecord();
			}
			crst.Close();
		}
		catch(HresError exc)
		{			ATLTRACE(exc.what().c_str());		}

		m_pCtrl->Animate(true);
	}
}

CWorkFlowBuilder::~CWorkFlowBuilder()
{
	if(m_pCtrl)
		delete m_pCtrl;
}
void CWorkFlowBuilder::Run(bool bRun)
{
	if(m_pCtrl)
		m_pCtrl->Animate(bRun);
}
void CWorkFlowBuilder::SetChannelValues(std::map<long, double> vals)
{
	if(m_pCtrl)
		m_pCtrl->SetChannelValues(vals);
}

void CWorkFlowBuilder::SetPosition(LPRECT lpNewPos)
{
	ATLASSERT(lpNewPos);
	m_pCtrl->MoveWindow(lpNewPos);
}
