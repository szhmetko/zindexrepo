// $Header: /rtpClient/StatReportWnd.cpp 2     13.04.08 21:17 Sergey Zhmetko $ 
#include "stdafx.h"
#include <math.h>
#include <time.h>
#include "ApproxRanges.h"
#include "dataContainer.h"
#include "config.h"
#include "statReportWnd.h"

extern SLineSettings g_CurLine;
CPng::CPng() :
	  m_uPicHeight(200)
	, m_uPicWidth(300)
	, m_dHighLimit(0.0)
	, m_dLowLimit(0.0)
	, m_maxIndex(0)
	, m_Total(0)
{}

CPng::~CPng()
{}

const CPng& CPng::operator <<(double val)
{
	for(int i = 0; i < HISTOGRAM_STEPS; ++i)
		if(m_Ranges[i].TestVal(val))	break;
	m_Total++;
	return *this;
}

void CPng::Limits(double low, double high)
{	
	m_Total = 0;
	m_dHighLimit = high; m_dLowLimit = low;	
	double dStep = (high - low) / (HISTOGRAM_STEPS - 2);
	
	//reset all ranges
	for(int i = 0; i < HISTOGRAM_STEPS; ++i)
		m_Ranges[i].occurred = 0;

	//set all ranges
	m_Ranges[0].SetRange(0.0, low);
	m_Ranges[HISTOGRAM_STEPS - 1].SetRange(high, 10000.0);
	for(i = 1; i < HISTOGRAM_STEPS - 1; ++i)
	{
		m_Ranges[i].SetRange(low + (i-1)*dStep, low + i*dStep);
	}
}

double CPng::CalculateK()
{
	double ret = 0.0;
	for(int i = 0; i < HISTOGRAM_STEPS; ++i )
	{
		if( m_Ranges[i].occurred > ret)
		{
			m_maxIndex = i;
			ret = m_Ranges[i].occurred;
		}
	}
	return ret*1.2;
}

BOOL CPng::Polyline(std::vector <POINT> arPts)
{
	POINT *pts = new POINT[arPts.size()];
	int i = 0;
	for( std::vector <POINT>::iterator iter = arPts.begin(); iter != arPts.end(); ++iter )
		pts[i++] = *iter;
	BOOL ret = ::Polyline(m_hDC, pts, arPts.size());
	delete [] pts;
	return ret;
}

void CPng::DrawPng(LPCSTR szPath)
{
//create a test image
	COLORREF clrBG = RGB(255, 255, 255);
	COLORREF clrBorder = RGB(200, 200, 200);
	COLORREF clrLegal = RGB(0, 255, 0);
	COLORREF clrIlLegal = RGB(255, 0, 0);

	if(!Create( CSize(m_uPicWidth, m_uPicHeight+20 )) )
	{::MessageBox(0, "Fail Create png device context", "", MB_OK);}
	
	HFONT oldFont = SelectFont(AtlGetDefaultGuiFont());
	COLORREF oldClr = GetTextColor();
	Erase( clrBG );
	double k = CalculateK();
	CRect recBar(0, 0, 0, m_uPicHeight);
	UINT uWidth = UINT(0.05*m_uPicWidth);
	for(int i = 0; i < HISTOGRAM_STEPS; ++i)
	{
		recBar.right = (i==0 || i==(HISTOGRAM_STEPS-1))?recBar.left + m_uPicWidth/4:recBar.left + uWidth ;
		recBar.top = m_uPicHeight - UINT(m_uPicHeight/k*m_Ranges[i].occurred);

		CRect RecBorder = recBar;
		RecBorder.InflateRect(-2, -2);
		if(m_Ranges[i].occurred)
			FillSolidRect(RecBorder, (i==0 || i==(HISTOGRAM_STEPS-1))? clrIlLegal : clrLegal );
		std::vector <POINT> ticks;
		const int TICKLENGTH = 10;
		const int LABELWIDTH = 80;
		const int LABELHEIGHT = 20;
		CRect rcLabel(0, 0, LABELWIDTH, LABELHEIGHT);
		char szLabel[64] = {0};
		if( i == 1 || i == (HISTOGRAM_STEPS-1) || i == (HISTOGRAM_STEPS/2) )
		{
			POINT pt;
			pt.x = recBar.left;	pt.y = recBar.bottom;				ticks.push_back(pt);
			pt.x = recBar.left;	pt.y = recBar.bottom - TICKLENGTH;	ticks.push_back(pt);
			sprintf(szLabel, "%.2f", m_Ranges[i].Low);

			rcLabel.MoveToXY(recBar.left - LABELWIDTH/2, recBar.bottom + 3);
			FillSolidRect(rcLabel, clrBG );
			DrawText(szLabel, -1, &rcLabel, DT_CENTER | DT_WORDBREAK );
		}
		if( (i == m_maxIndex || i == 0 || i == (HISTOGRAM_STEPS-1) ) && m_Ranges[i].occurred > 0 )
		{
			sprintf(szLabel, "%.1f%%", m_Ranges[i].occurred/double(m_Total)*100.0);
			CRect rcPercent = rcLabel;
			DrawText(szLabel, -1, &rcPercent, DT_CALCRECT );
			rcPercent.MoveToXY(recBar.left - rcPercent.Width()/2 + recBar.Width()/2, 
				recBar.top - rcPercent.Height());
			FillSolidRect(rcPercent, clrBG );
			DrawText(szLabel, -1, &rcPercent, DT_CENTER | DT_WORDBREAK );
		}

		Polyline(ticks);

		recBar.left += (i==0 || i==(HISTOGRAM_STEPS-1))?(m_uPicWidth/4):long(0.05*m_uPicWidth);
	}


	
	CRect rcTitle(int(0.1*m_uPicWidth),0,int(m_uPicWidth*0.9),20);
	char szMsg[] = _T("������������� ��������");
	

	FillSolidRect( &rcTitle, clrBG );

	if (oldFont) SelectFont(oldFont);
	if (oldClr) SetTextColor( oldClr );

	CString path = (szPath)?szPath:"c:\\test_image.png";
	SaveAsPNG(path);
}

CPng::CDRange::CDRange()
{}

void CPng::CDRange::SetRange(double low, double high)
{
	High = high; 
	Low = low;		
}

bool CPng::CDRange::TestVal(double val)
{
	if( (val < High) && (val > Low) )
	{
		occurred++;
		return true;
	}
	return false;
}

CStatReportWnd::CStatReportWnd()
		: m_pData(0)
		, m_pEtal(0)
		, m_pMeas(0)
		, m_pProb(0)
		, m_pCurPiece(0)
		, m_strProbFName("chl2_7")

{
	char tmpFile[_MAX_PATH] = {'\0'};
	//		if( ::GetTempPath(_MAX_PATH, tmpFile) == NULL )
	m_strPageTmpPath = CConfig::GetWD().c_str() + CString(REPORTSDIR);// �������� ������������ ���. ����������

	//��������� ���������� ����� ������� ����� � ����. ������
	m_strPageTmpPath = 	::GetTempFileName(m_strPageTmpPath, "$rp", 0, tmpFile) ? tmpFile: \
		m_strPageTmpPath + CString("curReport.htm");
	//		::CopyFile(m_strPageTmpPath, m_strPageTmpPath+".htm", FALSE);
	::DeleteFile(m_strPageTmpPath);
	///������ ������������� �������
		SetValue("LineName", g_CurLine.m_strTitle.c_str() );
		SetValue("GistogramPath", "test_image.png" );
		SetValue("DiamOuterRefer", "����������");
		SetValue("Model", "����������");
		SetValue("VlinRefer", "����������");
		SetValue("URefer", "����������");
		SetValue("Umeas", "����������");
		SetValue("VlinDeviation", "����������");
		SetValue("WeightViolation", "����������");

	/*
			��������.����������������(���.�����������);		// ����������� ��������
			��������.����������������(���.���);				// ������������ �������
			��������.����������������(���.���);				// ����������� ��������
			��������.����������������(���.������);			// �������� ����������
			��������.����������������(���.�����);			// ������� ��������� ���������

	*/
		SetValue("�����������", "�����������");
		SetValue("���", "���");
		SetValue("���", "���");
		SetValue("������", "������");
		SetValue("�����", "�����");
}

BOOL CStatReportWnd::PreTranslateMessage(MSG* pMsg)
{
	if((pMsg->message < WM_KEYFIRST || pMsg->message > WM_KEYLAST) &&
	   (pMsg->message < WM_MOUSEFIRST || pMsg->message > WM_MOUSELAST))
		return FALSE;

	// give HTML page a chance to translate this message
	return (BOOL)SendMessage(WM_FORWARDMSG, 0, (LPARAM)pMsg);
}

LRESULT CStatReportWnd::OnDestroy(UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	::DeleteFile(m_strPageTmpPath+".htm");
	::DeleteFile(m_strPageTmpPath+".png");
	m_valsMap.clear();
	if(m_pCurPiece)
	{
		delete m_pCurPiece;
		m_pCurPiece = 0;
	}
	return 0;
}
bool	CStatReportWnd::Create(HWND hParent, CDataContainer* pData)
{
	if(pData)
		m_pCurPiece = new CPieceSearcher(*pData);

	m_pData = pData;
	HWND Hwebro = CWindowImpl<CStatReportWnd, CAxWindow>::Create(hParent, rcDefault, "about:blank", 
		WS_CHILD 
		| WS_VISIBLE 
		| WS_CLIPSIBLINGS 
		| WS_CLIPCHILDREN 
		| WS_HSCROLL 
		| WS_VSCROLL, 
		WS_EX_CLIENTEDGE);
	return true;
}

BOOL CStatReportWnd::OnBeforeNavigate2(IDispatch* pDisp, const String& szURL, DWORD dwFlags, const String& szTargetFrameName, CSimpleArray<BYTE>& pPostedData, const String& szHeaders)
{   // Return TRUE to cancel
	int iSlashPos = szURL.Find("_cmd:");
	if(iSlashPos >= 0)
	{
		CString szPartURL = szURL.Right(szURL.GetLength() - iSlashPos);

		if( _T("_cmd:Save") == szPartURL )
			ExecWB(OLECMDID_SAVEAS,OLECMDEXECOPT_DODEFAULT,NULL,NULL);
		if( _T("_cmd:PrintPreview") == szPartURL )
			ExecWB(OLECMDID_PRINTPREVIEW,OLECMDEXECOPT_DODEFAULT,NULL,NULL);
		if( _T("_cmd:Print") == szPartURL )
			ExecWB(OLECMDID_PRINT,OLECMDEXECOPT_DODEFAULT,NULL,NULL);
		if( szPartURL.Find("_cmd:Recalc") >= 0  )
		{
			CString strData = szPartURL.Right( szPartURL.GetLength() - strlen("_cmd:Recalc") );
			ParsePageData(strData);
			LardTemplate(false);//TODO:
		}
		return TRUE;
	}
	return FALSE;
}

void	CStatReportWnd::ParsePageData(CString& rData)
{
	char* buffer = new char[rData.GetLength()+1];
	strncpy(buffer, rData.GetBuffer(0), rData.GetLength()+1);

	int idx = 0;
	char seps[4];	memset( seps, 0, sizeof(seps) );
	seps[0] = '&';
	seps[1] = '\n';
	char *token = strtok( buffer, seps );

	while( token != NULL )
	{
		CString key, val, tmp = token;
		int iPos = tmp.Find('=');
		if( iPos > 0)
		{
/*
		double		dDiameter;			//��������� ���������� ���������� ��������	out/in
		double		MaterialDensity;	//��������� ���������	out/in
		double		DiamOuterRefer;		//�������� �����������	out/in
		long		VlinRefer;			//������������� ��������	out/in

		std::string Model;				//����� ������		out/in
		std::string op_name;			//��������		out/in

*/
			key = tmp.Left(iPos);
			val = tmp.Right(tmp.GetLength() - iPos - 1);
			if( key == _T("dDiameter") )
				m_values.dDiameter = atof(val.GetBuffer(0));
			if( key == _T("MaterialDensity") )
				m_values.MaterialDensity = atof(val.GetBuffer(0));
			if( key == _T("DiamOuterRefer") )
				m_values.DiamOuterRefer = atof(val.GetBuffer(0));
			if( key == _T("VlinRefer") )
				m_values.VlinRefer = atol(val.GetBuffer(0));

			if( key == _T("op_name") )
				m_values.op_name = val;
			if( key == _T("Model") )
				m_values.Model = val;
//				SetValue(key, val);
		}
		token = strtok( NULL, seps );
	}


	delete [] buffer;
}

void	CStatReportWnd::SetValue(LPCSTR key, LPCSTR val)
{
	m_valsMap[key] = val;
}
void	CStatReportWnd::SetValue(LPCSTR key, int val)
{
	char szValue[64] = {'\0'};
	sprintf(szValue, "%ld", val);
	m_valsMap[key] = szValue;
}
void	CStatReportWnd::SetValue(LPCSTR key, double val, int precision)
{
	char szValue[64] = {'\0'};
	char szFormat[16] = {'\0'};
	sprintf(szFormat, "%%.%df", precision);
	sprintf(szValue, szFormat, val);
	m_valsMap[key] = szValue;
}

long CStatReportWnd::LardPPTemplate(std::map<long, long> &rPiecesMap)
{
	CWaitCursor wc;
	CString	strTableBody = "<tr><td class='rephead' width=5%%>�����</td>"
					"<td class='rephead' width=15%%>����</td>"
					"<td class='rephead' width=15%%>����</td>"
					"<td class='rephead' width=5%%>�����</td>"
					"<td class='rephead'width=30%%>� ������</td>"
					"<td class='rephead' width=30%%>� ��������</td></tr>";

	std::map<long, long>::iterator iter;

	dbutils::CDBSession locSession;
	HRESULT res = locSession.Open();
	if( FAILED(res) )
	{
		AtlTraceErrorRecords(res);
		return 0;
	}
	for(iter = rPiecesMap.begin(); iter != rPiecesMap.end(); iter++)
	{
		strTableBody += GetPieceInfo(iter->first, locSession);
	}
	locSession.Close();

	SetValue("TableBody", strTableBody);
	return _BuildPage(PPREPORT_TEMPLATEPATH);
}

long	CStatReportWnd::_BuildPage(CString strPath)
{
	CString strTemplatePath = CConfig::GetWD().c_str() + strPath;
	CString strCSSPath = CConfig::GetWD().c_str() + CString(CSSPATH);
	
	CString	strContent;

	char buffer[64] = {'\0'};
	FILE* f_htm = 0;
	if( (f_htm = fopen(strTemplatePath, "rt")) != NULL )
	{
		while( !feof(f_htm) )
		{
			size_t ret = fread( buffer, sizeof(char), 63, f_htm );
			if(ret && !ferror(f_htm))
			{
				buffer[ret] = 0;
				strContent += buffer;
			}
			else	break;
		}
		std::map <std::string, std::string>::iterator iter = m_valsMap.begin();
		for( ; iter != m_valsMap.end(); iter++)
		{
			CString	strKey = CString(LEFTDELIMITER) + iter->first.c_str() + CString(RIGHTDELIMITER);
			strContent.Replace(strKey, iter->second.c_str());
		}
		fclose(f_htm);
	}
	if( (f_htm = fopen(m_strPageTmpPath + ".htm", "wt")) != NULL )
	{
		fwrite(strContent.GetBuffer(0), sizeof(char), strContent.GetLength(), f_htm);
		fclose(f_htm);
	}
	Navigate(m_strPageTmpPath + ".htm");
	return strContent.GetLength();
}

long CStatReportWnd::LardTemplate(bool bDefaultReference)
{
	_PrepareValues(bDefaultReference);
	return _BuildPage(REPORT_TEMPLATEPATH);
}
void CStatReportWnd::SetDiameterInfo(int iEtal, int iMeas, int iProb)
{
	ATLASSERT(m_pData);
	m_pEtal = 0;	m_pMeas = 0;	m_pProb = 0;
	CHMAP_ITER iter;
	CDataContainer &data = *m_pData;
	iter = g_ChannelsMap.find(iEtal);
	if( iter != g_ChannelsMap.end() )
	{
		CChannelSettingsAccessor& cac = iter->second;
		m_pEtal = data[cac.m_fname];
	}
	iter = g_ChannelsMap.find(iMeas);
	if( iter != g_ChannelsMap.end() )
	{
		CChannelSettingsAccessor& cac = iter->second;
		m_pMeas = data[cac.m_fname];
	}
	iter = g_ChannelsMap.find(iProb);
	if( iter != g_ChannelsMap.end() )
	{
		CChannelSettingsAccessor& cac = iter->second;
		m_pProb = data[cac.m_fname];
		m_strProbFName = cac.m_fname;
	}
}
void	CStatReportWnd::_PrepareValues(bool bDefaultReference)
{
		////���������� ������
//		double Dinner = 1.72;			//���������� �������
/*		double Douter = 3.70;			//�������� �������
		double DlowLimit = 3.43;		//������ ������
		double DhighLimit = 4.15;		//������� ������  -  �����������
		double MaterialDensity = 3430.0;	//��������� ���������
		double Vlin = 100.0;
*/		///
	ATLASSERT(m_pData);
	CDataContainer& data = *m_pData;
	long lSize = m_pCurPiece->length();
	double detal = 0.0;
	//format dates
	char buffer[128] = {'\0'};
	DATE dtFrom = m_pCurPiece->DTfrom();
	DATE dtTo = m_pCurPiece->DTto();
	time_t tmTo = dbutils::CDBSession::VariantTimeToUnixTime(dtTo);
	time_t tmFrom = dbutils::CDBSession::VariantTimeToUnixTime(dtFrom);
	{
		tmTo = max(0, tmTo);
		struct tm* tm_start = localtime( &tmTo );
		if(tm_start)	
		{
			strftime(buffer, 128, "%d-%m-%Y", tm_start);
			SetValue("EndDate", buffer );
			strftime(buffer, 128, "%H:%M:%S", tm_start);
			SetValue( "EndTime", buffer);
		}
		tmFrom = max(0, tmFrom);
		tm_start = localtime( &tmFrom );
		if(tm_start)	
		{
			strftime(buffer, 128, "%d-%m-%Y", tm_start);
			SetValue("StartDate", buffer );
			strftime(buffer, 128, "%H:%M:%S", tm_start);
			SetValue( "StartTime", buffer);
		}
		time_t tmDiff = (tmTo - tmFrom);
		tmDiff = max(0, tmDiff);
		{
			BYTE hours, mins, seconds;
			hours = tmDiff / 3600;
			mins = (tmDiff - hours * 3600) / 60;
			seconds = tmDiff - hours*3600 - mins*60;
			sprintf(buffer, "%02d:%02d:%02d", hours, mins, seconds);
			SetValue( "DiffTime", buffer);
		}
	}
	//deal with actual data
	SetValue( "Length", lSize);
	if( lSize > 2 )
	{
		
		//DiamEtalon
		if(m_pEtal)
		{
			detal = m_pEtal[m_pCurPiece->last()-1];//max(m_pEtal[0], m_pEtal[lSize-1]);
			SetValue( "DiamEtalon", detal);
		}

		///������ ��������
		double MaterialDensity = bDefaultReference ? 3430.0 : m_values.MaterialDensity;
		double DiamOuterRefer = bDefaultReference ? detal : m_values.DiamOuterRefer;

		double from = bDefaultReference ? DiamOuterRefer*0.9 : DiamOuterRefer - m_values.dDiameter;
		double to = bDefaultReference ? DiamOuterRefer*1.1 : DiamOuterRefer + m_values.dDiameter;
		m_png.Limits(from, to);

		double* pVlin = data["vlin"];
		double vAvg = 0.0, dAvg = 0.0, dSquareSum = 0.0; 
		double dSize = double(lSize);
		long lProbCounter = 0;
		CString strProbs, strRProbs, strComma = ", ";
		for( long i = m_pCurPiece->first(); i < m_pCurPiece->last(); ++i )
		{
			if(pVlin)	vAvg += pVlin[i] / dSize;
			if(m_pMeas)
			{
				m_png << m_pMeas[i];
				dSquareSum += m_pMeas[i]*m_pMeas[i];
				dAvg += m_pMeas[i] / dSize;
			}
			if(m_pProb)
			{
				if(m_pProb[i] != 0)
				{
					sprintf(buffer, "%ld", i - m_pCurPiece->first()+1);
					strProbs = lProbCounter ? (strProbs+strComma+buffer) : (strProbs+buffer);
					sprintf(buffer, "%ld", lSize-(i+1- m_pCurPiece->first()));
					strRProbs = lProbCounter ? (buffer+strComma+strRProbs) : (buffer+strRProbs);
					lProbCounter += m_pProb[i];
				}
			}
		}

		SetValue( "VlinAvg", vAvg);
		
		SetValue( "ProbsNum", lProbCounter);

		SetValue( "DiamMeasAvg", dAvg);

		SetValue( "ProbsMeters", strProbs);
		SetValue( "ProbsRMeters", strRProbs);
		
		//����������
		
		double dm = PI * MaterialDensity / 4 * (dSquareSum - dSize*DiamOuterRefer*DiamOuterRefer)*1e-6;

		SetValue("DiamOuterRefer", DiamOuterRefer);
		SetValue( "dDiameter", bDefaultReference ? 0.1*detal : m_values.dDiameter);
		SetValue("Model", m_values.Model.c_str());
		SetValue("op_name", m_values.op_name.c_str());
		SetValue("MaterialDensity", bDefaultReference ? MaterialDensity : m_values.MaterialDensity);

		long VlinRefer = bDefaultReference ? long(vAvg) : m_values.VlinRefer;
		SetValue("VlinRefer", VlinRefer);

		SetValue("URefer", "����������");
		
		SetValue("Umeas", "����������");

		SetValue("VlinDeviation", (VlinRefer-vAvg));

		SetValue("WeightViolation", dm, 3);

		m_png.DrawPng(m_strPageTmpPath + ".png");
		SetValue("GistogramPath", m_strPageTmpPath + ".png" );

		SetReferenceInfo();
	}
}

bool	CStatReportWnd::SetReferenceInfo()
{
	SetValue("�����������", "");
	SetValue("���", "");
	SetValue("���", "");
	SetValue("������", "");
	SetValue("�����", "");
	using namespace dbutils;

	CDBSession locSession;
	HResult hres;
	try
	{
		hres = locSession.Open();
		CCommand<CAccessor<CCableDescriptionAccessor> >	cdrst;
		cdrst.m_piece_id = m_lFetchedPieceID;
		hres = cdrst.Open(locSession, CConfig::GetValue("cabledescription/select").c_str());
		while( cdrst.MoveNext() == S_OK )
			SetValue(cdrst.m_paramName, cdrst.m_paramVal);
		cdrst.Close();
		locSession.Close();
	}
	catch(HresError he)
	{
		::MessageBox(0, he.what().c_str(), "", MB_OK);
		return false;
	}
	return true;
}

CString	CStatReportWnd::GetPieceInfo(long lid, dbutils::CDBSession& ses)
{	//	id	����	����	�����	����������	�����������	������������
	CString strRet = "<tr>"
		, strProbs("&nbsp;"), strRProbs("&nbsp;"), strTmp;
	UINT	uProbsCount = 0;
	long lLength = 0;

	CCommand<CAccessor<CPiecesDetailAccessor> > prst;
	prst.m_recid = lid;
	char szSQL[1024] = {'\0'};
	//sprintf(szSQL, "select rec_id, dtm_on, dtm_off, length from cablepieces where rec_id=%ld", lid);
	if( SUCCEEDED(prst.Open(ses, CConfig::GetValue("cablepieces/selectsingle").c_str())) )
	{
		if( prst.MoveFirst() == S_OK )
		{
			strTmp.Format("<td class='repdata' width=5%%>%ld</td>", prst.m_length);
			lLength = prst.m_length;
			strRet += strTmp;

			SYSTEMTIME DT;
			VariantTimeToSystemTime(prst.m_dtmon, &DT);
			strTmp.Format("<td  class='repdata'>%02d-%02d-%02d %02d:%02d %02d</td>",
				DT.wDay, DT.wMonth, DT.wYear - 2000, DT.wHour, DT.wMinute, DT.wSecond);
			strRet += strTmp;

			VariantTimeToSystemTime(prst.m_dtmoff, &DT);
			strTmp.Format("<td  class='repdata'>%02d-%02d-%02d %02d:%02d %02d</td>",
				DT.wDay, DT.wMonth, DT.wYear - 2000, DT.wHour, DT.wMinute, DT.wSecond);
			strRet += strTmp;
		}
		prst.Close();
	}
	sprintf(szSQL, "select meter, %s from valsbymeterrow where piece_id=%ld and %s>0", 
		m_strProbFName.c_str(), lid, m_strProbFName.c_str());
	CCommand<CAccessor<CMetersAccessor> >	mrst;

	if( SUCCEEDED(mrst.Open(ses, szSQL)) )
	{
		while( mrst.MoveNext() == S_OK )
		{
			strTmp.Format("%ld ", mrst.m_meter);
			strProbs += strTmp;
			strTmp.Format("%ld ", lLength - mrst.m_meter);
			strRProbs += strTmp;
			uProbsCount += mrst.m_chl;
		}
		mrst.Close();
	}
	
	strTmp.Format("<td  class='repdata' width=5%%>%ld</td>", uProbsCount);
	strRet += strTmp;

	strRet += "<td  class='repdata'>" + strProbs + "</td>";
	strRet += "<td  class='repdata'>" + strRProbs + "</td>";

	strRet += "</tr>";
	return strRet;
}

CStatReportWnd::CPieceSearcher::CPieceSearcher(CDataContainer& rData)
			 : m_Data(rData)
			, meterStart(1)
			, meterEnd(1)
{
	Full();
}

void CStatReportWnd::CPieceSearcher::Max() 
{
	m_arVlin = m_Data["vlin"];
	m_arMeters = m_Data["meter"];
	if( m_Data.GetNumRows() > 0 && m_arVlin && m_arMeters)
	{
		reset();
		for( long i = meterStart - 1; i < m_Data.GetNumRows(); ++i )
			AddValue(m_arMeters[i], m_arVlin[i]);
		AddValue(m_arMeters[i-1], 0.0);

		if( meterStart == meterEnd )	//��� ����� �� ��������
			meterEnd = m_Data.GetNumRows();
	}
}

void CStatReportWnd::CPieceSearcher::Manual(long from, long to) 
{
	m_arVlin = m_Data["vlin"];
	m_arMeters = m_Data["meter"];
	if( m_Data.GetNumRows() > 0 && m_arVlin && m_arMeters)
	{
		reset();
		from = min(from, m_Data.GetNumRows());	from = max(from, 1);
		to = min(to, m_Data.GetNumRows());	to = max(to, 1);
		if( from < to )				
		{
			meterStart =	from;
			meterEnd =	to;
		}
		else
		{
			meterStart =	1;
			meterEnd =	m_Data.GetNumRows();
		}
		
	}
}

void CStatReportWnd::CPieceSearcher::Full() 
{
	m_arVlin = m_Data["vlin"];
	m_arMeters = m_Data["meter"];
	if( m_Data.GetNumRows() > 0 && m_arVlin && m_arMeters)
	{
		reset();
		meterStart;
		meterEnd =	m_Data.GetNumRows();
	}
}

void CStatReportWnd::CPieceSearcher::AddValue(long meter, double val)
{
	if(val <= 0.0)
	{
		long lenPrev = meterEnd - meterStart;
		long lenCurr = meter - meterLastLabel;
		if(lenCurr >= lenPrev)
		{
			meterEnd = meter;
			meterStart = meterLastLabel;
		}
		meterLastLabel = meter;
	}
}

long CStatReportWnd::CPieceSearcher::length(){return meterEnd - meterStart + 1;}
long CStatReportWnd::CPieceSearcher::first(){return meterStart;}
long CStatReportWnd::CPieceSearcher::last(){return meterEnd;}

DATE CStatReportWnd::CPieceSearcher::DTfrom()
{
	DATE* pDate = m_Data["dtm_upd"];
	return (pDate && meterStart < m_Data.GetNumRows()) ? pDate[meterStart - 1] : 0;
}

DATE CStatReportWnd::CPieceSearcher::DTto()
{
	DATE* pDate = m_Data["dtm_upd"];
	return (pDate && meterEnd < m_Data.GetNumRows()+1) ? pDate[meterEnd - 1] : 0;
}

void CStatReportWnd::CPieceSearcher::reset()
{
	if( m_Data.GetNumRows() > 0 && m_arVlin && m_arMeters)
	{
		meterStart =		m_arMeters[0];
		meterEnd =			m_arMeters[0];
		meterLastLabel =	m_arMeters[0];
	}
}
