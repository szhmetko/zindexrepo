// $Header: /rtpClient/dbutils.cpp 4     11.07.08 2:01 Sergey Zhmetko $ 
#include "stdafx.h"
#include "dbutils.h"
#include <time.h>
extern	SLineSettings	g_CurLine;
using namespace dbutils;
CDBSession::CDBSession() : m_pPropRowset(0)
{}

CDBSession::~CDBSession()
{
	if(m_pPropRowset)
		delete m_pPropRowset;
}

HRESULT CDBSession::Open()
{
	char szHost[64] = {'\0'};	
	HRESULT		hr;
	CDataSource db;
	CDBPropSet	dbinit(DBPROPSET_DBINIT);

	dbinit.AddProperty(DBPROP_AUTH_PASSWORD, g_CurLine.m_strPassword.c_str() );
	dbinit.AddProperty(DBPROP_AUTH_PERSIST_SENSITIVE_AUTHINFO, false);
	dbinit.AddProperty(DBPROP_AUTH_USERID, g_CurLine.m_strUserName.c_str() );
	dbinit.AddProperty(DBPROP_INIT_CATALOG, g_CurLine.m_strDB.c_str() );
	dbinit.AddProperty(DBPROP_INIT_DATASOURCE, g_CurLine.m_strHostName.c_str() );
	dbinit.AddProperty(DBPROP_INIT_LCID, (long)1049);
	dbinit.AddProperty(DBPROP_INIT_PROMPT, (short)4);
	hr = db.Open(_T("SQLOLEDB.1"), &dbinit);
	if (FAILED(hr))
		return hr;

	return CSession::Open(db);
}

CDBPropSet* CDBSession::GetPropRowset(long  dwAccess)
{
	if(m_pPropRowset)
		delete m_pPropRowset;
	m_pPropRowset = new CDBPropSet(DBPROPSET_ROWSET);
	m_pPropRowset->AddProperty(DBPROP_IRowsetChange, true);
	m_pPropRowset->AddProperty(DBPROP_UPDATABILITY, dwAccess);

	return m_pPropRowset;
}

HRESULT CDBSession::DumbQuery(std::string strSQL)
{
	CCommand< CDynamicAccessor > QueryCommand;
	return QueryCommand.Open(*this, strSQL.c_str(), NULL, NULL, DBGUID_DEFAULT, false);
}

long CDBSession::getCount(std::string strPartSQL)
{
	long dwRows = 0;
	
	CCommand<CDynamicAccessor> CountRst;
	std::string strSQL = "select count(1) from " + strPartSQL;
	ATLTRACE(strSQL.c_str());
	HRESULT res = CountRst.Open(*this, strSQL.c_str());
	if( FAILED(res) )
		return -1;
	res = CountRst.MoveFirst();
	if( FAILED(res) )
	{
		CountRst.Close();
		return -1;
	}
	dwRows = *(long*)CountRst.GetValue(1);
	CountRst.Close();
	
	return dwRows;
}

long CDBSession::getScalar(LPCSTR sql)
{
	long ret = 0;
	CCommand<CDynamicAccessor> cRst;
	HRESULT res = cRst.Open(*this, sql);
	if( FAILED(res) )
		return -1;
	res = cRst.MoveFirst();
	if( FAILED(res) )
	{
		cRst.Close();
		return -1;
	}
	ret = *(long*)cRst.GetValue(1);
	cRst.Close();
	
	return ret;
}


DATE CDBSession::DBTimeStampToDate( DBTIMESTAMP &rDBts )
{

	SYSTEMTIME sDT;
	sDT.wYear		= rDBts.year;
	sDT.wMonth		= rDBts.month;
	sDT.wDay		= rDBts.day;
	sDT.wHour		= rDBts.hour;

	sDT.wMinute		= rDBts.minute;
	sDT.wSecond		= rDBts.second;
	sDT.wMilliseconds = rDBts.fraction / 10e6;
	DATE ret;
	if( SystemTimeToVariantTime(&sDT, &ret) == 0 )
		ret = 0.0;
	return ret;
}

time_t CDBSession::SystemTimeToUnixTime(SYSTEMTIME &rST)
{
	time_t unixTime = time(0);
	struct tm tme;
	memset(&tme, 0 , sizeof(tm));
	tme = *localtime(&unixTime);

	tme.tm_hour = rST.wHour;
	tme.tm_min = rST.wMinute;
	tme.tm_sec = rST.wSecond;
	tme.tm_mday = rST.wDay;

	tme.tm_year = rST.wYear - 1900;
	tme.tm_mon = rST.wMonth - 1;
	tme.tm_isdst = -1;

	unixTime = mktime( &tme );
	return unixTime;
}

void CDBSession::UnixTimeToSystemTime(time_t uxTime, SYSTEMTIME& retSDT)
{
	struct tm tme;
	memset(&tme, 0 , sizeof(tm));
	tme.tm_isdst = -1;
	tme = *localtime(&uxTime);

	retSDT.wHour = tme.tm_hour;
	retSDT.wMinute = tme.tm_min;
	retSDT.wSecond = tme.tm_sec;
	retSDT.wDay = tme.tm_mday;

	retSDT.wYear =  tme.tm_year + 1900;
	retSDT.wMonth = tme.tm_mon + 1;
}

time_t CDBSession::VariantTimeToUnixTime(DATE rDBdt)
{
	time_t unixTime = time(0);
	SYSTEMTIME sDT;
	struct tm tme;
	memset(&tme, 0 , sizeof(tm));
	memset(&sDT, 0 , sizeof(SYSTEMTIME));
	tme = *localtime(&unixTime);
	
	if( VariantTimeToSystemTime(rDBdt, &sDT) )
	{
		tme.tm_hour = sDT.wHour;
		tme.tm_min = sDT.wMinute;
		tme.tm_sec = sDT.wSecond;
		tme.tm_mday = sDT.wDay;

		tme.tm_year = sDT.wYear - 1900;
		tme.tm_mon = sDT.wMonth - 1;
		tme.tm_isdst = -1;

		unixTime = mktime( &tme );
		return unixTime;
	}
	return 0;
}

int CDBSession::SystemTimeToDBTimeStamp( SYSTEMTIME &rSDT, DBTIMESTAMP &rdbDT  )
{
	rdbDT.year		= rSDT.wYear;
	rdbDT.month		= rSDT.wMonth;
	rdbDT.day		= rSDT.wDay;
	rdbDT.hour		= rSDT.wHour;

	rdbDT.minute	= rSDT.wMinute;
	rdbDT.second	= rSDT.wSecond;
	rdbDT.fraction	= 0;
	return 0;
}

std::string HresError::what()
{
	CDBErrorInfo ErrorInfo;
	ULONG        cRecords;
	HRESULT      hr;
	ULONG        i;
	CComBSTR     bstrDesc, bstrHelpFile, bstrSource;
	GUID         guid;
	DWORD        dwHelpContext;
	WCHAR        wszGuid[40];
	USES_CONVERSION;

	CString errorMessage = "";


	errorMessage.Format( _T("OLE DB Error Record dump for hr = 0x%x\n"), m_hrErr );

	LCID lcLocale = GetSystemDefaultLCID();

	hr = ErrorInfo.GetErrorRecords(&cRecords);
	if (FAILED(hr) && ErrorInfo.m_spErrorInfo == NULL)
	{
		CString strTemp;
		strTemp.Format( _T("No OLE DB Error Information found: hr = 0x%x\n"), hr );
		errorMessage += strTemp;
	}
	else
	{
		for (i = 0; i < cRecords; i++)
		{
			hr = ErrorInfo.GetAllErrorInfo(i, lcLocale, &bstrDesc, &bstrSource, &guid,
										&dwHelpContext, &bstrHelpFile);
			if (FAILED(hr))
			{
				CString strTemp;
				strTemp.Format(_T("OLE DB Error Record dump retrieval failed: hr = 0x%x\n"), hr);
				errorMessage += strTemp;
				return errorMessage;
			}
			StringFromGUID2(guid, wszGuid, sizeof(wszGuid) / sizeof(WCHAR));

			CString strTemp;
			strTemp.Format(
				_T("Row #: %4d Source: \"%s\" Description: \"%s\" Help File: \"%s\" Help Context: %4d GUID: %s\n"),
				i, OLE2T(bstrSource), OLE2T(bstrDesc), OLE2T(bstrHelpFile), dwHelpContext, OLE2T(wszGuid) );
			errorMessage += strTemp;

			bstrSource.Empty();
			bstrDesc.Empty();
			bstrHelpFile.Empty();
		}

		errorMessage += _T("OLE DB Error Record dump end\n");
	}
	return errorMessage;
}

HresError::HresError(HRESULT hr)	{	m_hrErr = hr;	}
HresError::~HresError(){}

HResult& HResult::operator = (HRESULT hr)
{
	  Assign(hr);
	  return *this;
}

HResult::HResult(HRESULT hr)	{ Assign(hr); }

void HResult::Assign(HRESULT hr) throw( std::exception )
{
	if( FAILED(m_hr = hr) )
		throw	HresError(hr);
}
