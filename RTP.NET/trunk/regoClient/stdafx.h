// $Header: /rtpClient/stdafx.h 9     12.10.08 20:40 Sergey Zhmetko $ 
// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__950DC682_B542_4737_ADB5_007051AB5D24__INCLUDED_)
#define AFX_STDAFX_H__950DC682_B542_4737_ADB5_007051AB5D24__INCLUDED_

// Change these values to use different versions
#define _WIN32_WINNT 0x0400
#define _WIN32_IE	0x0400
#define _RICHEDIT_VER	0x0100
#define _ATL_USE_CSTRING_FLOAT
#define _ATL_USE_DDX_FLOAT
#define _WTL_USE_CSTRING
#include <atlbase.h>
#include <atlapp.h>

extern CAppModule _Module;

#include <atlwin.h>

#include <atlframe.h>
#include <atlctrls.h>
#include <atldlgs.h>
#include <atlctrlw.h>
#include <atlcom.h>
#include <atlhost.h>
#include <atlctrlx.h>
#include <atldbcli.h>
#include <map>
#include <list>
#include <string>
#include <vector>
#include <atlprint.h>
#include <atlmisc.h>
#include <atlddx.h>
#include "accessors.h"
#include "definitions.h"
#include "dbutils.h"

#include <GdiPlus.h>
//#include <vld.h>
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__950DC682_B542_4737_ADB5_007051AB5D24__INCLUDED_)
