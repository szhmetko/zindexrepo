//$Header: /rtpClient/WFDesigner.cpp 6     11.07.08 2:01 Sergey Zhmetko $
// WFDesigner.cpp: implementation of the CWFDesigner class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include "WFDesigner.h"
#include "config.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CWFDesigner::CWFDesigner(int iRowHeight) : m_iRowHeight(iRowHeight)
{
	ATLTRACE("%d images loaded.\n", m_imgCache.size());
}

CWFDesigner::~CWFDesigner()
{}


LRESULT CWFDesigner::OnChildWndDestroyed(UINT, WPARAM wParam, LPARAM, BOOL&)
{
	if(wParam)
	{
		CWFRow*  prw = reinterpret_cast<CWFRow*>(wParam);
		iterator iter = begin();
		while(iter != end())
		{
			CWFRow& curRow = *iter;
			if(prw == &curRow)
			{
				erase(iter);
				break;
			}
			iter++;
		}
		//remove(*prw);
		Layout();
		Invalidate();
		if(size())rbegin()->SetFocus();
	}
	return 0;
}

LRESULT CWFDesigner::OnWndPosChanged(UINT, WPARAM wParam, LPARAM, BOOL& bHandled)
{
	Layout();
	bHandled = FALSE;
	return 0;
}


size_t CWFDesigner::InsertRow(int after)
{
	CRect rcDsgnr;
	GetClientRect(&rcDsgnr);
	int height = m_iRowHeight > 0 ? m_iRowHeight : 100;
	CRect rcRow(0, 0, rcDsgnr.Width(), height);
	ClientToScreen(&rcDsgnr);
	CWFRow row(*this, 80);
	push_back(row);
	CString cap;
	static int counter = 0;
	cap.Format("%d", ++counter);
	//int scrw = dc.GetDeviceCaps(HORZRES);
	//int scrh = dc.GetDeviceCaps(VERTRES);
	back().Create(m_hWnd, rcRow, cap);
	if(m_iRowHeight <= 0 )
	{
		// need repositioning and resize all cells
		Layout();
		Invalidate();	// ?
	}
	return size();
}

void CWFDesigner::Layout()
{
	CRect rcDsgnr, rcRow(0, 0, 0, 0);
	GetClientRect(&rcDsgnr);
	int rowHeight = m_iRowHeight > 0 ? m_iRowHeight : !empty() ? rcDsgnr.Height() / size() : 10;
	rcRow.right = rcDsgnr.Width();
	rcRow.bottom = rowHeight;

	//iterate through cell collection
	ClientToScreen(&rcRow);
	iterator iter = begin();
	while(iter != end())
	{
		iter->MoveWindow(rcRow);
		rcRow.OffsetRect(0, rcRow.Height());
		iter++;
	}
}

LRESULT CWFDesigner::OnKeyUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if(wParam == VK_INSERT)
	{	
		SendMessage(WM_COMMAND, IDS_WFD_CMD_ADDROW, 0);
	}
	return 0;
}

LRESULT CWFDesigner::OnCreate(UINT, WPARAM wParam, LPARAM, BOOL& bHandled)
{
	m_cmds.push_back(IDS_WFD_CMD_ADDROW);

	bHandled = FALSE;	//to process with CCommandSupplier
	Load();
	return 0;
}

LRESULT CWFDesigner::OnCommand(UINT, WPARAM wParam, LPARAM, BOOL&)
{
	WORD wID = LOWORD(wParam);
	switch(wID)
	{
	case IDS_WFD_CMD_ADDROW:
		InsertRow();
		Layout();
		Invalidate();
		break;
	}
	return 0;
}

int CWFDesigner::Save()
{
	using namespace dbutils;
	CDBSession	locSession;
	HResult hres;
	try
	{
		hres = locSession.Open();
		hres = locSession.DumbQuery(CConfig::GetValue("wf_removeall").c_str());

		locSession.Close();
	}
	catch(HresError he)
	{
		::MessageBox(0, he.what().c_str(), "", MB_OK);
		return 0;
	}

	iterator iter = begin();
	while(iter != end())
	{
		iter->Save();
		iter++;
	}
	return size();
}

int CWFDesigner::Load()
{
	using namespace dbutils;
	CDBSession locSession;
	HResult hres;
	try
	{
		hres = locSession.Open();
		CCommand < CAccessor<CWorkFlowCellAccessor> > rrst;
		hres = rrst.Open(locSession, CConfig::GetValue("wflowrow/select").c_str() );
		
		while(rrst.MoveNext() == S_OK)
		{
			InsertRow();
			back().m_item_id = rrst.m_item_id;
			back().Load();
		}
		rrst.Close();

		locSession.Close();
		return size();
	}
	catch(HresError he)
	{
		::MessageBox(0, he.what().c_str(), "", MB_OK);
	}
	return -1;
}
