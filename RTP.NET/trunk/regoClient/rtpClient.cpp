// $Header: /rtpClient/rtpClient.cpp 6     1.05.08 11:14 Sergey Zhmetko $ 
// rtpClient.cpp : main source file for rtpClient.exe
//

#include "stdafx.h"

#include "resource.h"

#include "aboutdlg.h"
#include "ChildView.h"
#include "LegendWnd.h"
#include "MainFrm.h"
#include "..\Skin\SkinX.h"
#include "..\Skin\SkinX_i.c"
#include "ApproxRanges.h"

CAppModule _Module;

CHANNELS_MAP	g_ChannelsMap;
LIMITS_MAP		g_LimitsMap;
SLineSettings	g_CurLine;
THERMOPAIRS_MAP	g_tPairsMap;

int Run(LPTSTR /*lpstrCmdLine*/ = NULL, int nCmdShow = SW_SHOWDEFAULT)
{
	CMessageLoop theLoop;
	_Module.AddMessageLoop(&theLoop);

	CMainFrame wndMain;

	if(wndMain.CreateEx() == NULL)
	{
		ATLTRACE(_T("Main window creation failed!\n"));
		return 0;
	}

	wndMain.ShowWindow(SW_SHOWMAXIMIZED);

	int nRet = theLoop.Run();

	_Module.RemoveMessageLoop();
	return nRet;
}

int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE /*hPrevInstance*/, LPTSTR lpstrCmdLine, int nCmdShow)
{
	ULONG_PTR gdiplusToken; 
	HRESULT hRes = ::CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
	ATLASSERT(SUCCEEDED(hRes));

	using namespace Gdiplus;
	GdiplusStartupInput gdiplusStartupInput; 
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	// this resolves ATL window thunking problem when Microsoft Layer for Unicode (MSLU) is used
	::DefWindowProc(NULL, 0, 0, 0L);

	AtlInitCommonControls(ICC_COOL_CLASSES | ICC_BAR_CLASSES | ICC_WIN95_CLASSES | ICC_DATE_CLASSES);	
	// add flags to support other controls


	hRes = _Module.Init(NULL, hInstance);
	ATLASSERT(SUCCEEDED(hRes));
	AtlAxWinInit();
	CComPtr<ISkinX>	spISkinX;
	spISkinX.CoCreateInstance(CLSID_MacSkin);
	if (spISkinX)
		spISkinX->InstallSkin(_Module.m_dwMainThreadID);

	int nRet = Run(lpstrCmdLine, nCmdShow);

	spISkinX.Release();
	_Module.Term();
	
	GdiplusShutdown(gdiplusToken);

	::CoUninitialize();

	return nRet;
}
