// $Header: /rtpClient/ChildView.h 8     13.04.08 22:10 Sergey Zhmetko $ 
// ChildView.h: interface for the CChildView class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CHILDVIEW_H__4F96F9D4_FC51_44CB_BF16_955C523085A4__INCLUDED_)
#define AFX_CHILDVIEW_H__4F96F9D4_FC51_44CB_BF16_955C523085A4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <WTLTabViewCtrl.h>
#include <atlcrack.h>

class CMainFrame;
class CGenericTab;
class CChildView : public CWTLTabViewCtrl 
{
	protected:
	enum TAB_TYPE{	TT_MONITOR	= 0, TT_ARCHIVE = 1, TT_PIECES = 2, TT_WORKFLOW = 3};

public:
	CChildView();
	virtual ~CChildView();
	virtual void OnTabRemoved( int inTabIndex );
public:
	void AddPiecesTab(const CTabsAccessor &rst);
	void AddMonitorTab(const CTabsAccessor& rst);
	void AddArchiveTab(const CTabsAccessor&);
	void AddWorkFlowTab(const CTabsAccessor&);

	bool ConnectTo();
	void SetListener(CMainFrame*);
	CGenericTab*	GetViewTab(int);
	CGenericTab*	GetActiveViewTab();
	void SetTabsZoomMode(OscZoomMode, bool);
	DECLARE_WND_SUPERCLASS(NULL, CWTLTabViewCtrl::GetWndClassName())

	BOOL PreTranslateMessage(MSG* pMsg);

	BEGIN_MSG_MAP_EX(CChildView)
		CHAIN_MSG_MAP(CWTLTabViewCtrl)
        REFLECTED_NOTIFY_CODE_HANDLER_EX(TCN_SELCHANGE, OnChangeSelection)
        //REFLECT_NOTIFICATIONS()
	END_MSG_MAP()

	LRESULT OnChangeSelection(LPNMHDR);
	
private:
	CMainFrame* m_pMainFrame;
};

#endif // !defined(AFX_CHILDVIEW_H__4F96F9D4_FC51_44CB_BF16_955C523085A4__INCLUDED_)
