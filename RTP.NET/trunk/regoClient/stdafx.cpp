// $Header: /rtpClient/stdafx.cpp 4     13.04.08 22:10 Sergey Zhmetko $ 
// stdafx.cpp : source file that includes just the standard includes
//	rtpClient.pch will be the pre-compiled header
//	stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

#if (_ATL_VER < 0x0700)
#include <atlimpl.cpp>
#endif //(_ATL_VER < 0x0700)
#pragma comment(lib, "GdiPlus.lib")
#pragma comment(lib, "pngthmb.lib")
