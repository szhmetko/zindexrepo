// $Header: /rtpClient/row.cpp 4     1.05.08 11:14 Sergey Zhmetko $ 
#include "stdafx.h"
#include "row.h"
#include "cell.h"
#include "wflow.h"

CWorkFlowRow::CWorkFlowRow(unsigned short uHeight, const CWorkFlowRowAccessor& rac, IImageSettings& parent) 
				: CWorkFlowRowAccessor(rac)
				, m_bgColor(RGB(255, 255, 255))
				, m_frColor(RGB(0, 0, 255))
				, m_uHeight(uHeight)
				, m_bShowGrid(false)
				, m_ctrlParent(parent)
{}

CWorkFlowRow::~CWorkFlowRow()
{
	const_iterator citer = begin();
	while( citer != end() )
	{
		if(*citer)
			delete *citer;
		citer++;
	}
	clear();
}

LRESULT CWorkFlowRow::OnCreate(UINT, WPARAM, LPARAM, BOOL&)
{
	//_CleanUp();
	return 0;
}

LRESULT CWorkFlowRow::OnPaint(UINT, WPARAM wParam, LPARAM, BOOL&)
{
	PAINTSTRUCT ps;
	CDC dc = BeginPaint(&ps);

	_DrawBackground(dc);

	EndPaint(&ps);
	return 0;
}

LRESULT CWorkFlowRow::OnSize(UINT, WPARAM, LPARAM, BOOL&)
{
	Layout();
	Invalidate();
	return 0;
}
CGenericWorkFlowCell* CWorkFlowRow::AddBMPCell(const CWorkFlowCellAccessor&  cac)
{
	CRect rcItem; GetClientRect(&rcItem);
	CBMPWorkFlowCell* pNewCell = new CBMPWorkFlowCell(cac, m_ctrlParent.GetImageDir());
	if(pNewCell)
	{
		DWORD dwStyle = WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;
		dwStyle |= m_bShowGrid ? WS_BORDER : 0;
	#ifdef _DEBUG
//				dwStyle |= WS_BORDER;
	#endif
		pNewCell->Create(m_hWnd, rcItem, NULL, dwStyle, 0);
		push_back(pNewCell);
	}
	return pNewCell;
}
CGenericWorkFlowCell* CWorkFlowRow::AddValueCell(const CWorkFlowCellAccessor& cac)
{
	CRect rcItem; GetClientRect(&rcItem);
	CValueWorkFlowCell* pNewCell = new CValueWorkFlowCell(cac);
	if(pNewCell)
	{
		DWORD dwStyle = WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;
		dwStyle |= m_bShowGrid ? WS_BORDER : 0;
	#ifdef _DEBUG
//				dwStyle |= WS_BORDER;
	#endif
		pNewCell->Create(m_hWnd, rcItem, NULL, dwStyle, 0);
		push_back(pNewCell);
	}
	return pNewCell;
}

void CWorkFlowRow::Layout()
{
	if( !empty() )
	{
		float fRatio = m_ctrlParent.PictureRatio();
		CRect rcClient;
		GetClientRect(&rcClient);
		long lWidth = rcClient.Height()/fRatio;//rcClient.Width() / m_cells.size();
		rcClient.right = rcClient.left + lWidth;

		iterator citer = begin();
		while( citer != end() )
		{
			CGenericWorkFlowCell* pCell = *citer;
			if(pCell)
				pCell->MoveWindow(&rcClient);
			rcClient.OffsetRect(lWidth, 0);
			citer++;
		}
	}
}

void CWorkFlowRow::_DrawBackground(CDC& dc)
{
	CRect rcClient;
	GetClientRect(&rcClient);
	dc.FillSolidRect(&rcClient, m_bgColor);
}
void CWorkFlowRow::Animate(bool bEnabled)
{
	const_iterator citer = begin();
	while( citer != end() )
	{
		CGenericWorkFlowCell* pCell = *citer;
		if(pCell)
			pCell->Animate(bEnabled);
		citer++;
	}
}
unsigned short CWorkFlowRow::Height(){return m_uHeight;}

void CWorkFlowRow::SetBgColor(COLORREF newClr){ m_bgColor = newClr; Invalidate();}

int CWorkFlowRow::Fill(CSession& ses)
{
	using namespace dbutils;
	try
	{
		CCommand<CAccessor<CWorkFlowCellAccessor> >	rst;
		HResult hres = S_OK;

		rst.m_parent_id = m_item_id;
		hres = rst.Open( ses, CConfig::GetValue("wflowcell/select").c_str());

		while(rst.MoveNext() == S_OK)
		{
			switch(rst.m_subtype)
			{
			case 0:
				AddBMPCell(rst);
				break;
			case 1:
				AddValueCell(rst);
				break;
			}
			ATLTRACE("cell founded\n");
			rst.ClearRecord();
		}
		rst.Close();
	}
	catch(HresError exc)
	{
		ATLTRACE(exc.what().c_str());
	}

	return 0;
}
void CWorkFlowRow::SetChannelValues(std::map<long, double> vals)
{
	iterator citer = begin();
	while( citer != end() )
	{
		if(*citer)
			(*citer)->SetChannelValues(vals);
		citer++;
	}
}