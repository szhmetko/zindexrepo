// $Header: /rtpClient/accessors.h 11    11.07.08 2:01 Sergey Zhmetko $ 
#if !defined(AFX_ACCESSORS_H__INCLUDED_)
#define AFX_ACCESSORS_H__INCLUDED_

#define _BLOB_ENTRY_CODE_EX(nOrdinal, IID, flags, dataOffset, lengthOffset, statusOffset) \
	if (pBuffer != NULL) \
	{ \
		CAccessorBase::FreeType(DBTYPE_IUNKNOWN, pBuffer + dataOffset); \
	} \
	else if (pBinding != NULL) \
	{ \
		DBOBJECT* pObject = NULL; \
		ATLTRY(pObject = new DBOBJECT); \
		if (pObject == NULL) \
			return E_OUTOFMEMORY; \
		pObject->dwFlags = flags; \
		pObject->iid     = IID; \
		CAccessorBase::Bind(pBinding, nOrdinal, DBTYPE_IUNKNOWN, sizeof(IUnknown*), 0, 0, eParamIO, \
			dataOffset, lengthOffset, statusOffset, pObject); \
		pBinding++; \
	} \
	nColumns++;

#define BLOB_ENTRY_LENGTH_STATUS(nOrdinal, IID, flags, data, length, status) \
	_BLOB_ENTRY_CODE_EX(nOrdinal, IID, flags, offsetbuf(data), offsetbuf(length), offsetbuf(status));

class CChannelSettingsAccessor
{
public:
	TCHAR	m_descr[51];	// ??????? ????????
	BYTE	m_num;	// ????? ??????
	LONG	m_color;	// ???? ???????
	float	m_k;		//?-?
	TCHAR	m_fname[17];
//	LONG	m_chlid;
	bool	bVisibleCol;
	int		iColNum;
	int		m_tPairID;
BEGIN_PARAM_MAP(CChannelSettingsAccessor)
   COLUMN_ENTRY(1, m_num)
END_PARAM_MAP()
BEGIN_COLUMN_MAP(CChannelSettingsAccessor)
	COLUMN_ENTRY(1, m_color)
	COLUMN_ENTRY(2, m_k)
	COLUMN_ENTRY(3, m_descr)
	COLUMN_ENTRY(4, m_fname)
	COLUMN_ENTRY(5, m_num)
	COLUMN_ENTRY(6, m_tPairID)
END_COLUMN_MAP()

	void ClearRecord()
	{
		memset(this, 0, sizeof(*this));
	}
};
class CScaleSettingsAccessor
{
public:
	LONG m_lim_id;	// ID 
	TCHAR	m_limit_name[32];	// ???????? ???????
	double m_limit_val;	// ???????? 
//	DATE m_dt;		//???? ??????????

BEGIN_COLUMN_MAP(CScaleSettingsAccessor)
	COLUMN_ENTRY(1, m_limit_name)
	COLUMN_ENTRY(2, m_limit_val)
//	COLUMN_ENTRY(5, m_descr)
	COLUMN_ENTRY(3, m_lim_id)
END_COLUMN_MAP()

	void ClearRecord()
	{
		memset(this, 0, sizeof(*this));
	}
};

class CTPairsAccessor
{
public:
	LONG m_id;	// ID 
	TCHAR	m_name[33];	// ???????? ???????
	DATE m_dt_upd;		//???? ??????????

BEGIN_COLUMN_MAP(CTPairsAccessor)
	COLUMN_ENTRY(1, m_id)
	COLUMN_ENTRY(2, m_name)
	COLUMN_ENTRY_TYPE(3, DBTYPE_DATE, m_dt_upd)
END_COLUMN_MAP()

	void ClearRecord()
	{
		memset(this, 0, sizeof(*this));

		SYSTEMTIME dt;	ZeroMemory( &dt, sizeof(SYSTEMTIME) );
		GetLocalTime( &dt );
		SystemTimeToVariantTime( &dt, &m_dt_upd );
	}
};
class CApproxAccessor
{
public:
	long	m_tpair_id;
	BYTE	m_range_num;
	double	m_offset;
	double	m_k;
	long	m_u_from;

BEGIN_PARAM_MAP(CApproxAccessor)
   COLUMN_ENTRY(1, m_tpair_id)
END_PARAM_MAP()
BEGIN_COLUMN_MAP(CApproxAccessor)
	COLUMN_ENTRY(1, m_tpair_id)
	COLUMN_ENTRY(2, m_range_num)
	COLUMN_ENTRY(3, m_offset)
	COLUMN_ENTRY(4, m_k)
	COLUMN_ENTRY(5, m_u_from)
END_COLUMN_MAP()

	void ClearRecord()
	{
		memset(this, 0, sizeof(*this));
	}
};

class CConfigAccessor
{
public:
	char	m_var_name[33];
	char	m_var_val[33];
	char	m_param[33];

BEGIN_PARAM_MAP(CConfigAccessor)
   COLUMN_ENTRY(1, m_param)
END_PARAM_MAP()
BEGIN_COLUMN_MAP(CConfigAccessor)
	COLUMN_ENTRY(1, m_var_name)
	COLUMN_ENTRY(2, m_var_val)
END_COLUMN_MAP()

	void ClearRecord()
	{
		memset(this, 0, sizeof(*this));
	}
};
class CCableDescriptionAccessor
{
public:
	LONG	m_piece_id;	// ID 
	TCHAR	m_paramName[65];	// ???????? ?????????
	TCHAR	m_paramVal[129];	// ????????  ?????????

BEGIN_PARAM_MAP(CCableDescriptionAccessor)
   COLUMN_ENTRY(1, m_piece_id)
END_PARAM_MAP()
BEGIN_COLUMN_MAP(CCableDescriptionAccessor)
	COLUMN_ENTRY(1, m_piece_id)
	COLUMN_ENTRY(2, m_paramName)
	COLUMN_ENTRY(3, m_paramVal)
END_COLUMN_MAP()

	void ClearRecord()	{		memset(this, 0, sizeof(*this));	}

};
class CMetersAccessor
{
public:
	long	m_meter;
	short	m_chl;
BEGIN_COLUMN_MAP(CMetersAccessor)
	COLUMN_ENTRY(1, m_meter)
	COLUMN_ENTRY(2, m_chl)
END_COLUMN_MAP()

	void ClearRecord()
	{
		memset(this, 0, sizeof(*this));
	}
};
class CTabsChannelsAccessor
{
public:
	BYTE	m_curves_num;
	LONG	m_tab_id;
	TCHAR	m_alias[17];
	TCHAR	m_main_title[33];
	TCHAR	m_primary_units[33];
	TCHAR	m_sec_units[33];
	LONG	m_bgcolor;
	double	m_primary_low_lim_id;
	double	m_sec_low_lim_id;
	double	m_primary_high_lim_id;
	double	m_sec_high_lim_id;
	LONG	m_num;			//
	LONG	m_id;			//

	long			m_parent_id;


BEGIN_PARAM_MAP(CTabsChannelsAccessor)
   COLUMN_ENTRY(1, m_parent_id)
END_PARAM_MAP()
BEGIN_ACCESSOR_MAP(CTabsChannelsAccessor, 2)	//	??? ??????
   BEGIN_ACCESSOR(0, true)
	COLUMN_ENTRY(1, m_curves_num)
	COLUMN_ENTRY(2, m_tab_id)
	COLUMN_ENTRY(3, m_alias)
	COLUMN_ENTRY(4, m_main_title)
	COLUMN_ENTRY(5, m_primary_units)
	COLUMN_ENTRY(6, m_sec_units)
	COLUMN_ENTRY(7, m_bgcolor)
	COLUMN_ENTRY(8, m_primary_low_lim_id)
	COLUMN_ENTRY(9, m_primary_high_lim_id)
	COLUMN_ENTRY(10, m_sec_low_lim_id)
	COLUMN_ENTRY(11, m_sec_high_lim_id)
	COLUMN_ENTRY(12, m_num)
	COLUMN_ENTRY(13, m_id)
   END_ACCESSOR()
   BEGIN_ACCESSOR(1, false) // not an auto accessor	//  ??? ????????? / ??????????
	COLUMN_ENTRY(1, m_curves_num)
	COLUMN_ENTRY(2, m_tab_id)
	COLUMN_ENTRY(3, m_alias)
	COLUMN_ENTRY(4, m_main_title)
	COLUMN_ENTRY(5, m_primary_units)
	COLUMN_ENTRY(6, m_sec_units)
	COLUMN_ENTRY(7, m_bgcolor)
	COLUMN_ENTRY(8, m_primary_low_lim_id)
	COLUMN_ENTRY(9, m_primary_high_lim_id)
	COLUMN_ENTRY(10, m_sec_low_lim_id)
	COLUMN_ENTRY(11, m_sec_high_lim_id)
	COLUMN_ENTRY(12, m_num)
   END_ACCESSOR()
END_ACCESSOR_MAP()

	void ClearRecord()	{	memset(this, 0, sizeof(*this));	}
	CString FormatRow()
	{
		CString tr("sergey");
		tr.Format("%s (%d)", m_main_title, m_curves_num);
		return tr;
	}
};



class CCurvesAccessor
{
public:
	LONG			m_id;
	LONG			m_dchl_id;
	LONG			m_group_id;
	VARIANT_BOOL	m_bPrimary;
	TCHAR			m_name[51];
	LONG			m_clr;
	LONG			m_num;
	TCHAR			m_fname[17];
	long			m_parent_id;

BEGIN_PARAM_MAP(CCurvesAccessor)
   COLUMN_ENTRY(1, m_parent_id)
END_PARAM_MAP()
BEGIN_ACCESSOR_MAP(CCurvesAccessor, 2)	
   BEGIN_ACCESSOR(0, true)	//	??? ??????
	COLUMN_ENTRY(1, m_dchl_id)
	COLUMN_ENTRY(2, m_group_id)
	COLUMN_ENTRY_TYPE(3, DBTYPE_BOOL, m_bPrimary)
	COLUMN_ENTRY(4, m_name)
	COLUMN_ENTRY(5, m_clr)
	COLUMN_ENTRY(6, m_num)
	COLUMN_ENTRY(7, m_fname)
	COLUMN_ENTRY(8, m_id)
   END_ACCESSOR()
   BEGIN_ACCESSOR(1, false) // not an auto accessor	//  ??? ????????? / ?????????? 
	COLUMN_ENTRY(1, m_dchl_id)/*select dchl_num, group_id, bPrimary from  curves*/
	COLUMN_ENTRY(2, m_group_id)
	COLUMN_ENTRY_TYPE(3, DBTYPE_BOOL, m_bPrimary)
   END_ACCESSOR()
END_ACCESSOR_MAP()

	void ClearRecord()	{	memset(this, 0, sizeof(*this));	}
	CString FormatRow(){return m_name;}
};

class CTabsAccessor
{
public:
	LONG	m_num;			//
	TCHAR	m_caption[33];	// 
	TCHAR	m_alias[17];	// 
	SHORT	m_chan_num;		//
	TCHAR	m_tbl_name[33];	// 
	LONG	m_id;			//

	long m_parent_id;


BEGIN_PARAM_MAP(CTabsAccessor)
   COLUMN_ENTRY(1, m_parent_id)
END_PARAM_MAP()
BEGIN_ACCESSOR_MAP(CTabsAccessor, 2)	//	??? ??????
   BEGIN_ACCESSOR(0, true)
	COLUMN_ENTRY(1, m_num)
	COLUMN_ENTRY(2, m_caption)
	COLUMN_ENTRY(3, m_alias)
	COLUMN_ENTRY(4, m_chan_num)
	COLUMN_ENTRY(5, m_tbl_name)
	COLUMN_ENTRY(6, m_id)
   END_ACCESSOR()
   BEGIN_ACCESSOR(1, false) // not an auto accessor	//  ??? ????????? / ??????????
	COLUMN_ENTRY(1, m_num)
	COLUMN_ENTRY(2, m_caption)
	COLUMN_ENTRY(3, m_alias)
	COLUMN_ENTRY(4, m_chan_num)
	COLUMN_ENTRY(5, m_tbl_name)
   END_ACCESSOR()
END_ACCESSOR_MAP()


	void ClearRecord()	{	memset(this, 0, sizeof(*this));	}
	CString FormatRow()
	{
		CString tr;
		tr.Format("%s (%d)", m_caption, m_chan_num);
		return tr;
	}
};

class CGenericWorkFlowAccessor  
{
	typedef CGenericWorkFlowAccessor thisClass;
public:
	int	 m_item_id;			//PRIMARY KEY
	BYTE m_type;			//?????? ??? ??????? 0-??????; 1-??????
	BYTE m_subtype;			//??? ??????/?????? (0-??????;1-????????) (0-?????? ????????; 1-?????? ????????)
	int	 m_parent_id;		//
	
	//??? ??????? (Image_id1,image_id2,image_id...) - ?????? ?????-?? (??? ???????? -???? id) 
	//??? ????? ??-? - #1,#2, ... (?????? ???????) .??? ????? caption ????????
	char m_content[256];	

	BEGIN_ACCESSOR_MAP(thisClass, 2)	//	??? ??????
	   BEGIN_ACCESSOR(0, true)
		   COLUMN_ENTRY(1, m_parent_id)
		   COLUMN_ENTRY(2, m_type)
		   COLUMN_ENTRY(3, m_subtype)
		   COLUMN_ENTRY(4, m_content)
		   COLUMN_ENTRY(5, m_item_id)
	   END_ACCESSOR()
	   BEGIN_ACCESSOR(1, false) // not an auto accessor	//  ??? ????????? / ??????????
		   COLUMN_ENTRY(1, m_parent_id)
		   COLUMN_ENTRY(2, m_type)
		   COLUMN_ENTRY(3, m_subtype)
		   COLUMN_ENTRY(4, m_content)
	   END_ACCESSOR()
	END_ACCESSOR_MAP()
/*	BEGIN_COLUMN_MAP(thisClass)
	   COLUMN_ENTRY(1, m_parent_id)
	   COLUMN_ENTRY(2, m_type)
	   COLUMN_ENTRY(3, m_subtype)
	   COLUMN_ENTRY(4, m_item_id)
	   COLUMN_ENTRY(5, m_content)
	END_COLUMN_MAP()
*/	void ClearRecord()	{		ZeroMemory(this, sizeof(*this));	}

};
class CWorkFlowRowAccessor : public CGenericWorkFlowAccessor  
{
	typedef CWorkFlowRowAccessor thisClass;
public:
};
class CWorkFlowCellAccessor : public CGenericWorkFlowAccessor  
{
	typedef CWorkFlowCellAccessor thisClass;
public:
	BEGIN_PARAM_MAP(thisClass)
		SET_PARAM_TYPE(DBPARAMIO_INPUT)
		COLUMN_ENTRY(1, m_parent_id)
	END_PARAM_MAP()
};

class CImagesAccessor
{
public:
	ISequentialStream* m_data;
	long	m_id;
	LARGE_INTEGER m_written;
	LARGE_INTEGER m_created;

	ULONG m_BLOBDATA_LENGTH;
	ULONG m_BLOBDATA_STATUS;
BEGIN_ACCESSOR_MAP(CImagesAccessor, 2)	//	??? ??????
	BEGIN_ACCESSOR(0, true)
	 	COLUMN_ENTRY(1, m_written)
	 	COLUMN_ENTRY(2, m_created)
		BLOB_ENTRY_LENGTH_STATUS(3, IID_ISequentialStream, STGM_READ, m_data, m_BLOBDATA_LENGTH, m_BLOBDATA_STATUS)
	 	COLUMN_ENTRY(4, m_id)
	END_ACCESSOR()
	BEGIN_ACCESSOR(1, false)
	 	COLUMN_ENTRY(1, m_written)
	 	COLUMN_ENTRY(2, m_created)
		BLOB_ENTRY_LENGTH_STATUS(3, IID_ISequentialStream, STGM_READ, m_data, m_BLOBDATA_LENGTH, m_BLOBDATA_STATUS)
	END_ACCESSOR()
END_ACCESSOR_MAP()

	void ClearRecord()		{			memset(this, 0, sizeof(*this));		}
};

class CPiecesDetailAccessor
{
public:
	LONG m_recid;
	DATE m_dtmon;
	DATE m_dtmoff;
	LONG m_length;

BEGIN_PARAM_MAP(CPiecesDetailAccessor)
   COLUMN_ENTRY(1, m_recid)
END_PARAM_MAP()
BEGIN_COLUMN_MAP(CPiecesDetailAccessor)
	COLUMN_ENTRY(1, m_recid)
	COLUMN_ENTRY_TYPE(2, DBTYPE_DATE, m_dtmon)
	COLUMN_ENTRY_TYPE(3, DBTYPE_DATE, m_dtmoff)
	COLUMN_ENTRY(4, m_length)
END_COLUMN_MAP()

	void ClearRecord(){memset(this, 0, sizeof(*this));}
};

#endif // !defined(AFX_ACCESSORS_H__INCLUDED_)
