// $Header: /rtpClient/ConstructorDlg.cpp 7     12.10.08 20:39 Sergey Zhmetko $ 
#include "stdafx.h"
#include "resource.h"
#include "ConstructorDlg.h"
#include "TabChannelsDlg.h"
#include "WFDesignerDlg.h"
#include "config.h"

LRESULT CTabParamsDlg::OnInitDialog(UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	CenterWindow(GetParent());
	CComboBox cmb;
	
	cmb = GetDlgItem(IDC_CALIAS);
	cmb.AddString("alert");
	cmb.AddString("monitor");
	cmb.AddString("archive");
	cmb.AddString("pieces");
	cmb.AddString("workflow");
	//cmb.SetCurSel(0);
	cmb.SelectString(-1, m_TabInfo.m_alias);


	cmb = GetDlgItem(IDC_CTBLNAME);
	cmb.AddString("ValsByTimeRow");
	cmb.AddString("ValsByMeterRow");
	//cmb.SetCurSel(0);
	cmb.SelectString(-1, m_TabInfo.m_tbl_name);
	SetDlgItemText(IDC_ECAPTION, m_TabInfo.m_caption);
	return TRUE;
}

LRESULT CTabParamsDlg::OnCloseCmd(WORD, WORD wID, HWND, BOOL&)
{
	EndDialog(wID);
	return 0;
}

CTabParamsDlg::CTabParamsDlg(){	m_TabInfo.ClearRecord();}

CTabParamsDlg::CTabParamsDlg(const CTabsAccessor& tac)
				: m_TabInfo(tac)
{}

LRESULT CTabParamsDlg::OnOk(WORD, WORD wID, HWND, BOOL& bHandled)
{
	CComboBox cmb;
	
	cmb = GetDlgItem(IDC_CALIAS);
	cmb.GetLBText(cmb.GetCurSel(), m_TabInfo.m_alias);	

	cmb = GetDlgItem(IDC_CTBLNAME);
	cmb.GetLBText(cmb.GetCurSel(), m_TabInfo.m_tbl_name);	

	::GetWindowText(GetDlgItem(IDC_ECAPTION),
		m_TabInfo.m_caption, sizeof(m_TabInfo.m_caption) - 1 );
	EndDialog(wID);
	return 0;
}

LRESULT CConstructorDlg::OnNodeSelect(int, LPNMHDR, BOOL& bHandled)
{
	CTreeItem selItem = m_wndTabsTree.GetSelectedItem();
	if( selItem != NULL)	//�����-�� ���� ������
	{
		SItem* pItem = (SItem*)selItem.GetData();


		HWND btnEdit = GetDlgItem(IDC_BEDIT);
		HWND btnAdd = GetDlgItem(IDC_BADD);
		HWND btnRemove = GetDlgItem(IDC_BREMOVE);

		::ShowWindow(btnEdit, SW_SHOW );
		::ShowWindow(btnAdd, SW_SHOW );
		::ShowWindow(btnRemove, SW_SHOW );
		if( pItem )
		{
			m_curSelection.id = pItem->id;
			m_curSelection.uType = pItem->uType;

			switch(pItem->uType)
			{
				case TIT_CURVE:
					::ShowWindow(btnAdd, SW_HIDE );
					::ShowWindow(btnEdit, SW_HIDE );
					break;
				case TIT_TABCHANNEL:
				case TIT_TAB:
					::ShowWindow(btnRemove, selItem.HasChildren()?SW_HIDE:SW_SHOW );
					break;
			}
		}
		else
		{
			::ShowWindow( btnEdit, SW_HIDE );
			::ShowWindow(btnRemove, SW_HIDE );
		}
	}

	return 0;
}

bool CConstructorDlg::EditTab(SItem* pItem)
{
	using namespace dbutils;
	if(pItem)
	{
		CDBSession	locSession;
		HResult hres;
		try
		{
			hres = locSession.Open();
			CCommand<CAccessor<CTabsAccessor> >rst;
			rst.m_parent_id = pItem->id;
			hres = rst.Open(locSession, CConfig::GetValue("tabs/delete").c_str()
				, locSession.GetPropRowset(DBPROPVAL_UP_CHANGE));
			if( rst.MoveFirst() == S_OK )
			{
				if( strcmp(CharLower(rst.m_alias), _T("workflow")) == 0 )
				{
					EndDialog(IDCANCEL);

					CWFDesignerDlg dlg; dlg.DoModal();
				}
				else
				{
					CTabParamsDlg dlg(rst); 
					if(IDOK == dlg.DoModal())
					{
						strcpy(rst.m_alias, dlg.m_TabInfo.m_alias);
						strcpy(rst.m_tbl_name, dlg.m_TabInfo.m_tbl_name);
						strcpy(rst.m_caption, dlg.m_TabInfo.m_caption);

						hres = rst.SetData(1);
					}
				}
			}
			rst.Close();
			locSession.Close();
			return true;
		}
		catch(HresError he)
		{
			::MessageBox(0, he.what().c_str(), "", MB_OK);
			return false;
		}
	}
	return false;
}
bool CConstructorDlg::AddTab()
{
	CTabParamsDlg dlg;
	if( dlg.DoModal() == IDOK )
	{
		long pid = 0;
		using namespace dbutils;
		CDBSession	locSession;
		HResult hres;
		try
		{
			hres = locSession.Open();
			CCommand<CAccessor<CTabsAccessor> > rst;
			hres = rst.Open(locSession, CConfig::GetValue("tabs/insert").c_str()
				, locSession.GetPropRowset(DBPROPVAL_UP_INSERT));
			rst.ClearRecord();
			rst.m_num = ( rst.MoveFirst() == S_OK ) ?  rst.m_num + 1 : 0;

			strcpy(rst.m_alias, dlg.m_TabInfo.m_alias);
			strcpy(rst.m_tbl_name, dlg.m_TabInfo.m_tbl_name);
			strcpy(rst.m_caption, dlg.m_TabInfo.m_caption);
			rst.m_chan_num = 0;
			
			hres = rst.Insert(1);
			rst.Close();
			locSession.Close();
			return true;
		}
		catch(HresError he)
		{
			::MessageBox(0, he.what().c_str(), "", MB_OK);
		}
	}
	return false;

}

bool CConstructorDlg::RemoveTab(SItem* pItem)
{
	ATLASSERT(pItem);
	using namespace dbutils;
	CDBSession	locSession;
	HResult hres;
	try
	{
		hres = locSession.Open();
		CCommand<CAccessor<CTabsAccessor> > rst;
		rst.m_parent_id = pItem->id;
		hres = rst.Open(locSession, CConfig::GetValue("tabs/delete").c_str()
			, locSession.GetPropRowset(DBPROPVAL_UP_DELETE));
		rst.ClearRecord();
		hres = rst.MoveFirst();
		hres = rst.Delete();
		rst.Close();

		hres = rst.Open(locSession, CConfig::GetValue("tabs/update").c_str()
			, locSession.GetPropRowset(DBPROPVAL_UP_CHANGE));
		unsigned int uTabNum = 0;
		while( rst.MoveNext() == S_OK )
		{
			rst.m_num = uTabNum;
			hres = rst.SetData(1);
			uTabNum++;
		}
		rst.Close();
		locSession.Close();
		return true;
	}
	catch(HresError he)
	{
		::MessageBox(0, he.what().c_str(), "", MB_OK);
	}
	return false;
}

bool CConstructorDlg::AddTabChannel(SItem* pParentItem)
{
	CTabChannelsDlg dlg;
	if( pParentItem && dlg.DoModal() == IDOK )
	{
		long cNum = 0;
		using namespace dbutils;
		CDBSession	locSession;
		HResult hres;
		try
		{
			hres = locSession.Open();
			CCommand<CAccessor<CTabsChannelsAccessor> > rst;
			rst.m_parent_id = pParentItem->id;
			hres = rst.Open(locSession, CConfig::GetValue("tabschannels/insert").c_str()
				, locSession.GetPropRowset(DBPROPVAL_UP_INSERT));
			rst.ClearRecord();
			cNum = ( rst.MoveFirst() == S_OK ) ?  rst.m_num + 1 : 0;
			if( cNum >= OSC_MAX_CHANNELS - 1 )
			{
				CString msg;msg.LoadString(IDS_TCHANLIMIT_ARRIVED);
				MessageBox(msg);
				rst.Close();
				return false;
			}			memcpy(&rst, &dlg.m_tchlInfo, sizeof(CTabsChannelsAccessor) );
			rst.m_num = cNum;
			rst.m_tab_id = pParentItem->id;
			
			hres = rst.Insert(1);
			rst.Close();
			locSession.Close();
			return true;
		}
		catch(HresError he)
		{
			::MessageBox(0, he.what().c_str(), "", MB_OK);
		}
	}
	return false;
}

bool CConstructorDlg::RemoveTabChannel(SItem* pItem)
{
	ATLASSERT(pItem);
	using namespace dbutils;
	CDBSession	locSession;
	HResult hres;
	try
	{
		hres = locSession.Open();
		CCommand<CAccessor<CTabsChannelsAccessor> > rst;
		rst.m_parent_id = pItem->id;
		hres = rst.Open(locSession, CConfig::GetValue("tabschannels/delete").c_str()
			, locSession.GetPropRowset(DBPROPVAL_UP_DELETE));
		rst.ClearRecord();
		hres = rst.MoveFirst();
		long parentTab_id = rst.m_tab_id;
		hres = rst.Delete();
		rst.Close();

		rst.m_parent_id = parentTab_id;
		hres = rst.Open(locSession, CConfig::GetValue("tabschannels/update").c_str()
			, locSession.GetPropRowset(DBPROPVAL_UP_CHANGE));
		unsigned int uTabChlNum = 0;
		while( rst.MoveNext() == S_OK )
		{
			rst.m_num = uTabChlNum;
			hres = rst.SetData(1);
			uTabChlNum++;
		}
		rst.Close();
		locSession.Close();
		return true;
	}
	catch(HresError he)
	{
		::MessageBox(0, he.what().c_str(), "", MB_OK);
	}
	return false;
}

bool CConstructorDlg::EditTabChannel(SItem* pItem)
{

	using namespace dbutils;
	if(pItem)
	{
		CDBSession	locSession;
		HResult hres;
		try
		{
			hres = locSession.Open();
			CCommand<CAccessor<CTabsChannelsAccessor> >rst;
			rst.m_parent_id = pItem->id;
			hres = rst.Open(locSession, CConfig::GetValue("tabschannels/delete").c_str()
				, locSession.GetPropRowset(DBPROPVAL_UP_CHANGE));
			if( rst.MoveFirst() == S_OK )
			{

				CTabChannelsDlg dlg(&rst);
				if( dlg.DoModal() == IDOK )
				{
					memcpy(&rst, &dlg.m_tchlInfo, sizeof(CTabsChannelsAccessor) );
					hres = rst.SetData(1);
				}
			}
			rst.Close();
			locSession.Close();
			return true;
		}
		catch(HresError he)
		{
			::MessageBox(0, he.what().c_str(), "", MB_OK);
			return false;
		}
	}
	return false;
}

bool CConstructorDlg::AddCurve(SItem* pParentItem)
{
	CCurveInfoDlg dlg;
	if( pParentItem && dlg.DoModal() == IDOK )
	{
		using namespace dbutils;
		CDBSession	locSession;
		HResult hres;
		try
		{
			hres = locSession.Open();
			CCommand<CAccessor<CCurvesAccessor> > rst;
			rst.m_parent_id = pParentItem->id;


			hres = rst.Open(locSession, CConfig::GetValue("curves/insert").c_str()
				, locSession.GetPropRowset(DBPROPVAL_UP_INSERT));
			rst.ClearRecord();
			//hres = rst.MoveFirst();
			memcpy(&rst, &dlg.m_curve, sizeof(CCurvesAccessor) );
			rst.m_group_id = pParentItem->id;
			
			hres = rst.Insert(1);
			rst.Close();
			locSession.Close();
			return true;
		}
		catch(HresError he)
		{
			::MessageBox(0, he.what().c_str(), "", MB_OK);
		}

	}
	else
		return false;
	return true;
}

bool CConstructorDlg::RemoveCurve(SItem* pItem)
{
	using namespace dbutils;
	CDBSession	locSession;
	HResult hres;
	try
	{
		hres = locSession.Open();
		CString strSQL;
		strSQL.Format("delete  from  curves where rec_id=%d", pItem->id);
		hres = locSession.DumbQuery(std::string(strSQL));

		locSession.Close();
		return true;
	}
	catch(HresError he)
	{
		::MessageBox(0, he.what().c_str(), "", MB_OK);
	}

	return false;
}

LRESULT CConstructorDlg::OnAdd(WORD, WORD, HWND, BOOL& bHandled)
{
	// ������� ����� ������, ��� �����������!!!!!
	CTreeItem selItem = m_wndTabsTree.GetSelectedItem();
	if( selItem != NULL)	//�����-�� ���� ������
	{
		m_bAffected = false;
		SItem* pItem = (SItem*)selItem.GetData();
		
		if( pItem  == NULL )	//root selected
			m_bAffected =  AddTab();
		else
		{
			switch( pItem->uType )
			{
			case TIT_TAB:	//TIT_TAB = 0, TIT_TABCHANNEL, TIT_CURVE
				m_bAffected =  AddTabChannel( pItem );
				break;
			case TIT_TABCHANNEL:
				m_bAffected =  AddCurve( pItem );
				break;
			}
		}
		if(m_bAffected)
			_FillTree();
	}
	else
	{
		MessageBox(_T("�� ������ �� ���� �����!") );
	}
	return 0;
}

LRESULT CConstructorDlg::OnRemove(WORD, WORD, HWND, BOOL& bHandled)
{
	CTreeItem selItem = m_wndTabsTree.GetSelectedItem();

	if( selItem.HasChildren() )
	{
		MessageBox( _T("������� ����� ������� ���������") );
		return 0;
	}

	if( selItem != NULL)	//�����-�� ���� ������
	{
		m_bAffected = false;
		SItem* pItem = (SItem*)selItem.GetData();
		
		if( pItem  == NULL )	//root selected
			return 0;
		else
		{
			switch( pItem->uType )
			{
			case TIT_TAB:	//TIT_TAB = 0, TIT_TABCHANNEL, TIT_CURVE
				m_bAffected =  RemoveTab(pItem);
				break;
			case TIT_TABCHANNEL:
				{
					CTreeItem pr = selItem.GetParent();
					if(!pr.IsNull())m_curSelection = *(PSItem)pr.GetData();
					m_bAffected =  RemoveTabChannel( pItem );
				}
				break;
			case TIT_CURVE:
				{
					CTreeItem pr = selItem.GetParent();
					if(!pr.IsNull())m_curSelection = *(PSItem)pr.GetData();
					m_bAffected =  RemoveCurve( pItem );
				}
				break;
			}
		}
		if(m_bAffected)
			_FillTree();
	}
	else
	{
		MessageBox(_T("�� ������ �� ���� �����!") );
	}
	return 0;
}

LRESULT CConstructorDlg::OnEdit(WORD, WORD, HWND, BOOL& bHandled)
{
	CTreeItem selItem = m_wndTabsTree.GetSelectedItem();

	if( selItem != NULL)	//�����-�� ���� ������
	{
		m_bAffected = false;
		SItem* pItem = (SItem*)selItem.GetData();
		
		if( pItem  == NULL )	//root selected
			return 0;
		else
		{
			switch( pItem->uType )
			{
			case TIT_TAB:	//TIT_TAB = 0, TIT_TABCHANNEL, TIT_CURVE
					m_bAffected =  EditTab(pItem);
				break;
			case TIT_TABCHANNEL:
				m_bAffected =  EditTabChannel( pItem );
				break;
			case TIT_CURVE:
//					m_bAffected =  EditCurve( pItem );	//impossible
				break;
			}
		}
		if(m_bAffected)
			_FillTree();
	}
	else
	{
		MessageBox(_T("�� ������ �� ���� �����!") );
	}
	return 0;
}

LRESULT CConstructorDlg::OnInitDialog(UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	DlgResize_Init(false, false, WS_CLIPCHILDREN);
	CenterWindow(GetParent());
	m_bAffected = false;
	DoDataExchange(false);
	m_wndTabsTree.SetImageList();
	_FillTree();
	return TRUE;
}

LRESULT CConstructorDlg::OnCloseCmd(WORD, WORD wID, HWND, BOOL& bHandled)
{
	EndDialog(wID);
	return 0;
}

LRESULT CConstructorDlg::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	_CleanUpTree();
	return 0;
}

void CConstructorDlg::_CleanUpTree()
{
	_CleanUpTreeItem( m_wndTabsTree.GetRootItem() );
	m_wndTabsTree.DeleteAllItems();
}

void CConstructorDlg::_CleanUpTreeItem(CTreeItem& rItem)
{
	if( rItem.m_hTreeItem )
	{
		CString strTxt;
		rItem.GetText(strTxt);
		if( rItem.HasChildren() )
		{
			CTreeItem childItem = rItem.GetChild();
			if( childItem.m_hTreeItem )
				_CleanUpTreeItem( childItem );
			while( CTreeItem nextItem = childItem.GetNextSibling() )
			{
				_CleanUpTreeItem( nextItem );
				childItem = nextItem;
			}

		}
		SItem* pItem = (SItem*)rItem.GetData();
		if( pItem )
		{
			delete pItem;
			rItem.SetData(0);
		}
	}
}

void CConstructorDlg::_FillTree()
{
	using namespace dbutils;
	_CleanUpTree();
	CTreeItem iRoot = m_wndTabsTree.InsertItem ( _T("�������"), -1, -1, TVI_ROOT, TVI_LAST );
	iRoot.SetData(0);
	
	CDBSession	locSession;
	CTreeItem needToSelect = iRoot;
	HResult hres;
	try
	{
		hres = locSession.Open();
		CCommand<CAccessor<CTabsAccessor> >rst;
		hres = rst.Open(locSession, CConfig::GetValue("tabs/select").c_str());
		CString strRow;
		while( rst.MoveNext() == S_OK )
		{
			strRow.Format("%d.%s -- %s (%d)", rst.m_num,
				rst.m_caption, rst.m_tbl_name,  rst.m_chan_num);
			CTreeItem curItem = iRoot.AddTail(strRow, 0);
			SItem* pItem = new SItem;
			pItem->id = rst.m_id;
			pItem->num = rst.m_num;
			pItem->uType = TIT_TAB;
			pItem->clr = RGB(0, 0, 60);
			curItem.SetData((DWORD_PTR)pItem);
			if(m_curSelection.id == pItem->id && m_curSelection.uType == pItem->uType)
				needToSelect = curItem;
			HTREEITEM tret = _FillTabChannels(rst.m_id, curItem);
			if(tret != 0)
				needToSelect = tret;
		}
		rst.Close();
	}
	catch(HresError he)
	{
		::MessageBox(0, he.what().c_str(), "", MB_OK);
	}
	m_wndTabsTree.Select(needToSelect, TVGN_CARET);
}

HTREEITEM CConstructorDlg::_FillTabChannels( int TabID, CTreeItem &tiParent )
{

	using namespace dbutils;
	
	CDBSession	locSession;
	HTREEITEM ret = 0;
	HResult hres;
	try
	{
		hres = locSession.Open();
		CCommand<CAccessor<CTabsChannelsAccessor> >rst;
		rst.m_parent_id = TabID;
		hres = rst.Open(locSession, CConfig::GetValue("tabschannels/select").c_str());
		CString strRow;
		while( rst.MoveNext() == S_OK )
		{
			strRow.Format("%d.%s (%d)", rst.m_num, rst.m_main_title, rst.m_curves_num);
			CTreeItem curItem = tiParent.AddTail(strRow, 1);

			SItem* pItem = new SItem;
			pItem->id = rst.m_id;
			pItem->num = rst.m_num;
			pItem->uType = TIT_TABCHANNEL;
			pItem->clr = RGB(0, 50, 0);
			curItem.SetData((DWORD_PTR)pItem);
			if(m_curSelection.id == pItem->id && m_curSelection.uType == pItem->uType)
				ret = curItem;
			HTREEITEM cret = _FillCurves(rst.m_id, curItem);
			if(cret != 0)
				ret = cret;
		}
	}
	catch(HresError he)
	{
		::MessageBox(0, he.what().c_str(), "", MB_OK);
	}
	return ret;
}

HTREEITEM CConstructorDlg::_FillCurves( int tchlID, CTreeItem &tiParent )
{
	using namespace dbutils;
	
	CDBSession	locSession;
	HTREEITEM ret = 0;
	HResult hres;
	try
	{
		hres = locSession.Open();
		CCommand<CAccessor<CCurvesAccessor> >rst;
		rst.m_parent_id = tchlID;
		hres = rst.Open(locSession, CConfig::GetValue("curves/select").c_str());
		CString strRow;
		while( rst.MoveNext() == S_OK )
		{
			strRow.Format("%d.%s", rst.m_num, rst.m_name);
			
			CTreeItem curItem = tiParent.AddTail(strRow, rst.m_bPrimary ? 2 : 3);

			SItem* pItem = new SItem;
			pItem->id = rst.m_id;
			pItem->num = rst.m_num;
			pItem->uType = TIT_CURVE;
			pItem->clr = RGB(60, 0, 60);
			curItem.SetData((DWORD_PTR)pItem);
			if(m_curSelection.id == pItem->id && m_curSelection.uType == pItem->uType)
				ret = curItem;
		}
	}
	catch(HresError he)
	{
		::MessageBox(0, he.what().c_str(), "", MB_OK);
	}
	return ret;
}

LRESULT CConstructorDlg::OnRemoveAll(WORD, WORD, HWND, BOOL& bHandled)
{
	CString msg;
	msg.LoadString(IDS_CREMOVEALL_ALERT);
	if(IDYES == ::MessageBox(0, msg, "", MB_YESNO))
	{
		using namespace dbutils;
		CDBSession	locSession;
		HResult hres;
		try
		{
			hres = locSession.Open();
			hres = locSession.DumbQuery(CConfig::GetValue("removeall").c_str());

			locSession.Close();
			return true;
		}
		catch(HresError he)
		{
			::MessageBox(0, he.what().c_str(), "", MB_OK);
		}
	}
	return 0;
}
