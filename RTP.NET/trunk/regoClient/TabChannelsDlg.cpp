// $Header: /rtpClient/TabChannelsDlg.cpp 3     13.04.08 17:22 Sergey Zhmetko $ 
// TabChannelsDlg.cpp: implementation of the CTabChannelsDlg class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include "TabChannelsDlg.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
extern CHANNELS_MAP	g_ChannelsMap;

void CColorButton::DrawItem ( LPDRAWITEMSTRUCT lpdis )
{
	CDCHandle dc = lpdis->hDC;
	CDC dcMem;

	dcMem.CreateCompatibleDC ( dc );
	dc.SaveDC();
	dcMem.SaveDC();

	dc.FillSolidRect ( &lpdis->rcItem, m_clr );

	if ( lpdis->itemState & ODS_SELECTED ) 
		dc.BitBlt ( 1, 1, 80, 80, dcMem, 0, 0, SRCCOPY );
	else
		dc.BitBlt ( 0, 0, 80, 80, dcMem, 0, 0, SRCCOPY );

	dcMem.RestoreDC(-1);
	dc.RestoreDC(-1);
}

CTabChannelsDlg::CTabChannelsDlg(CTabsChannelsAccessor* pItem) :
			m_bEditMode(pItem != 0)
{
	memset( &m_tchlInfo, 0, sizeof(CTabsChannelsAccessor) );
	if(!m_bEditMode)
	{
		m_tchlInfo.m_sec_low_lim_id = 1;
		m_tchlInfo.m_sec_high_lim_id = 2;
		m_tchlInfo.m_primary_low_lim_id = 1;
		m_tchlInfo.m_primary_high_lim_id = 2;
	}
	else
	{
		m_tchlInfo = *pItem;
	}
}

LRESULT CTabChannelsDlg::OnSetBgClr(WORD , WORD wID, HWND , BOOL& )
{
	CColorDialog clrDlg(m_tchlInfo.m_bgcolor, CC_FULLOPEN);
	if( IDOK == clrDlg.DoModal() )
	{
		m_tchlInfo.m_bgcolor = clrDlg.GetColor();
		m_btnBgClr.m_clr = m_tchlInfo.m_bgcolor;
		m_btnBgClr.Invalidate();
	}
	return 0;
}

LRESULT CTabChannelsDlg::OnOk(WORD , WORD wID, HWND , BOOL& )
{
	if( !DoDataExchange(true) )
	{
		MessageBox ( "��������� ������ �����������", 0, MB_ICONWARNING );
		return 0;
	}

	EndDialog(wID);
	return 0;
}

LRESULT CTabChannelsDlg::OnInitDialog(UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	CenterWindow(GetParent());
	m_btnBgClr.SubclassWindow ( GetDlgItem(IDC_BCHNGBGCOLOR) );
	m_btnBgClr.m_clr = m_tchlInfo.m_bgcolor;
	_FillAliases();
	DoDataExchange(false);

	CString strwndTitle = m_bEditMode ? "��������� ������ ������" : "�������� ������ ������";
	CString strDoneBtn = m_bEditMode ? "��������" : "��������";
	
	SetWindowText(strwndTitle);
	SetDlgItemText(IDOK, strDoneBtn);
	return TRUE;
}

LRESULT CTabChannelsDlg::OnCloseCmd(WORD, WORD wID, HWND, BOOL& bHandled)
{
	EndDialog(wID);
	return 0;
}

void CTabChannelsDlg::_FillAliases()
{
	CComboBox cmbAliases = GetDlgItem(IDC_CTABCHLALIASES);
	cmbAliases.AddString("velocity");
	cmbAliases.AddString("alerts");
	cmbAliases.AddString("diameter");
	cmbAliases.AddString("temperatures");
	cmbAliases.SetCurSel(0);
}

CCurveInfoDlg::CCurveInfoDlg()
{
	m_curve.ClearRecord();
	m_curve.m_bPrimary = 1;
}

LRESULT CCurveInfoDlg::OnOk(WORD , WORD wID, HWND , BOOL& )
{
	CListViewCtrl chlsList = (CListViewCtrl)GetDlgItem(IDC_LCHANNELS);
	CButton btn = GetDlgItem(IDC_CBPRIMARY);

	int i = chlsList.GetNextItem( -1, LVNI_ALL | LVNI_SELECTED);
	if( i == -1 )
		return 0;

	m_curve.m_dchl_id = chlsList.GetItemData(i);
	m_curve.m_bPrimary = btn.GetCheck();

	EndDialog(wID);

	return 0;
}

LRESULT CCurveInfoDlg::OnInitDialog(UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	CenterWindow(GetParent());
	_popList();
	CButton btn = GetDlgItem(IDC_CBPRIMARY);
	btn.SetCheck(m_curve.m_bPrimary);
	return TRUE;
}

LRESULT CCurveInfoDlg::OnCloseCmd(WORD, WORD wID, HWND, BOOL& bHandled)
{
	EndDialog(wID);
	return 0;
}

void CCurveInfoDlg::_popList()
{
	using namespace std;
	CListViewCtrl chlsList = (CListViewCtrl)GetDlgItem(IDC_LCHANNELS);
	ListView_SetExtendedListViewStyle(chlsList, LVS_EX_FULLROWSELECT);
	const int  iCols = 2;
	TCHAR rgtsz[iCols][32] = {_T("�"), _T("���")};
	LV_COLUMN		lvcolumn;
	CRect rect;
	chlsList.GetWindowRect( &rect );

	for(int i = 0; i < iCols; i++)  
	{
		lvcolumn.mask = LVCF_FMT | LVCF_SUBITEM | LVCF_TEXT | LVCF_WIDTH | LVCF_ORDER;
		lvcolumn.fmt = LVCFMT_LEFT;
		lvcolumn.pszText = rgtsz[i];
		lvcolumn.iSubItem = i;
		lvcolumn.iOrder = i;
		if(i == 0)
			lvcolumn.cx = 40;//
		else 
			lvcolumn.cx = (rect.Width() - 40) / (iCols - 1) - 2;
		chlsList.InsertColumn(i, &lvcolumn);  
	}


	LV_ITEM			lvitem;
	map<int, CChannelSettingsAccessor>::const_iterator citer = g_ChannelsMap.begin();
	while(citer != g_ChannelsMap.end())
	{
		const CChannelSettingsAccessor& cac = citer->second;

		CString szCell;
		lvitem.mask = LVIF_TEXT | LVIF_PARAM;
		lvitem.iItem = 0;
		lvitem.iSubItem = 0;
		lvitem.lParam = (LPARAM)cac.m_num;

		szCell.Format("%d", cac.m_num);
		lvitem.pszText = szCell.GetBuffer(0);
		chlsList.InsertItem(&lvitem);

		lvitem.iSubItem = 1;
		lvitem.mask = LVIF_TEXT | 0;
		lvitem.pszText = (LPSTR)cac.m_descr;
		chlsList.SetItem(&lvitem);

		citer++;
	}
}
