// $Header: /rtpClient/ChildView.cpp 8     11.07.08 2:00 Sergey Zhmetko $ 
// ChildView.cpp: implementation of the CChildView class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ChildView.h"
#include "resource.h"
#include "DynamicArchiveView.h"
#include "DynamicMonitorView.h"
#include "DynamicPiecesView.h"
#include "DynamicWFlowView.h"
#include "config.h"
#include "LegendWnd.h"
#include "MainFrm.h"
#include "LineSelectCombo.h"

extern	SLineSettings	g_CurLine;
extern CHANNELS_MAP	g_ChannelsMap;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CChildView::CChildView() : m_pMainFrame(0)
{}

CChildView::~CChildView()
{}

BOOL CChildView::PreTranslateMessage(MSG* pMsg)		
{
	pMsg;
	return IsDialogMessage(pMsg);
}

void CChildView::OnTabRemoved( int inTabIndex )
{
	LPARAM theTabParam = GetTabParam( inTabIndex );
	if ( theTabParam != 0 )
	{
		TAB_TYPE theTabImage = TAB_TYPE(GetTabImage( inTabIndex ));
		switch( theTabImage )
		{
			case TT_ARCHIVE:
			{
				CDynamicArchiveView* theWindowPtr = reinterpret_cast<CDynamicArchiveView*>( theTabParam );
				theWindowPtr->DestroyWindow();
				delete theWindowPtr;
				break;
			}
			case TT_MONITOR:
			{
				CDynamicMonitorView* theWindowPtr = reinterpret_cast<CDynamicMonitorView*>( theTabParam );
				theWindowPtr->DestroyWindow();
				delete theWindowPtr;
				break;
			}
			case TT_PIECES:
			{
				CDynamicPiecesView* theWindowPtr = reinterpret_cast<CDynamicPiecesView*>( theTabParam );
				theWindowPtr->DestroyWindow();
				delete theWindowPtr;
				break;
			}
			case TT_WORKFLOW:
			{
				CDynamicWFlowView* theWindowPtr = reinterpret_cast<CDynamicWFlowView*>( theTabParam );
				theWindowPtr->DestroyWindow();
				delete theWindowPtr;
				break;
			}
		}
	}
}

CGenericTab* CChildView::GetViewTab(int inTabIndex)
{
	LPARAM theTabParam = GetTabParam(inTabIndex);
	if(theTabParam)
		return reinterpret_cast<CGenericTab*>(theTabParam);

	return 0;
}

CGenericTab* CChildView::GetActiveViewTab()
{
	return GetViewTab(GetActiveTabIndex());
}

void CChildView::AddArchiveTab(const CTabsAccessor& rst)
{
	CDynamicArchiveView* dv = new CDynamicArchiveView(rst);
	dv->Create( *this );
	AddTab( dv->m_caption, *dv, TRUE, TT_ARCHIVE, (LPARAM) dv );				
}

void CChildView::AddMonitorTab(const CTabsAccessor &rst)
{
	CDynamicMonitorView* dv = new CDynamicMonitorView(rst);
	dv->Create( *this );
	AddTab( dv->m_caption, *dv, TRUE, TT_MONITOR, (LPARAM) dv );				
}

void CChildView::AddPiecesTab(const CTabsAccessor &rst)
{
	CDynamicPiecesView* dv = new CDynamicPiecesView(rst);
	dv->Create( *this );
	AddTab( dv->m_caption, *dv, TRUE, TT_PIECES, (LPARAM) dv );				
}

void CChildView::AddWorkFlowTab(const CTabsAccessor &rst)
{
	CDynamicWFlowView* dv = new CDynamicWFlowView(rst);
	dv->Create( *this );
	AddTab( dv->m_caption, *dv, TRUE, TT_WORKFLOW, (LPARAM) dv );				
}

bool CChildView::ConnectTo()
{
	CLineSelectDlg dlg;
	if(IDOK != dlg.DoModal())
		return true;
	RemoveAllTabs();
	/////////////
	/*
	g_CurLine.m_strUserName = "client";
	g_CurLine.m_strPassword = "4YYDJ";
	g_CurLine.m_strHostName = "rabbit";
	g_CurLine.m_strDB = "lknv60";*/
	//////////////
	using namespace dbutils;
	CDBSession locSession;
	HResult hres;
	try
	{
		hres = locSession.Open();
		CCommand < CAccessor<CChannelSettingsAccessor> > crst;
		hres = crst.Open(locSession, CConfig::GetValue("channels/select").c_str() );
		
		while(crst.MoveNext() == S_OK)
		{
			g_ChannelsMap[crst.m_num] = crst;
			crst.ClearRecord();
		}
		crst.Close();

		////////*******Load thermo Pairs settings in memory****///////////
		CCommand<CAccessor<CTPairsAccessor> >	tprst;
		hres = tprst.Open( locSession, CConfig::GetValue("tpairs/select").c_str() );
		while( tprst.MoveNext() == S_OK )
		{
			CRangesGroup rg(tprst);
			g_tPairsMap[tprst.m_id] = rg;
		}
		tprst.Close();

		CCommand<CAccessor<CTabsAccessor> >	trst;
		hres = trst.Open(locSession, CConfig::GetValue("tabs/select").c_str());
		while( trst.MoveNext() == S_OK )
		{
			if( strcmp(CharLower(trst.m_alias), _T("archive")) == 0 )
				AddArchiveTab(trst);
			else if( strcmp(CharLower(trst.m_alias), _T("monitor")) == 0 )
				AddMonitorTab(trst);
			else if( strcmp(CharLower(trst.m_alias), _T("pieces")) == 0 )
				AddPiecesTab(trst);
			else if( strcmp(CharLower(trst.m_alias), _T("workflow")) == 0 )
				AddWorkFlowTab(trst);
		}

		locSession.Close();
	}
	catch(HresError he)
	{
		::MessageBox(0, he.what().c_str(), "", MB_OK);
	}

	SetActiveTab(0);
	CGenericTab* pTab = GetActiveViewTab();
	if(pTab)		pTab->UpdateLegend(m_pMainFrame->m_legend);

	return false;
}

void CChildView::SetListener(CMainFrame* pFrame)	{	m_pMainFrame = pFrame;	}

LRESULT CChildView::OnChangeSelection(LPNMHDR)
{
	CGenericTab* pTab = GetActiveViewTab();
	if(pTab)		pTab->UpdateLegend(m_pMainFrame->m_legend);
	SetMsgHandled(FALSE);
	return 0;
}

void CChildView::SetTabsZoomMode(OscZoomMode zm, bool bEnb)
{
	for(int idx = 0; idx < GetTabCount(); ++idx)
	{
		CGenericTab* tb = GetViewTab(idx);
		if(tb)tb->ChangeZoom(zm, bEnb);
	}
}
