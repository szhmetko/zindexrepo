// $Header: /rtpClient/GenericTab.cpp 11    12.10.08 20:40 Sergey Zhmetko $ 
// GenericTab.cpp: implementation of the CGenericTab class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include "GenericTab.h"
#include "config.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGenericTab::CGenericTab(const CTabsAccessor& tac, bool bZoomEnabled, bool bSilent)
					: CTabsAccessor(tac)
					, m_bZoomEnabled(bZoomEnabled)
					, m_bSilent(bSilent)
{
	m_bByTime = std::string(m_tbl_name) == "ValsByTimeRow";
	HRESULT res = _loadChilds(tac.m_id);
}

CGenericTab::~CGenericTab()
{}

void CGenericTab::ChangeZoom(OscZoomMode zm, bool bEnabled)
{
	m_plot.EnableZoom(bEnabled && m_bZoomEnabled);
	m_plot.ZoomMode( zm );
}

void CGenericTab::ResultSaveAs(const char* szFileName, int nType)
{
	switch(nType)
	{
	case 0:
	default:
		m_data.ExportToCSV(szFileName);
	}
}

bool CGenericTab::GetFrameRect(HENHMETAFILE hEmf, LPRECT lpRect)
{
	ATLASSERT(lpRect);

	ENHMETAHEADER	emh;
	// Get the header from the enhanced metafile.
	ZeroMemory( &emh, sizeof(ENHMETAHEADER) );
	emh.nSize = sizeof(ENHMETAHEADER);
	if( ::GetEnhMetaFileHeader( hEmf, sizeof( ENHMETAHEADER ), &emh ) == 0 )
		return false;
	
	lpRect->top = emh.rclFrame.top;
	lpRect->bottom = emh.rclFrame.bottom;
	lpRect->left = emh.rclFrame.left;
	lpRect->right = emh.rclFrame.right;
	return true;
}

void CGenericTab::PlotPrint()
{
	using namespace std;
	PRINTDLG pd;
	ZeroMemory(&pd, sizeof(pd));
	pd.lStructSize = sizeof(pd);
	pd.hwndOwner   = 0;
	pd.hDevMode    = NULL;     // Don't forget to free or store hDevMode
	pd.hDevNames   = NULL;     // Don't forget to free or store hDevNames
	pd.Flags       = PD_USEDEVMODECOPIESANDCOLLATE | PD_RETURNDC; 
	pd.nCopies     = 1;
	pd.nFromPage   = 0x01;
	pd.nToPage     = m_TabChlsVector.size();
	pd.nMinPage    = 0x01;
	pd.nMaxPage    = m_TabChlsVector.size();
	
	if( PrintDlg(&pd) == TRUE )
	{
		// Get pointers to the two setting structures.
		DEVNAMES FAR *lpDevNames = (DEVNAMES FAR *)::GlobalLock(pd.hDevNames);
		
		CString szDriver((LPTSTR)lpDevNames + lpDevNames->wDriverOffset);
		string szDevice((LPTSTR)lpDevNames + lpDevNames->wDeviceOffset);
		CString szOutput((LPTSTR)lpDevNames + lpDevNames->wOutputOffset);

		::GlobalUnlock(pd.hDevNames);

		
		DOCINFO	di;
		ZeroMemory( &di, sizeof(DOCINFO) );

		di.cbSize = sizeof(DOCINFO); 
		di.lpszDocName = "Enhanced metafile print"; 
		di.lpszOutput = (LPTSTR) NULL; 
//				di.lpszDataType = (LPTSTR) NULL; 
		di.fwType = 0;
		
		int nError = StartDoc(pd.hDC, &di); 

		string strEmfPath;
		string strWD = CConfig::GetWD();
		for(int i = 0; i < m_TabChlsVector.size(); i++)
		{
			if( (i + 1) > pd.nToPage || (i + 1) < pd.nFromPage )
				continue;

			CRect boundXAxisRect;	//rectangle for emf play
			CRect boundYAxisRect;	//rectangle for emf play
			CRect boundChlRect;	//rectangle for emf play
			CRect boundAllRect;	//rectangle for emf create - big rectangle

			bool ret = m_plot.GetBottomXAxis()->RecordToEMF(strWD + "bX.emf", &boundXAxisRect );
			ret = m_plot.GetLeftYAxis( i )->RecordToEMF(strWD + "lY.emf", &boundYAxisRect );
			ret = m_plot.GetChannel( i )->RecordToEMF(strWD + "body.emf", &boundChlRect );
			ret = m_plot.GetRightYAxis( i )->RecordToEMF(strWD + "rY.emf", &boundYAxisRect );
			
			//calculating big picture dimensions
			CRect bXRect, lYRect;

			strEmfPath = strWD + "bX.emf";
			HENHMETAFILE hEmfFile = ::GetEnhMetaFile( strEmfPath.c_str() );
			GetFrameRect(hEmfFile, &bXRect);
			DeleteEnhMetaFile( hEmfFile );

			strEmfPath = strWD + "lY.emf";
			hEmfFile = ::GetEnhMetaFile( strEmfPath.c_str() );
			GetFrameRect(hEmfFile, &lYRect);
			DeleteEnhMetaFile( hEmfFile );

			boundAllRect.left = 0;
			boundAllRect.top = 0;
			boundAllRect.right = bXRect.Width() + lYRect.Width()*2;
			boundAllRect.bottom = lYRect.Height() + bXRect.Height()*2;


			std::string strFileName = strWD + "all.emf";
			HDC dcEMFall = ::CreateEnhMetaFile(0, strFileName.c_str(), &boundAllRect, _T("emfpic") );

			///////emf output starts here
			////------------------------------------play plots--------------------------///
			strFileName = strWD + "body.emf";
			HENHMETAFILE hSourceEMF = ::GetEnhMetaFile( strFileName.c_str() );

			boundChlRect.OffsetRect( boundYAxisRect.Width(), boundXAxisRect.Height() );
			::PlayEnhMetaFile( dcEMFall, hSourceEMF, &boundChlRect );
			::DeleteEnhMetaFile(hSourceEMF);
			////------------------------------------play plots--------------------------///

			
			////------------------------------------play bottom X axis--------------------------///
			strFileName = strWD + "bX.emf";
			hSourceEMF = ::GetEnhMetaFile( strFileName.c_str() );

			boundXAxisRect.OffsetRect( boundYAxisRect.Width() - OSC_OBJ_OFFSET_Y, 
				boundChlRect.Height() + boundXAxisRect.Height() + OSC_OBJ_OFFSET_X );
			::PlayEnhMetaFile( dcEMFall, hSourceEMF, &boundXAxisRect );
			::DeleteEnhMetaFile(hSourceEMF);
			////------------------------------------play bottom X axis--------------------------///
		


			////------------------------------------play left Y axis--------------------------///
			strFileName = strWD + "lY.emf";
			hSourceEMF = ::GetEnhMetaFile( strFileName.c_str() );

			boundYAxisRect.OffsetRect( 0, boundXAxisRect.Height() - OSC_OBJ_OFFSET_X );
			::PlayEnhMetaFile( dcEMFall, hSourceEMF, &boundYAxisRect );
			::DeleteEnhMetaFile(hSourceEMF);
			////------------------------------------play left Y axis--------------------------///

			
			////------------------------------------play right Y axis--------------------------///
			strFileName = strWD + "rY.emf";
			hSourceEMF = ::GetEnhMetaFile( strFileName.c_str() );

			boundYAxisRect.OffsetRect( boundXAxisRect.Width() + boundYAxisRect.Width() - OSC_OBJ_OFFSET_X, 0 );
			::PlayEnhMetaFile( dcEMFall, hSourceEMF, &boundYAxisRect );
			::DeleteEnhMetaFile(hSourceEMF);
			////------------------------------------play right Y axis--------------------------///

			///////emf output ends here

			HENHMETAFILE hEMHF = ::CloseEnhMetaFile(dcEMFall);
			
			////printer troubles
			
			// Begin a print job by calling the StartDoc function.

			// Inform the driver that the application is about to begin sending data.
			nError = StartPage(pd.hDC); 

			CRect outputRect;	ZeroMemory( &outputRect, sizeof(CRect) );

			int iXp =  GetDeviceCaps(pd.hDC, HORZRES); 
			int iYp =  GetDeviceCaps(pd.hDC, VERTRES); 
			float fScale = float(iXp) / float( boundAllRect.Width() );

			outputRect.right = iXp;
			outputRect.bottom = boundAllRect.Height() * fScale;
			outputRect.InflateRect( -outputRect.Width() * 0.1, -outputRect.Height() * 0.1 );
			outputRect.OffsetRect( outputRect.Width() * 0.05, outputRect.Height() * 0.05 );

			::PlayEnhMetaFile( pd.hDC, hEMHF, &outputRect );
					
			nError = EndPage(pd.hDC); 


			////printer troubles
			
			::DeleteEnhMetaFile(hEMHF);

		}
		
		nError = EndDoc(pd.hDC); 
	
		DeleteDC(pd.hDC);
	}
}

void CGenericTab::UpdateLegend(CLegendWindow& legend)
{
	legend.EraseAll(false);
	for( TAB_CHMAP_ITER iter = m_chlsInUseMap.begin(); 
		iter != m_chlsInUseMap.end(); ++iter )
		legend.AddItem(iter->first, false);
	
	legend.SetCaption(m_caption, true);
}

HRESULT CGenericTab::_loadChilds(int Tab_id)
{
	using namespace dbutils;
	m_chlsInUseMap.clear();

	CDBSession	locSession;
	HResult hres;
	try
	{
		hres = locSession.Open();
		CCommand<CAccessor<CTabsChannelsAccessor> >	rst;
		rst.m_parent_id = Tab_id;	// setup parameter value
		
		hres = rst.Open(locSession, CConfig::GetValue("tabschannels/select").c_str());
		while( rst.MoveNext() == S_OK )
		{
			CGenericTabChannel tchl(rst);
			for( int i = 0; i < tchl.m_ChNumVector.size(); ++i )
			{
				int iChanNum = tchl.m_ChNumVector[i].iChlNum;
				m_chlsInUseMap[iChanNum] = iChanNum;
			}
			m_TabChlsVector.push_back(tchl);

		}
		rst.Close();
		locSession.Close();
	}
	catch(HresError he)
	{
		::MessageBox(0, he.what().c_str(), "", MB_OK);
		return hres;
	}
	return S_OK;
}

CGenericTabChannel::~CGenericTabChannel(){}

CGenericTabChannel::CGenericTabChannel(const CTabsChannelsAccessor& tcac)
				: CTabsChannelsAccessor(tcac)
{
	HRESULT res = _loadChilds(tcac.m_id);
}

HRESULT CGenericTabChannel::_loadChilds(int tabChl_id)
{
	using namespace dbutils;
	m_ChNumVector.clear();

	CDBSession	locSession;
	HResult hres;
	try
	{
		hres = locSession.Open();
		CCommand<CAccessor<CCurvesAccessor> >	rst;
		rst.m_parent_id = tabChl_id;	// setup parameter value
		hres = rst.Open(locSession, CConfig::GetValue("curves/select").c_str());
		while( rst.MoveNext() == S_OK )
		{
			SCurve crv;	crv.reset();
			crv.bPrimary = (rst.m_bPrimary!=0);
			crv.iChlNum = rst.m_num;
			crv.clr = rst.m_clr;

			m_ChNumVector.push_back(crv);

		}
		rst.Close();
	}
	catch(HresError he)
	{
		::MessageBox(0, he.what().c_str(), "", MB_OK);
		return hres;
	}
	return S_OK;
}

long CGenericTab::ShowChart(LPCSTR szFilter, LPCSTR szSort)
{
	using namespace std;
	using namespace dbutils;

	long lCount = m_data.PopulateData(m_tbl_name, szFilter, szSort);
	if(lCount > 1)
	{
		m_plot.LastPoints(lCount);
		
		//temporary storage for first curves from each channel
		double ** initPts = (double **)malloc( m_TabChlsVector.size()*sizeof(double*) );

		///loop trough channels
		size_t i, j;
		for(i = 0; i < m_TabChlsVector.size(); i++)
		{
			CGenericTabChannel& tchl = m_TabChlsVector[i];
			double sec_k = 1.0;	//�-� ��������� ��-� ��������� ��� �� ���������
			
			m_plot.GetChannel(i)->SetYRange(tchl.m_primary_low_lim_id, 
				tchl.m_primary_high_lim_id );

			// ------------------------------�������------------------------------//
			if( (string(tchl.m_alias) == "diameter")
				&& (tchl.m_ChNumVector.size() > 2) )
			{//��� ��� ������, ���������� �  ������ = 3

				int chl_num_etal = tchl.m_ChNumVector[0].iChlNum;	//������
				int chl_num_meas = tchl.m_ChNumVector[1].iChlNum;	//�������
				int chl_num_ = tchl.m_ChNumVector[2].iChlNum;	//������
				double iZeroLevel = 0.0;
				CHMAP_ITER iter = g_ChannelsMap.find(chl_num_etal);
				if( iter != g_ChannelsMap.end() )
				{
					CChannelSettingsAccessor& cac = iter->second;
					//double* Heap = m_data[cac.m_fname];
					iZeroLevel = m_data.LastVal(string(cac.m_fname));

					//if(Heap)
					//	iZeroLevel = Heap[m_data.size() - 1];
				}
				m_plot.GetChannel(i)->SetYRange(iZeroLevel - tchl.m_primary_low_lim_id, 
						iZeroLevel + tchl.m_primary_high_lim_id );

			}
			// ------------------------------�������------------------------------//
			m_plot.GetChannel(i)->RemoveAllCurves();

			if( tchl.m_ChNumVector.size() > 0 )
			{
				int chl_num = tchl.m_ChNumVector[0].iChlNum;
				CHMAP_ITER iter = g_ChannelsMap.find(chl_num);
				if( iter != g_ChannelsMap.end() )
				{
					CChannelSettingsAccessor& cac = iter->second;
					initPts[i] = m_data[cac.m_fname];//.getColumn(cac.m_fname);
				}			
				else
					initPts[i] = 0;
			}
			else
				initPts[i] = 0;
		}

		
		m_plot.SetPoints(lCount-1, initPts, lCount-1);

	////////////////////set up time range
		if(m_bByTime)
		{
			time_t Ftm = CDBSession::VariantTimeToUnixTime(m_data.FirstVal("dtm_upd"));
			time_t Ltm = CDBSession::VariantTimeToUnixTime(m_data.LastVal("dtm_upd"));
			m_plot.GetBottomXAxis()->SetRange(Ftm, Ltm);
		}
	////////////////////set up time range

		for(i = 0; i < m_TabChlsVector.size(); i++)
		{
			CGenericTabChannel& tchl = m_TabChlsVector[i];
			for( j = 0; j < tchl.m_ChNumVector.size(); ++j )
			{
				SCurve	&crv =	tchl.m_ChNumVector[j]; 
				int chl_num = crv.iChlNum;
				crv.k = (crv.bPrimary) ? 1.0 : tchl.m_primary_high_lim_id / tchl.m_sec_high_lim_id;
				CHMAP_ITER iter = g_ChannelsMap.find(chl_num);
				if( iter != g_ChannelsMap.end() )
				{
					CChannelSettingsAccessor& cac = iter->second;
					double* Heap = m_data[cac.m_fname];
					crv.clr = cac.m_color;
					if(Heap)
						m_plot.GetChannel(i)->AddCurve(Heap, crv.clr, 
						(j==2)&&(std::string(tchl.m_alias) == "diameter") ? crv.k*1e6 : crv.k);
				}
			}
	/// setting up secondary axises		
			if( string(tchl.m_alias) != "diameter" )
				m_plot.GetRightYAxis(i)->SetRange(tchl.m_sec_low_lim_id, tchl.m_sec_high_lim_id);
	/// setting up secondary axises	 EOF
		}
		

		for(i = 0; i < m_TabChlsVector.size(); i++)
			m_plot.GetChannel(i)->LayoutCurves();

		free(initPts);
	}
	else
	{
		CString strMsg;
		strMsg.LoadString(IDS_MSG_NODATA);
		if(!m_bSilent)
			::MessageBox(0, strMsg, "", MB_OK|MB_ICONINFORMATION);

	}
	return lCount;
}
