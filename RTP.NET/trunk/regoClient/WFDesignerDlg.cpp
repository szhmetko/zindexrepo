//$Header: /rtpClient/WFDesignerDlg.cpp 5     11.07.08 2:01 Sergey Zhmetko $
// WFDesignerDlg.cpp: implementation of the CWFDesignerDlg class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include "WFDesignerDlg.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CWFDesignerDlg::CWFDesignerDlg() : m_designer(200)
{}

CWFDesignerDlg::~CWFDesignerDlg()
{}

LRESULT CWFDesignerDlg::OnInitDialog(UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	CRect rcClient;
	DlgResize_Init(false, true, WS_CLIPCHILDREN);
	::GetClientRect(GetParent(), &rcClient);
	MoveWindow(&rcClient);

	
	::GetWindowRect(GetDlgItem(IDC_WFDESIGNER), &rcClient);
	m_designer.Create(m_hWnd, rcClient, "");
	using namespace dbutils;
	try
	{
/*		CDBSession ses;
		HRESULT res = ses.Open();

		m_pWflow = new CDesignWorkFlowCtrl(ses);
		if(m_pWflow)
		{
			m_pWflow->SubclassWindow(GetDlgItem(IDC_WFDESIGNER));

			CCommand<CAccessor<CWorkFlowRowAccessor> >	crst;
			HResult hres = S_OK;
			hres = crst.Open( ses, CConfig::GetValue("wflowrow/select").c_str());

			while(crst.MoveNext() == S_OK)
			{
				CWorkFlowRow* pRow = 0;
				pRow = m_pWflow->AddRow(crst, 200);
				pRow->Fill(ses);
				crst.ClearRecord();
			}
			crst.Close();
		}
		ses.Close();*/
	}
	catch(HresError exc)
	{
		MessageBox(exc.what().c_str());
	}

	return TRUE;
}

LRESULT CWFDesignerDlg::OnCancel(WORD, WORD wID, HWND, BOOL& bHandled)
{
	EndDialog(wID);
	return 0;
}

LRESULT CWFDesignerDlg::OnSave(WORD, WORD wID, HWND, BOOL& bHandled)
{
	m_designer.Save();

	EndDialog(wID);
	return 0;
}

LRESULT CWFDesignerDlg::OnWndPosChanged(UINT, WPARAM, LPARAM lParam, BOOL& bHandled)
{
	if(m_designer.IsWindow())
	{
		CRect rcClient;
		::GetWindowRect(GetDlgItem(IDC_WFDESIGNER), &rcClient);
		m_designer.MoveWindow(&rcClient);
	}
	bHandled = FALSE;
	return 0;
}
