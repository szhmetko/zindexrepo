// $Header: /rtpClient/DynamicMonitorView.h 3     13.04.08 17:22 Sergey Zhmetko $ 
// DynamicMonitorView.h: interface for the CDynamicMonitorView class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DYNAMICMONITORVIEW_H__B6B84F17_3843_453F_89A5_DDB34F00D251__INCLUDED_)
#define AFX_DYNAMICMONITORVIEW_H__B6B84F17_3843_453F_89A5_DDB34F00D251__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"
#include "GenericTab.h"

class CDynamicMonitorView : public CGenericRTPView<CDynamicMonitorView>
{
	class CTimeSpanData
	{
	public:
		const char* m_szDisplayText;
		DWORD		m_dwRefreshPeriod;
		long		m_dwMinutes;

		CTimeSpanData(const char* szDt, DWORD pd, long mn)
			: m_szDisplayText(szDt)
			, m_dwRefreshPeriod(pd)
			, m_dwMinutes(mn)
		{}
	};
public:
	BOOL PreTranslateMessage(MSG* pMsg)
	{
		return IsDialogMessage(pMsg);	
	}

	CDynamicMonitorView(const CTabsAccessor &rst);
	virtual ~CDynamicMonitorView();
	enum { IDD = IDD_MONITOR_FORM };

	BEGIN_MSG_MAP(CDynamicMonitorView)
		MESSAGE_HANDLER(WM_SIZE, OnSize)
		MESSAGE_HANDLER(WM_TIMER, OnTimer)
		COMMAND_ID_HANDLER(IDC_RANGE_COMBO, OnRangeSelChanged)
		CHAIN_MSG_MAP(baseGenericClass)
		DEFAULT_REFLECTION_HANDLER()
	END_MSG_MAP()
	
	void InitializeTab();
	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnRangeSelChanged(WORD, WORD, HWND, BOOL&);
protected:
	std::list<CTimeSpanData> m_spansList;
};
#endif // !defined(AFX_DYNAMICMONITORVIEW_H__B6B84F17_3843_453F_89A5_DDB34F00D251__INCLUDED_)
