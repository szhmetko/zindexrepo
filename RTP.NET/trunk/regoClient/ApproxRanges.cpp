// $Header: /rtpClient/ApproxRanges.cpp 4     13.04.08 17:15 Sergey Zhmetko $ 
// ApproxRanges.cpp: implementation of the CApproxRanges class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ApproxRanges.h"
#include "Config.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CRange::CRange(){}

CRange::~CRange(){}

bool CRange::ValInMe(long val)
{
	bool bTest = val >= m_u_from;
	bTest = val < m_u_to;
	return (val >= m_u_from) && (val < m_u_to);
}

void CRange::Fill(CApproxAccessor& apac, long Uto)
{
	m_offset = apac.m_offset;
	m_k = apac.m_k;
	m_u_from = apac.m_u_from;
	m_num = apac.m_range_num;
	m_u_to = Uto;
}

double CRange::Calculate(double preVal)
{
	preVal = m_offset + (preVal - m_u_from)*m_k;	//������� ������������
	return preVal;
}

CRangesGroup::CRangesGroup(){}

CRangesGroup::~CRangesGroup(){}

CRangesGroup::CRangesGroup(const CTPairsAccessor& tac ) : 
		CTPairsAccessor(tac)
{ 
	using namespace dbutils;
	CDBSession locSession;
	HResult hres;
	try
	{
		hres = locSession.Open();
		CCommand<CAccessor<CApproxAccessor> >	aprst;
		aprst.m_tpair_id = m_id;
		hres = aprst.Open( locSession, CConfig::GetValue("approx/select").c_str());

		long TempTo = -1;
		while( aprst.MoveNext() == S_OK )
		{
			switch(aprst.m_range_num)
			{
			case 1:
			case 2:
			case 3:
			case 4:
				m_arRanges[aprst.m_range_num-1].Fill(aprst, TempTo);
				TempTo = aprst.m_u_from;
				break;
			case 5:
				m_arRanges[4].Fill(aprst, 10000);
				TempTo = aprst.m_u_from;
				break;
			}
		}
		aprst.Close();
		locSession.Close();
	}
	catch(HresError he)
	{
		::MessageBox(0, he.what().c_str(), "", MB_OK);
	}

}

double CRangesGroup::Calculate(double preVal)
{
	for(int i = 0; i < 5; ++i)
	{
		if( m_arRanges[i].ValInMe(preVal) )
		{
			preVal = m_arRanges[i].Calculate(preVal);
			break;
		}
	}
	return preVal;
}
