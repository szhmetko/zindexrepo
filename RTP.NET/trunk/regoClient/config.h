// $Header: /rtpClient/config.h 5     11.05.08 11:47 Sergey Zhmetko $ 
// Config.h: interface for the CConfig class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CONFIG_H__B3FF1489_FAA0_44E3_9D8E_E5F12B558241__INCLUDED_)
#define AFX_CONFIG_H__B3FF1489_FAA0_44E3_9D8E_E5F12B558241__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CConfig  
{
public:
	CConfig();
	virtual ~CConfig();

/*	class CConfigAccessor
	{
		typedef CConfigAccessor thisClass;
	public:
		char	m_var_name[33];
		char	m_var_val[33];

	BEGIN_COLUMN_MAP(thisClass)
		COLUMN_ENTRY(1, m_var_name)
		COLUMN_ENTRY(2, m_var_val)
	END_COLUMN_MAP()

	void ClearRecord(){		memset(this, 0, sizeof(*this));	}
	DEFINE_COMMAND( thisClass, _T("select var_name, var_val from config") )
	};*/
	static std::string GetValue(LPCSTR);
	static std::string GetValue(std::string);
	static std::string GetXMLContent();
	static std::string m_Xml;
	static std::string GetWD();
	static std::string GetValue(const char*, CSession&);
	static std::string GetValue(std::string, CSession&);
	static std::string GetDBValue(std::string);
};

#endif // !defined(AFX_CONFIG_H__B3FF1489_FAA0_44E3_9D8E_E5F12B558241__INCLUDED_)
