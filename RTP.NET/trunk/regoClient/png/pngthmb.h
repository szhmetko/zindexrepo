
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the PNGTHMB_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// PNGTHMB_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef PNGTHMB_EXPORTS
#define PNGTHMB_API __declspec(dllexport)
#else
#define PNGTHMB_API __declspec(dllimport)
#endif

  #pragma warning(disable: 4275)
  #pragma warning(disable: 4251)
// This class is exported from the pngthmb.dll
class PNGTHMB_API CPngthmb {
public:
	CPngthmb(void);
	// TODO: add your methods here.
};

class PNGTHMB_API CWebImageDC : public CDC  
{
public:
	CWebImageDC();
	virtual ~CWebImageDC();

	BOOL Create(CSize &pImageSize);

	BOOL SaveAsPNG(CString &pFileName,
		BOOL pInterlaced = TRUE,
		BOOL pTransparent = FALSE,
		COLORREF pTransparentColor = RGB(255,255,255));
/*
	BOOL StreamAsPNG(CMemFile * pMemFile,
		BOOL pInterlaced = TRUE,
		BOOL pTransparent = FALSE,
		COLORREF pTransparentColor = RGB(255,255,255));
*/
	BOOL BaseSaveAsPNG(FILE* fPng,//CFile &pFile,
		BOOL pInterlaced,
		BOOL pTransparent,
		COLORREF pTransparentColor);

	void Erase(COLORREF pColor);
	CSize cImageSize;
	CBitmap cBitmap;
	CBitmap cOldBitmap;
};
extern PNGTHMB_API int nPngthmb;

PNGTHMB_API int fnPngthmb(void);

