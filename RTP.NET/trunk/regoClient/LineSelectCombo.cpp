// $Header: /rtpClient/LineSelectCombo.cpp 3     13.04.08 17:22 Sergey Zhmetko $ 
// LineSelectCombo.cpp: implementation of the CLineSelectCombo class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include "LineSelectCombo.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLineSelectCombo::CLineSelectCombo(int iDefault)
		: m_iDefault(iDefault)
{}

void	CLineSelectCombo::Populate()
{
	int iLines = init();
	for(int i = 0; i < m_linesVector.size(); ++i )
	{
		int iInserted = AddString(m_linesVector[i].m_strTitle.c_str());
		if( iInserted >= 0 )
			SetItemData(iInserted, i);
	}
	SetCurSel(m_iDefault);
}

int	CLineSelectCombo::init()
{
	char full[_MAX_PATH] = {0};
	char drive[_MAX_DRIVE] = {0};
	char dir[_MAX_DIR] = {0};
	char fname[_MAX_FNAME] = {0};
	char ext[_MAX_EXT] = {0};
	char datPath[_MAX_PATH + _MAX_FNAME] = {0};

	::GetModuleFileName(0, full, _MAX_PATH);
	_splitpath( full, drive, dir, fname, ext );
	strcpy(datPath,  drive);
	strcat(datPath, dir);
	strcat(datPath, LINES_FILENAME);

	FILE* fLines = 0;
	fLines = fopen(datPath, "r");
	if( fLines )
	{
		char	buffer[1024] = {0};
		char seps[8] = {0};
		seps[0] = LINEDATA_DELIMITER;
		seps[1] = '\n';

		while( fgets( buffer, sizeof(buffer), fLines ) != NULL )
		{
			SLineSettings CurLine;
			int idx = 0;
			char *token = strtok( buffer, seps );
			CurLine.m_strTitle = token;
			while( token != NULL )
			{
				token = strtok( NULL, seps );
				if(token == NULL)	break;
				switch(idx)
				{
				case 0:
					{
						std::string strType = token;
						CurLine.m_bReadOnly = (strType != "local");
						CurLine.m_strPassword = (strType == "local")?"4YYDJ":"3FF3T";
						CurLine.m_strUserName = (strType == "local")?"client":"roclient";
						break;
					}
				case 1:
					CurLine.m_strHostName = token;
					break;
				case 2:
					CurLine.m_strDB = /*::GetWD() + "\\" + */std::string(token);
					break;
				case 3:
					CurLine.m_strServiceShortName = token;
					break;
				}
				idx++;
			}
			m_linesVector.push_back(CurLine);
		}
		fclose(fLines);
	}
	else
	{
		::MessageBox(::GetParent(m_hWnd), "���� �������� ����� �� ������!", "", MB_OK);
		return -1;
	}
	return m_linesVector.size();
}


LRESULT CLineSelectDlg::OnInitDialog(UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	DoDataExchange(false);
	m_combo.Populate();
	CenterWindow(GetParent());
	return TRUE;
}


LRESULT CLineSelectDlg::OnOk(WORD, WORD wID, HWND, BOOL& bHandled)
{
	
	int iItem = m_combo.GetCurSel();
	if( iItem != CB_ERR )
	{
		iItem = m_combo.GetItemData( iItem );
		if( iItem != CB_ERR )
		{
			g_CurLine = m_combo.m_linesVector[iItem];
			g_CurLine.bIsVitalSetBlocked = true;
		}
	}
	EndDialog(wID);
	return 0;
}
LRESULT CLineSelectDlg::OnCloseCmd(WORD, WORD wID, HWND, BOOL& bHandled)
{
	EndDialog(wID);
	return 0;
}

