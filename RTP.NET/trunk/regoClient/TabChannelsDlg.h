// $Header: /rtpClient/TabChannelsDlg.h 3     13.04.08 17:22 Sergey Zhmetko $ 
// TabChannelsDlg.h: interface for the CTabChannelsDlg class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TABCHANNELSDLG_H__6EBF55AF_8C15_4D9F_9DB7_24E0D6BB1AA0__INCLUDED_)
#define AFX_TABCHANNELSDLG_H__6EBF55AF_8C15_4D9F_9DB7_24E0D6BB1AA0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CColorButton : public CWindowImpl<CColorButton, CButton>,
                      public COwnerDraw<CColorButton>
{
public:

	COLORREF m_clr;
    BEGIN_MSG_MAP(CColorButton)
        CHAIN_MSG_MAP_ALT(COwnerDraw<CColorButton>, 1)
        DEFAULT_REFLECTION_HANDLER()
    END_MSG_MAP()
 
	void DrawItem(LPDRAWITEMSTRUCT);
};

class CTabChannelsDlg : public CDialogImpl<CTabChannelsDlg>
						, public CWinDataExchange<CTabChannelsDlg>
{
public:
	enum { IDD = IDD_DTABCHANNELINFO };

	CTabsChannelsAccessor m_tchlInfo;
	CColorButton	m_btnBgClr;

	CTabChannelsDlg(CTabsChannelsAccessor* pItem = 0);

	BEGIN_DDX_MAP(CTabChannelsDlg)
		DDX_TEXT_LEN(IDC_ELEFTCAPTION, m_tchlInfo.m_primary_units, sizeof(m_tchlInfo.m_primary_units) - 1 )
		DDX_TEXT_LEN(IDC_ERIGHTCAPTION, m_tchlInfo.m_sec_units, sizeof(m_tchlInfo.m_sec_units) - 1 )
		DDX_TEXT_LEN(IDC_EMAINTITLE, m_tchlInfo.m_main_title, sizeof(m_tchlInfo.m_main_title) - 1 )
		DDX_TEXT_LEN(IDC_CTABCHLALIASES, m_tchlInfo.m_alias, sizeof(m_tchlInfo.m_alias) - 1 )
		DDX_FLOAT_RANGE(IDC_EPRIM_HIGH_LIM, m_tchlInfo.m_primary_high_lim_id, 0, 10000)
		DDX_FLOAT_RANGE(IDC_EPRIM_LOW_LIM, m_tchlInfo.m_primary_low_lim_id, 0, 10000)
		DDX_FLOAT_RANGE(IDC_ESEC_HIGH_LIM, m_tchlInfo.m_sec_high_lim_id, 0, 10000)
		DDX_FLOAT_RANGE(IDC_ESEC_LOW_LIM, m_tchlInfo.m_sec_low_lim_id, 0, 10000)
		DDX_INT(IDC_ECURVESNUM, m_tchlInfo.m_curves_num)
	END_DDX_MAP()

	BEGIN_MSG_MAP(CTabChannelsDlg)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		COMMAND_ID_HANDLER(IDOK, OnOk)
		COMMAND_ID_HANDLER(IDCANCEL, OnCloseCmd)
		COMMAND_ID_HANDLER(IDC_BCHNGBGCOLOR, OnSetBgClr)
		REFLECT_NOTIFICATIONS()
	END_MSG_MAP()

	LRESULT OnSetBgClr(WORD , WORD wID, HWND , BOOL& );
	
	LRESULT OnOk(WORD, WORD wID, HWND, BOOL& );
	LRESULT OnInitDialog(UINT, WPARAM, LPARAM, BOOL& bHandled);
	LRESULT OnCloseCmd(WORD, WORD wID, HWND, BOOL& bHandled);
private:
	//routines
	void _FillAliases();

	//attributes
	bool m_bEditMode;
};

class CCurveInfoDlg : public CDialogImpl<CCurveInfoDlg>
{
public:
	enum { IDD = IDD_DADDCURVE };
	CCurvesAccessor m_curve;

	CCurveInfoDlg();

	BEGIN_MSG_MAP(CCurveInfoDlg)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		COMMAND_ID_HANDLER(IDOK, OnOk)
		COMMAND_ID_HANDLER(IDCANCEL, OnCloseCmd)
		DEFAULT_REFLECTION_HANDLER()	
	END_MSG_MAP()

	LRESULT OnOk(WORD , WORD wID, HWND , BOOL& );
	LRESULT OnInitDialog(UINT, WPARAM, LPARAM, BOOL& bHandled);
	LRESULT OnCloseCmd(WORD, WORD wID, HWND, BOOL& bHandled);
private:
	//routines
	void _popList();
	//attributes
};

#endif // !defined(AFX_TABCHANNELSDLG_H__6EBF55AF_8C15_4D9F_9DB7_24E0D6BB1AA0__INCLUDED_)
