// $Header: /rtpClient/DataContainer.h 6     2.05.08 0:29 Sergey Zhmetko $ 
// DataContainer.h: interface for the CDataContainer class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DATACONTAINERM_H__4FFC9BFF_3F1A_4185_899A_8345CAF78445__INCLUDED_)
#define AFX_DATACONTAINERM_H__4FFC9BFF_3F1A_4185_899A_8345CAF78445__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

extern THERMOPAIRS_MAP	g_tPairsMap;
extern CHANNELS_MAP g_ChannelsMap;

class CDataContainer : public std::map<std::string, double* >
{
	typedef  std::map<std::string, long> FNAME2TPAIRID_MAP;
	typedef  FNAME2TPAIRID_MAP::iterator FNAME2TPAIRID_ITER;

	typedef  std::map<std::string, double> COEFF_MAP;
	typedef  COEFF_MAP::iterator CoeffMapIterator;

	typedef /*std::vector<double>*/double* DataVector;
	typedef std::map<std::string, DataVector> DataMap;
	typedef DataMap::iterator	DataMapIter;
	typedef std::pair<std::string, DataVector> KeyValuePair;

public:
	CDataContainer();
	virtual ~CDataContainer();

	long PopulateData(const char* = "ValsByMeterRow", const char* = ""
		, const char* = " order by rec_id asc", bool = false);
	double LastVal(std::string);

	double FirstVal(std::string);
	long GetNumRows();
	void clear();	//override
	long ExportToCSV(LPCSTR);
private:
	double _throwApproxFilter(double, std::string, double);
	double _bind(CCommand<CDynamicAccessor>&, ULONG);
	int _fillCoeffMap();
	double _lookupCoeffByFname(std::string);
	long _lookupTPairIDByFname(const std::string&);
private:
	LPBYTE m_pRawData;
	long m_NumRows;
	std::string				m_strTemperBasePoint;
	COEFF_MAP				m_cmap;
	FNAME2TPAIRID_MAP		m_fldTPairsMap;

};
#endif // !defined(AFX_DATACONTAINERM_H__4FFC9BFF_3F1A_4185_899A_8345CAF78445__INCLUDED_)
