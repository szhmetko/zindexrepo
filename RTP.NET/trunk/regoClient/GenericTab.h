// $Header: /rtpClient/GenericTab.h 9     1.05.08 10:56 Sergey Zhmetko $ 
// GenericTab.h: interface for the CGenericTab class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GENERICTAB_H__3C652F92_8425_4A08_A6EE_2794E9EDD9C3__INCLUDED_)
#define AFX_GENERICTAB_H__3C652F92_8425_4A08_A6EE_2794E9EDD9C3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ApproxRanges.h"
#include "DataContainer.h"
#include "LegendWnd.h"
#include "Oscilloscope.h"


class CGenericTabChannel : public CTabsChannelsAccessor
{
public:
	CGenericTabChannel(const CTabsChannelsAccessor& tcac);

	virtual ~CGenericTabChannel();

	std::vector<SCurve> m_ChNumVector;		//contains channel's m_num 
protected:
//routines
	HRESULT _loadChilds(int tabChl_id);
	
};
class CGenericTab : public CTabsAccessor
{
public:
	CGenericTab(const CTabsAccessor&, bool bZoomEnabled = true, bool bSilent = false);

	virtual ~CGenericTab();

protected:
//routines
	HRESULT _loadChilds(int Tab_id);
	//����� ������� ������� ��� ������ ��� ������� � ������������� SQL �������
public:
	long ShowChart(LPCSTR, LPCSTR = "order by rec_id asc");
	OscilloscopeCtrl	m_plot;
	TAB_CHANNELS_MAP	m_chlsInUseMap;		
	std::vector<CGenericTabChannel> m_TabChlsVector;
	CDataContainer	m_data;
	bool m_bZoomEnabled;
protected:
	bool m_bByTime;
	bool m_bSilent;
public:
	void ChangeZoom(OscZoomMode zm, bool bEnabled);
	void ResultSaveAs(const char* szFileName, int nType);
	void PlotPrint();
	void UpdateLegend(CLegendWindow&);
private:
	bool GetFrameRect(HENHMETAFILE hEmf, LPRECT lpRect);
};

template <class T>class CGenericRTPView : 
			  public CGenericTab
			, public CDialogImpl<T>
//			, public CMessageMap
//			, public CDialogResize<T>
{
public:
	typedef CGenericRTPView<T> baseGenericClass;
	BEGIN_MSG_MAP(CGenericRTPView)
//		MESSAGE_HANDLER(WM_CHANGE_ZOOM_MODE, OnChangeZoom)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		MESSAGE_HANDLER(WM_SHOWWINDOW, OnShowWindow)
		MESSAGE_HANDLER(WM_SAVE_RESULT_AS, OnResultSaveAs)
		MESSAGE_HANDLER(WM_PLOTS_PRINT, OnPlotsPrint)
	END_MSG_MAP()

	CGenericRTPView<T>(const CTabsAccessor& tac, bool bZoomEnabled = true, bool bSilent = false)
			: CGenericTab(tac, bZoomEnabled, bSilent)
	{}

	LRESULT OnShowWindow(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
//		if(wParam && !lParam)
//			ATLTRACE("OnShowWindow");
		return 0;
	}
	/*
	virtual LRESULT OnChangeZoom(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		T* pT = static_cast<T*>(this);
		pT->ChangeZoom(true);
		return 0;
	}*/

	virtual LRESULT OnResultSaveAs(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		T* pT = static_cast<T*>(this);
		pT->ResultSaveAs("", 0);
		return 0;
	}

	virtual LRESULT OnPlotsPrint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		T* pT = static_cast<T*>(this);
		pT->PlotPrint();
		return 0;
	}

	
	virtual LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		T* pT = static_cast<T*>(this);
		pT->InitializeTab();
		return 0;
	}	
	
	void InitializeTab()
	{
		T* pT = static_cast<T*>(this);
		RECT rec = {0, 0, 0, 0};
		CStatic stWnd = (CStatic)pT->GetDlgItem(IDC_PLOT);
/*
		for(size_t i = 0; i < OSC_MAX_CHANNELS; i++)
			m_values[i] = NULL;
		*/
		
		m_plot.SubclassWindow(stWnd, m_TabChlsVector.size());
		stWnd.SetWindowText(m_caption);
		m_plot.PointStyle(None);
		m_plot.RightYAxis(true);

		if(m_bByTime)
			m_plot.GetBottomXAxis()->SetTimeAxis();


		for(int i = 0; i < m_TabChlsVector.size(); i++)
		{
			m_plot.GetChannel(i)->SetBgColor(m_TabChlsVector[i].m_bgcolor);
			m_plot.GetChannel(i)->SetTitleText(m_TabChlsVector[i].m_main_title);
			m_plot.GetChannel(i)->SetYAxisesText(
				m_TabChlsVector[i].m_primary_units, 
				m_TabChlsVector[i].m_sec_units);
		}
		m_plot.EnableZoom(false);
	}

};
#endif // !defined(AFX_GENERICTAB_H__3C652F92_8425_4A08_A6EE_2794E9EDD9C3__INCLUDED_)
