// $Header: /rtpClient/DataContainer.cpp 7     1.05.08 11:14 Sergey Zhmetko $ 
// DataContainerM.cpp: implementation of the CDataContainer class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ApproxRanges.h"
#include "Config.h"
#include "DataContainer.h"
#include <time.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
using namespace dbutils;

CDataContainer::CDataContainer()
{
//	int ret = _setExcludes();
	m_pRawData = 0;
	int ret = _fillCoeffMap();
	dbutils::CDBSession locSession;
	
	HResult hres = S_OK;
	try
	{
		hres = locSession.Open();
		m_strTemperBasePoint = CConfig::GetValue("���. ����", locSession);
	}
	catch(HresError exc){}
	locSession.Close();
}

CDataContainer::~CDataContainer()
{
	clear();
}

double CDataContainer::_throwApproxFilter(double preVal, std::string strFname, double TemperBase)
{
	long tPairID = -1;

	tPairID = _lookupTPairIDByFname(strFname);
	if( tPairID <= 0L )
		return preVal;
	THERMOPAIRS_ITER tp_iter = g_tPairsMap.find(tPairID);
	if( tp_iter != g_tPairsMap.end() )
	{
		CRangesGroup& rg = tp_iter->second;
		preVal = TemperBase + rg.Calculate(preVal);
	}
	return preVal;
}

double CDataContainer::_bind(CCommand< CDynamicAccessor >& rst, ULONG uCol)
{
	DBTYPE dType; 
	rst.GetColumnType(uCol, &dType);

	switch(dType)
	{
	case DBTYPE_I2:	//channels data
		 return double(*(SHORT*)rst.GetValue(uCol));
	case DBTYPE_I4:	//meter and (rec_i, piece_id)
		 return double(*(INT*)rst.GetValue(uCol));
	case DBTYPE_DBTIMESTAMP:	//dtm_upd
		return dbutils::CDBSession::DBTimeStampToDate(*(DBTIMESTAMP*)rst.GetValue(uCol));
	case DBTYPE_R8:	// vlin
	case DBTYPE_DATE:	//date
		 return double(*(double*)rst.GetValue(uCol));
		break;
	}
	return 0.0;
}
	
int CDataContainer::_fillCoeffMap()
{
	m_cmap.clear();
	CHMAP_ITER iter;

	m_fldTPairsMap.clear();
	FNAME2TPAIRID_ITER tpIter;

	int iRow = 0;
	for(iter = g_ChannelsMap.begin(); iter != g_ChannelsMap.end(); iter++)
	{
		CChannelSettingsAccessor& cac = iter->second;
		m_cmap[cac.m_fname] = cac.m_k;
		m_fldTPairsMap[cac.m_fname] = cac.m_tPairID;
	}
	
	return m_cmap.size();
}

double CDataContainer::_lookupCoeffByFname(std::string strFname)
{
	CoeffMapIterator iter = m_cmap.find(strFname);
	if( iter != m_cmap.end() )
		return iter->second;
	else
		return 1.0;
}

long CDataContainer::_lookupTPairIDByFname(const std::string& strFname)
{
	FNAME2TPAIRID_ITER iter = m_fldTPairsMap.find(strFname);
	if( iter != m_fldTPairsMap.end() )
		return iter->second;
	else
		return 0L;
}
long CDataContainer::PopulateData(const char* szTable, const char* szPartSQL
								  , const char* szSort, bool bDtDoubt)
{
	CWaitCursor wc;
	USES_CONVERSION;
	clear();
	CDBSession locSession;
	HResult hres = S_OK;
	try
	{
		using namespace std;
		hres = locSession.Open();

		
		m_NumRows = locSession.getCount( string(szTable) + " " + string(szPartSQL) );
		if( m_NumRows <= 0 )	return m_NumRows;

		char szSelectHead[128] = {0};
		sprintf(szSelectHead, "select top %ld * from ", m_NumRows);

		string strSQL = szSelectHead + string(szTable) + " " + szPartSQL + " " + string(szSort);
		
		
		ATLTRACE(strSQL.c_str());

		CCommand< CDynamicAccessor > drst;

		hres = drst.Open(locSession, strSQL.c_str());

		vector<double> arK;	// for provide coeffs
		vector<string> arFields;
		
		
		for (long idx = 0; drst.MoveNext() == S_OK && idx < m_NumRows; ++idx)
		{
			
			if(idx == 0)
			{
				m_pRawData = new BYTE[drst.GetColumnCount()*m_NumRows*sizeof(double)];
				arFields.resize(drst.GetColumnCount());
				arK.resize(drst.GetColumnCount());
			}
			for(ULONG col = 0; col < drst.GetColumnCount(); ++col)
			{
				//std::string strColName = OLE2T(drst.GetColumnName(col + 1));
				if(idx == 0)	//first row
				{
					const char* szColName = OLE2T(drst.GetColumnName(col + 1));
					arFields[col] = szColName;
					//DataVector tmpVector(lRows);
					KeyValuePair kvp;
					arK[col] = _lookupCoeffByFname(szColName);
					kvp.second = new ( &m_pRawData[0] + col * m_NumRows*sizeof(double) )double[m_NumRows];//tmpVector;
					kvp.first = szColName;
					insert(kvp);
				}
				DataMapIter iter = find(arFields[col]);
				if(iter != end())
				{
					double val = _bind(drst, col + 1);
					
					//���.����
					double TemperBase = FirstVal(m_strTemperBasePoint);

					double k = (col < arK.size()) ? arK[col] : 1.0;
					val = _throwApproxFilter(val*k, arFields[col], TemperBase);
					//iter->second.at(idx) = val;
					iter->second[idx] = val;

				}
			}
		}
		return m_NumRows;
	}
	catch(HresError exc)
	{
		ATLTRACE(exc.what().c_str());
		clear();
	}
	return -1;
}

double CDataContainer::LastVal(std::string strKey)
{
	DataMapIter iter = find(strKey);
	if(iter != end() && m_NumRows > 0)
		return iter->second[m_NumRows - 1];
	return 0.0;
}
double CDataContainer::FirstVal(std::string strKey)
{
	DataMapIter iter = find(strKey);
	if(iter != end() && m_NumRows > 0)
		return iter->second[0];
	return 0.0;
}
void CDataContainer::clear()
{/*
	DataMap::const_iterator iter = begin();
	while(iter != end())
	{
		delete [] iter->second;
		iter++;
	}*/
	if(m_pRawData)
	{
		delete [] m_pRawData;
		m_pRawData = 0;
	}
	DataMap::clear();
}

long CDataContainer::GetNumRows(){return m_NumRows;}

long CDataContainer::ExportToCSV(LPCSTR szFileName)
{
	using namespace std;
	long len = 0;
	CWaitCursor wc;

	map<string, string> Fld2Desc;//����=>�������� ������
	map<string, string>::iterator ssiter;
	CHMAP_ITER iter;
	for(iter = g_ChannelsMap.begin(); iter != g_ChannelsMap.end(); iter++)
	{
		CChannelSettingsAccessor& cac = iter->second;
		Fld2Desc[cac.m_fname] = cac.m_descr;
	}
	Fld2Desc["dtm_upd"] = _T("�����");

	FILE* fLog = 0;
	string strFileName = string(szFileName) + ".csv";
	if( (fLog = fopen( strFileName.c_str(), "w")) != NULL )
	{
		for( const_iterator miter = begin(); miter != end(); miter++ )//������ �����
		{
			ssiter = Fld2Desc.find(miter->first);
			if( ssiter != Fld2Desc.end() )
			{
				fprintf(fLog,"%s%c", ssiter->second.c_str(), DATA_DELIMITER);
			}
			
		}
		fputs("\n", fLog);

		for(long i = 0; i < m_NumRows; ++i )
		{

			for( const_iterator miter = begin(); miter != end(); miter++ )
			{
				ssiter = Fld2Desc.find(miter->first);
				if( ssiter == Fld2Desc.end() )
					continue;
				double* Heap = miter->second;
				if(Heap)
				{
					if( miter->first == "dtm_upd" )
					{
						char szOutput[32] = {'\0'};
						time_t Time_i = 
							dbutils::CDBSession::VariantTimeToUnixTime(Heap[i]);
						struct tm* tm_start = localtime( &Time_i );
						strftime(szOutput, sizeof(szOutput), "%H:%M:%S", tm_start);
						fprintf(fLog,"%s%c", szOutput, DATA_DELIMITER);
					}
					else if( miter->first == "meter" )
						fprintf(fLog,"%ld%c", long(Heap[i]), DATA_DELIMITER);
					else
						fprintf(fLog,"%.2f%c", Heap[i], DATA_DELIMITER);
						

				}

			}
			fputs("\n", fLog);
		}
		len = ftell(fLog);
		fclose(fLog);
	}
	else 
	{
		::MessageBox(0, _T("������ ������ �����"), _T("������!"), 
			MB_OK|MB_ICONEXCLAMATION);
		return -1;
	}
	return len;
}
