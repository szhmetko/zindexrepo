// LineSelectCombo.h: interface for the CLineSelectCombo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LINESELECTCOMBO_H__156EF832_8F32_456D_B57B_D1338FB61005__INCLUDED_)
#define AFX_LINESELECTCOMBO_H__156EF832_8F32_456D_B57B_D1338FB61005__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define		LINES_FILENAME	"lines.dat"
#define		LINEDATA_DELIMITER	'\t'

extern	SLineSettings	g_CurLine;
class CLineSelectCombo : public CWindowImpl<CLineSelectCombo, CComboBox>
 {
public:
	CLineSelectCombo(int iDefault = 0);
	void	Populate();

	BEGIN_MSG_MAP(CLineSelectCombo)
        DEFAULT_REFLECTION_HANDLER()
    END_MSG_MAP()

	std::vector<SLineSettings>	m_linesVector;
private:
	int	init();
	//attributes
	int	m_iDefault;
};

class CLineSelectDlg : public CDialogImpl<CLineSelectDlg>
	, public CWinDataExchange<CLineSelectDlg>
{
public:
	enum { IDD = IDD_LINESELECTDLG };

	BEGIN_MSG_MAP(CLineSelectDlg)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		COMMAND_ID_HANDLER(IDOK, OnOk)
		COMMAND_ID_HANDLER(IDCANCEL, OnCloseCmd)
        REFLECT_NOTIFICATIONS()
	END_MSG_MAP()

    BEGIN_DDX_MAP(CLineSelectDlg)
        DDX_CONTROL(IDC_LINESELECTCOMBO, m_combo)
    END_DDX_MAP()
	LRESULT OnInitDialog(UINT, WPARAM, LPARAM, BOOL&);
	LRESULT OnOk(WORD, WORD wID, HWND, BOOL&);
	LRESULT OnCloseCmd(WORD, WORD wID, HWND, BOOL&);

	CLineSelectCombo	m_combo;
};


#endif // !defined(AFX_LINESELECTCOMBO_H__156EF832_8F32_456D_B57B_D1338FB61005__INCLUDED_)
