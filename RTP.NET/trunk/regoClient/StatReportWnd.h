// $Header: /rtpClient/StatReportWnd.h 2     13.04.08 21:17 Sergey Zhmetko $ 
// statReportWindow.h : interface of the CStatReportWnd class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_WTLBROWSERVIEW_H__27BEDF70_F34D_4698_A226_534A3D32E98B__INCLUDED_)
#define AFX_WTLBROWSERVIEW_H__27BEDF70_F34D_4698_A226_534A3D32E98B__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "resource.h"
#include "webro.h"
#include "png\\pngthmb.h"

#define	REPORT_TEMPLATEPATH			"reports\\statReport.htm"
#define	PPREPORT_TEMPLATEPATH		"reports\\statPPReport.htm"
#define	CSSPATH						"reports\\strep.css"
#define	REPORTSDIR					"reports"
#define	LEFTDELIMITER				"{*"
#define	RIGHTDELIMITER				"*}"
#define	PI			atan(1.0)

extern CHANNELS_MAP	g_ChannelsMap;


class CPng : public CWebImageDC
{
#define		HISTOGRAM_STEPS		12
public:
	class CDRange
	{
	public:
		CDRange();

		void SetRange(double low, double high);
		bool TestVal(double val);
	public:
		double	Low;
		double	High;
		long	occurred;
	};

	CPng();
	~CPng();

	const CPng& operator<<(double val);
	void Limits(double low, double high);
	double CalculateK();

	BOOL Polyline(std::vector <POINT> arPts);
	void DrawPng(LPCSTR szPath = 0);

private:
	UINT	m_uPicWidth;
	UINT	m_uPicHeight;
	double	m_dLowLimit;
	double	m_dHighLimit;

	int		m_maxIndex;
	int		m_Total;
	CDRange	m_Ranges[HISTOGRAM_STEPS];
};

class CStatReportWnd : public CWindowImpl<CStatReportWnd, CAxWindow>, public CWebBrowser2<CStatReportWnd>
{
public:
	struct SVALUES
	{
		double		dDiameter;			//��������� ���������� ���������� ��������	out/in
		double		MaterialDensity;	//��������� ���������	out/in
		double		DiamOuterRefer;		//�������� �����������	out/in
		long		VlinRefer;			//������������� ��������	out/in

		std::string Model;				//����� ������		out/in
		std::string op_name;			//��������		out/in

		SVALUES(){	memset(this, 0, sizeof(*this));	}
	}m_values;

	class CPieceSearcher
	{
	public:
		CPieceSearcher(CDataContainer&);

		void Max();
		void Manual(long from, long to) ;
		void Full();
		void AddValue(long meter, double val);
		long length();
		long first();
		long last();

		DATE DTfrom();
		DATE DTto();
	private:
		void reset();
		long	meterLastLabel;
		double* m_arVlin;
		double* m_arMeters;
		DATE*	m_arDTs;
		CDataContainer& m_Data;
		long	meterStart;
		long	meterEnd;
	};
	CStatReportWnd();
	
	BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_WND_SUPERCLASS(NULL, CAxWindow::GetWndClassName())


	BEGIN_MSG_MAP(CStatReportWnd)
		MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
		CHAIN_MSG_MAP(CWebBrowser2<CStatReportWnd>)
	END_MSG_MAP()

	LRESULT OnDestroy(UINT, WPARAM, LPARAM, BOOL& bHandled);

	bool	Create(HWND hParent, CDataContainer* pData);

	BOOL OnBeforeNavigate2(IDispatch* pDisp, const String& szURL, DWORD dwFlags, const String& szTargetFrameName, CSimpleArray<BYTE>& pPostedData, const String& szHeaders);

	void	ParsePageData(CString&);

	void	SetValue(LPCSTR key, LPCSTR val);
	void	SetValue(LPCSTR key, int val);
	void	SetValue(LPCSTR key, double val, int precision = 2);

	long LardPPTemplate(std::map<long, long> &rPiecesMap);

	CString	GetPieceInfo(long lid, dbutils::CDBSession& ses);
	
	long LardTemplate(bool bDefaultReference = true);	//�������� ���������� ����� �� �������� �����. ����������
	void SetDiameterInfo(int iEtal, int iMeas, int iProb);	//set channels numbers only
private:
	void	_DefaultReference()
	{}
	long	_BuildPage(CString strPath);
	void	_PrepareValues(bool bDefaultReference = true);

	bool	SetReferenceInfo();

	//	Attributes	
	CString	m_strPageTmpPath;
	std::string	m_strProbFName;
	CDataContainer* m_pData;
	std::map <std::string, std::string/*value*/>	m_valsMap;	//key is placeholder like VlinAvg, i.e. {*VlinAvg*}

//	long m_mFrom, m_mTo;
	double* m_pEtal;
	double* m_pMeas;
	double* m_pProb;
	CPng	m_png;
public:
	CPieceSearcher	*m_pCurPiece;
	long			m_lFetchedPieceID;
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WTLBROWSERVIEW_H__27BEDF70_F34D_4698_A226_534A3D32E98B__INCLUDED_)
