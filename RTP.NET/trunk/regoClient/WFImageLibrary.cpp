// $Header: /rtpClient/WFImageLibrary.cpp 3     13.05.08 4:04 Sergey Zhmetko $ 
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "config.h"
#include "resource.h"
#include "WFImageLibrary.h"

extern CHANNELS_MAP	g_ChannelsMap;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CWFImageCache::CWFImageCache()
{
	using namespace std;
	using namespace dbutils;
	using namespace Gdiplus;

	CCommand<CAccessor<CImagesAccessor> >	rs;

	try
	{
		CRect rcDesired = GetDesiredPictureSize();
		CDBSession locSession;
		HResult hr = locSession.Open();
		hr = rs.Open(locSession, CConfig::GetValue("images/select").c_str());
		rs.ClearRecord();
		while( S_OK == rs.MoveNext() )
		{
			if ( DBSTATUS_S_OK != rs.m_BLOBDATA_STATUS )
				throw	HresError(hr);

			IStream* pIStream = NULL;
			HBITMAP hBitmap = 0;

			LPVOID pImage = ::CoTaskMemAlloc(rs.m_BLOBDATA_LENGTH);
			if (rs.m_data->Read(pImage, rs.m_BLOBDATA_LENGTH, NULL) == S_OK)
			{
				::CreateStreamOnHGlobal(pImage, TRUE, &pIStream);
				Bitmap* pBmp = Bitmap::FromStream(pIStream, TRUE);

				Color clrBkground;
				Status s = pBmp->GetHBITMAP(clrBkground, &hBitmap);
				if(hBitmap)
				{
					CBitmapHandle resizedBMP = (HBITMAP)::CopyImage(hBitmap, IMAGE_BITMAP
						, rcDesired.Width(), rcDesired.Height(), LR_COPYRETURNORG);
					value_type pr(rs.m_id, resizedBMP);
					insert(pr);
					::DeleteObject(hBitmap);
					
				}
				delete pBmp;
			}
			::CoTaskMemFree(pImage);
		}
	}
	catch(HresError he)
	{
		::MessageBox(0, he.what().c_str(), "", MB_OK);
	}
}

CWFImageCache::clear()
{
	for(iterator iter = begin(); iter != end(); ++iter)
	{
		CBitmapHandle bmp = iter->second;
		bmp.DeleteObject();
	}
	baseContainer::clear();
}

CWFImageCache::~CWFImageCache()
{
	clear();
}

CRect CWFImageCache::GetDesiredPictureSize()
{
	CRect rcDesired(0, 0, 100, 100);
	float fRatio;				//��������� ������ ������/������
	std::string strValue = CConfig::GetDBValue("img_ratio");
	fRatio = max( 1.0f, atof(strValue.c_str()) );

	HWND hWnd = ::GetTopWindow(0);
	if(::IsWindow(hWnd))
	{
		CPaintDC dc(hWnd);
		int scrw = dc.GetDeviceCaps(HORZRES);
		int scrh = dc.GetDeviceCaps(VERTRES);
		rcDesired.right = scrw / 13;
		rcDesired.bottom = rcDesired.Width()*fRatio;
	}
	return rcDesired;
}

CImageLibDlg::CImageLibDlg(CWFImageCache& imgs) : m_images(imgs)
{}


LRESULT CImageLibDlg::OnInitDialog(UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	CRect rcDesired = m_images.GetDesiredPictureSize();
	CRect rcSelItem;
	CStatic stSelItem = GetDlgItem(IDC_CURRENTITEM);
	stSelItem.GetWindowRect(&rcSelItem);
	rcSelItem.right = rcSelItem.left + rcDesired.Width();
	rcSelItem.bottom = rcSelItem.top + rcDesired.Height();
	stSelItem.MoveWindow(&rcSelItem);

	DlgResize_Init(false, false, WS_CLIPCHILDREN);
	m_imgList = GetDlgItem(IDC_IMAGELIST);

	if(m_ilAll.Create(rcDesired.Width(), rcDesired.Height(), ILC_COLOR32, 0, 10))
	{
		m_imgList.SetImageList(m_ilAll, LVSIL_NORMAL);

		CWFImageCache::const_iterator citer = m_images.begin();
		LVITEM item;
		while(citer != m_images.end())
		{
			int imgPos = m_ilAll.Add(citer->second, RGB(0, 0, 0));
			item.mask = LVIF_IMAGE | LVIF_PARAM | LVIF_TEXT;
			item.iItem = imgPos;
			item.iSubItem = 0;
			item.pszText = (LPTSTR)"ITEM";
			item.state = 0;
			item.stateMask = 0;
			item.iImage = imgPos;
			item.lParam = citer->first;

			//m_imgList.InsertItem(imgPos, 0, LVIF_IMAGE, NULL, imgPos, 0, 0, 0);
			m_imgList.InsertItem(&item);
			citer++;
		}
	}
	return 0;
}

LRESULT CImageLibDlg::OnCloseCmd(WORD, WORD wID, HWND, BOOL& bHandled)
{
	EndDialog(wID);
	return 0;
}

LRESULT CImageLibDlg::OnOK(WORD, WORD wID, HWND, BOOL& bHandled)
{
	int i = m_imgList.GetNextItem( -1, LVNI_ALL | LVNI_SELECTED);
	while( i != -1 )
	{
		DWORD dwData = m_imgList.GetItemData(i);
		push_back(dwData);
		i = m_imgList.GetNextItem( i, LVNI_ALL | LVNI_SELECTED);
	}

	EndDialog(wID);
	return 0;
}

CChlsLibDlg::CChlsLibDlg(const std::list<CUtility>& rlist) : baseContainer(rlist)
				, m_locChannels(g_ChannelsMap)

{
	for(CHANNELS_MAP::iterator citer = m_locChannels.begin(); citer != m_locChannels.end(); ++citer)
	{
		CChannelSettingsAccessor& cac = citer->second;
		cac.bVisibleCol = false;
	}
	for(const_iterator iter = begin(); iter != end(); ++iter)
	{
		citer = m_locChannels.find( BYTE(iter->m_channelID) );
		if(citer != m_locChannels.end())
			citer->second.bVisibleCol = true;
	}
}

LRESULT CChlsLibDlg::OnInitDialog(UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	DlgResize_Init(false, false, WS_CLIPCHILDREN);
	m_leftList = GetDlgItem(IDC_CHL_NOTINUSE);
	ListView_SetExtendedListViewStyle(m_leftList, LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	m_clist.SubclassWindow(GetDlgItem(IDC_CHL_INUSE));
	ListView_SetExtendedListViewStyle(m_clist, LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	setColumns();
	FillLists();
	return 0;
}

LRESULT CChlsLibDlg::OnCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL&)
{
	using namespace std;
	WORD wID = LOWORD(wParam);
	int i = 0;
	switch(wID)
	{
	case IDC_BADD:
		{
			list<int> locSelected;
			i = m_leftList.GetNextItem( - 1, LVNI_ALL | LVNI_SELECTED);
			while( i != -1 )
			{
				locSelected.push_back(i);
				i = m_leftList.GetNextItem( i, LVNI_ALL | LVNI_SELECTED);
			}
			for(list<int>::reverse_iterator riter = locSelected.rbegin(); 
				riter != locSelected.rend(); ++riter)
				Add(*riter);
		}
		break;
	case IDC_BREMOVE:
		{
			list<int> locSelected;
			i = m_clist.GetNextItem( - 1, LVNI_ALL | LVNI_SELECTED);
			while( i != -1 )
			{
				locSelected.push_back(i);
				i = m_clist.GetNextItem( i, LVNI_ALL | LVNI_SELECTED);
			}
			for(list<int>::reverse_iterator riter = locSelected.rbegin(); 
				riter != locSelected.rend(); ++riter)
				Remove(*riter);
		}
		break;
	case IDC_BREMOVEALL:
		for(i = m_clist.GetItemCount() - 1; i >=0; --i )
		{
			Remove(i);
		}
		break;
	case IDC_BUP:
	case IDC_BDOWN:
		break;
	case IDOK:
		{
			CString txtCell;
			for(int row = 0; row < m_clist.GetItemCount(); ++row )
			{
				DWORD dwData = m_clist.GetItemData(row);
				if(dwData)
				{
					CUtility& cut = *(CUtility*)dwData;

					for(int col = 1; col < m_clist.GetHeader().GetItemCount(); ++col)
					{
						if(m_clist.GetItemText(row, col, txtCell) )
						{
							txtCell.Replace("#", "");
							txtCell.Replace("|", "");
							txtCell.Replace(":", "");
							txtCell.Replace(".", "");

							switch(col)
							{
							case 2:	//scale
								cut.m_scale = atoi( txtCell.GetBuffer(0) );
								break;
							case 1://prefix
								cut.m_strPrefix = txtCell;break;
							case 3:	//postfix
								cut.m_strPostfix = txtCell;break;
							}
						}
					}
				}
			}
		}
	case IDCANCEL:
		EndDialog(wID);
		break;
	}
	return 0;
}

void CChlsLibDlg::setColumns()
{
	using namespace dbutils;
	const int iCols = 4;
	LV_COLUMN		lvcolumn;
	CRect rect;

	m_leftList.GetClientRect( &rect );
	lvcolumn.mask = LVCF_FMT | LVCF_SUBITEM | LVCF_TEXT | LVCF_WIDTH | LVCF_ORDER;
	lvcolumn.fmt = LVCFMT_CENTER;
	lvcolumn.iSubItem = 0;
	lvcolumn.iOrder = 0;
	lvcolumn.pszText = _T("�����");
	lvcolumn.cx = rect.Width() * .9;
	m_leftList.InsertColumn(0, &lvcolumn); 
	
	TCHAR rgtsz[iCols][32] = {_T("��������"), _T("T����1"), _T("��������"), _T("�����2")};
	m_clist.GetClientRect( &rect );


	for(int i = 0; i < iCols; i++)  
	{
		lvcolumn.pszText = rgtsz[i];
		lvcolumn.iSubItem = i;
		lvcolumn.iOrder = i;
		lvcolumn.cx = rect.Width() / iCols - 1;
		switch(i)
		{
		case 0:
			lvcolumn.cx = .45 * rect.Width();
			m_clist.InsertColumn(i, &lvcolumn, CGridCtrl::CONTROLTYPE::EDIT_NONE); 
			break;
		case 1:
		case 2:
		case 3:
			lvcolumn.cx = .16 * rect.Width();
			m_clist.InsertColumn(i, &lvcolumn, CGridCtrl::CONTROLTYPE::EDIT_TEXT);
			break;
		}
	}
}

void CChlsLibDlg::FillLists()
{

	/// fill not in use listbox
	LV_ITEM			lvitem;
	int i;
	lvitem.mask = LVIF_TEXT | LVIF_PARAM;
	m_leftList.DeleteAllItems();
	i = 0;
	for(CHANNELS_MAP::iterator citer = m_locChannels.begin(); citer != m_locChannels.end(); ++citer)
	{
		const CChannelSettingsAccessor& cac = citer->second;
		if(!cac.bVisibleCol)
		{
			lvitem.iItem = i++;
			lvitem.iSubItem = 0;
			lvitem.lParam = (LPARAM)&cac;
			lvitem.pszText = LPSTR(cac.m_descr);
			m_leftList.InsertItem(&lvitem);
		}
	}


	m_clist.DeleteAllItems();

	i = 0;
	CString strCell;
	for(const_iterator iter = begin(); iter != end(); ++iter)
	{
		const CUtility& cut = *iter;
		CHANNELS_MAP::const_iterator citer = m_locChannels.find(BYTE(cut.m_channelID));
		if(citer != m_locChannels.end())
		{
			lvitem.mask = LVIF_TEXT | LVIF_PARAM;
			const CChannelSettingsAccessor& cac = citer->second;
			lvitem.iItem = i;
			lvitem.iSubItem = 0;
			lvitem.lParam = (LPARAM)&cut;

			lvitem.pszText = LPSTR(cac.m_descr);
			m_clist.InsertItem(&lvitem);


			lvitem.mask = LVIF_TEXT | 0;
			lvitem.pszText = LPSTR(cut.m_strPrefix.c_str());
			lvitem.iSubItem = 1;
			m_clist.SetItem(&lvitem);

			lvitem.iSubItem = 2;
			strCell.Format("%d", cut.m_scale);
			lvitem.pszText = strCell.GetBuffer(0);
			m_clist.SetItem(&lvitem);

			lvitem.iSubItem = 3;

			lvitem.pszText = LPSTR(cut.m_strPostfix.c_str());
			m_clist.SetItem(&lvitem);
			i++;
		}
	}
}

void CChlsLibDlg::Add(int nItem)
{
	DWORD dwData = m_leftList.GetItemData(nItem);
	if(dwData)
	{
		CChannelSettingsAccessor& cac = *(CChannelSettingsAccessor*)dwData;
		cac.bVisibleCol = true;
		m_leftList.DeleteItem(nItem);
		_AddInUseItem(cac);
	}
}

void CChlsLibDlg::Remove(int nItem)
{
	DWORD dwData = m_clist.GetItemData(nItem);
	if(dwData)
	{
		CUtility& cut = *(CUtility*)dwData;
		_AddNotInUseItem(cut);
		remove(cut);
		m_clist.DeleteItem(nItem);
	}
}

void CChlsLibDlg::_AddNotInUseItem(const CUtility& fromItem)
{
	CHANNELS_MAP::iterator citer = m_locChannels.find(BYTE(fromItem.m_channelID));

	if(citer != m_locChannels.end())
	{
		LVITEM lvitem;
		lvitem.mask = LVIF_TEXT | LVIF_PARAM;
		CChannelSettingsAccessor& cac = citer->second;
		cac.bVisibleCol = false;
		lvitem.iItem = m_leftList.GetItemCount();
		lvitem.iSubItem = 0;
		lvitem.lParam = (LPARAM)&cac;
		lvitem.pszText = LPSTR(cac.m_descr);
		m_leftList.InsertItem(&lvitem);
	}
}

void CChlsLibDlg::_AddInUseItem(const CChannelSettingsAccessor& fromItem)
{
	CUtility cut(fromItem.m_num, 2);
	push_back(cut);
	LV_ITEM			lvitem;
	CString strCell;
	lvitem.mask = LVIF_TEXT | LVIF_PARAM;
	lvitem.iItem = m_clist.GetItemCount();
	lvitem.iSubItem = 0;
	lvitem.lParam = (LPARAM)&back();

	lvitem.pszText = LPSTR(fromItem.m_descr);
	m_clist.InsertItem(&lvitem);


	lvitem.mask = LVIF_TEXT | 0;
	lvitem.pszText = LPSTR(cut.m_strPrefix.c_str());
	lvitem.iSubItem = 1;
	m_clist.SetItem(&lvitem);

	lvitem.iSubItem = 2;
	strCell.Format("%d", cut.m_scale);
	lvitem.pszText = strCell.GetBuffer(0);
	m_clist.SetItem(&lvitem);

	lvitem.iSubItem = 3;

	lvitem.pszText = LPSTR(cut.m_strPostfix.c_str());
	m_clist.SetItem(&lvitem);
}
