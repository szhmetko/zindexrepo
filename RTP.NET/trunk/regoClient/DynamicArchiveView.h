// $Header: /rtpClient/DynamicArchiveView.h 5     13.04.08 17:22 Sergey Zhmetko $ 
// DynamicArchiveView.h: interface for the CDynamicArchiveView class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DYNAMICARCHIVEVIEW_H__D776F881_A11C_4EC9_997F_6D71A8D06864__INCLUDED_)
#define AFX_DYNAMICARCHIVEVIEW_H__D776F881_A11C_4EC9_997F_6D71A8D06864__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"
#include "GenericTab.h"
class CDynamicArchiveView : public CGenericRTPView<CDynamicArchiveView>
{
public:
	BOOL PreTranslateMessage(MSG* pMsg)
	{
		return IsDialogMessage(pMsg);	
	}

	CDynamicArchiveView(const CTabsAccessor &rst);
	virtual ~CDynamicArchiveView();
	enum { IDD = IDD_ARCHIVE_FORM };

	BEGIN_MSG_MAP(CDynamicArchiveView)
		MESSAGE_HANDLER(WM_SIZE, OnSize)
		COMMAND_HANDLER(IDC_BREFRESH, BN_CLICKED, OnButtonRefresh)
		CHAIN_MSG_MAP(baseGenericClass)
		DEFAULT_REFLECTION_HANDLER()
	END_MSG_MAP()
	
	void InitializeTab();
	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnButtonRefresh(WORD, WORD, HWND, BOOL&);
};

#endif // !defined(AFX_DYNAMICARCHIVEVIEW_H__D776F881_A11C_4EC9_997F_6D71A8D06864__INCLUDED_)
