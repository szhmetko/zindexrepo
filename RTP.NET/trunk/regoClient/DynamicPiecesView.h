// $Header: /rtpClient/DynamicPiecesView.h 5     13.04.08 22:10 Sergey Zhmetko $ 
// DynamicPiecesView.h: interface for the CDynamicPiecesView class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DYNAMICPIECESVIEW_H__5417518C_45D6_4200_8C4F_25C70C6873FD__INCLUDED_)
#define AFX_DYNAMICPIECESVIEW_H__5417518C_45D6_4200_8C4F_25C70C6873FD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"
#include "GenericTab.h"
#include "PiecesLists.h"
#include "StatReportWnd.h"

class CDynamicPiecesView : public CGenericRTPView<CDynamicPiecesView>
					, public CDialogResize<CDynamicPiecesView>
					, public CWinDataExchange<CDynamicPiecesView>
//					, public CMessageFilter
			
{
public:
	BOOL PreTranslateMessage(MSG* pMsg)
	{
		return IsDialogMessage(pMsg);	
	}

	CDynamicPiecesView(const CTabsAccessor &rst);
	virtual ~CDynamicPiecesView();
	enum { IDD = IDD_PIECES_FORM };

	BEGIN_MSG_MAP(CDynamicPiecesView)
		MESSAGE_HANDLER(WM_SIZE, OnSize)
//		COMMAND_HANDLER(IDC_BREFRESH, BN_CLICKED, OnButtonRefresh)
		MESSAGE_HANDLER(WM_COMMAND, OnCommand)
		
		COMMAND_ID_HANDLER(IDC_PIECELENGTH_COMBO, OnMetersComboSelChanged)
		CHAIN_MSG_MAP(baseGenericClass)
		CHAIN_MSG_MAP(CDialogResize<CDynamicPiecesView>)
//		DEFAULT_REFLECTION_HANDLER()
        REFLECT_NOTIFICATIONS()
	END_MSG_MAP()
    BEGIN_DDX_MAP(CDynamicPiecesView)
        DDX_CONTROL(IDC_PIECES_LIST, m_shortList)
        DDX_CONTROL(IDC_FULL_INFO_LIST, m_fullList)
    END_DDX_MAP()
    BEGIN_DLGRESIZE_MAP(CDynamicPiecesView)
		
		DLGRESIZE_CONTROL(IDC_PIECES_LIST, DLSZ_SIZE_Y )
		DLGRESIZE_CONTROL(IDC_DISPLAY_AS_TABLE, DLSZ_MOVE_Y )
		DLGRESIZE_CONTROL(IDC_DISPLAY_AS_GRAPH, DLSZ_MOVE_Y )
		DLGRESIZE_CONTROL(IDC_DISPLAY_AS_PROTOCOL, DLSZ_MOVE_Y )
		DLGRESIZE_CONTROL(IDC_PIECELENGTH_COMBO, DLSZ_MOVE_Y )
		DLGRESIZE_CONTROL(IDC_EMETERTO, DLSZ_MOVE_Y )
		DLGRESIZE_CONTROL(IDC_EMETERFROM, DLSZ_MOVE_Y )
		DLGRESIZE_CONTROL(IDC_BREFRESH, DLSZ_MOVE_Y )
		DLGRESIZE_CONTROL(IDC_PLOT, (DLSZ_SIZE_Y | DLSZ_SIZE_X) )
		DLGRESIZE_CONTROL(IDC_BREFRESH_SHLIST, DLSZ_MOVE_Y )
		DLGRESIZE_CONTROL(IDC_FULL_INFO_LIST, (DLSZ_SIZE_Y | DLSZ_SIZE_X) )
	END_DLGRESIZE_MAP()
	
	void InitializeTab();
	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//	LRESULT OnButtonRefresh(WORD, WORD, HWND, BOOL&);
	LRESULT OnMetersComboSelChanged(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
protected:
	void PiecesDlgResize_UpdateLayout(int cxWidth, int cyHeight);
	void _SetDisplayMetersRange(long from, long to);
	void	DisplayViews();
	//controls 
	CStatReportWnd	m_statReportWnd;
	CPiecesShortList m_shortList;
	CPiecesFullList  m_fullList;
	CComboBox m_pcsLenCombo;
	CEdit m_editMeterFrom; 
	CEdit m_editMeterTo;
	//misc
	unsigned int	m_displayMode;
	long			m_lFetchedPieceID;
};


#endif // !defined(AFX_DYNAMICPIECESVIEW_H__5417518C_45D6_4200_8C4F_25C70C6873FD__INCLUDED_)
