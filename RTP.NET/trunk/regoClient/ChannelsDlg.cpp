// $Header: /rtpClient/ChannelsDlg.cpp 5     12.10.08 20:39 Sergey Zhmetko $ 
// ChannelsDlg.cpp: implementation of the CChannelsDlg class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include "ChannelsDlg.h"
#include "config.h"
#include <atldbsch.h>
#include "ApproxRanges.h"
#include <atlmisc.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
extern CHANNELS_MAP	g_ChannelsMap;
extern THERMOPAIRS_MAP	g_tPairsMap;

LRESULT CChannelsDlg::OnInitDialog(UINT, WPARAM, LPARAM, BOOL& bHandled)
{
//	DoDataExchange(false);
	DlgResize_Init(false, true, WS_CLIPCHILDREN);
	CenterWindow(GetParent());

	m_clist.SubclassWindow( GetDlgItem(IDC_CHANNELLIST) );
	setColumns();
	FillList();
	return TRUE;
}

LRESULT CChannelsDlg::OnCommand(UINT, WPARAM wParam, LPARAM, BOOL& bHandled)
{
	switch(LOWORD(wParam))
	{
	case IDC_BSAVE:
		{
			using namespace dbutils;
			CDBSession locSession;
			HResult hres;
			try
			{
				hres = locSession.Open();
//				char strSQL[1024] = {'\0'};
				CCommand<CAccessor<CChannelSettingsAccessor> > chlRst;

				for(int row = 0; row < m_clist.GetItemCount(); ++row )
				{
					CString txtCell;
					m_clist.GetItemText(row, 0, txtCell);
					int iChlNum = atoi( txtCell.GetBuffer(0) );
					chlRst.m_num = iChlNum;
//					sprintf(strSQL, "select clr, k, name, fname, num from channels where num=%d", iChlNum);
//					ATLTRACE(strSQL);

					hres = chlRst.Open( locSession, CConfig::GetValue("channels/update").c_str(), 
						locSession.GetPropRowset(DBPROPVAL_UP_CHANGE) );
					if( chlRst.MoveFirst() == S_OK )
					{	
						for(int col = 1; col < m_clist.GetHeader().GetItemCount(); ++col)
						{
							if(m_clist.GetItemText(row, col, txtCell) )
							{
								switch(col)
								{
								case 1:	//��������
									strncpy(chlRst.m_descr, txtCell.GetBuffer(0),
										sizeof(chlRst.m_descr)-1 );
									break;
								case 2:	//�-�
									chlRst.m_k = atof( txtCell.GetBuffer(0) );
									break;
								case 3:	//��� ���-���
									chlRst.m_tPairID = m_clist.m_columns[col]->GetCellData(txtCell);
									//SaveTPairInfo(iChlNum, txtCell.GetBuffer(0), &locSession);
									break;
								case 4:	//����
									strncpy(chlRst.m_fname, txtCell.GetBuffer(0),
										sizeof(chlRst.m_fname)-1 );
									break;
								case 5:	//����
									chlRst.m_color = m_clist.GetItemData(row);
									break;
								}
							}
						}
						hres = chlRst.SetData();
						chlRst.ClearRecord();
					}
					chlRst.Close();
						
				}

				locSession.Close();
				EndDialog(IDOK);
			}
			catch(HresError he)
			{
				::MessageBox(0, he.what().c_str(), "", MB_OK);
			}
		}
		break;
	case IDC_BDISCARD:
		EndDialog(IDOK);	break;
	}
	return 0;
}

LRESULT CChannelsDlg::OnSize(UINT, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CSize xy = lParam;
	DlgResize_UpdateLayout(xy.cx, xy.cy);
	CRect stRec;
	m_clist.GetClientRect(&stRec);

	int iCols = m_clist.GetHeader().GetItemCount();
	LVCOLUMN col;
	col.mask = LVCF_WIDTH;
	m_clist.SetRedraw(FALSE);
	for( int i = 0; i < iCols; ++i )
	{
		if( m_clist.GetColumn(i, &col) && i != 0 )
		{
		   col.cx = (stRec.right-stRec.left)/iCols - 2;
		   m_clist.SetColumn(i, &col);
		}
	}
	m_clist.SetRedraw(TRUE);
	return 0;
}
void CChannelsDlg::setColumns()
{
	using namespace dbutils;
	const int iCols = 6;
	TCHAR rgtsz[iCols][32] = {_T("�"), _T("��������"), _T("�-�"), 
		_T("���.���-���"), _T("����"), _T("����") };
	LV_COLUMN		lvcolumn;
	CRect rect;
	m_clist.GetClientRect( &rect );


	CDBSession locSession;
	HResult hres;
	try
	{
		hres = locSession.Open();
		for(int i = 0; i < iCols; i++)  
		{
			lvcolumn.mask = LVCF_FMT | LVCF_SUBITEM | LVCF_TEXT | LVCF_WIDTH | LVCF_ORDER;
			lvcolumn.fmt = LVCFMT_CENTER;
			lvcolumn.pszText = rgtsz[i];
			lvcolumn.iSubItem = i;
			lvcolumn.iOrder = i;
			lvcolumn.cx = rect.Width() / iCols;
			switch(i)
			{
			case 0:
				lvcolumn.cx = 25;  
			case 5:
				m_clist.InsertColumn(i, &lvcolumn, CGridCtrl::CONTROLTYPE::EDIT_NONE); 
				break;
			case 1:
			case 2:
				m_clist.InsertColumn(i, &lvcolumn, CGridCtrl::CONTROLTYPE::EDIT_TEXT);
				break;
			case 3:
				{
					CGridCtrl::CGridColumn* pCol = 
						m_clist.InsertColumn(i, &lvcolumn, CGridCtrl::CONTROLTYPE::EDIT_DROPDOWNLIST);
					if( pCol )
					{
						THERMOPAIRS_MAP::const_iterator iter = g_tPairsMap.begin();
						while(iter != g_tPairsMap.end())
						{
							const CTPairsAccessor& tac = iter++->second;
							pCol->AddComboItem(CString(tac.m_name), tac.m_id);
						}
					}
					break;
				}
			case 4:
				{
					CGridCtrl::CGridColumn* pCol = 
						m_clist.InsertColumn(i, &lvcolumn, CGridCtrl::CONTROLTYPE::EDIT_DROPDOWNLIST);
					if( pCol )
					{
						CColumns cols;
						hres = cols.Open(locSession, NULL, NULL, "ValsByMeterRow"); 

						while( cols.MoveNext() == S_OK )
							pCol->AddComboItem(CString(cols.m_szColumnName), 0);
						cols.Close();
					}
					break;
				}
			}
		}
		locSession.Close();
	}
	catch(HresError he)
	{
		::MessageBox(0, he.what().c_str(), "", MB_OK);
	}
}

DWORD CChannelsDlg::OnPrePaint(int idCtrl, LPNMCUSTOMDRAW lpNMCD){	return CDRF_NOTIFYITEMDRAW;}

DWORD CChannelsDlg::OnItemPrePaint(int idCtrl, LPNMCUSTOMDRAW lpNMCD){	return CDRF_NOTIFYPOSTPAINT;}

DWORD CChannelsDlg::OnItemPostPaint( int idCtrl, NMCUSTOMDRAW* pnmcd )
{
	if( idCtrl == IDC_CHANNELLIST )
	{
		NMLVCUSTOMDRAW *pCD = (NMLVCUSTOMDRAW*)pnmcd;
		CRect siRect;
		m_clist.GetSubItemRect(pCD->nmcd.dwItemSpec, 5, 0, &siRect);
		CDC dc = pCD->nmcd.hdc;
		COLORREF bkColor =  pnmcd->lItemlParam;
		siRect.InflateRect(-2, -1, -2, -1);
		dc.FillSolidRect( &siRect, bkColor );
		dc.Detach();
	}
	return CDRF_DODEFAULT;
}

void CChannelsDlg::FillList()
{
	m_clist.DeleteAllItems();

	LV_ITEM			lvitem;
	int i = 0;

	CHMAP_ITER iter;
	CString strCell("");
	for(iter = g_ChannelsMap.begin(); iter != g_ChannelsMap.end(); iter++)
	{
		CChannelSettingsAccessor& cac = iter->second;
		lvitem.mask = LVIF_TEXT | LVIF_PARAM;
		lvitem.iItem = i;
		lvitem.iSubItem = 0;
		lvitem.lParam = (LPARAM)cac.m_color;

		strCell.Format("%d", cac.m_num);
		lvitem.pszText = strCell.GetBuffer(0);
		m_clist.InsertItem(&lvitem);


		lvitem.mask = LVIF_TEXT | 0;
		lvitem.iSubItem = 1;
		lvitem.pszText = cac.m_descr;
		m_clist.SetItem(&lvitem);

		lvitem.iSubItem = 2;
		strCell.Format("%4.4f", cac.m_k);
		lvitem.pszText = strCell.GetBuffer(0);
		m_clist.SetItem(&lvitem);

		lvitem.iSubItem = 3;
		THERMOPAIRS_MAP::const_iterator iter = g_tPairsMap.find(cac.m_tPairID);
		strCell = iter != g_tPairsMap.end() ? iter->second.m_name : "";

		lvitem.pszText = strCell.GetBuffer(0);
		m_clist.SetItem(&lvitem);

		lvitem.iSubItem = 4;
		lvitem.pszText = cac.m_fname;
		m_clist.SetItem(&lvitem);

		lvitem.iSubItem = 5;
		strCell.Format("%d", cac.m_color);
		lvitem.pszText = strCell.GetBuffer(0);
		m_clist.SetItem(&lvitem);
		++i;
	}
}

LRESULT CChannelsDlg::OnLButtonDblclk(UINT /*uMsg*/, WPARAM cx, LPARAM cy, BOOL& bHandled) 
{
	if( cy == 5 )
	{
		COLORREF clr = (COLORREF)m_clist.GetItemData(cx);
		CColorDialog clrDlg(clr, CC_FULLOPEN);
		if( IDOK == clrDlg.DoModal() )
		{
			m_clist.SetItemData(cx, DWORD(clrDlg.GetColor()));
			m_clist.Update(cx);
		}
		m_clist.EndEdit(true);

	}
	return 0;
}
