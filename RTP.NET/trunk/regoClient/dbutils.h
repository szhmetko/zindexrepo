// $Header: /rtpClient/dbutils.h 3     11.07.08 2:01 Sergey Zhmetko $ 
#ifndef __DBUTILS_H_INCLUDED__
#define __DBUTILS_H_INCLUDED__


namespace dbutils
{
	class CDBSession : public CSession
	{
	public:
		CDBSession();
		~CDBSession();

		CDBPropSet* GetPropRowset(long  dwAccess);
		HRESULT Open();
		HRESULT DumbQuery(std::string strSQL);
		long getCount(std::string strPartSQL);
		long getScalar(LPCSTR);
		static DATE DBTimeStampToDate( DBTIMESTAMP &rDBts );

		static time_t SystemTimeToUnixTime(SYSTEMTIME &rST);

		static void UnixTimeToSystemTime(time_t uxTime, SYSTEMTIME& retSDT);

		static time_t VariantTimeToUnixTime(DATE rDBdt);

		static int SystemTimeToDBTimeStamp( SYSTEMTIME &rSDT, DBTIMESTAMP &rdbDT);
	private:
		CDBPropSet	*m_pPropRowset;
	};
	class HresError : public std::exception
	{
		public:
			HresError(HRESULT hr);
			virtual ~HresError();
			std::string what();
		private:
			HRESULT	m_hrErr;
	};
	class HResult
	{
	public:
	   /// Test the HRESULT in the constructor.
	   HResult(HRESULT hr = S_OK);

	   /// Test failure of the received hr. If FAILED(hr), the function 
	   /// AtlThrow() will be called.
	   HResult& operator = (HRESULT hr);

	   /// Extractor of the stored HRESULT.
		operator HRESULT () { return m_hr; }

	private:
		void Assign(HRESULT hr) throw( std::exception );
		HRESULT m_hr; // the stored HRESULT
	};
};

#endif // __DBUTILS_H_INCLUDED__
