//$Header: /rtpClient/WFDRow.cpp 7     11.07.08 2:01 Sergey Zhmetko $
// WFDRow.cpp: implementation of the CGenericWFDRow class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include "WFDRow.h"
#include "config.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CWFRow::CWFRow(CWFDesigner& d, int cellWidth) : m_iCellWidth(cellWidth)
				, m_parent(d)
{}


CWFRow::~CWFRow()
{}


LRESULT CWFRow::OnChildWndDestroyed(UINT, WPARAM wParam, LPARAM, BOOL&)
{
	if(wParam)
	{
		CGenericWFCell*  pcl = reinterpret_cast<CGenericWFCell*>(wParam);
		remove(*pcl);
		Layout();
		Invalidate();
		if(size())
		{
			CWFRowCell wfcell = *rbegin();
			wfcell->SetFocus();
		}
	}
	return 0;
}

LRESULT CWFRow::OnCommand(UINT, WPARAM wParam, LPARAM, BOOL&)
{
	WORD wID = LOWORD(wParam);
	switch(wID)
	{
	case IDS_WFD_CMD_ADDBMP:
		InsertCell(ctBMP);
		Layout();
		Invalidate();
		break;
	case IDS_WFD_CMD_ADDVAL:
		InsertCell(ctVALUE);
		Layout();
		Invalidate();
		break;
	case IDS_WFD_CMD_DELROW:break;
	}
	return 0;
}

LRESULT CWFRow::OnKeyUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	switch(wParam)
	{
	case VK_INSERT:
		SendMessage(WM_COMMAND, IDS_WFD_CMD_ADDVAL, 0);	break;
	case VK_RETURN:
		SendMessage(WM_COMMAND, IDS_WFD_CMD_EDITROW, 0);	break;
	}
	bHandled = FALSE;
	return 0;
}

LRESULT CWFRow::OnCreate(UINT, WPARAM wParam, LPARAM, BOOL& bHandled)
{
	m_cmds.push_back(IDS_WFD_CMD_DELROW);
	m_cmds.push_back(IDS_WFD_CMD_EDITROW);
	m_cmds.push_back(IDS_WFD_CMD_ADDBMP);
	m_cmds.push_back(IDS_WFD_CMD_ADDVAL);
	
	bHandled = FALSE;
	return 0;
}

LRESULT CWFRow::OnWndPosChanged(UINT, WPARAM wParam, LPARAM, BOOL& bHandled)
{
	Layout();
	bHandled = FALSE;
	return 0;
}

size_t CWFRow::InsertCell(const CWorkFlowCellAccessor& cc, CELL_TYPE ct, int after)
{
	size_t ret = InsertCell(ct, after);
	using namespace std;
	switch(ct)
	{
	case ctVALUE:
		{
			CValuesWFCell& cell  = (CValuesWFCell&)*back();
			vector<string> items = CUtility::explode((char*)cc.m_content, "|");
			vector<string>::const_iterator iter = items.begin();
			while(iter != items.end())
			{
				CUtility util(iter->c_str());
				cell.push_back(util);
				iter++;
			}		
		}
		break;
	case ctBMP:
		{
			CBMPWFCell& cell  = (CBMPWFCell&)*back();
			const char * Delimiter = "|";
			vector<string> Entities = CUtility::explode((char*)(cc.m_content)
				, Delimiter);
			vector<string>::const_iterator iter = Entities.begin();

			while(iter != Entities.end())
			{
				int ID = atoi(iter->c_str());
				cell.AddImage(ID);
				iter++;
			}

		}
		break;
	}
	return ret;
}

size_t CWFRow::InsertCell(CELL_TYPE ct, int after)
{
	CRect rcRow;
	GetClientRect(&rcRow);
	int width = m_iCellWidth > 0 ? m_iCellWidth : 10;
	CRect rcCell(0, 0, width, rcRow.Height());
	ClientToScreen(&rcCell);
	CString cap;
	static int counter = 0;
	cap.Format("%d", ++counter);
	switch(ct)
	{
	case ctVALUE:
		{
			CValuesWFCell cell(*this);

			push_back(cell);
			back()->Create(m_hWnd, rcCell, cap);
		}
		break;
	case ctBMP:
		{
			CBMPWFCell cell(*this);
			push_back(cell);
			back()->Create(m_hWnd, rcCell, cap);
		}
		break;
	}
	//int scrw = dc.GetDeviceCaps(HORZRES);
	//int scrh = dc.GetDeviceCaps(VERTRES);
	if(m_iCellWidth <= 0 )
	{
		// need repositioning and resize all cells
		Layout();
		Invalidate();	// ?
	}
	return size();
}

int CWFRow::Save()
{
	using namespace dbutils;
	CDBSession	locSession;
	HResult hres;
	try
	{
		hres = locSession.Open();
		CCommand<CAccessor<CWorkFlowCellAccessor> > rst;
		hres = rst.Open(locSession, CConfig::GetValue("workflow/insert").c_str()
			, locSession.GetPropRowset(DBPROPVAL_UP_INSERT));
		rst.ClearRecord();
		hres = rst.MoveFirst();

		strcpy(rst.m_content, "row");
		rst.m_parent_id = 0;
		rst.m_type = 0;
		
		hres = rst.Insert(1);
		rst.Close();
		m_item_id = locSession.getScalar(CConfig::GetValue("workflow/identity").c_str());
		locSession.Close();

		iterator iter = begin();
		while(iter != end())
		{
			CGenericWFCell& cell  = (CGenericWFCell&)*(*iter);
			cell.Save();
			iter++;
		}

		return size();
	}
	catch(HresError he)
	{
		::MessageBox(0, he.what().c_str(), "", MB_OK);
		return 0;
	}
}

int CWFRow::Load()
{
	using namespace dbutils;
	CDBSession locSession;
	HResult hres;
	try
	{
		hres = locSession.Open();
		CCommand < CAccessor<CWorkFlowCellAccessor> > crst;
		crst.m_parent_id = m_item_id;
		hres = crst.Open( locSession, CConfig::GetValue("wflowcell/select").c_str());

		while(crst.MoveNext() == S_OK)
		{
			InsertCell(crst, static_cast<CELL_TYPE>(crst.m_subtype));
			/*InsertRow();
			back().Load();*/
		}
		crst.Close();

		locSession.Close();
		return size();
	}
	catch(HresError he)
	{
		::MessageBox(0, he.what().c_str(), "", MB_OK);
	}
	return -1;
}

void CWFRow::Layout()
{
	CRect rcRow, rcCell(0, 0, 0, 0);
	GetClientRect(&rcRow);
	int cellWidth = m_iCellWidth > 0 ? m_iCellWidth : !empty() ? rcRow.Width() / size() : 10;
	rcCell.right = cellWidth;
	rcCell.bottom = rcRow.Height();

	//iterate through cell collection
	ClientToScreen(&rcCell);
	iterator iter = begin();
	while(iter != end())
	{
		CWFRowCell wfcell = *iter;
		wfcell->MoveWindow(rcCell);
		rcCell.OffsetRect(rcCell.Width(), 0);
		iter++;
	}
}
