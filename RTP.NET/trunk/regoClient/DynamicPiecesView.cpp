// $Header: /rtpClient/DynamicPiecesView.cpp 6     12.10.08 20:39 Sergey Zhmetko $ 
// DynamicPiecesView.cpp: implementation of the CDynamicPiecesView class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DynamicPiecesView.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDynamicPiecesView::CDynamicPiecesView( const CTabsAccessor& tac)
				: CGenericRTPView<CDynamicPiecesView>(tac)
				, m_lFetchedPieceID(-100L)
{}

CDynamicPiecesView::~CDynamicPiecesView()
{}

void CDynamicPiecesView::InitializeTab()
{
	CGenericRTPView<CDynamicPiecesView>::InitializeTab();
	DlgResize_Init(false, true, WS_CLIPCHILDREN);
	DoDataExchange(false);

	m_editMeterFrom = GetDlgItem(IDC_EMETERFROM); 
	m_editMeterTo = GetDlgItem(IDC_EMETERTO); 
	m_pcsLenCombo = GetDlgItem(IDC_PIECELENGTH_COMBO); 
	SendMessage(WM_COMMAND, IDC_BREFRESH_SHLIST);

	::SendMessage(GetDlgItem(IDC_DISPLAY_AS_TABLE), BM_SETCHECK, 1, 0L);
	m_displayMode = IDC_DISPLAY_AS_TABLE;
	m_fullList.SetColumns(3);

	m_statReportWnd.Create( m_hWnd, &m_data );
	CRect rec; GetClientRect( &rec );
	m_statReportWnd.MoveWindow(rec);

	//��������� ����� ������ ������ ��� ����. ���������
	CString arModes[3] = {_T("����"), _T("�������"), _T("������������")};
	for(int i = 0; i < 3; ++i)
		m_pcsLenCombo.AddString( arModes[i] );

	m_pcsLenCombo.SetCurSel(0);
	
	DisplayViews();
}

LRESULT CDynamicPiecesView::OnCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	WORD wID = LOWORD(wParam);
	switch(wID)
	{
	case IDC_BREFRESH_SHLIST:
		m_shortList.Fill();
		break;
	case IDC_DISPLAY_AS_GRAPH:
	case IDC_DISPLAY_AS_TABLE:
	case IDC_DISPLAY_AS_PROTOCOL:
		m_displayMode = wID;
		m_pcsLenCombo.ShowWindow(wID == IDC_DISPLAY_AS_PROTOCOL ? SW_SHOW : SW_HIDE);
		m_editMeterTo.ShowWindow(wID == IDC_DISPLAY_AS_PROTOCOL ? SW_SHOW : SW_HIDE);
		m_editMeterFrom.ShowWindow(wID == IDC_DISPLAY_AS_PROTOCOL ? SW_SHOW : SW_HIDE);
		if(0 == m_pcsLenCombo.GetCurSel())				
		{
			m_editMeterTo.ShowWindow(SW_HIDE);
			m_editMeterFrom.ShowWindow(SW_HIDE);
		}
		break;
	case IDC_BREFRESH:
		{
			int i = m_shortList.GetNextItem( -1, LVNI_ALL | LVNI_SELECTED);
			if( i != -1 )
			{
				DWORD dwData = m_shortList.GetItemData(i);

				//if( m_lFetchedPieceID != dwData )
				{
					CString strFilter;
					strFilter.Format("where piece_id = %ld", dwData);
					 
					m_lFetchedPieceID = dwData;
					long lRows = ShowChart(strFilter, " order by meter asc");
					m_fullList.DataBind(&m_data);


					switch( m_pcsLenCombo.GetCurSel() )
					{
					case 0:		//����
						m_statReportWnd.m_pCurPiece->Full();
						break;
					case 1:		//������
						{
							char strBuf[32] = {'\0'};
							long from = 1, to = 1;
							GetDlgItemText(IDC_EMETERFROM, strBuf, sizeof(strBuf));
							from = atol(strBuf);
							GetDlgItemText(IDC_EMETERTO, strBuf, sizeof(strBuf));
							to = atol(strBuf);
							m_statReportWnd.m_pCurPiece->Manual(from, to);
						}
						break;
					case 2:		//������������
						m_statReportWnd.m_pCurPiece->Max();
						break;
					}
					m_statReportWnd.m_lFetchedPieceID = m_lFetchedPieceID;
					m_statReportWnd.LardTemplate();
					_SetDisplayMetersRange(m_statReportWnd.m_pCurPiece->first(),
						m_statReportWnd.m_pCurPiece->last());	
							
					DisplayViews();
				}
			}
		}
		break;
	default: bHandled = FALSE;
	}
	return 0;
}
void CDynamicPiecesView::_SetDisplayMetersRange(long from, long to)
{
	CString strBuf;
	strBuf.Format("%ld", from);
	SetDlgItemText(IDC_EMETERFROM, strBuf);
	strBuf.Format("%ld", to);
	SetDlgItemText(IDC_EMETERTO, strBuf);
}

LRESULT CDynamicPiecesView::OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CSize xy = lParam;
	PiecesDlgResize_UpdateLayout(xy.cx, xy.cy);
	CSize sz(lParam);
	CRect plotRect;
	m_plot.GetWindowRect(&plotRect);
	ScreenToClient(plotRect);
	plotRect.bottom = sz.cy;
	plotRect.right = sz.cx;
	m_plot.MoveWindow(&plotRect);
	m_statReportWnd.MoveWindow(&plotRect);

	for(size_t i = 0; i < m_TabChlsVector.size(); i++)
		m_plot.ShowChannel(i);
	return 0;
}	

void CDynamicPiecesView::PiecesDlgResize_UpdateLayout(int cxWidth, int cyHeight)
{
	{
		ATLASSERT(::IsWindow(m_hWnd));
		//SetRedraw(FALSE);
		RECT rectGroup = { 0, 0, 0, 0 };
		for(int i = 0; i < m_arrData.GetSize(); i++)
		{
			if((m_arrData[i].m_dwResizeFlags & _DLSZ_BEGIN_GROUP) != 0)	// start of a group
			{
				int nGroupCount = m_arrData[i].GetGroupCount();
				ATLASSERT(nGroupCount > 0 && i + nGroupCount - 1 < m_arrData.GetSize());
				rectGroup = m_arrData[i].m_rect;
				int j;
				for(j = 1; j < nGroupCount; j++)
				{
					rectGroup.left = min(rectGroup.left, m_arrData[i + j].m_rect.left);
					rectGroup.top = min(rectGroup.top, m_arrData[i + j].m_rect.top);
					rectGroup.right = max(rectGroup.right, m_arrData[i + j].m_rect.right);
					rectGroup.bottom = max(rectGroup.bottom, m_arrData[i + j].m_rect.bottom);
				}
				RECT rcThis;
				RECT rcNext;
				for(j = 0; j < nGroupCount; j++)
				{
					int xyStartNext = -1;
					if((j < (nGroupCount - 1)) && ((m_arrData[i + j].m_dwResizeFlags & (DLSZ_SIZE_X | DLSZ_SIZE_Y)) != 0) && ((m_arrData[i + j + 1].m_dwResizeFlags & (DLSZ_SIZE_X | DLSZ_SIZE_Y)) != 0))
					{
						CWindow ctlThis = GetDlgItem(m_arrData[i + j].m_nCtlID);
						ctlThis.GetWindowRect(&rcThis);
						::MapWindowPoints(NULL, m_hWnd, (LPPOINT)&rcThis, 2);
						CWindow ctlNext = GetDlgItem(m_arrData[i + j + 1].m_nCtlID);
						ctlNext.GetWindowRect(&rcNext);
						::MapWindowPoints(NULL, m_hWnd, (LPPOINT)&rcNext, 2);
						if((m_arrData[i + j].m_dwResizeFlags & DLSZ_SIZE_X) == DLSZ_SIZE_X && (m_arrData[i + j + 1].m_dwResizeFlags & DLSZ_SIZE_X) == DLSZ_SIZE_X)
						{
							if(rcNext.left >= rcThis.right)
								xyStartNext = m_arrData[i + j + 1].m_rect.left;
						}
						else if((m_arrData[i + j].m_dwResizeFlags & DLSZ_SIZE_Y) == DLSZ_SIZE_Y && (m_arrData[i + j + 1].m_dwResizeFlags & DLSZ_SIZE_Y) == DLSZ_SIZE_Y)
						{
							if(rcNext.top >= rcThis.bottom)
								xyStartNext = m_arrData[i + j + 1].m_rect.top;
						}
					}
					DlgResize_PositionControl(cxWidth, cyHeight, rectGroup, m_arrData[i + j], true, xyStartNext);
				}
				// increment to skip all group controls
				i += nGroupCount - 1;
			}
			else		// one control entry
			{
				DlgResize_PositionControl(cxWidth, cyHeight, rectGroup, m_arrData[i], false);
			}
		}
		//SetRedraw(TRUE);
		//RedrawWindow(NULL, NULL, RDW_ERASE | RDW_INVALIDATE | RDW_UPDATENOW | RDW_ALLCHILDREN);
	}
}

void CDynamicPiecesView::DisplayViews()
{
	m_fullList.ShowWindow(m_displayMode == IDC_DISPLAY_AS_TABLE ? SW_SHOW : SW_HIDE);
	m_plot.ShowWindow(m_displayMode == IDC_DISPLAY_AS_GRAPH ? SW_SHOW : SW_HIDE);
	m_statReportWnd.ShowWindow((m_displayMode == IDC_DISPLAY_AS_PROTOCOL)?SW_SHOW:SW_HIDE);
}

LRESULT CDynamicPiecesView::OnMetersComboSelChanged(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& /*bHandled*/)
{
	if( wNotifyCode == CBN_SELENDOK )
	{
		ATLTRACE("CBN_SELENDOK");
		CComboBox	cmb = GetDlgItem(IDC_PIECELENGTH_COMBO);

		int iSel = cmb.GetCurSel();

		BOOL bAuto = ( iSel != 1 );
		::SendMessage(GetDlgItem(IDC_EMETERFROM), EM_SETREADONLY, bAuto, 0L);
		::SendMessage(GetDlgItem(IDC_EMETERTO), EM_SETREADONLY, bAuto, 0L);
		
		::ShowWindow(GetDlgItem(IDC_EMETERFROM), (iSel==0)?SW_HIDE:SW_SHOW);
		::ShowWindow(GetDlgItem(IDC_EMETERTO), (iSel==0)?SW_HIDE:SW_SHOW);

	}
	return 0;
}
