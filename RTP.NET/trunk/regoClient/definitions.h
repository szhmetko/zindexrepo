// $Header: /rtpClient/definitions.h 7     2.05.08 0:29 Sergey Zhmetko $ 
typedef  std::map<int, CChannelSettingsAccessor> CHANNELS_MAP;	//key is chan num
typedef  CHANNELS_MAP::iterator CHMAP_ITER;

typedef  std::map<int, /*==*/int> TAB_CHANNELS_MAP;	//��� ���������� ������
typedef  TAB_CHANNELS_MAP::iterator TAB_CHMAP_ITER;

typedef  std::map<int, float> LIMITS_MAP;	//key is chan lim_id
typedef  LIMITS_MAP::iterator LIMIT_ITER;

typedef struct
{
	int			iChlNum;	//� ������
	bool		bPrimary;	//�� ����� ���
	COLORREF	clr;		//����
	double		k;			//�-� ��� ��-�/������
	double*		vls;		//��������� �� ������ ��������
	POINT*		pts;		//��������� �� ������ �����
	POINT*		dpts;		//��������� �� ������ ������������ �����
	void	reset(){	memset(this, 0, sizeof(*this));	k = 1.0; }
}SCurve;
typedef struct
{
	std::string	m_strTitle;
	std::string	m_strDB;
	std::string	m_strDBFileName;
	std::string	m_strUserName;
	std::string	m_strPassword;
	std::string	m_strHostName;
	std::string	m_strServiceShortName;
	bool	m_bReadOnly;
	bool	bIsVitalSetBlocked;
	void Clear(){
		m_strTitle = "";
		m_strDB = "";
		m_strUserName = "";
		m_strPassword = "";
		m_strHostName = "";
		m_strServiceShortName = "";
		m_bReadOnly = true;
		bIsVitalSetBlocked = true;
	}
}SLineSettings;


#define		WM_SAVE_RESULT_AS		WM_USER + 13
#define		WM_SETWAIT_MSG			WM_USER + 14
#define		WM_CHANGE_ZOOM_MODE		WM_USER + 15
#define		WM_PLOTS_PRINT			WM_USER + 16
#define		WM_FULLSCREEN_ONOFF		WM_USER + 3

#define		WM_MONITORTIMER_ONOFF		WM_USER + 17

#define		OSC_MAX_CHANNELS		10
#define		DATA_DELIMITER			';'

enum OscZoomMode { ZoomRect = 0, ZoomHorz, ZoomVert    };
