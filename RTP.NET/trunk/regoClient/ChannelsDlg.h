// $Header: /rtpClient/ChannelsDlg.h 4     13.04.08 17:22 Sergey Zhmetko $ 
// ChannelsDlg.h: interface for the CChannelsDlg class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CHANNELSDLG_H__70CB91DC_A05D_45C6_A2EF_F3AA8B3D29D3__INCLUDED_)
#define AFX_CHANNELSDLG_H__70CB91DC_A05D_45C6_A2EF_F3AA8B3D29D3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "mygrid.h"
class CChannelsDlg   : public CDialogImpl<CChannelsDlg>
//						public CWinDataExchange<CChannelsDlg>,
						, public CCustomDraw<CChannelsDlg>
						, public CDialogResize<CChannelsDlg>
{
public:
	enum { IDD = IDD_DCHANNELSSETTINGS };


	BEGIN_MSG_MAP(CChannelsDlg)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		MESSAGE_HANDLER(WM_SIZE, OnSize)
		MESSAGE_HANDLER(WM_COMMAND, OnCommand)

		MESSAGE_HANDLER(WM_LBUTTONDBLCLK,OnLButtonDblclk)
        CHAIN_MSG_MAP(CCustomDraw<CChannelsDlg>)
		CHAIN_MSG_MAP(CDialogResize<CChannelsDlg>)
        REFLECT_NOTIFICATIONS()
	END_MSG_MAP()

    BEGIN_DLGRESIZE_MAP(CChannelsDlg)
		DLGRESIZE_CONTROL(IDC_BSAVE, DLSZ_MOVE_X)
		DLGRESIZE_CONTROL(IDC_BDISCARD,DLSZ_MOVE_X)
		DLGRESIZE_CONTROL(IDC_CHANNELLIST, DLSZ_SIZE_Y | DLSZ_SIZE_X )
	END_DLGRESIZE_MAP()

	LRESULT OnSize(UINT, WPARAM, LPARAM, BOOL& bHandled);
	LRESULT OnInitDialog(UINT, WPARAM, LPARAM, BOOL& bHandled);
	LRESULT OnCommand(UINT, WPARAM, LPARAM, BOOL& bHandled);
	LRESULT OnLButtonDblclk(UINT, WPARAM, LPARAM, BOOL&);

	DWORD OnPrePaint(int idCtrl, LPNMCUSTOMDRAW);
	DWORD OnItemPrePaint(int idCtrl, LPNMCUSTOMDRAW);
	DWORD OnItemPostPaint(int idCtrl, LPNMCUSTOMDRAW);
private:
	void setColumns();
	void FillList();
	CGridCtrl m_clist;

};

#endif // !defined(AFX_CHANNELSDLG_H__70CB91DC_A05D_45C6_A2EF_F3AA8B3D29D3__INCLUDED_)
