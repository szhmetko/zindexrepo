// $Header: /rtpClient/LegendWnd.h 4     13.04.08 17:22 Sergey Zhmetko $ 
// LegendWnd.h: interface for the CLegendWindow class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LEGENDWND_H__71F786C4_4B10_4E1D_B9D9_C61393451D8A__INCLUDED_)
#define AFX_LEGENDWND_H__71F786C4_4B10_4E1D_B9D9_C61393451D8A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define		ITEMHEIGHT	15

extern	CHANNELS_MAP	g_ChannelsMap;

#define IDC_CHANNELSETTINGS	1
typedef CWinTraits<   WS_POPUP	
					| WS_VISIBLE	
					| WS_CAPTION
					| WS_SYSMENU
					| WS_TILED
					, WS_EX_TOOLWINDOW /*| WS_EX_TOPMOST */> CLegendWindowTraits;

class CLegendWindow : public CWindowImpl<CLegendWindow, CWindow, CLegendWindowTraits>
{
public:
	CLegendWindow( UINT uWidth = 100, std::string strCaption = "", CPoint &ptLeftTop = CPoint(0, 0) );
	~CLegendWindow();
	void	RecalcUpdate();
	void	SetCaption(std::string strCaption, bool bRedraw = true);

	DECLARE_WND_CLASS(_T("Legend Window Class"))
 
    BEGIN_MSG_MAP(CLegendWindow)
        MESSAGE_HANDLER(WM_CLOSE, OnClose)
        MESSAGE_HANDLER(WM_CREATE, OnCreate)
        MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
        MESSAGE_HANDLER(WM_PAINT, OnPaint)
        MESSAGE_HANDLER(WM_SYSCOMMAND, OnSysCommand)
		REFLECT_NOTIFICATIONS()
    END_MSG_MAP()
 
protected:
    LRESULT OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnChannelSettings(WORD , WORD , HWND , BOOL& );
    LRESULT OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

    LRESULT OnPaint(UINT, WPARAM wParam, LPARAM, BOOL& bHandled);
public:
	bool CreateAndDisplay(HWND hParent, std::string strCaption = _T("�������") );
	void EraseAll(bool bRedraw = true);
	int AddItem(int iChlNum, bool bRedraw = true);
private:
	void _DrawBkgnd(CDC& dc);
	void _DrawItem(CDC& dc, int iOffset, CChannelSettingsAccessor &cac);
	void _Layout();

	std::vector<int> m_itemsVector;
	std::string	m_strCaption;
	CRect m_rcWnd;
	UINT	m_uWidth;
	CPoint	m_ptLeftTop;
	COLORREF m_clrText;
	COLORREF m_clrBg;
	CButton		m_but;
};

#endif // !defined(AFX_LEGENDWND_H__71F786C4_4B10_4E1D_B9D9_C61393451D8A__INCLUDED_)
