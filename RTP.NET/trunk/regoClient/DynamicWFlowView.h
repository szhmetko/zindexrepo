// $Header: /rtpClient/DynamicWFlowView.h 4     11.07.08 2:00 Sergey Zhmetko $ 
// DynamicWFlowView.h: interface for the CDynamicWFlowView class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DYNAMICWFLOWVIEW_H__637298FB_FB6B_4217_8238_AB955B4276CB__INCLUDED_)
#define AFX_DYNAMICWFLOWVIEW_H__637298FB_FB6B_4217_8238_AB955B4276CB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"
#include "GenericTab.h"

class CWorkFlowBuilder;
class CDynamicWFlowView : public CGenericRTPView<CDynamicWFlowView> 
{
public:
	BOOL PreTranslateMessage(MSG* pMsg)
	{
		return IsDialogMessage(pMsg);	
	}

	CDynamicWFlowView(const CTabsAccessor &rst);
	virtual ~CDynamicWFlowView();
	enum { IDD = IDD_WFLOW_FORM };

	BEGIN_MSG_MAP(CDynamicWFlowView)
		MESSAGE_HANDLER(WM_SIZE, OnSize)
		MESSAGE_HANDLER(WM_TIMER, OnTimer)
		CHAIN_MSG_MAP(baseGenericClass)
		DEFAULT_REFLECTION_HANDLER()
	END_MSG_MAP()
	
	LRESULT OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	void InitializeTab();
private:
	CWorkFlowBuilder*	m_pCtrlBuilder;
};

#endif // !defined(AFX_DYNAMICWFLOWVIEW_H__637298FB_FB6B_4217_8238_AB955B4276CB__INCLUDED_)
