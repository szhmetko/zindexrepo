// $Header: /rtpClient/config.cpp 5     11.05.08 11:47 Sergey Zhmetko $ 
// Config.cpp: implementation of the CConfig class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Config.h"
#include "resource.h"
#import <msxml3.dll>
#include <comutil.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CConfig::CConfig(){}

CConfig::~CConfig(){}


std::string CConfig::GetValue(std::string key){return GetValue(key.c_str());}

std::string CConfig::GetValue(LPCSTR key)
{
	using namespace dbutils;
	using namespace MSXML2;
	HResult res;
	try
	{
		IXMLDOMDocumentPtr pXMLDom = NULL;
		res = pXMLDom.CreateInstance(__uuidof(MSXML2::DOMDocument30));
		pXMLDom->async = VARIANT_FALSE;

		if( pXMLDom->loadXML(m_Xml.c_str()) != VARIANT_TRUE )
		{		
			ATLTRACE("Failed load xml data from file.\n%s\n",
				(LPCSTR)pXMLDom->parseError->Getreason());
		}
		else
		{
			// Query a single node.
			std::string xPath = "//sql/" + std::string(key);
			IXMLDOMNodePtr pNode = pXMLDom->selectSingleNode(xPath.c_str());
			if (pNode == NULL)
			{
				ATLTRACE("Invalid node fetched.\n%s\n",
					(LPCSTR)pXMLDom->parseError->Getreason());
			}
			else
			{
				_variant_t value("");
				res = pNode->get_nodeTypedValue(&value);
				return _bstr_t(value);
			}
			if(pNode)pNode.Release();
		}
		if(pXMLDom)pXMLDom.Release();

	}
	catch(dbutils::HresError he)
	{
		::MessageBox(0, he.what().c_str(), "", MB_OK);
	}

	return "";
}
std::string CConfig::GetXMLContent()
{
	HANDLE hResMem;

	HRSRC hRes = ::FindResource(0, MAKEINTRESOURCE(IDR_SQL), RT_HTML);
	if(hRes)
	{
		hResMem = ::LoadResource(0, hRes);
		std::string lBuf = (LPCSTR)::LockResource(hResMem);
		UnlockResource(hResMem);
		FreeResource(hResMem);
		return lBuf;
	}
	return "";
}

std::string CConfig::GetWD()
{
	char full[_MAX_PATH];
	char drive[_MAX_DRIVE];
	char dir[_MAX_DIR];
	char fname[_MAX_FNAME];
	char ext[_MAX_EXT];

	::GetModuleFileName(0, full, _MAX_PATH);
	_splitpath( full, drive, dir, fname, ext );
	return std::string(drive) + dir;
}

std::string CConfig::GetValue(const char* key, CSession& session)
{
	return GetValue(std::string(key), session);
}

std::string CConfig::GetValue(std::string key, CSession& session)
{
	using namespace dbutils;
	CCommand<CAccessor<CConfigAccessor> >	cfgrst;
	HResult hres = S_OK;
	try
	{
		std::string strSQL = "select var_name, var_val from config where var_name='" + key + "'";
		hres = cfgrst.Open( session, strSQL.c_str());

		hres = cfgrst.MoveFirst();
//		std::string result = cfgrst.m_var_val;
		cfgrst.Close();
		return cfgrst.m_var_val;
	}
	catch(HresError exc)
	{
		ATLTRACE(exc.what().c_str());
	}
	return "";
}

std::string CConfig::GetDBValue(std::string what)
{
	using namespace dbutils;
	CCommand<CAccessor<CConfigAccessor> >	cfgrst;
	CDBSession locSession;
	HResult hres = S_OK;
	try
	{
		hres = locSession.Open();
		strncpy(cfgrst.m_param, what.c_str(), sizeof(cfgrst.m_param) - 1);
		hres = cfgrst.Open( locSession, CConfig::GetValue("config/selectsingle").c_str());

		hres = cfgrst.MoveFirst();
		cfgrst.Close();
		locSession.Close();
		return cfgrst.m_var_val;
	}
	catch(HresError exc)
	{
		ATLTRACE(exc.what().c_str());
	}
	return "";
}

std::string CConfig::m_Xml = CConfig::GetXMLContent();
