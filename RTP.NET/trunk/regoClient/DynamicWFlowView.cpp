// $Header: /rtpClient/DynamicWFlowView.cpp 5     12.10.08 20:39 Sergey Zhmetko $ 
// DynamicWFlowView.cpp: implementation of the CDynamicWFlowView class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DynamicWFlowView.h"
#include "WorfFlowBuilder.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDynamicWFlowView::CDynamicWFlowView( const CTabsAccessor& tac)
				: CGenericRTPView<CDynamicWFlowView>(tac)
{
	strcpy(m_tbl_name, "CurVal");
}

CDynamicWFlowView::~CDynamicWFlowView()
{
	if(m_pCtrlBuilder)
		delete m_pCtrlBuilder;
}

void CDynamicWFlowView::InitializeTab()
{
	using namespace dbutils;
	try
	{
		CDBSession locSession;
		HRESULT res = locSession.Open();
		m_pCtrlBuilder = new CWorkFlowBuilder(locSession, GetDlgItem(IDC_WORKFLOW));
		locSession.Close();
	}
	catch(HresError exc)
	{
		ATLTRACE(exc.what().c_str());
	}
	SetTimer(1, 10000);

}

LRESULT CDynamicWFlowView::OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CStatic st = GetDlgItem(IDC_WORKFLOW);
	CRect rcClient;
	GetClientRect(&rcClient);
	st.MoveWindow(&rcClient);
	return 0;
}

LRESULT CDynamicWFlowView::OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if( 1 == wParam  && IsWindowVisible())
	{
		if(!m_pCtrlBuilder)		return -1;

	//////////////////////////////////////////////////////

		const char szPartSQL[] = " where  dtm_upd between  dateadd(n, -1, getdate()) and getdate()";
		long lCount = m_data.PopulateData(m_tbl_name, szPartSQL, " order by dtm_upd desc");
			 
		if(lCount > 1)
		{
			CHANNELS_MAP::const_iterator iter = g_ChannelsMap.begin();
			std::map <long, double> lastData;
			int nLastIdx = 1;//lCount - 1;
			while(iter != g_ChannelsMap.end())
			{
				CChannelSettingsAccessor cac = iter->second;
				double* pData = m_data[cac.m_fname];
				if(pData)
					lastData[cac.m_num] = pData[nLastIdx];
				iter++;
			}
			double* pVlin = m_data["vlin"];
			if(pVlin)
				m_pCtrlBuilder->Run(pVlin[nLastIdx] > 0.1);

			m_pCtrlBuilder->SetChannelValues(lastData);
		}
		else
			m_pCtrlBuilder->Run(false);

		return lCount;
	}
	return 0;
}
