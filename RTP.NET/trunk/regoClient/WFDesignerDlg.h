//$Header: /rtpClient/WFDesignerDlg.h 4     11.07.08 2:01 Sergey Zhmetko $
// WFDesignerDlg.h: interface for the CWFDesignerDlg class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WFDESIGNERDLG_H__2F16C118_D79D_4828_BED0_6D8D2769E0C1__INCLUDED_)
#define AFX_WFDESIGNERDLG_H__2F16C118_D79D_4828_BED0_6D8D2769E0C1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "WFDesigner.h"

class CWFDesignerDlg : public CDialogImpl<CWFDesignerDlg>
						, public CDialogResize<CWFDesignerDlg>
{
public:
	enum { IDD = IDD_WFDESIGNER };

	CWFDesignerDlg();
	virtual ~CWFDesignerDlg();

	BEGIN_MSG_MAP(CWFDesignerDlg)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		MESSAGE_HANDLER(WM_WINDOWPOSCHANGED, OnWndPosChanged)
		COMMAND_ID_HANDLER(IDOK, OnSave)
		COMMAND_ID_HANDLER(IDCANCEL, OnCancel)
		CHAIN_MSG_MAP(CDialogResize<CWFDesignerDlg>)
 	END_MSG_MAP()
    BEGIN_DLGRESIZE_MAP(CWFDesignerDlg)
		DLGRESIZE_CONTROL(IDOK,				DLSZ_MOVE_X | DLSZ_MOVE_Y)
		DLGRESIZE_CONTROL(IDCANCEL,			DLSZ_MOVE_X | DLSZ_MOVE_Y)
		DLGRESIZE_CONTROL(IDC_WFDESIGNER,	DLSZ_SIZE_Y | DLSZ_SIZE_X )
	END_DLGRESIZE_MAP()

	LRESULT OnInitDialog(UINT, WPARAM, LPARAM, BOOL& bHandled);
	LRESULT OnCancel(WORD wNotifyCode, WORD wID, HWND, BOOL& bHandled);
	LRESULT OnSave(WORD wNotifyCode, WORD wID, HWND, BOOL& bHandled);
	LRESULT OnWndPosChanged(UINT, WPARAM, LPARAM, BOOL&);
protected:
	CWFDesigner m_designer;

};



#endif // !defined(AFX_WFDESIGNERDLG_H__2F16C118_D79D_4828_BED0_6D8D2769E0C1__INCLUDED_)
