// $Header: /rtpClient/cell.cpp 6     1.05.08 10:56 Sergey Zhmetko $ 
#include "stdafx.h"
#include "cell.h"
#include <comutil.h>

CGenericWorkFlowCell::CGenericWorkFlowCell(const CWorkFlowCellAccessor& wfc_cac)
	: m_rcBound(0, 100, 75, 295)
{
	strcpy(m_content, wfc_cac.m_content);
	m_item_id = wfc_cac.m_item_id;
	m_type = wfc_cac.m_type;
	m_subtype = wfc_cac.m_subtype;
	m_parent_id = wfc_cac.m_parent_id;
}

void CGenericWorkFlowCell::SetBgColor(COLORREF bgColor)
{
	m_bgColor = bgColor;
	Invalidate(FALSE);
}

COLORREF CGenericWorkFlowCell::GetBgColor() { return m_bgColor; }

void CGenericWorkFlowCell::SetFrColor(COLORREF frColor)
{
	m_frColor = frColor;
	Invalidate(FALSE);
}

COLORREF CGenericWorkFlowCell::GetFrColor() { return m_frColor; }

LRESULT CGenericWorkFlowCell::OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& /*bHandled*/)
{
	DoAnimation();
	return 0;
}

void CGenericWorkFlowCell::SetChannelValues(std::map<long, double> vals)
{}
LRESULT CGenericWorkFlowCell::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& /*bHandled*/)
{
	LRESULT lRes = DefWindowProc(uMsg, wParam, lParam);
	CPaintDC dc(m_hWnd); 
	m_VirtScreenDC.CreateCompatibleDC(dc);
	m_VirtScreenBitmap.CreateCompatibleBitmap(dc, 2, 2);
		
	ATLASSERT(::IsWindow(m_hWnd));

//	m_bgColor = RGB(250, 250, 255);//::GetSysColor(CTLCOLOR_DLG);
//	m_frColor = 0;


	return lRes;
}

LRESULT CGenericWorkFlowCell::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	RECT rc;
	GetClientRect(&rc);
		
	CPaintDC dc(m_hWnd);
	dc.BitBlt(0, 0, rc.right, rc.bottom, m_VirtScreenDC, 0, 0, SRCCOPY);

	bHandled = TRUE;
	return 1;
}
//DEL LRESULT CGenericWorkFlowCell::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
//DEL {
//DEL 	return 0;
//DEL }

LRESULT CGenericWorkFlowCell::OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& /*bHandled*/)
{
	Layout();
	Invalidate(FALSE);
		
	return 0;
}

void CGenericWorkFlowCell::DoAnimation(){}

void	CGenericWorkFlowCell::Animate(bool bEnable = true)
{
//	m_bAnimated = bEnable;
	if(bEnable)		
		SetTimer(1, 500);
	else
		KillTimer(1);
}
CBMPWorkFlowCell::CBMPWorkFlowCell(const CWorkFlowCellAccessor& cac, std::string dir) : 
	CGenericWorkFlowCell(cac)
	, m_strImageDir(dir)
{
	const char * szMask = "%08d";
	const char * Delimiter = "|";
	std::vector<std::string> Entities = CUtility::explode(m_content, Delimiter);
	std::vector<std::string>::const_iterator iter = Entities.begin();

	char szFilename[MAX_PATH] = {0};
	
	while(iter != Entities.end())
	{
		int ID = atoi(iter->c_str());

		sprintf(szFilename, szMask, ID);
		m_strBMPPaths.push_back(m_strImageDir + szFilename);
		iter++;
	}
	ATLTRACE(szMask, 265);

}
CBMPWorkFlowCell::~CBMPWorkFlowCell(){_CleanUp();}

void CBMPWorkFlowCell::_CleanUp()
{
	while( !m_dequehBitmaps.empty() )
	{
//		CBitmap
		HBITMAP hCurBitmap = m_dequehBitmaps.back();
		if(hCurBitmap)
			if(::DeleteObject(hCurBitmap)) hCurBitmap = 0;
		m_dequehBitmaps.pop_back();
	}
}
void CBMPWorkFlowCell::DoAnimation()
{
	// called from OnTimer function of base class
	if(m_dequehBitmaps.empty())	return;
	
	HBITMAP hFront = m_dequehBitmaps.front();
	m_dequehBitmaps.pop_front();
	m_dequehBitmaps.push_back(hFront);

	HBITMAP hCurBitmap = m_dequehBitmaps.front();
	if(hCurBitmap)
		m_VirtScreenDC.SelectBitmap(hCurBitmap);		
	Invalidate();
}
void CBMPWorkFlowCell::AddAnimationBMP(const char* szPath)
{
	m_strBMPPaths.push_back(szPath);
}
HBITMAP CBMPWorkFlowCell::LoadBitmapFromFile(const char* szPath)
{
	using namespace Gdiplus;
	HBITMAP hBitmap = 0;
	_bstr_t str(szPath);
	Bitmap* pBmp = Bitmap::FromFile(str, TRUE);
	//Status s = pBmp->SetResolution(20.0, 20.0);
	Color clrBkground;
	Status s = pBmp->GetHBITMAP(clrBkground, &hBitmap);
	//pBmp->GetPropertySize(&size, &count);
	delete pBmp;
	return hBitmap;
}
HBITMAP CBMPWorkFlowCell::LoadBitmapFromBMPFile(const char* szPath, RECT& rcBound)
{
	BITMAP bm = {0};
	HPALETTE hPalette = 0;
	//use LoadImage to get the image loaded into dib DIBSection
	HBITMAP hBitmap = (HBITMAP)LoadImage(_Module.GetResourceInstance(), szPath, IMAGE_BITMAP, 
						rcBound.right - rcBound.left, rcBound.bottom - rcBound.top,
						LR_CREATEDIBSECTION | LR_DEFAULTSIZE | LR_LOADFROMFILE);

	if(NULL == hBitmap)	return FALSE;

	//get the color depth of the DIBSection
	GetObject(hBitmap, sizeof(BITMAP), &bm);
	
	// if DIBSection is 256 color or less, it has a color table
	if( bm.bmBitsPixel * bm.bmPlanes <= 8 )
	{
		OutputDebugString("dib section has color table\n");
	}
	else		// it has no color table, so use  a halftone palette
	{
//		HDC hRefDC = GetDC(0);
//		HPALETTE hPalette = CreateHalftonePalette(hRefDC);
//		ReleaseDC(0, hRefDC);
	}
	return hBitmap;
}
void CBMPWorkFlowCell::Layout()
{
	CRect rc;
	GetClientRect(&rc);

	_CleanUp();

	for(int idx  = 0; idx < m_strBMPPaths.size(); ++idx)
	{
		CBitmapHandle bmp = LoadBitmapFromFile(m_strBMPPaths[idx].c_str());
		if(bmp)
		{
			HBITMAP hBmp = (HBITMAP)::CopyImage(bmp, IMAGE_BITMAP, rc.Width(), rc.Height(), LR_COPYRETURNORG);
			bmp.DeleteObject();
			m_dequehBitmaps.push_back(hBmp);
		}
	}

	if(!m_dequehBitmaps.empty())
	{
		HBITMAP hCurBitmap = m_dequehBitmaps.front();

		if(hCurBitmap)
			m_VirtScreenDC.SelectBitmap(hCurBitmap);		
	}
}
CValueWorkFlowCell::CValueWorkFlowCell(const CWorkFlowCellAccessor& cac) : 
		CGenericWorkFlowCell(cac)
{
	m_bgColor = RGB(255, 255, 255);
	m_frColor = 0;
	LOGFONT lf = {0};
	lf.lfHeight = 14;
	lf.lfWeight = FW_NORMAL;
	strcpy(lf.lfFaceName, "Arial cyr");
	m_font.CreateFontIndirect(&lf);

	std::vector<std::string> items = CUtility::explode(m_content, "|");
	std::vector<std::string>::const_iterator iter = items.begin();
	while(iter != items.end())
	{
		CUtility util(iter->c_str());
		m_arChannels.push_back(util);
		iter++;
	}
}
void CValueWorkFlowCell::Layout()
{
 	CPaintDC dc(m_hWnd); 
	BITMAP BMStruct; 
	BMStruct.bmWidth = BMStruct.bmHeight = 0;

	int scrw = dc.GetDeviceCaps(HORZRES);
	int scrh = dc.GetDeviceCaps(VERTRES);

	CBitmap TempBM; TempBM.CreateCompatibleBitmap(dc,1,1);
	m_VirtScreenDC.SelectBitmap(TempBM);

	m_VirtScreenBitmap.DeleteObject();

	if(!m_VirtScreenBitmap.CreateCompatibleBitmap(dc, scrw, scrh))
	{
		ATLTRACE("CreateCompatibleBitmap failed!");
		return;
	}
		
	m_VirtScreenDC.SelectBitmap(m_VirtScreenBitmap);		

	{
		CWaitCursor wc;
		_DrawBackground(m_VirtScreenDC);
		_DrawText(m_VirtScreenDC);
	}
}
void CValueWorkFlowCell::_DrawBackground(CDC& dc)
{
	RECT rc;
	GetClientRect(&rc);
	dc.FillSolidRect(&rc, m_bgColor);
}
void CValueWorkFlowCell::_DrawText(CDC& dc)
{
	const int iHeight = 12;	//height or row in pixels
	COLORREF oldClr = dc.GetTextColor();
	dc.SetTextColor( m_frColor );
	HFONT oldFont = dc.SelectFont(m_font);

	CRect rcItem;
	GetClientRect(&rcItem);
	rcItem.bottom -= 2;
	rcItem.top = rcItem.bottom - iHeight;

	std::vector<CUtility>::const_iterator iter = m_arChannels.begin();
	while(iter != m_arChannels.end())
	{
		CUtility util = *iter;
		std::string strText = util.FormatString();
		dc.DrawText(strText.c_str(), -1, &rcItem, 
			DT_EDITCONTROL | DT_CENTER | DT_SINGLELINE );
		rcItem.OffsetRect(0, -iHeight - 2);
		iter++;
	}
	
	if (oldFont) dc.SelectFont(oldFont);
	if (oldClr) dc.SetTextColor( oldClr );
}
const CGenericWorkFlowCell&  CGenericWorkFlowCell::operator=(CWorkFlowCellAccessor& wfc_cac)
{
	strcpy(m_content, wfc_cac.m_content);
	m_item_id = wfc_cac.m_item_id;
	m_type = wfc_cac.m_type;
	m_subtype = wfc_cac.m_subtype;
	m_parent_id = wfc_cac.m_parent_id;
	return *this;
}
void CValueWorkFlowCell::SetChannelValues(std::map<long, double> vals)
{
	std::map<long, double>::const_iterator oiter = vals.begin();
	while(oiter != vals.end())
	{
		std::vector<CUtility>::iterator iiter = m_arChannels.begin();
		while(iiter != m_arChannels.end())
		{
//			CUtility& util = *iiter;
			if(iiter->m_channelID == oiter->first)
				iiter->m_value = oiter->second;
			iiter++;
		}
		oiter++;
	}
	Layout();
	Invalidate();
}
