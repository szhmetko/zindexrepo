// $Header: /rtpClient/WFImageLibrary.h 3     13.05.08 4:04 Sergey Zhmetko $ 
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WFIMAGELIBRARY_H__A842117E_3951_4C97_9D66_E496A5ADC958__INCLUDED_)
#define AFX_WFIMAGELIBRARY_H__A842117E_3951_4C97_9D66_E496A5ADC958__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "mygrid.h"
#include "utility.h"

class CWFImageCache : public std::map<long, CBitmapHandle>
{
	typedef std::map<long, CBitmapHandle> baseContainer;
public:
	CWFImageCache();
	virtual ~CWFImageCache();
	clear();
	static CRect GetDesiredPictureSize();
};

class CImageLibDlg : public CDialogImpl<CImageLibDlg>
					, public CDialogResize<CImageLibDlg>
					, public std::list<long>
{
public:
	enum { IDD = IDD_WFIMG_LIB };

	CImageLibDlg(CWFImageCache&);
    BEGIN_DLGRESIZE_MAP(CImageLibDlg)
		DLGRESIZE_CONTROL(IDCANCEL, DLSZ_MOVE_Y | DLSZ_MOVE_X)
		DLGRESIZE_CONTROL(IDOK, DLSZ_MOVE_Y | DLSZ_MOVE_X)
		DLGRESIZE_CONTROL(IDC_IMAGELIST, DLSZ_SIZE_X | DLSZ_SIZE_Y)
		DLGRESIZE_CONTROL(IDC_CURRENTITEM, DLSZ_MOVE_X)
	END_DLGRESIZE_MAP()

	BEGIN_MSG_MAP(CImageLibDlg)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		COMMAND_ID_HANDLER(IDOK, OnOK)
		COMMAND_ID_HANDLER(IDCANCEL, OnCloseCmd)
		CHAIN_MSG_MAP(CDialogResize<CImageLibDlg>)
	END_MSG_MAP()

	LRESULT OnInitDialog(UINT, WPARAM, LPARAM, BOOL& bHandled);
	LRESULT OnCloseCmd(WORD, WORD wID, HWND, BOOL& bHandled);
	LRESULT OnOK(WORD, WORD wID, HWND, BOOL& bHandled);
private:
	CListViewCtrl m_imgList;
	CImageList	m_ilAll;
	CWFImageCache& m_images;
};


class CChlsLibDlg : public CDialogImpl<CChlsLibDlg>
					, public CDialogResize<CChlsLibDlg>
					, public std::list<CUtility>
{
	typedef std::list<CUtility> baseContainer;
public:
	enum { IDD = IDD_WFCHLS_LIB };
	CChlsLibDlg(const std::list<CUtility>& rlist);
    BEGIN_DLGRESIZE_MAP(CChlsLibDlg)
		DLGRESIZE_CONTROL(IDCANCEL, DLSZ_MOVE_Y | DLSZ_MOVE_X)
		DLGRESIZE_CONTROL(IDOK, DLSZ_MOVE_Y | DLSZ_MOVE_X)
	END_DLGRESIZE_MAP()

	BEGIN_MSG_MAP(CChlsLibDlg)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		MESSAGE_HANDLER(WM_COMMAND, OnCommand)
		CHAIN_MSG_MAP(CDialogResize<CChlsLibDlg>)
	END_MSG_MAP()

	LRESULT OnCommand(UINT, WPARAM, LPARAM, BOOL& bHandled);
	LRESULT OnInitDialog(UINT, WPARAM, LPARAM, BOOL& bHandled);

protected:
	void Add(int);
	void Remove(int);
	CGridCtrl m_clist;
	CListViewCtrl m_leftList;
	void setColumns();
	void FillLists();
	CHANNELS_MAP m_locChannels;
private:
	void _AddInUseItem(const CChannelSettingsAccessor& itm);
	void _AddNotInUseItem(const CUtility& itm);
};

#endif // !defined(AFX_WFIMAGELIBRARY_H__A842117E_3951_4C97_9D66_E496A5ADC958__INCLUDED_)
