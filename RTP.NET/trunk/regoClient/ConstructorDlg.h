// $Header: /rtpClient/ConstructorDlg.h 6     12.10.08 20:40 Sergey Zhmetko $ 
// ConstructorDlg.h : interface of the CConstructorDlg class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CONSTRUCTORDLG_H__INCLUDED_)
#define AFX_CONSTRUCTORDLG_H__INCLUDED_

#include <atlcrack.h>

typedef struct 
{
	LONG id;
	LONG num;
	USHORT uType;
	COLORREF clr;
} SItem, *PSItem;

//#include "TabChannelInfo.h"
//#include "CurveInfoDlg.h"
class CTabParamsDlg : public CDialogImpl<CTabParamsDlg>
{
public:
	enum { IDD = IDD_DTABPARAMETERS };

	BEGIN_MSG_MAP(CTabParamsDlg)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		COMMAND_ID_HANDLER(IDOK, OnOk)
		COMMAND_ID_HANDLER(IDCANCEL, OnCloseCmd)
	END_MSG_MAP()
	CTabParamsDlg();
	CTabParamsDlg(const CTabsAccessor& tac);
	LRESULT OnInitDialog(UINT, WPARAM, LPARAM, BOOL& bHandled);
	LRESULT OnCloseCmd(WORD, WORD wID, HWND, BOOL&);
	LRESULT OnOk(WORD, WORD wID, HWND, BOOL& bHandled);
	//attributes
	CTabsAccessor m_TabInfo;
};

class CTabsTreeCtrl : public CWindowImpl<CTabsTreeCtrl, CTreeViewCtrlEx>,
                       public CCustomDraw<CTabsTreeCtrl>
{
public:
    BEGIN_MSG_MAP(CTabsTreeCtrl)
		MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
        CHAIN_MSG_MAP_ALT(CCustomDraw<CTabsTreeCtrl>, 1)
        DEFAULT_REFLECTION_HANDLER()
    END_MSG_MAP()

	CTabsTreeCtrl() : m_cxImage(16), m_cyImage(16)
	{}
	LRESULT OnDestroy(UINT, WPARAM, LPARAM, BOOL& bHandled)
	{
		if(!m_ImageList.IsNull())m_ImageList.RemoveAll();
		m_ImageList.Destroy();
		return 0;
	}
	void SetImageList()
	{
		BOOL bCreate = m_ImageList.Create(m_cxImage, m_cyImage, ILC_COLOR32 | ILC_MASK, 4, 4);
		CTreeViewCtrlEx::SetImageList(m_ImageList, TVSIL_NORMAL);
		AddIcon(IDI_ITAB);
		AddIcon(IDI_ITABCHANNEL);
		AddIcon(IDI_CURVE_PRI);
		AddIcon(IDI_CURVE_SEC);
	}
	int AddIcon(_U_STRINGorID icon, HMODULE hModule = _Module.GetResourceInstance())
	{
		HICON hIcon = (HICON)::LoadImage(
			hModule,
			icon.m_lpstr,
			IMAGE_ICON, m_cxImage, m_cyImage, LR_SHARED);
		return hIcon ? m_ImageList.AddIcon(hIcon) : -1;
	}
	DWORD OnPrePaint(int idCtrl, LPNMCUSTOMDRAW lpNMCD)
	{
		return CDRF_NOTIFYITEMDRAW;
	}

	DWORD OnItemPrePaint(int idCtrl, LPNMCUSTOMDRAW lpNMCD)
	{
		NMTVCUSTOMDRAW* pnmtv = (NMTVCUSTOMDRAW*) lpNMCD;

		
		SItem* pItem = (SItem*)lpNMCD->lItemlParam;
		if(pItem)
		{
			pnmtv->clrText = pItem->clr;
		}

		return CDRF_DODEFAULT;
	}
private:
	CImageList m_ImageList;
	int m_cxImage, m_cyImage;
};

class CConstructorDlg : public CDialogImpl<CConstructorDlg>
						, public CDialogResize<CConstructorDlg>
						, public CWinDataExchange<CConstructorDlg>
{
public:
	enum { IDD = IDD_DCONSTRUCTOR };
	enum { TIT_TAB = 0, TIT_TABCHANNEL, TIT_CURVE };


    BEGIN_DLGRESIZE_MAP(CConstructorDlg)
		DLGRESIZE_CONTROL(IDC_BADD, DLSZ_MOVE_X)
		DLGRESIZE_CONTROL(IDC_BREMOVE, DLSZ_MOVE_X)
		DLGRESIZE_CONTROL(IDC_BEDIT, DLSZ_MOVE_X)
		DLGRESIZE_CONTROL(IDC_TTABSLIST, DLSZ_SIZE_Y | DLSZ_SIZE_X )
		DLGRESIZE_CONTROL(IDC_BREMOVEALL, DLSZ_MOVE_X )
		
	END_DLGRESIZE_MAP()
	BEGIN_MSG_MAP(CConstructorDlg)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		COMMAND_ID_HANDLER(IDOK, OnCloseCmd)
		COMMAND_ID_HANDLER(IDCANCEL, OnCloseCmd)
		COMMAND_ID_HANDLER(IDC_BADD, OnAdd)
		COMMAND_ID_HANDLER(IDC_BREMOVE, OnRemove)
		COMMAND_ID_HANDLER(IDC_BEDIT, OnEdit)
		COMMAND_ID_HANDLER(IDC_BREMOVEALL, OnRemoveAll)
		MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
		NOTIFY_HANDLER(IDC_TTABSLIST, TVN_SELCHANGED, OnNodeSelect)
		CHAIN_MSG_MAP(CDialogResize<CConstructorDlg>)
        REFLECT_NOTIFICATIONS()

	END_MSG_MAP()

    BEGIN_DDX_MAP(CConstructorDlg)
        DDX_CONTROL(IDC_TTABSLIST, m_wndTabsTree)
    END_DDX_MAP()

	LRESULT OnNodeSelect(int, LPNMHDR, BOOL& bHandled);
	bool AddTab();
	bool EditTab(SItem* pItem);
	bool RemoveTab(SItem* pItem);
	bool EditTabChannel(SItem* pSelItem);
	bool EditCurve(SItem* pSelItem);
	bool AddTabChannel(SItem* pParentItem);
	bool AddCurve(SItem* pParentItem);

	bool RemoveCurve(SItem* pItem);

	bool RemoveTabChannel(SItem* pItem);
	
	LRESULT OnAdd(WORD, WORD, HWND, BOOL& bHandled);
	LRESULT OnRemove(WORD, WORD, HWND, BOOL& bHandled);
	LRESULT OnEdit(WORD, WORD, HWND, BOOL& bHandled);
	LRESULT OnRemoveAll(WORD, WORD, HWND, BOOL& bHandled);

	LRESULT OnInitDialog(UINT, WPARAM, LPARAM, BOOL& bHandled);

	LRESULT OnCloseCmd(WORD, WORD wID, HWND, BOOL& bHandled);
	LRESULT OnDestroy(UINT, WPARAM, LPARAM, BOOL& bHandled);
private:
	void _CleanUpTree();
	void _CleanUpTreeItem(CTreeItem& rItem);
	void _FillTree();
	HTREEITEM _FillTabChannels( int TabID, CTreeItem &tiParent );
	HTREEITEM _FillCurves( int tchlID, CTreeItem &tiParent );
	CTabsTreeCtrl	m_wndTabsTree;
	SItem m_curSelection;
	public:
		bool	m_bAffected;

};


#endif // !defined(AFX_CONSTRUCTORDLG_H__INCLUDED_)
