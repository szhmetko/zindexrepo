// $Header: /rtpClient/include/cell.h 4     1.05.08 10:56 Sergey Zhmetko $ 
#ifndef _CELL_H_INCLUDED_
#define _CELL_H_INCLUDED_

//#include "workflowaccessor.h"
#include "utility.h"
#include <deque>

//abstract helper class 

class  CGenericWorkFlowCell : public CWindowImpl<CGenericWorkFlowCell, CStatic, CControlWinTraits >
			, public CWorkFlowCellAccessor
{
protected:
	COLORREF	m_bgColor;
	COLORREF	m_frColor;

///virtual screen section
	CBitmap		m_VirtScreenBitmap;
	CDC	m_VirtScreenDC;
	CRect		m_rcBound;

public:
	CGenericWorkFlowCell(const CWorkFlowCellAccessor& wfc_cac);
	virtual ~CGenericWorkFlowCell(){}

	const CGenericWorkFlowCell& operator=(CWorkFlowCellAccessor& wfc_cac);

	DECLARE_WND_SUPERCLASS(NULL, CStatic::GetWndClassName())

	// Interface

	void SetBgColor(COLORREF);
	COLORREF GetBgColor();

	void SetFrColor(COLORREF frColor);
	COLORREF GetFrColor();


	// Message map and handlers
	BEGIN_MSG_MAP(CGenericWorkFlowCell)
		MESSAGE_HANDLER(WM_CREATE, OnCreate)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_SIZE, OnSize)
		MESSAGE_HANDLER(WM_TIMER, OnTimer)
		DEFAULT_REFLECTION_HANDLER()
	END_MSG_MAP()
	LRESULT OnTimer(UINT, WPARAM, LPARAM, BOOL&);
	LRESULT OnCreate(UINT, WPARAM, LPARAM, BOOL&);
	LRESULT OnPaint(UINT, WPARAM wParam, LPARAM, BOOL&);
	LRESULT OnSize(UINT, WPARAM, LPARAM, BOOL&);

	void	Animate(bool);
	virtual void SetChannelValues(std::map<long, double> vals);
protected:

	// Implementation
	virtual void Layout() = 0;
	virtual void DoAnimation();
};

class CBMPWorkFlowCell : public CGenericWorkFlowCell
{
public:
protected:
	CBitmap		m_BMPfromFile;
	bool m_bAnimated;
	std::string		m_strImageDir;

	void Layout();
	void DoAnimation();
public:
	CBMPWorkFlowCell(const CWorkFlowCellAccessor& wfc_cac, std::string);
	~CBMPWorkFlowCell();
	void AddAnimationBMP(const char*);

	static HBITMAP LoadBitmapFromBMPFile(const char*, RECT&);
	static HBITMAP LoadBitmapFromFile(const char*);
protected:
	void	_CleanUp();
	std::deque<HBITMAP>	m_dequehBitmaps;
	std::vector<std::string>	m_strBMPPaths;
};
class CValueWorkFlowCell : public CGenericWorkFlowCell
{
public:
	//ctor
	CValueWorkFlowCell(const CWorkFlowCellAccessor& wfc_cac);
	CFont		m_font;
	std::vector<CUtility> m_arChannels;
	void SetChannelValues(std::map<long, double> vals);
protected:
	void Layout();
protected:
	virtual void _DrawBackground(CDC&);
	virtual void _DrawText(CDC&);

};
#endif	// _CELL_H_INCLUDED_