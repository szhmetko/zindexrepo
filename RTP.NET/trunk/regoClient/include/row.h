// $Header: /rtpClient/include/row.h 4     1.05.08 10:56 Sergey Zhmetko $ 
#ifndef _ROW_H_INCLUDED_
#define _ROW_H_INCLUDED_

//#include "workflowaccessor.h"
class CGenericWorkFlowCell;
class IImageSettings;

class  CWorkFlowRow : public CWindowImpl<CWorkFlowRow, CStatic, CControlWinTraits >
			, public CWorkFlowRowAccessor
			, protected std::list<CGenericWorkFlowCell*>
{
//	typedef std::vector<CGenericWorkFlowCell*> CELLS_COLLECTION;

	COLORREF	m_bgColor;
	COLORREF	m_frColor;
	unsigned short	m_uHeight;

public:
//	const CWorkFlowRow& operator=(CWorkFlowRowAccessor& wfc_cac);

	DECLARE_WND_SUPERCLASS(NULL, CStatic::GetWndClassName())

	// Interface

	void SetBgColor(COLORREF);
	COLORREF GetBgColor();

	void SetFrColor(COLORREF frColor);
	COLORREF GetFrColor();

	// Message map and handlers
	BEGIN_MSG_MAP(CWorkFlowRow)
		MESSAGE_HANDLER(WM_CREATE, OnCreate)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_SIZE, OnSize)
		DEFAULT_REFLECTION_HANDLER()
	END_MSG_MAP()
	LRESULT OnCreate(UINT, WPARAM, LPARAM, BOOL&);
	LRESULT OnPaint(UINT, WPARAM wParam, LPARAM, BOOL&);
	LRESULT OnSize(UINT, WPARAM, LPARAM, BOOL&);

public:
	void SetChannelValues(std::map<long, double> vals);
	int Fill(CSession&);
	//ctor / dtor
	CWorkFlowRow(unsigned short, const CWorkFlowRowAccessor&, IImageSettings&);
	~CWorkFlowRow();
	virtual CGenericWorkFlowCell* AddBMPCell(const CWorkFlowCellAccessor& );
	virtual CGenericWorkFlowCell* AddValueCell(const CWorkFlowCellAccessor& );
	unsigned short Height();
	void Animate(bool);
protected:
	virtual void Layout();
	IImageSettings&	m_ctrlParent;

protected:
	void _DrawBackground(CDC&);
//	CELLS_COLLECTION	m_cells;
	bool		m_bShowGrid;
	// Implementation
};
#endif	// _ROW_H_INCLUDED_