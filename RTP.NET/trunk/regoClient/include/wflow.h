// $Header: /rtpClient/include/wflow.h 5     1.05.08 10:56 Sergey Zhmetko $ 
#ifndef _WFLOW_H_INCLUDED_
#define _WFLOW_H_INCLUDED_

#include "row.h"
#include "Config.h"


//abstract helper class 
class IImageSettings{
public:
	virtual float PictureRatio()=0;
	virtual std::string GetImageDir()=0;
};
template< class T, class TBase = CStatic, class TWinTraits = CControlWinTraits >
class ATL_NO_VTABLE CWorkFlowCtrlImpl : public CWindowImpl<T, TBase, TWinTraits>
					, protected std::list <CWorkFlowRow*>
					, public IImageSettings
{
	typedef CWindowImpl<T, TBase, TWinTraits>	thisClass;
//	typedef std::vector <CWorkFlowRow*>	ROWS_VECTOR;
public:
	DECLARE_WND_SUPERCLASS(NULL, TBase::GetWndClassName())

	// Operations
	BOOL SubclassWindow(HWND hWnd)
	{
		ATLASSERT(m_hWnd == NULL);
		ATLASSERT(::IsWindow(hWnd));

	#ifdef _DEBUG
		// Check class
		char szBuffer[16] = {0};
		if( ::GetClassName(hWnd, szBuffer, (sizeof(szBuffer)/sizeof(TCHAR)) - 1) ) 
			ATLASSERT(::lstrcmpi(szBuffer, TBase::GetWndClassName()) == 0);
	#endif

		BOOL bRet = CWindowImpl<T, TBase, TWinTraits>::SubclassWindow(hWnd);
		if( bRet ) _Init();
		return bRet;
	}

	~CWorkFlowCtrlImpl()
	{
		iterator riter = begin();
		while( riter != end() )
		{
			CWorkFlowRow* pRow = *riter;
			if(pRow)
				delete pRow;
			riter++;
		}
		clear();
	}
	// Interface

	// Message map and handlers
	BEGIN_MSG_MAP(thisClass)
		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_SIZE, OnSize)
		REFLECT_NOTIFICATIONS()
	END_MSG_MAP()

	LRESULT OnMouseMove(UINT, WPARAM, LPARAM, BOOL&){return 0;}
	LRESULT OnSize(UINT, WPARAM, LPARAM, BOOL&)
	{
		Layout();
		return 0;
	}

	CWorkFlowRow* AddRow(const CWorkFlowRowAccessor& rac, unsigned short uHeight = 0)
	{
//		if( !m_rows.empty() )
		{
			CRect rcItem;
			if(uHeight == 0)
			{
				GetClientRect(&rcItem);
				uHeight = rcItem.Width() / (size() + 1);
			}

			IImageSettings& rT = static_cast<IImageSettings&>(*this);
			CWorkFlowRow* pNewRow = new CWorkFlowRow(uHeight, rac, rT);
			DWORD dwStyle = WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;
#ifdef _DEBUG
//			dwStyle |= WS_BORDER;
#endif
			pNewRow->Create(m_hWnd, rcItem, NULL, dwStyle, 0);
			push_back(pNewRow);
			Layout();
			return	pNewRow;
		}
		return 0;
	}/*	
	CWorkFlowRow* Row(int idx)
	{
		return m_rows[idx];
	}*/
	void Animate(bool bEnabled)
	{
		iterator riter = begin();
		while( riter != end() )
		{
			CWorkFlowRow* pRow = *riter;
			if(pRow)
				pRow->Animate(bEnabled);
			riter++;
		}
	}
	float PictureRatio()	{ return m_fRatio;	}
	std::string GetImageDir()	{ return m_strImageDir;	}
	void SetChannelValues(std::map<long, double> vals)
	{
		iterator riter = begin();
		while( riter != end() )
		{
			CWorkFlowRow* pRow = *riter;
			if(pRow)
				pRow->SetChannelValues(vals);
			riter++;
		}
	}
protected:
	float m_fRatio;				//��������� ������ ������/������
	std::string m_strImageDir;		//������������� ����

	// Implementation
	void ReadConfig()
	{
		T* pT = static_cast<T*>(this);
		std::string strValue = CConfig::GetValue("img_ratio", pT->GetDBSession());
		m_fRatio = max( 1.0f, atof(strValue.c_str()) );

		m_strImageDir = CConfig::GetWD() + CConfig::GetValue("images_dir", pT->GetDBSession());

	}
	void _Init()
	{
		ATLASSERT(::IsWindow(m_hWnd));
		ModifyStyle(0, WS_BORDER);
//		ReadConfig();
		Layout();
	}
	void Layout()
	{
		ATLASSERT(::IsWindow(m_hWnd));

		CRect rcItem;
		GetClientRect(&rcItem);
		iterator riter = begin();
		while( riter != end() )
		{
			CWorkFlowRow* pRow = *riter;
			if(pRow)
			{
				rcItem.bottom = rcItem.top + pRow->Height();
				pRow->MoveWindow(&rcItem);
				rcItem.top = rcItem.bottom;
			}
			riter++;
		}
	}
	
	//attributes
//	ROWS_VECTOR	m_rows;
};
class CWorkFlowCtrl : public CWorkFlowCtrlImpl<CWorkFlowCtrl>
{
public:
	CWorkFlowCtrl(CSession& session);
	virtual ~CWorkFlowCtrl();
	CSession&	GetDBSession();
	virtual void DownLoadPictures();
	DECLARE_WND_SUPERCLASS(_T("__"), GetWndClassName())  
protected:
	CSession&	m_session;
	std::list<std::string> m_pictures;
};

#endif		//_WFLOW_H_INCLUDED_