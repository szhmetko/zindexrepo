#if !defined(AFX_MYGIRD_H__INCLUDED_)
#define AFX_MYGIRD_H__INCLUDED_
#include <atlmisc.h>
#include <vector>

class CGridCtrl : public CWindowImpl<CGridCtrl, CListViewCtrl>
			, public CCustomDraw<CGridCtrl>
{
	friend class CGridColumn;
	friend class CGridCell;
	friend class CGridCellText;
	friend class CGridCellDropdownlist;
public:
	class CEditText;
	class CEditDropdownlist;
	class CGridColumn;
	class CGridCellText;
	class CGridCellDropdownlist;
public:
	DECLARE_WND_CLASS_EX("MYWTLGRID", CS_DBLCLKS, COLOR_BTNFACE)
	typedef CWindowImpl<CGridCtrl, CListViewCtrl> baseClass;

	enum CONTROLTYPE { EDIT_NONE, EDIT_TEXT, EDIT_DROPDOWNLIST, EDIT_DROPDOWN, EDIT_DATE, EDIT_TIME	};
	enum ALIGN 	{	LEFT, CENTER, RIGHT	};
	
    BEGIN_MSG_MAP(CGridCtrl)
		MESSAGE_HANDLER(WM_LBUTTONDBLCLK,OnLButtonDblclk)
		MESSAGE_HANDLER(WM_CHAR, OnChar)
        CHAIN_MSG_MAP_ALT(CCustomDraw<CGridCtrl>, 1)
        DEFAULT_REFLECTION_HANDLER()
//		CHAIN_MSG_MAP(baseClass)
//		if(uMsg==WM_COMMAND || uMsg==WM_NOTIFY) 
//			REFLECT_NOTIFICATIONS()
    END_MSG_MAP()

	CGridCtrl() : m_bEditing(false){}
	~CGridCtrl()
	{
		DeleteAllColumns();
	}
	void DeleteAllColumns() 
	{
		long count = m_columns.size();
		while(count--) delete m_columns[count];
		m_columns.clear();
	}
	
	DWORD OnItemPostPaint( int idCtrl, NMCUSTOMDRAW* pnmcd )
	{
		NMLVCUSTOMDRAW *pCD = (NMLVCUSTOMDRAW*)pnmcd;
		CRect siRect;
/*		GetSubItemRect(pCD->nmcd.dwItemSpec, 3, 0, &siRect);
		CDC dc = pCD->nmcd.hdc;
		COLORREF bkColor =  m_ChannelsVector[pnmcd->lItemlParam].m_color;
		dc.FillSolidRect( &siRect, bkColor );
		dc.Detach();
*/
		return CDRF_DODEFAULT;
	}
	BOOL SubclassWindow(HWND hWnd)
	{
		ATLASSERT(m_hWnd==NULL);
		ATLASSERT(::IsWindow(hWnd));

		BOOL bRet = CWindowImpl<CGridCtrl, CListViewCtrl>::SubclassWindow(hWnd);
		if( bRet ) _Init();
		return bRet;
	}
	void StartEdit(long row=-1,long col=-1) 
	{
		if(m_bEditing) return;
		if(col<0) col = 0;
//		if(row<0) row = m_nSelectedRow;
		if(row<0) return;

//		if(m_dwStyle & GS_EX_READONLY) return;

		SizeControls(SWP_SHOWWINDOW);
		for(long i = 0; i < m_columns.size();i++) 
		{
			CGridColumn* pEditCol = m_columns[i];
			if(!pEditCol->GetReadOnly()) 
			{
				CSize cell(m_nSelectedRow, col);
				pEditCol->SetDisplayText(cell);
				if( col >= 0 && i >= col ) 
				{
					pEditCol->SetFocus();
					col = -1;
				}
			}
		}
		m_bEditing = true;
	}
	
	void SizeControls(DWORD dwSWPFlags = 0) 
	{
		dwSWPFlags |= SWP_NOZORDER;
		for(long i=0;i < m_columns.size(); i++) 
		{
			CRect rc;
			GetSubItemRect( m_nSelectedRow, i, 0, &rc);
			rc.InflateRect(0, 1);
//			GetCellRect(rc, m_nSelectedRow, i, true);
			CGridColumn *col = m_columns[i];
			m_columns[i]->SetWindowPos(rc, dwSWPFlags );
		}
	}
	bool EndEdit(int bAbort, DWORD selData = 0) 
	{
		ATLASSERT(IsWindow());
		if(!m_bEditing) return true;

		if(!bAbort) 
		{

		}
		for(long i = 0; i < m_columns.size(); i++)
		{
			m_columns[i]->SetWindowPos(rcDefault, SWP_HIDEWINDOW|SWP_NOMOVE|SWP_NOZORDER);
			if(!bAbort) 
			{
				CSize cell(m_nSelectedRow, 0);
				m_columns[i]->UpdateGridDisplayText(m_nSelectedRow);

			}
		}
		m_bEditing = false;
		SetFocus();
		return true;
	}
	//message handlers
	LRESULT OnChar(UINT /*uMsg*/, WPARAM nChar, LPARAM lParam, BOOL& /*bHandled*/) 
	{
		if(nChar == VK_F2)
		{
			ATLTRACE("VK_F2");
		}
		return 0;
	}
	LRESULT OnLButtonDblclk(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) 
	{
		CPoint pt;
		pt.x = GET_X_LPARAM(lParam);
		pt.y = GET_Y_LPARAM(lParam);

		CSize cell;
		if( GetCell(pt, cell) )
			StartEdit(cell.cx, cell.cy);
		::SendMessage(GetParent(), uMsg, cell.cx, cell.cy);
		bHandled = FALSE;
		return 0;
	}
	CGridColumn* InsertColumn(int nCol, LV_COLUMN* pColStruct, CONTROLTYPE cType = EDIT_NONE)
	{
		CGridColumn* pCol = new CGridColumn(*this);
		memcpy( pCol, pColStruct, sizeof(LV_COLUMN) );
		m_columns.push_back(pCol);
		pCol->m_nType = cType;
		pCol->CreateEdit();
		CWindowImpl<CGridCtrl, CListViewCtrl>::InsertColumn(nCol, pCol);
		return pCol;
	}
private:
	bool GetCell(CPoint &point, CSize& cell) 
	{
		cell.cx = 0;
		m_nSelectedRow = HitTest( point, NULL );
	
		// Get the top and bottom row visible
		m_nSelectedRow = GetTopIndex();
		int bottom = m_nSelectedRow + GetCountPerPage();
		if( bottom > GetItemCount() )
			bottom = GetItemCount();

		// Get the number of columns
		CHeaderCtrl HdrCtl = GetDlgItem(0);
		int nColumnCount = HdrCtl.GetItemCount();
		
		// Loop through the visible rows
		for( ;m_nSelectedRow <= bottom; m_nSelectedRow++)
		{
			// Get bounding rect of item and check whether point falls in it.
			CRect rect;
			GetItemRect( m_nSelectedRow, &rect, LVIR_BOUNDS );

			if( rect.PtInRect(point) )
			{
				// Now find the column
				for( int colnum = 0; colnum < nColumnCount; colnum++ )
				{
					int colwidth = GetColumnWidth(colnum);
					if( point.x >= rect.left && point.x <= (rect.left + colwidth ) )
					{
						cell.cy = colnum;
						cell.cx = m_nSelectedRow;
						return true;
					}
					rect.left += colwidth;
				}
			}
		}
		return false;
	}
	void _Init()
	{
		SetExtendedListViewStyle(LVS_EX_FULLROWSELECT | LVS_EX_UNDERLINEHOT | LVS_EX_FLATSB);
		m_font = AtlGetDefaultGuiFont();
		m_nSelectedRow = -1;
	}
	bool m_bEditing;
	int m_nSelectedRow;
public:
	std::vector<CGridColumn*> m_columns;
	CFont						m_font;
public:
	
	class CGridCell
	{
	public:
		CGridCell(CONTROLTYPE type, CGridColumn& column) : m_type(type), m_column(column) {}
		~CGridCell(){}
		virtual void SetFocus() {}
		virtual void Create(CFontHandle hFont) 
		{
			m_hWnd = NULL;
		}

		virtual void SetWindowPos(RECT& rc,DWORD dwSWPFlags) 
		{
			if(!m_hWnd) return;
			CWindow wnd(m_hWnd);
			wnd.SetWindowPos(NULL, &rc, dwSWPFlags);
		}
		virtual void SetDisplayText(LPTSTR lpszText){}
		virtual void GetDisplayText(LPTSTR lpszText, int){}
		HWND		m_hWnd;
		virtual void AddComboItem(CString& str, DWORD dwData){}
		virtual void DeleteAllComboItems(){}
		virtual DWORD GetCellData(LPCSTR){return 0;}

	protected:
		CONTROLTYPE	m_type;
		CGridColumn &m_column;
	};
	class CEditText : public CWindowImpl<CEditText,CEdit> 
	{
	public:
		DECLARE_WND_CLASS_EX(NULL,0,-1)

		BEGIN_MSG_MAP(CEditText)
			MESSAGE_HANDLER(WM_CHAR,OnChar)
			MESSAGE_HANDLER(WM_KILLFOCUS,OnKillFocus)
		END_MSG_MAP()

		CEditText(CGridColumn& column) : m_column(column) {}

		LRESULT OnChar(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
			bHandled = FALSE;
			if(wParam==VK_RETURN || wParam==VK_ESCAPE) 
			{
				m_column.m_grid.EndEdit(wParam==VK_ESCAPE);
				bHandled = TRUE;
			}
			return 0;
		}

		LRESULT OnKillFocus(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& bHandled) 
		{
			m_column.m_grid.EndEdit(0);
			bHandled = FALSE;
			return 0;
		}
	protected:
		CGridColumn&	m_column;
	}; 
	class CGridColumn : public tagLVCOLUMNA
	{
	public:
		CGridColumn(CGridCtrl& rGrid) : m_grid(rGrid)
			, m_pCell(0)
		{}
		~CGridColumn()
		{
			if(m_pCell) 
			{
				delete m_pCell;
				m_pCell = NULL;
			}
		}
		void AddComboItem(CString& str, DWORD dwData)
		{
			m_pCell->AddComboItem(str, dwData);
		}
		void DeleteAllComboItems()
		{
			m_pCell->DeleteAllComboItems();
		}

		DWORD GetCellData(LPCSTR txt)
		{
			return m_pCell->GetCellData(txt);
		}
		void UpdateGridDisplayText(int iRow)
		{
			if(iRow < 0 || iRow > m_grid.GetItemCount() )	return;
			char buffer[32] = {'\0'};
			m_pCell->GetDisplayText(buffer, sizeof(buffer) - 1 );
			if(m_nType != EDIT_NONE)
				m_grid.SetItemText(iRow, iSubItem, buffer);
		}
		void SetDisplayText(CSize& rCell)
		{
			char szText[32] = {'\0'};
			CString strContent;
			m_grid.GetItemText( rCell.cx, iSubItem, szText, sizeof(szText) - 1 );
			//iOrder
			m_pCell->SetDisplayText(szText);
		}
		bool GetReadOnly() 
		{
			return (m_nType == EDIT_NONE);
		}
		void SetWindowPos(RECT& rc,DWORD dwSWPFlags) 
		{
			if(m_pCell)
				m_pCell->SetWindowPos(rc,dwSWPFlags);
		}
		void CreateEdit()
		{
			switch(m_nType) 
			{
				case EDIT_NONE:
					m_pCell = new CGridCellNone(*this);
					break;
				case EDIT_TEXT:
					m_pCell = new CGridCellText(*this, m_nAlign);
					break;
				case EDIT_DROPDOWN:
//					m_pCell = new CCellDropdown(*this);
					break;
				case EDIT_DROPDOWNLIST:
					m_pCell = new CGridCellDropdownlist(*this);
					break;
				case EDIT_DATE:
				case EDIT_TIME:
//					m_pCell = new CCellDate(*this,m_nType==EDIT_TIME);
					break;
			}
			if(m_pCell) 
				m_pCell->Create((HFONT)m_grid.m_font);
		}
		void SetFocus() 
		{
			if(m_pCell)			m_pCell->SetFocus();
		}
		CGridCtrl& m_grid;
		ALIGN m_nAlign;
		long					m_nType;
	private:
		CGridCell* m_pCell;
	};

	
	class CGridCellText : public CGridCell 
	{
	public:
		CGridCellText(CGridColumn& column,ALIGN nAlign) : CGridCell(EDIT_TEXT,column), m_wndEdit(column) 
		{}

		virtual void Create(CFontHandle hFont) {
			DWORD dwFlags = 0;
			if(m_column.m_nAlign==RIGHT)
				dwFlags |= ES_RIGHT;
			else if(m_column.m_nAlign==CENTER)
				dwFlags |= ES_CENTER;

			m_hWnd = m_wndEdit.Create(m_column.m_grid,rcDefault,NULL,WS_CHILD|WS_TABSTOP|ES_AUTOHSCROLL|dwFlags,WS_EX_CLIENTEDGE);
			m_wndEdit.SetFont(hFont);
		}

		virtual void SetFocus() 
		{
			m_wndEdit.SetFocus();
			m_wndEdit.SetSel(0,-1);
		}
		void GetDisplayText(LPTSTR lpszText, int Length)
		{
			::GetWindowText(m_wndEdit, lpszText, Length);
		}

		void SetDisplayText(LPTSTR lpszText)
		{
			::SetWindowText(m_wndEdit, lpszText);
		}
		void AddComboItem(CString& str, DWORD dwData){}
		void DeleteAllComboItems(){}
	protected:
		CEditText	m_wndEdit;
	};
	class CGridCellNone : public CGridCell 
	{
	public:
		CGridCellNone(CGridColumn& column) : CGridCell(EDIT_NONE,column) {}
	};


	class CEditDropdownlist : public CWindowImpl<CEditDropdownlist, CComboBox> 
	{
	public :
		DECLARE_WND_CLASS_EX("myCombo",0,-1)

		BEGIN_MSG_MAP(CEditDropdownlist)
			MESSAGE_HANDLER(WM_KEYDOWN,OnKeyDown)
			MESSAGE_HANDLER(WM_DESTROY,OnDestroy)
			MESSAGE_HANDLER(WM_KILLFOCUS, OnKillFocus)
			MESSAGE_HANDLER(WM_CHAR, OnChar)
			REFLECTED_COMMAND_CODE_HANDLER(CBN_SELCHANGE,OnSelChange)
			DEFAULT_REFLECTION_HANDLER()
		END_MSG_MAP()

		CEditDropdownlist(CGridColumn& column) : m_column(column) {}
		


		LRESULT OnKillFocus(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
		{
			int iSel = GetCurSel();
			m_column.m_grid.EndEdit(0, GetItemData(iSel));
			return 0;
		}
		LRESULT OnKeyDown(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& bHandled) 
		{
			bHandled = FALSE;
			if(!GetDroppedState() && (wParam==VK_RETURN || wParam==VK_ESCAPE)) 
			{
				int iSel = GetCurSel();
				m_column.m_grid.EndEdit(wParam==VK_ESCAPE, GetItemData(iSel));
				bHandled = TRUE;
			}
			return 0;
		}

		LRESULT OnChar(UINT /*uMsg*/, WPARAM nChar, LPARAM /*lParam*/, BOOL& /*bHandled*/)
		{
			if( nChar == VK_ESCAPE || nChar == VK_RETURN)
			{
				int iSel = GetCurSel();
				m_column.m_grid.EndEdit(nChar == VK_ESCAPE, GetItemData(iSel));
			}
			return 0;
		}
		LRESULT OnDestroy(UINT,WPARAM,LPARAM,BOOL& bHandled) {
			bHandled = FALSE;
			ResetContent();
			return 0;
		}

		LRESULT OnSelChange(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& bHandled) {
//			m_column.ValueModified();
			return 0;
		}

	protected:
		CGridColumn&	m_column;
	};

	class CGridCellDropdownlist : public CGridCell 
	{
	public:
		CGridCellDropdownlist(CGridColumn& column) : CGridCell(EDIT_DROPDOWNLIST,column), m_wndEdit(column) 
		{}

		virtual void Create(CFontHandle hFont) 
		{
			m_hWnd = m_wndEdit.Create(m_column.m_grid,rcDefault,NULL,WS_CHILD|WS_TABSTOP|WS_VSCROLL|CBS_AUTOHSCROLL|CBS_DROPDOWNLIST,0);
			m_wndEdit.SetFont(hFont);
		}

		void SetDisplayText(LPTSTR lpszText)
		{
			int iFounded = m_wndEdit.FindString(-1, lpszText);
//			if( iFounded < 0 )
//				m_wndEdit.SetCurSel((strlen(lpszText) > 0)? m_wndEdit.AddString(lpszText):0 );

//			else
				m_wndEdit.SetCurSel( iFounded );
//			::SetWindowText(m_wndEdit, lpszText);
		}
		void GetDisplayText(LPTSTR lpszText, int Length)
		{
			::GetWindowText(m_wndEdit, lpszText, Length);
		}
		virtual void SetWindowPos(RECT& rc,DWORD dwSWPFlags) 
		{
			OffsetRect(&rc, 0, -2);
			if(dwSWPFlags & SWP_SHOWWINDOW)  
			{
//				m_wndEdit.ResetContent();
//				int iItem = m_wndEdit.AddString("sergey");
//				m_wndEdit.SetItemData(iItem, 13);
			}
			rc.bottom += 100;
			CGridCell::SetWindowPos(rc, dwSWPFlags);
//			m_wndEdit.SetCurSel(0);
		}


		void AddComboItem(CString& str, DWORD dwData)
		{
			int iAdded = m_wndEdit.AddString(str);	
			m_wndEdit.SetItemData(iAdded, dwData);
		}
		void DeleteAllComboItems()
		{	m_wndEdit.ResetContent();	}
		virtual void SetFocus() 
		{
			m_wndEdit.SetFocus();
		}

		DWORD GetCellData(LPCSTR txt)
		{
			return m_wndEdit.FindString(-1, txt);
		}
	protected:
		CEditDropdownlist	m_wndEdit;
	};


};
#endif // !defined(AFX_MYGIRD_H__INCLUDED_)
