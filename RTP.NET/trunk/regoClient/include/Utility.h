// $Header: /rtpClient/include/Utility.h 5     11.07.08 2:01 Sergey Zhmetko $ 
// Utlity.h: interface for the CUtlity class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_UTLITY_H__208DEFAE_7674_4ED8_8E55_7CB5B5F8F703__INCLUDED_)
#define AFX_UTLITY_H__208DEFAE_7674_4ED8_8E55_7CB5B5F8F703__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CUtility  
{
public:
	static std::vector<std::string> explode(char* pBuffer, const char* pDelims);
	CUtility(const char*);
	CUtility();
	CUtility(short int cID, short int scale);
	std::string FormatString() const ;
	std::string FormatDBString() const ;
	bool operator==(const CUtility& c)const {return m_channelID > 0 && m_channelID == c.m_channelID;}
private:
	void Parse(const char*);
public:
	short int m_channelID;
	short int m_digits;
	short int m_scale;
	double m_value;
	std::string m_strPrefix;
	std::string m_strPostfix;
};

#endif // !defined(AFX_UTLITY_H__208DEFAE_7674_4ED8_8E55_7CB5B5F8F703__INCLUDED_)
