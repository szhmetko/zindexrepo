// $Header: /rtpClient/include/WorfFlowBuilder.h 5     1.05.08 10:56 Sergey Zhmetko $ 
// WorfFlowBuilder.h: interface for the CWorkFlowBuilder class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WORFFLOWBUILDER_H__89ED7CC6_B39F_46FA_ACE4_0C4A3242E0BF__INCLUDED_)
#define AFX_WORFFLOWBUILDER_H__89ED7CC6_B39F_46FA_ACE4_0C4A3242E0BF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CWorkFlowCtrl;

class CWorkFlowBuilder  
{
public:
	void SetPosition(LPRECT);
	CWorkFlowBuilder(CSession&, HWND);
	virtual ~CWorkFlowBuilder();
	void SetChannelValues(std::map<long, double>);
	void Run(bool bRun);
private:

	CWorkFlowCtrl* m_pCtrl;
	HWND		m_hWnd;
};

#endif // !defined(AFX_WORFFLOWBUILDER_H__89ED7CC6_B39F_46FA_ACE4_0C4A3242E0BF__INCLUDED_)
