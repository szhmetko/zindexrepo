//$Header: /rtpClient/include/WFDRow.h 7     11.07.08 2:01 Sergey Zhmetko $
// WFDRow.h: interface for the CGenericWFDRow class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GENERICWFDROW_H__31E15DB5_F7B1_432A_9C9C_AD21C4B2BF15__INCLUDED_)
#define AFX_GENERICWFDROW_H__31E15DB5_F7B1_432A_9C9C_AD21C4B2BF15__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "WFDCell.h"

class CWFRow : public CGenericWFCell
		, std::list<CWFRowCell>
		, CCommandSupplier<CWFRow, true, true>
{
	typedef CCommandSupplier<CWFRow, true, true> cmdSupplierClass;
public:
	friend class CWFDesigner;
	enum CELL_TYPE {ctBMP = 0, ctVALUE = 1};
	CWFRow(CWFDesigner&, int iCellWidth = -1);
	virtual ~CWFRow();
	size_t InsertCell(CELL_TYPE = ctVALUE, int = -1);
	size_t InsertCell(const CWorkFlowCellAccessor&, CELL_TYPE = ctVALUE, int = -1);
	int Load();
	int Save();

	BEGIN_MSG_MAP(CWFRow)
		MESSAGE_HANDLER(WM_CREATE, OnCreate)
		MESSAGE_HANDLER(WM_COMMAND, OnCommand)
		MESSAGE_HANDLER(WM_WINDOWPOSCHANGED, OnWndPosChanged)
		MESSAGE_HANDLER(WM_KEYUP, OnKeyUp)
		MESSAGE_HANDLER(WM_CHILDWND_DESTROYED, OnChildWndDestroyed)
		CHAIN_MSG_MAP(CGenericWFCell)
		CHAIN_MSG_MAP(cmdSupplierClass)
	END_MSG_MAP()
	LRESULT OnKeyUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnChildWndDestroyed(UINT, WPARAM wParam, LPARAM, BOOL&);
	LRESULT OnWndPosChanged(UINT, WPARAM wParam, LPARAM, BOOL&);
	LRESULT OnCreate(UINT, WPARAM wParam, LPARAM, BOOL&);
	LRESULT OnCommand(UINT, WPARAM wParam, LPARAM, BOOL&);
	
	CWFDesigner & m_parent;
protected:
	void Layout();
	int m_iCellWidth;
};

#endif // !defined(AFX_GENERICWFDROW_H__31E15DB5_F7B1_432A_9C9C_AD21C4B2BF15__INCLUDED_)
