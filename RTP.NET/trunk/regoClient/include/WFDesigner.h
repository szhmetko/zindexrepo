//$Header: /rtpClient/include/WFDesigner.h 5     11.07.08 2:01 Sergey Zhmetko $
// WFDesigner.h: interface for the CWFDesigner class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WFDESIGNER1_H__032FCA74_646D_4987_A5EB_96168F0F237A__INCLUDED_)
#define AFX_WFDESIGNER1_H__032FCA74_646D_4987_A5EB_96168F0F237A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "WFDRow.h"
#include "resource.h"
#include "WFImageLibrary.h"

class CWFDesigner : public CGenericWFCell
		, std::list<CWFRow>
		, CCommandSupplier<CWFDesigner, true, true>
{
	typedef CCommandSupplier<CWFDesigner, true, true> cmdSupplierClass;
public:
	CWFDesigner(int iRowHeight = -1);
	virtual ~CWFDesigner();
	size_t InsertRow(int = -1);
	int Load();
	int Save();

	BEGIN_MSG_MAP(CWFDesigner)
		MESSAGE_HANDLER(WM_WINDOWPOSCHANGED, OnWndPosChanged)
		MESSAGE_HANDLER(WM_CREATE, OnCreate)
		MESSAGE_HANDLER(WM_COMMAND, OnCommand)
		MESSAGE_HANDLER(WM_KEYUP, OnKeyUp)
		MESSAGE_HANDLER(WM_CHILDWND_DESTROYED, OnChildWndDestroyed)
		CHAIN_MSG_MAP(CGenericWFCell)
		CHAIN_MSG_MAP(cmdSupplierClass)
	END_MSG_MAP()
	LRESULT OnChildWndDestroyed(UINT, WPARAM wParam, LPARAM, BOOL&);
	LRESULT OnCreate(UINT, WPARAM wParam, LPARAM, BOOL&);
	LRESULT OnCommand(UINT, WPARAM wParam, LPARAM, BOOL&);
	LRESULT OnWndPosChanged(UINT, WPARAM wParam, LPARAM, BOOL&);
	LRESULT OnKeyUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	CWFImageCache m_imgCache;
protected:
	void Layout();
	int m_iRowHeight;
};

#endif // !defined(AFX_WFDESIGNER1_H__032FCA74_646D_4987_A5EB_96168F0F237A__INCLUDED_)
