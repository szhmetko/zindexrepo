// $Header: /rtpClient/include/WFDCell.h 7     11.07.08 2:00 Sergey Zhmetko $ 
// WFCell.h: interface for the CWFCell class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WFCELL_H__F2B9E706_EE18_458E_8B48_E219FFB432FD__INCLUDED_)
#define AFX_WFCELL_H__F2B9E706_EE18_458E_8B48_E219FFB432FD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "utility.h"
#include <deque>
class CMainDlg;

static UINT NEAR WM_CHILDWND_DESTROYED = RegisterWindowMessage("CHILDWND_DESTROYED");

typedef CWinTraits<   WS_POPUP	
					| WS_VISIBLE	
					| WS_TABSTOP
					/*| WS_BORDER
					| WS_CAPTION
					| WS_SYSMENU
					| WS_TILED*/
					, 0> CCellTraits;

typedef CWinTraits<   WS_CHILD	
					| WS_BORDER
					/*| WS_VISIBLE	
					| WS_CAPTION
					| WS_SYSMENU
					| WS_TILED*/
					, 0> CCellToolBarTraits;

class CGenericWFCell : public CWindowImpl<CGenericWFCell, CWindow, CCellTraits>
				, public CWorkFlowCellAccessor
{
public:
    BEGIN_MSG_MAP(CGenericWFCell)
		MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBgnd)
		MESSAGE_HANDLER(WM_SYSCOMMAND, OnSysCommand)
		MESSAGE_HANDLER(WM_KEYUP, OnKeyUp)
		MESSAGE_HANDLER(WM_ACTIVATE, OnActivate)
		REFLECT_NOTIFICATIONS()
    END_MSG_MAP()

private:
	LRESULT OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

protected:
	LRESULT OnKeyUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnEraseBgnd(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnActivate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
public:
	virtual CGenericWFCell* clone() const{return new CGenericWFCell(*this);}
	virtual int Load(){return 0;}
	virtual int Save(){return 0;}
};


class CWFRowCell
{
public:
	//default ctor
	CWFRowCell() : p(0), use(new size_t(1))	{}
	CWFRowCell(const CGenericWFCell& c) : p(c.clone()), use(new size_t(1))
	{}
	CWFRowCell(const CWFRowCell& h) : p(h.p), use(h.use) {++*use;}
	~CWFRowCell(){decr_use();}

	bool operator==(const CWFRowCell& c)const {return p->m_hWnd == c.p->m_hWnd;}
	CWFRowCell& operator=(CWFRowCell& src)
	{
		*++src.use;
		decr_use();
		p = src.p;
		use = src.use;
		return *this;
	}
	CGenericWFCell* operator->() const {if(p)return p; else throw std::logic_error("unbound CWFRowCell");}
	const CGenericWFCell& operator*() const {if(p)return *p; else throw std::logic_error("unbound CWFRowCell");}
private:
	void decr_use(){if(0== --*use){delete p; delete use;}}
	CGenericWFCell*	p;	//pointer to shared item
	size_t* use;
};

class CWFCellToolBar : public CWindowImpl<CWFCellToolBar, CWindow, CCellToolBarTraits>
					, protected std::list<CButton>
{
public:
	CWFCellToolBar();

	DECLARE_WND_CLASS(_T("WFCELLTOOLBAR"))
 
    BEGIN_MSG_MAP(CWFCellToolBar)
		MESSAGE_HANDLER(WM_CREATE, OnCreate)
		MESSAGE_HANDLER(WM_COMMAND, OnCommand)
		MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBgnd)
    END_MSG_MAP()
	HWND Construct(HWND, RECT&, const std::list<UINT>&);
	LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnCommand(UINT, WPARAM, LPARAM, BOOL&);
	LRESULT OnEraseBgnd(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	std::list<UINT> m_commands;
};

template <class T, bool t_bToolBar = true, bool t_bCtxMenu = false>
class CCommandSupplier
{
public:
	BEGIN_MSG_MAP(CCommandSupplier<T>)
		MESSAGE_HANDLER(WM_CONTEXTMENU, OnContextMenu)
		MESSAGE_HANDLER(WM_ACTIVATE, OnActivate)
		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_CREATE, OnCreate)
		MESSAGE_HANDLER(WM_WINDOWPOSCHANGED, OnWndPosChanged)
	END_MSG_MAP()
	CCommandSupplier() :
			m_tbPosHoriz(true)
			, m_tbAutoHide(true)
	{}

	LRESULT OnActivate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		WORD curState = LOWORD(wParam);
		if(m_tbAutoHide && WA_INACTIVE == curState && m_tb.IsWindow())m_tb.ShowWindow(SW_HIDE);
		bHandled = FALSE;
		return 0;
	}
	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		T* pT = static_cast<T*>(this);
		if(m_tbAutoHide && pT->m_hWnd == ::GetActiveWindow())
		{
			CPoint pt(LOWORD(lParam), HIWORD(lParam));
			CRect rcToolBar;pT->GetClientRect(&rcToolBar);
			rcToolBar.top = rcToolBar.bottom - 25;
			m_tb.ShowWindow(rcToolBar.PtInRect(pt) ? SW_SHOWNOACTIVATE : SW_HIDE);
		}
		return 0;
	}
	LRESULT OnWndPosChanged(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if(t_bToolBar && m_tb.IsWindow())
		{
			T* pT = static_cast<T*>(this);
			CRect rcTB;
			pT->GetClientRect(rcTB);
			rcTB.top = rcTB.bottom - 20;
			rcTB.left = rcTB.right - 22 * m_cmds.size();
			m_tb.MoveWindow(&rcTB);
		}
		return 0;
	}
	LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if(t_bToolBar)
		{
			T* pT = static_cast<T*>(this);
			CRect rcClient;
			pT->GetClientRect(rcClient);
			rcClient.top = rcClient.bottom - 20;
			m_tb.Construct(pT->m_hWnd, rcClient, m_cmds);
			m_tb.ShowWindow(m_tbAutoHide ? SW_HIDE : SW_SHOWNOACTIVATE);
		}
		return 0;
	}
	LRESULT OnContextMenu(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		using namespace std;
		if(t_bCtxMenu)
		{
			T* pT = static_cast<T*>(this);
			CPoint pt( GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
			CMenu menu;
			menu.CreatePopupMenu();
			list<UINT>::const_iterator citer = m_cmds.begin();
			int iRow = 0;
			while(citer != m_cmds.end())
			{
				UINT uID = *citer;
				menu.AppendMenu(MF_STRING , uID, LoadStringPair(uID).first.c_str());
				citer++;
			}
			menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, pt.x, pt.y, pT->m_hWnd);
		}
		return 0;
	}
public:
	static std::pair<std::string, std::string>	LoadStringPair(UINT uID)
	{
		using namespace std;
		pair<string, string> pr("???", "???");
		char buffer[512] = {0};
		int ret = ::LoadString(_Module.GetResourceInstance(), uID, buffer, sizeof(buffer)-1);
		if(ret > 0 )
		{
			std::vector<std::string> strs = CUtility::explode(buffer, "\n");
			if(!strs.empty())		pr.first = strs[0];
			if(strs.size() > 1)		pr.second = strs[1];
		}
		return pr;
	}
protected:
	std::list<UINT>	m_cmds;
	CWFCellToolBar m_tb;
	bool m_tbPosHoriz;
	bool m_tbAutoHide;
};

class CValuesWFCell : public CGenericWFCell
		, CCommandSupplier<CValuesWFCell, true, true>
		, std::list <CUtility>
{
	typedef CCommandSupplier<CValuesWFCell, true, true> cmdSupplierClass;
	friend class CWFRow;
public:
	CValuesWFCell(CWFRow& r) : m_parent(r){}
	int Save();
//	DECLARE_WND_CLASS(_T("VALWFCELLCLASS"))
    BEGIN_MSG_MAP(CValuesWFCell)
		MESSAGE_HANDLER(WM_CREATE, OnCreate)
		MESSAGE_HANDLER(WM_COMMAND, OnCommand)
		MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBkgnd)
		CHAIN_MSG_MAP(CGenericWFCell)
		CHAIN_MSG_MAP(cmdSupplierClass)
    END_MSG_MAP()
	
	CGenericWFCell* clone() const{return new CValuesWFCell(*this);}
protected:	
	LRESULT OnCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnEraseBkgnd(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

private:
	void _DrawText(CDCHandle& dc);
	CWFRow&	m_parent;
};

class CBMPWFCell : public CGenericWFCell
		, CCommandSupplier<CBMPWFCell, true, true>
		, std::deque<HBITMAP>
{
	typedef std::deque<HBITMAP> baseContainer;
	typedef CCommandSupplier<CBMPWFCell, true, true> cmdSupplierClass;
	friend class CWFRow;
public:
	CBMPWFCell(CWFRow& r);
	~CBMPWFCell();
	int Save();
	void clear();
    BEGIN_MSG_MAP(CBMPWFCell)
		MESSAGE_HANDLER(WM_CREATE, OnCreate)
		MESSAGE_HANDLER(WM_COMMAND, OnCommand)
		MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBkgnd)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		CHAIN_MSG_MAP(CGenericWFCell)
		CHAIN_MSG_MAP(cmdSupplierClass)
    END_MSG_MAP()
	
	CGenericWFCell* clone() const{return new CBMPWFCell(*this);}

	void AddImage(long);
	void RemoveCurrentImage();
protected:	
	LRESULT OnCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnEraseBkgnd(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
private:
	std::list<long> m_imgIDsList;
	CWFRow&	m_parent;
	CBitmap		m_VirtScreenBitmap;
	CDC	m_VirtScreenDC;
};
#endif // !defined(AFX_WFCELL_H__F2B9E706_EE18_458E_8B48_E219FFB432FD__INCLUDED_)
