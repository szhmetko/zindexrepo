// $Header: /rtpClient/PiecesLists.h 5     12.10.08 20:40 Sergey Zhmetko $ 
// PiecesLists.h: interfaces for the CPiecesShortList and CPiecesFullList classes
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PIECESLISTS_H__INCLUDED_)
#define AFX_PIECESLISTS_H__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <atlcrack.h>
class CDataContainer;
class CPiecesShortList : public CWindowImpl<CPiecesShortList, CListViewCtrl>,
                       public CCustomDraw<CPiecesShortList>
{
    BEGIN_MSG_MAP(CPiecesShortList)
        CHAIN_MSG_MAP_ALT(CCustomDraw<CPiecesShortList>, 1)
        DEFAULT_REFLECTION_HANDLER()
    END_MSG_MAP()
	DWORD OnPrePaint(int idCtrl, LPNMCUSTOMDRAW lpNMCD);
	DWORD OnItemPrePaint(int idCtrl, LPNMCUSTOMDRAW lpNMCD);
public:
	void Fill();
	void SetColumns();
	CString GetModel(long lPieceID);
};

class CPiecesFullList : public CWindowImpl<CPiecesFullList, CListViewCtrl>,
                       public CCustomDraw<CPiecesFullList>
{
    BEGIN_MSG_MAP(CPiecesFullList)
		MESSAGE_HANDLER(WM_CONTEXTMENU, OnContextMenu)
		MESSAGE_HANDLER(WM_SIZE, OnSize)
		MESSAGE_HANDLER(WM_COMMAND, OnCommand)
        REFLECTED_NOTIFY_CODE_HANDLER_EX(LVN_GETDISPINFO, OnGetDispInfo)
        CHAIN_MSG_MAP_ALT(CCustomDraw<CPiecesFullList>, 1)
        DEFAULT_REFLECTION_HANDLER()
    END_MSG_MAP()
	LRESULT OnContextMenu(UINT , WPARAM , LPARAM, BOOL& bHandled);
	LRESULT OnSize(UINT , WPARAM , LPARAM, BOOL& bHandled);
	LRESULT OnCommand(UINT , WPARAM , LPARAM, BOOL& bHandled);
	DWORD OnPrePaint(int idCtrl, LPNMCUSTOMDRAW lpNMCD);
	DWORD OnItemPrePaint(int idCtrl, LPNMCUSTOMDRAW lpNMCD);
	LRESULT OnGetDispInfo(LPNMHDR pnmh);
public:
//	void Fill();
	CPiecesFullList();
	void SetColumns(int = 6);
	void DataBind(CDataContainer*);
private:
	char* _FindValForSubitem(char* buf, unsigned int uI, unsigned int uSi);
	CHANNELS_MAP m_localChlMap;
	int m_iLastMenuItem;
	CDataContainer* m_pData;
};
#endif // !defined(AFX_PIECESLISTS_H__INCLUDED_)
