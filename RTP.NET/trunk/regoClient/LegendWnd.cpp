// $Header: /rtpClient/LegendWnd.cpp 4     13.04.08 17:22 Sergey Zhmetko $ 
// LegendWnd.cpp: implementation of the CLegendWnd class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include "LegendWnd.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLegendWindow::CLegendWindow( UINT uWidth, std::string strCaption, CPoint &ptLeftTop) :
		m_uWidth(uWidth) 
		, m_strCaption(strCaption)
		, m_ptLeftTop(ptLeftTop)
		, m_clrText( RGB(255, 255, 255) )
		, m_clrBg( RGB(154, 130, 126) )
{}

CLegendWindow::~CLegendWindow()
{}

void	CLegendWindow::RecalcUpdate(){	_Layout();Invalidate();	}

void	CLegendWindow::SetCaption(std::string strCaption, bool bRedraw)
{
	m_strCaption = strCaption;
	if(bRedraw)
		RecalcUpdate();
}


LRESULT CLegendWindow::OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = (wParam == SC_MINIMIZE) || (wParam == SC_MAXIMIZE);
	return 0;
}

LRESULT CLegendWindow::OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	::SendMessage(GetParent(), WM_COMMAND, ID_VIEW_LEGEND, 0);
    return 0;
}

LRESULT CLegendWindow::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    PostQuitMessage(0);
    return 0;
}

LRESULT CLegendWindow::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	HWND hwnd = m_but.Create(*this, CWindow::rcDefault, 0, 
		WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON | BS_FLAT, 0, IDC_CHANNELSETTINGS);
	::ShowWindow(m_but, SW_SHOW);

	m_rcWnd.top		= m_ptLeftTop.y;
	m_rcWnd.left	= m_ptLeftTop.x;
	_Layout();
	CenterWindow();
	return 0;
}

LRESULT CLegendWindow::OnPaint(UINT, WPARAM wParam, LPARAM, BOOL& bHandled)
{
	PAINTSTRUCT ps;
	CDC dc = BeginPaint(&ps);

	_DrawBkgnd(dc);
	for(int i = 0; i < m_itemsVector.size(); ++i )
	{
		CHMAP_ITER iter = g_ChannelsMap.find(m_itemsVector[i]);
		if( iter != g_ChannelsMap.end() )
		{
			CChannelSettingsAccessor &rcac = iter->second;
			_DrawItem(dc, i, rcac);
		}
	}

	EndPaint(&ps);
	bHandled = TRUE;

	return 0;
}

bool CLegendWindow::CreateAndDisplay(HWND hParent, std::string strCaption)
{
	m_strCaption = strCaption;
	m_rcWnd.left = m_ptLeftTop.x;
	m_rcWnd.top = m_ptLeftTop.y;
	if ( NULL == Create ( hParent, m_rcWnd, 0) )
	{
		ATLTRACE(_T("Legend Window creation failed!\n"));
		return false;
	}
	SetWindowText( m_strCaption.c_str() );
	ShowWindow(SW_SHOW);
	UpdateWindow();
	
	return true;
}

void CLegendWindow::EraseAll(bool bRedraw)
{
	m_itemsVector.erase( m_itemsVector.begin(), m_itemsVector.end() );
	if(bRedraw)
		RecalcUpdate();
}

int CLegendWindow::AddItem(int iChlNum, bool bRedraw)
{
	m_itemsVector.push_back(iChlNum);
	if(bRedraw)
		RecalcUpdate();
	return m_itemsVector.size();
}

void CLegendWindow::_DrawBkgnd(CDC& dc)
{
	RECT clRect;
	GetClientRect(&clRect);
	dc.FillSolidRect(clRect.left, clRect.top, clRect.right, clRect.bottom, m_clrBg );
}

void CLegendWindow::_DrawItem(CDC& dc, int iOffset, CChannelSettingsAccessor &cac)
{
	const int _frameWidth = 1;
	CRect	clntRect;
	GetClientRect(&clntRect);
	CRect rcBound = clntRect;
	rcBound.top = clntRect.top/* + 20*/ + iOffset*ITEMHEIGHT + 5;	//top indent
	rcBound.bottom = rcBound.top + ITEMHEIGHT;
	rcBound.right = rcBound.left + ITEMHEIGHT;

	rcBound.OffsetRect(3, 0);	//right indent
	
	CRect rcText = rcBound;

	rcBound.InflateRect(-_frameWidth*2, -_frameWidth*2 );
	dc.FillSolidRect(&rcBound, RGB(255, 255, 255) );
	rcBound.InflateRect(-_frameWidth, -_frameWidth );
	dc.FillSolidRect(&rcBound, cac.m_color );

	rcText.left = rcBound.right + 6;
	rcText.right = m_rcWnd.right;	//ok. all done

	CFont TextFont;
	LOGFONT lf;
	memset(&lf, 0, sizeof(LOGFONT));
	strcpy(lf.lfFaceName, "Arial Cyr");
	lf.lfHeight = 12;
	TextFont.CreateFontIndirect(&lf);

	HFONT oldFont = dc.SelectFont(TextFont);
	COLORREF oldClr = dc.GetTextColor();

	dc.SetTextColor( m_clrText );
	
	dc.FillSolidRect(&rcText, m_clrBg );
	dc.DrawText(cac.m_descr, -1, &rcText, DT_LEFT | DT_VCENTER 
		| DT_WORDBREAK | DT_WORD_ELLIPSIS);
	if (oldFont) dc.SelectFont(oldFont);
	if (oldClr) dc.SetTextColor( oldClr );
}
void CLegendWindow::_Layout()
{
	SetWindowText(m_strCaption.c_str());
	GetWindowRect(m_rcWnd);

	m_rcWnd.bottom = 40/*title bar height?*/ + m_rcWnd.top + m_itemsVector.size() * ITEMHEIGHT;
	m_rcWnd.right = m_rcWnd.left + m_uWidth;
	
	MoveWindow(&m_rcWnd);
}
