// $Header: /rtpClient/PiecesLists.cpp 5     12.10.08 20:40 Sergey Zhmetko $ 
// PiecesLists.cpp: implementations of the CPiecesShortList and CPiecesFullList classes.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include "PiecesLists.h"
#include "config.h"
#include "ApproxRanges.h"
#include "DataContainer.h"
#include <math.h>

extern CHANNELS_MAP	g_ChannelsMap;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
DWORD CPiecesShortList::OnPrePaint(int idCtrl, LPNMCUSTOMDRAW lpNMCD)
{
	return CDRF_NOTIFYITEMDRAW;
}

DWORD CPiecesShortList::OnItemPrePaint(int idCtrl, LPNMCUSTOMDRAW pnmcd)
{
	NMLVCUSTOMDRAW&  rCD = *(NMLVCUSTOMDRAW*)pnmcd;
	rCD.clrTextBk = (pnmcd->dwItemSpec  % 2) ? RGB(255, 232, 232) : RGB(232, 255, 232);
	return 0;
}

CString CPiecesShortList::GetModel(long lPieceID)
{
	using namespace dbutils;

	CString strModel;
	//strModel.Format("piece_id=%ld", lPieceID);
	strModel = _T("����");
	CDBSession locSession;
	HResult hres;
	try
	{
		hres = locSession.Open();
		CCommand<CAccessor<CCableDescriptionAccessor> >	cdrst;
		cdrst.m_piece_id = lPieceID;
		hres = cdrst.Open(locSession, CConfig::GetValue("cabledescription/select").c_str());
		while( cdrst.MoveNext() == S_OK )
		{
			if( CString(cdrst.m_paramName) == _T("���") )
			{
				strModel = cdrst.m_paramVal;
				break;
			}
		}
		cdrst.Close();
		locSession.Close();
		return strModel;
	}
	catch(HresError he)
	{
		::MessageBox(0, he.what().c_str(), "", MB_OK);
	}	
	
	return strModel;
}

void CPiecesShortList::Fill()
{
	ListView_SetExtendedListViewStyle(m_hWnd,
		LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES | LVS_EX_HEADERDRAGDROP| LVS_EX_FLATSB );

	DeleteAllItems();
	while( DeleteColumn(0) )	{}
	SetRedraw(FALSE);
	SetColumns();
	using namespace dbutils;
	CDBSession locSession;
	HResult hres;
	try
	{
		LV_ITEM			lvitem;
		CString szCell;
		CString lblStart, lblStop;
		lblStart.LoadString(IDS_START_LIST_LABEL);
		lblStop.LoadString(IDS_STOP_LIST_LABEL);
		long RowNum = 0;
		const char* dateFormat = "%02d-%02d-%02d %02d:%02d %02d";

		hres = locSession.Open();
		CCommand<CAccessor<CPiecesDetailAccessor> >rst;
		hres = rst.Open(locSession, CConfig::GetValue("cablepieces/select").c_str());
		while( rst.MoveNext() == S_OK )
		{
			lvitem.mask = LVIF_TEXT | LVIF_PARAM;
			lvitem.iItem = RowNum++;
			lvitem.iSubItem = 0;
			lvitem.lParam = (LPARAM)rst.m_recid;

			szCell = GetModel(rst.m_recid);
			if( szCell.IsEmpty() )	szCell = lblStart;

			lvitem.pszText = szCell.GetBuffer(0);

			InsertItem(&lvitem);

			lvitem.mask = LVIF_TEXT | 0;
			lvitem.iSubItem = 1;
			lvitem.pszText = "1";
			SetItem(&lvitem);


			SYSTEMTIME DT;
			VariantTimeToSystemTime(rst.m_dtmon, &DT);
			szCell.Format(dateFormat, DT.wDay, DT.wMonth, DT.wYear - 2000, DT.wHour, DT.wMinute, DT.wSecond);
			lvitem.iSubItem = 2;
			lvitem.pszText = szCell.GetBuffer(0);
			SetItem(&lvitem);//	if (iSubItem == 0)

	///second row
			lvitem.mask = LVIF_TEXT | LVIF_PARAM;
			lvitem.iItem = RowNum++;
			lvitem.iSubItem = 0;
			lvitem.lParam = (LPARAM)rst.m_recid;


			lvitem.pszText = "";

			InsertItem(&lvitem);

			lvitem.mask = LVIF_TEXT | 0;
			lvitem.iSubItem = 1;
			szCell = _T("");

			if(rst.m_length)	
				szCell.Format(_T("%ld"), rst.m_length);
			lvitem.pszText = szCell.GetBuffer(0);
			SetItem(&lvitem);


			time_t tOff = CDBSession::VariantTimeToUnixTime(rst.m_dtmoff);
			time_t tOn = CDBSession::VariantTimeToUnixTime(rst.m_dtmon);
			if( tOn >= tOff )
			{
				szCell = "";
			}
			else
			{
				VariantTimeToSystemTime(rst.m_dtmoff, &DT);
				szCell.Format(dateFormat, DT.wDay, DT.wMonth, DT.wYear - 2000, DT.wHour, DT.wMinute, DT.wSecond);
			}

			lvitem.iSubItem = 2;
			lvitem.pszText = szCell.GetBuffer(0);
			SetItem(&lvitem);//	if (iSubItem == 0)

		}
		SetRedraw(TRUE);

		rst.Close();
		locSession.Close();
	}
	catch(HresError he)
	{
		::MessageBox(0, he.what().c_str(), "", MB_OK);
	}

	SelectItem(GetItemCount() - 1);
	EnsureVisible(GetItemCount() - 1, FALSE);
}

void CPiecesShortList::SetColumns()
{
	const int iCols = 3;

	TCHAR rgtsz[iCols][32] = {_T("����/����"), _T("������"), _T("�����")};
	LV_COLUMN		lvcolumn;
	CRect rect;
	GetWindowRect( &rect );

	for(int i = 0; i < iCols; i++)  
	{
		lvcolumn.mask = LVCF_FMT | LVCF_SUBITEM | LVCF_TEXT | LVCF_WIDTH | LVCF_ORDER;
		lvcolumn.fmt = LVCFMT_LEFT;
		lvcolumn.pszText = rgtsz[i];
		lvcolumn.iSubItem = i;
		lvcolumn.iOrder = i;
		if(i == 0)
			lvcolumn.cx = 100;//rect.Width() / iCols;  
		else if (i == 1)
			lvcolumn.cx = 45;
		else if (i == 2)
			lvcolumn.cx = 100;
		InsertColumn(i, &lvcolumn);  
	}
}

CPiecesFullList::CPiecesFullList() : m_localChlMap(g_ChannelsMap)
			, m_pData(0)
{
	for( CHMAP_ITER ch_iter = m_localChlMap.begin(); 
		ch_iter != m_localChlMap.end(); ++ch_iter )
	{
		CChannelSettingsAccessor& cac = ch_iter->second;
		if( strcmp(cac.m_fname, _T("vlin")) == 0 )
			cac.bVisibleCol = true;

		if( strcmp(cac.m_fname, _T("meter")) == 0 )
			cac.bVisibleCol = true;

	}
}

DWORD CPiecesFullList::OnPrePaint(int idCtrl, LPNMCUSTOMDRAW lpNMCD)
{
	return CDRF_NOTIFYITEMDRAW;
}

DWORD CPiecesFullList::OnItemPrePaint(int idCtrl, LPNMCUSTOMDRAW pnmcd)
{
	NMLVCUSTOMDRAW&  rCD = *(NMLVCUSTOMDRAW*)pnmcd;
	rCD.clrTextBk = (pnmcd->dwItemSpec % 2) ? RGB(255, 255, 255) : RGB(230, 230, 230);
	return 0;
}

void CPiecesFullList::SetColumns(int iCols)
{
	{
		SetRedraw(FALSE);
		LV_COLUMN		lvcolumn;
		CRect rect;
		GetWindowRect( &rect );

		lvcolumn.mask = LVCF_FMT | LVCF_SUBITEM | LVCF_TEXT | LVCF_WIDTH | LVCF_ORDER;
		lvcolumn.fmt = LVCFMT_CENTER;
		lvcolumn.cx = rect.Width() / iCols - 2;  

		CHANNELS_MAP::iterator  iter;
		int iColNum = 0;
		for(iter = m_localChlMap.begin(); iter != m_localChlMap.end(); iter++)
		{
			CChannelSettingsAccessor& cac = iter->second;

			if( cac.bVisibleCol )
			{
				lvcolumn.pszText = cac.m_descr;
				lvcolumn.iSubItem = iColNum;
				lvcolumn.iOrder = iColNum;
				InsertColumn(iColNum, &lvcolumn);  
				cac.iColNum = iColNum;
				iColNum++;
			}
		}
		lvcolumn.pszText = _T("�����");
		lvcolumn.iSubItem = iColNum;
		lvcolumn.iOrder = iColNum;
		InsertColumn(iColNum, &lvcolumn);  
		SetRedraw(TRUE);

	}
}

LRESULT CPiecesFullList::OnSize(UINT , WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
		SetRedraw(FALSE);
	CHeaderCtrl hdr = GetHeader();
	CRect rect;
	GetWindowRect( &rect );
	int nColNum = hdr.GetItemCount();

	for(int idx = 0; idx < nColNum; ++idx)
	{
		//SetColumnWidth(idx, rect.Width() / nColNum - 2);
	}
		SetRedraw(TRUE);
	return 0;
}

LRESULT CPiecesFullList::OnContextMenu(UINT , WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CPoint pt( GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
	CHeaderCtrl& rHeader = GetHeader();
	CRect rcCol;
	rHeader.GetClientRect(&rcCol);
	rHeader.ClientToScreen(&rcCol);

	if(rcCol.PtInRect( pt ) && rHeader.IsWindowVisible() )
	{
		CMenu menu;
		menu.CreatePopupMenu();
		int iRow = 0;
		CHMAP_ITER iter;
		for(iter = m_localChlMap.begin(); iter != m_localChlMap.end(); iter++)
		{
			BOOL bInsert = menu.AppendMenu(MF_STRING , ID_COLUMNS_FIRST + iRow, 
				iter->second.m_descr);
			menu.CheckMenuItem(ID_COLUMNS_FIRST + iRow, 
				iter->second.bVisibleCol?MF_CHECKED:MF_UNCHECKED );
			++iRow;
		}
		BOOL bInsert = menu.AppendMenu(MF_STRING , ID_COLUMNS_FIRST + iRow, 
			_T("�����"));
		menu.CheckMenuItem(ID_COLUMNS_FIRST + iRow, MF_CHECKED);
		menu.EnableMenuItem(ID_COLUMNS_FIRST + iRow, MF_GRAYED);
		menu.EnableMenuItem(ID_COLUMNS_FIRST, MF_GRAYED);

		++iRow;
		bInsert = menu.AppendMenu(MF_STRING , ID_COLUMNS_FIRST + iRow, 	_T("�������� ���"));
		++iRow;
		bInsert = menu.AppendMenu(MF_STRING , ID_COLUMNS_FIRST + iRow, 	_T("������ ���"));
		m_iLastMenuItem = iRow;

		menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, pt.x, pt.y, m_hWnd);
	}
	return 0;
}

LRESULT CPiecesFullList::OnCommand(UINT , WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	WORD wID = LOWORD(wParam);
	int iCols = GetHeader().GetItemCount();

	while( DeleteColumn(0) )	{}

	if( wID - ID_COLUMNS_FIRST < m_iLastMenuItem - 1 )
	{
		CHMAP_ITER iter = m_localChlMap.begin();
		std::advance(iter, + (wID - ID_COLUMNS_FIRST) );	//advance to needed map item 
		CChannelSettingsAccessor& cac = iter->second;

		if( cac.bVisibleCol )
			//need remove and uncheck
		{
			iCols--;
			cac.bVisibleCol = false;
		}
		else
		{
			iCols++;
			cac.bVisibleCol = true;
		}
	}
	else
	{
		//all to visible
		bool bShowAll = (wID - ID_COLUMNS_FIRST != m_iLastMenuItem);
		for( CHMAP_ITER ch_iter = m_localChlMap.begin(); 
			ch_iter != m_localChlMap.end(); ++ch_iter )
		{
			CChannelSettingsAccessor& cac = ch_iter->second;
			cac.bVisibleCol = bShowAll;
			if( strcmp(cac.m_fname, _T("vlin")) == 0 )
				cac.bVisibleCol = true;

			if( strcmp(cac.m_fname, _T("meter")) == 0 )
				cac.bVisibleCol = true;

		}
		iCols = (bShowAll)?m_localChlMap.size():3;//2 + �����

	}
	SetColumns(iCols);
	return 0;
}

void CPiecesFullList::DataBind(CDataContainer* pData)
{
	ATLASSERT(pData);
	m_pData = pData;
	SetItemCount(pData->GetNumRows());
	ListView_SetExtendedListViewStyle(m_hWnd, LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES | LVS_EX_FLATSB );
	EnsureVisible(GetItemCount()-1, FALSE);
}

LRESULT CPiecesFullList::OnGetDispInfo(LPNMHDR pnmh)
{
	LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pnmh;
	LV_ITEM* pItem= &(pDispInfo)->item;

	char buffer[16] = {0};
	ATLASSERT(m_pData);
		////msdn code		

	if (pItem && pItem->mask & LVIF_TEXT ) //valid text buffer?
	{
		_FindValForSubitem(buffer, pItem->iItem, pItem->iSubItem);
		lstrcpy(pItem->pszText, buffer);
		pItem->mask |= LVIF_PARAM;
	}
	if (pItem->mask & LVIF_IMAGE)
	{
		pItem->lParam = pItem->iItem;
	}
	return 0;
}

char* CPiecesFullList::_FindValForSubitem(char* buf, unsigned int uI, unsigned int uSi)
{
	ATLASSERT(buf);
	ATLASSERT(m_pData);
	CDataContainer& data = *m_pData;
	int iChlNum = -1;
	int iVisibleOrder = -1;
	CHMAP_ITER iter;
	int iColNum = 0;
	bool bChlIsFound = false;

	///Run find loop

	size_t sz = m_localChlMap.size();
	for(iter = m_localChlMap.begin(); iter != m_localChlMap.end(); iter++)
	{
		CChannelSettingsAccessor &cac = iter->second;
		if( cac.bVisibleCol )
			iVisibleOrder++;
		if( uSi == iVisibleOrder )
		{
			iChlNum = cac.m_num;
			bChlIsFound = true;
			break;
		}
	}

	if( !bChlIsFound )	//that is date field probably
	{
		SYSTEMTIME DT;
		double * dataPtr = data["dtm_upd"];
		if(dataPtr)	
		{
			VariantTimeToSystemTime(dataPtr[uI], &DT);
			sprintf(buf, "%02d:%02d:%02d", DT.wHour, DT.wMinute, DT.wSecond);
		}
	}
	else
	{
		double * dataPtr = data[m_localChlMap[iChlNum].m_fname];
		if(dataPtr)	
		{
			const int iPrec = 2;
			double floorVal = floor(dataPtr[uI]);

			if( fabs(floorVal - dataPtr[uI]) < 9/pow(10, iPrec + 1) )
				sprintf(buf, "%ld", long(dataPtr[uI]));
			else
				sprintf(buf, "%.2f", dataPtr[uI]);

		}
	}
	return buf;
}
