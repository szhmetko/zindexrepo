﻿
using System.Configuration;
using Microsoft.Practices.ServiceLocation;
using Rtp.Infrastructure;
using Rtp.Infrastructure.Config;
using Rtp.Infrastructure.UnitOfWork;
using Rtp.Model;
using Rtp.Model.Line;
using Rtp.Model.Master;
using Rtp.Repository.Repositories;
using StructureMap;
using StructureMap.Configuration.DSL;
using StructureMap.ServiceLocatorAdapter;

namespace Rtp.Repository.Tests
{
    public class BootStrapper
    {
        public static void Initialize()
        {
            var ioc = CreateServiceLocator();
            ServiceLocator.SetLocatorProvider(() => ioc);
        }


        private static IServiceLocator CreateServiceLocator()
        {
            var x = new Registry();

            x.Scan(scan =>
            {
                scan.TheCallingAssembly();
                scan.AssemblyContainingType<NHUnitOfWork>();
                scan.AddAllTypesOf<IUnitOfWork>();
                scan.AddAllTypesOf<IConnectionInfo>();

            });

            x.For<IConnectionInfo>().Use(

                (c) =>
                {
                    string srv = ConfigurationSettings.AppSettings["MasterHost"];
                    string usr = ConfigurationSettings.AppSettings["MasterUser"];
                    string pwd = ConfigurationSettings.AppSettings["MasterPwd"];
                    string dbn = ConfigurationSettings.AppSettings["MasterDbName"];

                    return new ConnectionInfo(dbn, usr, srv, pwd);
                });

            x.For<IUnitOfWork>().Use<NHUnitOfWork>();
            x.For<ILinesRepository>().Use<LinesRepository>();

            x.For<ILineUnitOfWork>().Use((uow) =>
                            {
                                string srv = ConfigurationSettings.AppSettings["LineHost"];
                                string usr = ConfigurationSettings.AppSettings["LineUser"];
                                string pwd = ConfigurationSettings.AppSettings["LinePwd"];
                                string dbn = ConfigurationSettings.AppSettings["LineDbName"];

                                var conn = new ConnectionInfo(dbn, usr, srv, pwd);
                                return new NHUnitOfWork(conn);
                            });
            x.For<IModulesRepository>().Use<ModulesRepository>();
            x.For<IQueriesRepository>().Use<QueriesRepository>();
            x.For<IDataValuesRepository>().Use<DataValuesRoRepository>();

            x.For<IRawSqlRepository>().Use((r) =>
                                               {
                                                   string srv = ConfigurationSettings.AppSettings["LineHost"];
                                                   string usr = ConfigurationSettings.AppSettings["LineUser"];
                                                   string pwd = ConfigurationSettings.AppSettings["LinePwd"];
                                                   string dbn = ConfigurationSettings.AppSettings["LineDbName"];

                                                   var conn = new ConnectionInfo(dbn, usr, srv, pwd);

                                                   var repo = new RawSqlRepository(conn);
                                                   return repo;
                                               });
            
            IContainer container = new Container(x);

            return new StructureMapServiceLocator(container);
        }

    }
}
