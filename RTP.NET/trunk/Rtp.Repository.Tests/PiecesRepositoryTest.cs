﻿using System;
using NUnit.Framework;
using Rtp.Repository.Repositories;

namespace Rtp.Repository.Tests
{
    [TestFixture]
    public class PiecesRepositoryTest
    {
        [TestCase("rtp_lknv60")]
        public  void CanFetchAllPieces(string dbName)
        {
            var repo = new PiecesRepository(new NHUnitOfWork(new TestConnectionInfo(dbName)));

            var entities = repo.FindAll();
            Assert.IsTrue(entities.Count > 0);
        }

        [TestCase("rtp_lknv60")]
        public void CanCreatePiece(string dbName)
        {

            var uow = new NHUnitOfWork(new TestConnectionInfo("rtp_lknv60"));


            var repo = new PiecesRepository(uow);

            var newPiece = new Rtp.Model.Line.Piece
            {DtOff = System.DateTime.MinValue, DtOn = System.DateTime.Now, Length = 0};
            
            repo.Add(newPiece);
            
            uow.Commit();

            Assert.IsNotNull(newPiece.Id);
        }


        [TestCase(0, 0, 0, 1)]
        public void CanUpdateOldPieces(int days, int hours, int mins, int years)
        {
            var timeSpan = new TimeSpan(days + 365 * years, hours, mins, 0);

            var uow = new NHUnitOfWork(new TestConnectionInfo("krok_rtp_me90t"));

            var repo = new PiecesRepository(uow);

            repo.RecalculateCompleted(timeSpan);

            uow.Commit();

        }

        [TestCase]
        public void CanFetchPiecesViaHql()
        {
            var uow = new NHUnitOfWork(new TestConnectionInfo("rtp_lknv60"));

            var repo = new PiecesRepository(uow);

            var queriesRepo = new QueriesRepository(uow);

            //var sql = queriesRepo.FindBy("PIECES_LIST").QueryText;

            var sql = @"SELECT Id AS {pc.Id}, agg.Leng AS {pc.Length}, 
                dtOn AS {pc.DtOn}, dtOff AS {pc.DtOff},
                CASE WHEN 1>2 THEN '1st case' ELSE 'Пуск' END AS {pc.Description} 
                FROM Pieces pc JOIN (SELECT MAX(meter) as Leng, pieceId from xvals GROUP BY pieceId)agg ON agg.pieceId=pc.Id";
            var res = repo.Test(sql);

            uow.Commit();

        }
    }
}