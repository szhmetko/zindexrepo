﻿using NHibernate;
using NUnit.Framework;
using Rtp.Model.Master;
using Rtp.Repository.Repositories;
using Rtp.Repository.SessionStorage;

namespace Rtp.Repository.Tests
{
    [TestFixture]
    public class ThermoPairsRepositoryTest
    {

        [Test]
        public void CanFetchAllThermoPairs()
        {
            var connInfo = new TestConnectionInfo("rtp_master");
            var uow = new NHUnitOfWork(connInfo);

            var tpr = new ThermoPairsRepository(uow);

            var result = tpr.FindAll();

            Assert.IsNotNull(result.Count > 0);
        }


        [Test]
        public void CanUpdateThermoPair()
        {
            var connInfo = new TestConnectionInfo("rtp_master");

            var uow = new NHUnitOfWork(connInfo);

            var tpr = new ThermoPairsRepository(uow);

            var thermoPair = tpr.FindBy(0);

            thermoPair.Title = "Нет++";
            thermoPair.Description = null;

            tpr.Save(thermoPair);

            uow.Commit();

        }

        [Test]
        public void CanCreateThermoPair()
        {
            var connInfo = new TestConnectionInfo("rtp_master");

            var uow = new NHUnitOfWork(connInfo);

            var tpr = new ThermoPairsRepository(uow);

            var thermoPair = new ThermoPair {Title = "Новое", Description = null};

            tpr.Save(thermoPair);

            uow.Commit();

        }

        [Test]
        public void CanFetchApproxRules()
        {
            var connInfo = new TestConnectionInfo("rtp_master");

            ICriteria CriteriaQuery = NHibernateSessionManager.Instance.GetSessionFrom(connInfo.ConnectionString)
                .CreateCriteria(typeof(ApproxRule));

            var res = CriteriaQuery.List<ApproxRule>();
        }

    }
}
