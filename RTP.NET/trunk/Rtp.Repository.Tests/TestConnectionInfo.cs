﻿using Rtp.Infrastructure;

namespace Rtp.Repository.Tests
{

    internal class TestConnectionInfo : ConnectionInfo
    {
        public TestConnectionInfo(string dbName)
            : base(dbName, "sergey", "localhost", "dedabirAir")
        {

        }
    }
}
