﻿using System.Diagnostics;
using System.Text;
using Microsoft.Practices.ServiceLocation;
using NHibernate;
using NUnit.Framework;
using Rtp.Infrastructure;
using Rtp.Model;
using Rtp.Model.Line;
using Rtp.Repository.Repositories;
using Rtp.Repository.SessionStorage;

namespace Rtp.Repository.Tests
{
    [TestFixture]
    class DataValuesTests
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            BootStrapper.Initialize();
        }

        [TestCase("rtp_lknv60")]
        public void CanFetchAllDataValues(string databaseName)
        {
            var connInfo = new TestConnectionInfo(databaseName);

            //var uow = new NHUnitOfWork(connInfo.ConnectionString);
            
            DataValuesRoRepository dvr = new DataValuesRoRepository(connInfo);

            var result = dvr.FindAll();

            Assert.IsNotNull(result.Count > 0);
        }

        [Test]
        public void CanFetchDataValueById()
        {
            var connInfo = new TestConnectionInfo("lknv60");

            //var uow = new NHUnitOfWork(connInfo.ConnectionString);

            DataValuesRoRepository dvr = new DataValuesRoRepository(connInfo);

            var result = dvr.FindBy(1);

            Assert.IsNotNull(result);
        }

        [TestCase("lknv60")]
        [TestCase("mysql")]
        [TestCase("rishop_db")]
        public void CanCreateMultipleConnections(string databaseName)
        {
            //var uow = new NHUnitOfWork();
            var connInfo = new TestConnectionInfo(databaseName);

            var session = NHibernateSessionManager.Instance.GetSessionFrom(connInfo);

            var result = session.CreateSQLQuery("select database() res from dual")
                .AddScalar("res", NHibernateUtil.String).List();

            Assert.IsTrue(result.Count > 0);

            Assert.AreEqual(databaseName, result[0].ToString());

        }


        [Test]
        public void CanBulkInsert3()
        {
            var stopwatch = new Stopwatch();

            stopwatch.Start();
            var connInfo = new TestConnectionInfo("lknv60");

            var session = NHibernateSessionManager.Instance.GetSessionFrom(connInfo); 
            
            ITransaction tx = session.BeginTransaction();

            var strQuery = new StringBuilder("insert into data_vals (Value) VALUES (0)");

            for (int i = 0; i < 20000; i++)
            {
                strQuery.AppendFormat(",({0})", i);
                //DataValues dv = new DataValues { Value = i };

                //session.Save(dv);

                //query.SetInt32("Value", i).ExecuteUpdate();
            }

            var query =
                session.CreateSQLQuery(strQuery.ToString());
            query.ExecuteUpdate();

            tx.Commit();
            session.Close();

            stopwatch.Stop();

            var time = stopwatch.Elapsed;
        }

        [Test]
        public void CanFetchArchiveData()
        {

            var repo = ServiceLocator.Current.GetInstance<IQueriesRepository>();

            var connInfo = new TestConnectionInfo("rtp_lknv60");

            //var uow = new NHUnitOfWork(connInfo.ConnectionString);

            var  repoData = new DataValuesRoRepository(connInfo);

            var query = repo.FindBy("ARCHIVE");

            var res = repoData.FetchArchiveData(1159780245L, 1159780245L + 24 * 3600L, 1000, query.QueryText);

            foreach (System.Collections.Hashtable re in res)
            {
                var item = re;
            }

            Assert.IsNotNull(query);
        }

        [Test]
        public void CanFetchMonitorData()
        {

            var repo = ServiceLocator.Current.GetInstance<IQueriesRepository>();

            var connInfo = new TestConnectionInfo("rtp_lknv60");

            //var uow = new NHUnitOfWork(connInfo.ConnectionString);

            var repoData = new DataValuesRoRepository(connInfo);

            var query = repo.FindBy("MONITOR");

            var res = repoData.FetchMonitorData(60, 1000, query.QueryText);

            foreach (System.Collections.Hashtable re in res)
            {
                var item = re;
            }

            Assert.IsNotNull(query);
        }

        /*
        [Test]
        public void CanBulkInsert1()
        {
            var stopwatch = new Stopwatch();

            stopwatch.Start();
            var connInfo = new TestConnectionInfo("lknv60");

            var session = NHibernateSessionManager.Instance.GetSessionFrom(connInfo); 
            
            ITransaction tx = session.BeginTransaction();

            for (int i = 0; i < 1000; i++)
            {
                DataValues dv = new DataValues{Value = i};

                session.Save(dv);

                if(i % 100 == 0)
                {
                    //flush a batch of inserts and release memory:
                    session.Flush();
                    session.Clear();
                }
            }

            tx.Commit();
            session.Close();

            stopwatch.Stop();

            var time = stopwatch.Elapsed;
        }  */

        //[Test]
        //public void CanBulkInsert2()
        //{
        //    var stopwatch = new Stopwatch();

        //    stopwatch.Start();

        //    using (IStatelessSession statelessSession = SessionFactory.OpenStatelessSession())

        //    using (ITransaction transaction = statelessSession.BeginTransaction())
        //    {
        //        for (int i = 0; i < 100; i++)
        //        {
        //            DataValues dv = new DataValues { Value = i };
        //            statelessSession.Insert(dv);

        //        }



        //        transaction.Commit();

        //    }

        //    stopwatch.Stop();

        //    var time = stopwatch.Elapsed;
        //}

    }
}
