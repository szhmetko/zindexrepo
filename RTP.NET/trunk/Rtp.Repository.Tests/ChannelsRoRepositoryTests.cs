﻿using NUnit.Framework;
using Rtp.Repository.Repositories;

namespace Rtp.Repository.Tests
{
    [TestFixture]
    class ChannelsRoRepositoryTests
    {
        [Test]
        public void CanFetchAllChannels()
        {
            var linesRepo = new LinesRepository(new NHUnitOfWork(new TestConnectionInfo("rtp_master")));

            var lines = linesRepo.FindAll();
            Assert.IsTrue(lines.Count > 0);

            var channelsRepo = new ChannelsRoRepository(lines[0]);

            var channels = channelsRepo.FindAll();

            Assert.IsTrue(channels.Count > 0);

        }
    }
}
