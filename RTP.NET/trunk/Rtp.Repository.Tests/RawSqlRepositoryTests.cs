﻿
using System;
using System.Collections.Generic;
using Microsoft.Practices.ServiceLocation;
using NUnit.Framework;
using Rtp.Model;
using System.Linq;

namespace Rtp.Repository.Tests
{

    [TestFixture]
    public class RawSqlRepositoryTests
    {
        const string Sql = "SELECT 1 as Id, 'Бла' as Description UNION SELECT 2 as Id, 'Блаds' as Description UNION SELECT 13 as Id, 'Бла13' as Description";
        public class TestEntity
        {
            public Int64 Id { get; set; }
            public string Description { get; set; }
        }

        [SetUp]
        public void Initialize()
        {
            BootStrapper.Initialize();
        }

        [Test]
        public void CanQueryEntitiesList()
        {


            var repo = ServiceLocator.Current.GetInstance<IRawSqlRepository>();

            var typifiedEntities = repo.QueryEntities<TestEntity>(Sql, new Dictionary<string, object>());

            Assert.IsTrue(typifiedEntities.Count() > 0);
        }

        [Test]
        public void CanQueryEntitiesList1()
        {


            var repo = ServiceLocator.Current.GetInstance<IRawSqlRepository>();

            var typifiedEntities = repo.QueryEntities(Sql, new Dictionary<string, object>(), typeof(TestEntity))
                as IEnumerable<object>;

            //Assert.IsNotNull(typifiedEntities);
            //Assert.IsTrue(typifiedEntities.Count() > 0);
        }
    }
}
