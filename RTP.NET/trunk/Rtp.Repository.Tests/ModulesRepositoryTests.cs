﻿using Microsoft.Practices.ServiceLocation;
using NUnit.Framework;
using Rtp.Infrastructure;
using Rtp.Model.Line;
using Rtp.Repository.Repositories;

namespace Rtp.Repository.Tests
{
    [TestFixture]
    public class ModulesRepositoryTests
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            BootStrapper.Initialize();
        }

        [TestCase("lknv60")]
        public void CanFetchModules()
        {
            var repo = ServiceLocator.Current.GetInstance<IModulesRepository>();

            var modules = repo.FindAll();

            Assert.IsTrue(modules.Count > 0);
        }

        [TestCase("krok_rtp_troster", "sergey", "localhost", "dedabirAir")]
        public void CanAddModule(string dbn, string usr, string srv, string pwd)
        {
            var conn = new ConnectionInfo(dbn, usr, srv, pwd);

            var newChart = ModuleChart.Default;

            var newModule = Module.Default;

            using (var  uow = new NHUnitOfWork(conn))
            {
                var repo = new ModulesRepository(uow);

                repo.Add(newModule);

                newChart.Module = newModule;

                newModule.AddChart(newChart);

                var newChart1 = ModuleChart.Default;

                newModule.AddChart(newChart1);

                repo.Save(newModule);

                uow.Commit();

            }

            Assert.AreNotEqual(newModule.Id, -1);

            using (var uow = new NHUnitOfWork(conn))
            {
                var repo = new ModulesRepository(uow);

                newModule.RemoveChart(newChart);

                repo.Save(newModule);

                uow.Commit();
            }

            Assert.AreNotEqual(newModule.Id, Module.Default.Id);
            Assert.AreEqual(newModule.Charts.Count, 1);

        }

    }
}
