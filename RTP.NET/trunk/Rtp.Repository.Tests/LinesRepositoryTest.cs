﻿using NUnit.Framework;
using Rtp.Infrastructure.Config;
using Rtp.Infrastructure.Query;
using Rtp.Infrastructure.Utility.DatacollectorSettings;
using Rtp.Model.Master;
using Rtp.Repository.Repositories;

namespace Rtp.Repository.Tests
{
    [TestFixture]
    public class LinesRepositoryTest
    {

        [Test]
        public void CanFetchAllLines()
        {
            var uow = new NHUnitOfWork(new TestConnectionInfo("rtp_master"));

            var repository = new LinesRepository(uow);

            var result = repository.FindAll();

        }

        [TestCase("lknv60")]
        public void CanFetchLineByAlias(string dbName)
        {
            var uow = new NHUnitOfWork(new TestConnectionInfo("rtp_master"));

            var repository = new LinesRepository(uow);

            var queryObj = new Query();
            queryObj.Add(new Criterion("Alias", dbName,CriteriaOperator.Equal));

            var result = repository.FindBy(queryObj);

        }
        [Test]
        public void CanUpdateLine()
        {
            var uow = new NHUnitOfWork(new TestConnectionInfo("rtp_master"));

            var repository = new LinesRepository(uow);

            var line = repository.FindBy(3);

            line.Title = "Нет++";
            line.Description = null;

            repository.Save(line);

            uow.Commit();

        }

        [Test]
        public void CanCreateLine()
        {
            var uow = new NHUnitOfWork(new TestConnectionInfo("rtp_master"));

            var repository = new LinesRepository(uow);

            var settings = new UdpDatacollectorSettings()
            {
                Host = "localhost"
                , Port=123,
            };

            var line = new Line { Title = "Новое", Description = null, Collector = settings.ToXml(), ConnectionString = AppConfig.Instance.MasterConnection.ConnectionString };

            repository.Add(line);

            uow.Commit();

        }

    }
}
