﻿using Microsoft.Practices.ServiceLocation;
using NUnit.Framework;
using Rtp.Model.Line;

namespace Rtp.Repository.Tests
{
    [TestFixture]
    public class QueriesRepositoryTests
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            BootStrapper.Initialize();
        }

        [Test]
        public void CanFetchQueries()
        {
            var repo = ServiceLocator.Current.GetInstance<IQueriesRepository>();

            var queries = repo.FindAll();

            Assert.IsTrue(queries.Count > 0);
        }

        [Test]
        public void CanFetchQueryByKey()
        {
            var repo = ServiceLocator.Current.GetInstance<IQueriesRepository>();

            var query = repo.FindBy("MONITOR");

            Assert.IsNotNull(query);

        }
    }
}
