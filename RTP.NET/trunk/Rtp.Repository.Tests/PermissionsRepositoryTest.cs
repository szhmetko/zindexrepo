﻿using NUnit.Framework;
using Rtp.Model.Master;
using Rtp.Repository.Repositories;

namespace Rtp.Repository.Tests
{
    [TestFixture]
    public class PermissionsRepositoryTest
    {

        [Test]
        public void CanFetchPermissionFor()
        {
            var uow = new NHUnitOfWork(new TestConnectionInfo("rtp_master"));

            var epe = new EntityPermissionEngine(uow);

            //var repository = new PermissionsRepository(uow);

            //var result = repository.GetPermissionsFor("Lines");

            var perm = epe.GetPermissions<Line>("ADM");

            perm = epe.GetPermissions<ApproxRule>("ADM");

        }

    }
}
