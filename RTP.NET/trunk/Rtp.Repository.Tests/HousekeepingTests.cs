﻿using System;
using NUnit.Framework;
using Rtp.Infrastructure;
using Rtp.Repository.SessionStorage;

namespace Rtp.Repository.Tests
{
    [TestFixture]
    public class HousekeepingTests
    {

        private const string StrHouseKeepingScript1 = @"
CREATE TABLE  `Pieces_Tmp` (
  `Id` int(11) NOT NULL,
  `dtOn` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  `dtOff` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  `Length` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `Pieces_Tmp` (`Id`, `dtOn`, `dtOff`, `Length`) SELECT `Id`, `dtOn`, `dtOff`, `Length` FROM `Pieces` WHERE DATEDIFF(NOW(), `dtOn`) < :NUM_DAYS;

DROP TABLE `Pieces`;

CREATE TABLE `Pieces` (
  `Id` int(11) NOT NULL,
  `dtOn` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  `dtOff` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  `Length` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `IDX_cablePieces_dtm_on` (`dtOn`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


INSERT INTO `Pieces` (`Id`, `dtOn`, `dtOff`, `Length`) SELECT `Id`, `dtOn`, `dtOff`, `Length` FROM `Pieces_Tmp`;

DROP TABLE `Pieces_Tmp`;

CREATE TABLE `Xvals_Tmp` (
  `Id` bigint(11) NOT NULL AUTO_INCREMENT,
  `uxtime` bigint(20) DEFAULT NULL,
  `meter` int(11) NOT NULL DEFAULT '0',
  `prob` int(11) NOT NULL DEFAULT '0',
  `pieceId` int(11) NOT NULL DEFAULT '0',
  `DataType` int(11) NOT NULL,
  `c1` float DEFAULT NULL,
  `c2` float DEFAULT NULL,
  `c3` float DEFAULT NULL,
  `c4` float DEFAULT NULL,
  `c5` float DEFAULT NULL,
  `c6` float DEFAULT NULL,
  `c7` float DEFAULT NULL,
  `c8` float DEFAULT NULL,
  `c9` float DEFAULT NULL,
  `c10` float DEFAULT NULL,
  `c11` float DEFAULT NULL,
  `c12` float DEFAULT NULL,
  `c13` float DEFAULT NULL,
  `c14` float DEFAULT NULL,
  `c15` float DEFAULT NULL,
  `c16` float DEFAULT NULL,
  `c17` float DEFAULT NULL,
  `c18` float DEFAULT NULL,
  `c19` float DEFAULT NULL,
  `c20` float DEFAULT NULL,
  `c21` float DEFAULT NULL,
  `c22` float DEFAULT NULL,
  `c23` float DEFAULT NULL,
  `c24` float DEFAULT NULL,
  `c25` float DEFAULT NULL,
  `c26` float DEFAULT NULL,
  `c27` float DEFAULT NULL,
  `c28` float DEFAULT NULL,
  `c29` float DEFAULT NULL,
  `c30` float DEFAULT NULL,
  `c31` float DEFAULT NULL,
  `c32` float DEFAULT NULL,
  `vlin` float DEFAULT NULL,
  `dtm_upd` timestamp NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;


INSERT INTO `Xvals_Tmp` (`Id`, uxtime, meter, prob, pieceId, DataType, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, c14, c15, c16, c17, c18, c19, c20, c21, c22, c23, c24, c25, c26, c27, c28, c29, c30, c31, c32, vlin, dtm_upd ) SELECT `Id`, uxtime, meter, prob, pieceId, DataType, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, c14, c15, c16, c17, c18, c19, c20, c21, c22, c23, c24, c25, c26, c27, c28, c29, c30, c31, c32, vlin, dtm_upd FROM `Xvals` WHERE DATEDIFF(NOW(), `dtm_upd`) < :NUM_DAYS;

DROP TABLE `Xvals`;

CREATE TABLE `Xvals` (
  `Id` bigint(11) NOT NULL AUTO_INCREMENT,
  `uxtime` bigint(20) DEFAULT NULL,
  `meter` int(11) NOT NULL DEFAULT '0',
  `prob` int(11) NOT NULL DEFAULT '0',
  `pieceId` int(11) NOT NULL DEFAULT '0',
  `DataType` int(11) NOT NULL,
  `c1` float DEFAULT NULL,
  `c2` float DEFAULT NULL,
  `c3` float DEFAULT NULL,
  `c4` float DEFAULT NULL,
  `c5` float DEFAULT NULL,
  `c6` float DEFAULT NULL,
  `c7` float DEFAULT NULL,
  `c8` float DEFAULT NULL,
  `c9` float DEFAULT NULL,
  `c10` float DEFAULT NULL,
  `c11` float DEFAULT NULL,
  `c12` float DEFAULT NULL,
  `c13` float DEFAULT NULL,
  `c14` float DEFAULT NULL,
  `c15` float DEFAULT NULL,
  `c16` float DEFAULT NULL,
  `c17` float DEFAULT NULL,
  `c18` float DEFAULT NULL,
  `c19` float DEFAULT NULL,
  `c20` float DEFAULT NULL,
  `c21` float DEFAULT NULL,
  `c22` float DEFAULT NULL,
  `c23` float DEFAULT NULL,
  `c24` float DEFAULT NULL,
  `c25` float DEFAULT NULL,
  `c26` float DEFAULT NULL,
  `c27` float DEFAULT NULL,
  `c28` float DEFAULT NULL,
  `c29` float DEFAULT NULL,
  `c30` float DEFAULT NULL,
  `c31` float DEFAULT NULL,
  `c32` float DEFAULT NULL,
  `vlin` float DEFAULT NULL,
  `dtm_upd` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`),
  KEY `IDX_Xvals_Uxtime` (`uxtime`),
  KEY `IDX_XVals_piece_id` (`pieceId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

INSERT INTO `Xvals` (`Id`, uxtime, meter, prob, pieceId, DataType, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, c14, c15, c16, c17, c18, c19, c20, c21, c22, c23, c24, c25, c26, c27, c28, c29, c30, c31, c32, vlin, dtm_upd ) SELECT `Id`, uxtime, meter, prob, pieceId, DataType, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, c14, c15, c16, c17, c18, c19, c20, c21, c22, c23, c24, c25, c26, c27, c28, c29, c30, c31, c32, vlin, dtm_upd FROM `Xvals_Tmp`;

DROP TABLE `Xvals_Tmp`;


";

        [TestCase("rtp_test", "sergey", "localhost", "dedabirAir")]
        public void CleanUpTest1(string dbn, string usr, string srv, string pwd)
        {
            var conn = new ConnectionInfo(dbn, usr, srv, pwd);

            using (var session = NHibernateSessionManager.Instance.GetSessionFrom(conn))
            {
                try
                {
                    var query = session.CreateSQLQuery(StrHouseKeepingScript1).SetInt32("NUM_DAYS", 30).SetTimeout(600).ExecuteUpdate();

                }
                catch (Exception exception)
                {

                    throw;
                }

            }



        }
    }
}
