﻿
namespace Rtp.Model
{
    public interface IHasPermissions
    {
        bool CanEdit { get; }
        bool CanDelete { get; }
    }
}
