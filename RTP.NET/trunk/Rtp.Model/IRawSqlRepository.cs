﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Rtp.Model
{
    public interface IRawSqlRepository   
    {
        IEnumerable Query(string rawSql, IDictionary<string, object> @params);
        IEnumerable Query(string rawSql);
        IEnumerable<T> QueryEntities<T>(string rawSql, IDictionary<string, object> @params);
        IEnumerable QueryEntities(string rawSql, IDictionary<string, object> @params, Type entityType);
    } 
}
