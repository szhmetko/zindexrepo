﻿namespace Rtp.Model
{
    public interface IPermissionValueBase
    {
        bool CanCreate { get; }
        bool CanRetrieve { get; }
        
    }

    public interface IPermissionEntity
    {
        bool CanUpdate { get; }
        bool CanDelete { get; }
        
    }

    public interface IPermissionValue : IPermissionValueBase, IPermissionEntity
    {
    }
}
