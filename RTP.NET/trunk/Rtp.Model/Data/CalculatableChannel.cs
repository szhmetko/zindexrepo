﻿using System;
using Ciloci.Flee;
using Rtp.Infrastructure.Exceptions;
using Rtp.Model.Line;
using Rtp.Model.Master;

namespace Rtp.Model.Data
{
    /// <summary>
    /// Channel calculatable wrapper
    /// </summary>
    public class CalculatableChannel
    {
        public readonly Channel ChannelData;
        protected readonly IDynamicExpression DynamicExpression;

        public ThermoPair ThermoPair { get; set; }

        public CalculatableChannel(Channel channel, IRtpChannelsApi owner)
        {
            try
            {
                ChannelData = channel;

                var evaluationEnginecontext = new ExpressionContext(owner);
                evaluationEnginecontext.Imports.AddType(typeof (System.Math));
                evaluationEnginecontext.ParserOptions.DecimalSeparator = '.';
                evaluationEnginecontext.ParserOptions.FunctionArgumentSeparator = ',';
                evaluationEnginecontext.ParserOptions.RecreateParser();

                DynamicExpression = evaluationEnginecontext.CompileDynamic(channel.Expression);
            }
            catch (Exception exception)
            {

                throw new ChannelValueEvaluationException("Channel assigning exception", exception);


            }
        }

        public object Calculate(IRtpChannelsApi owner)
        {
            try
            {
                DynamicExpression.Owner = owner;

                object val = DynamicExpression.Evaluate();

                if(ThermoPair != null)
                {
                    try
                    {
                        return ThermoPair.CalculateValue(Convert.ToDouble(val)) + owner.ColdEnd;
                    }
                    catch(Exception exception)
                    {
                        throw new ChannelValueEvaluationException("Linearization evaluation exception", exception);
                    }
                }

                return val;

            }
            catch (Exception exception)
            {
                
                throw new ChannelValueEvaluationException("Channel evaluation exception", exception);


            }
        }
    }
}