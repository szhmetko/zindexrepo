﻿using System;
using Rtp.Infrastructure.Utility;

namespace Rtp.Model.Data
{
    public interface IRtpChannelsApi : IDataCollectorCallable
    {
        SilentIndexedCollection<SilentIndexedCollection<int, int>, int> Raw { get; }
        SilentIndexedCollection<SilentIndexedCollection<object, int>, int> Calculated { get; }
        long CurrentTimeStamp { get; }
        Func<int> AfterProcessCallback { get; set; }
        double ColdEnd { get; set; }

        int SetCallback(int metersReceived);
    }
}