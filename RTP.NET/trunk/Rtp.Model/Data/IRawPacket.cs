﻿using System;
using System.Collections.Generic;

namespace Rtp.Model.Data
{
    public interface IRawPacket : ICloneable
    {
        Dictionary<int, object> ChannelsValues { get; }
        string Uid { get; }
        long Timestamp { get; }
        PacketKind Kind {get;}
    }
}