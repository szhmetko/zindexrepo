﻿namespace Rtp.Model.Data
{
    public interface IDataCollectorCallable
    {
        int AddPiece();
        int AddByMeter2Cache(IRawPacket rawPacket);
        int AddByTime2Cache(IRawPacket rawPacket);
        int Flush();
    }
}