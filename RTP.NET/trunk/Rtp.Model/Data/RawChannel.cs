﻿namespace Rtp.Model.Data
{
    public class RawChannel
    {
        public int Num { get; set; }

        public byte LoByte { get; set; }

        public byte HiByte { get; set; }

        public virtual int GetValue()
        {
            return ((HiByte << 8) + LoByte);
        }
    }
}