﻿using System;

namespace Rtp.Model
{

    public interface IHasAuditInformation
    {
        DateTime dtModified { set; get; }
        DateTime dtCreated { set; get; }
    }
}
