﻿using System;
using System.Collections.Generic;
using Rtp.Infrastructure;
using Rtp.Infrastructure.Query;

namespace Rtp.Model
{
    public interface IReadOnlyRepository<T, TEntityKey> : IDisposable where T : IAggregateRoot
    {
        T FindBy(TEntityKey id);
        IList<T> FindAll();
        IEnumerable<T> FindAll(int index, int count);
        IEnumerable<T> FindBy(Query query);
        IEnumerable<T> FindBy(Query query, int index, int count);
    }
}
