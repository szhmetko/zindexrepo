﻿using System;
using Rtp.Infrastructure;

namespace Rtp.Model
{
    public interface IRepository<T, TEntityKey> : IReadOnlyRepository<T, TEntityKey> where T : IAggregateRoot
    {
        void Add(T entity);
        void Remove(T entity);
        void Save(T entity);

        void CommitChanges();
    }

}
