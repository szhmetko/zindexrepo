﻿namespace Rtp.Model.Master
{
    public interface ILinesRepository : IRepository<Line, int>
    {
        Line FindyByAlias(string alias);
    }
}
