﻿using System;
using System.Collections.Generic;
using Rtp.Infrastructure;
using Rtp.Infrastructure.PermissionAttributes;
using Rtp.Infrastructure.UIAttributes;
using System.Linq;

namespace Rtp.Model.Master
{
    [GridSettings(GridType.ListView, false)]
    [EntityPermission(EntityKey = "ThermoPairs")]
    public class ThermoPair : IAggregateRoot, IHasAuditInformation
    {
        public int Id { get; set; }

        [GridField("", 0, 250)]
        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime dtCreated { get; set; }

        public DateTime dtModified { get; set; }

        public IList<ApproxRule> ApproxRules { get; set; }

        public double CalculateValue(double preValue)
        {
            var approxRange = ApproxRules.FirstOrDefault(rule => rule.ValueIsInside(preValue));

            return approxRange != null ? approxRange.Calculate(preValue) : preValue;
        }
    }
}
