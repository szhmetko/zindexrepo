﻿using System;
using System.Collections.Generic;
using Rtp.Infrastructure;
using Rtp.Infrastructure.PermissionAttributes;
using Rtp.Infrastructure.UIAttributes;

namespace Rtp.Model.Master
{
    [GridSettings(GridType.ListView)]
    [EntityPermission(EntityKey = "Lines")]
    public class Line : IAggregateRoot, IHasAuditInformation, IConnectionInfo, ICloneable
    {
        public int LineId { get; set; }

        [GridField("Название")]
        public string Title { get; set; }

        [GridField("Информация")]
        public string Description { get; set; }

        [GridField("Создано")]
        public DateTime dtCreated { get; set; }

        [GridField("Изменено")]
        public DateTime dtModified { get; set; }

        [GridField("Соединение")]
        public string ConnectionString
        { get; set; }

        [GridField("Коллектор")]
        public string Collector { get; set; }

        public string Alias { get; set; }

        void IConnectionInfo.Store(string fileName)
        {
            throw new NotImplementedException();
        }

        bool IConnectionInfo.IsDefault
        {
            get { return false; }
        }

        object ICloneable.Clone()
        {
            return Clone();
        }

        public Line Clone()
        {
            return (Line)MemberwiseClone();
        }

        [GridField("Хол. спай (имя поля)")]
        public string ColdEndFieldName { get; set; }

        public IEnumerable<ThermoPair> ThermoPairs { get; set; }
    }
}
