﻿using System.Collections.Generic;

namespace Rtp.Model.Master
{
    public interface IThermoPairsRepository : IRepository<ThermoPair, int>
    {
        IEnumerable<ThermoPair> FindAllValid();
    }
}
