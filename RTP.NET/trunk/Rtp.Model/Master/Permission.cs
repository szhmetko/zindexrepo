﻿using Rtp.Infrastructure;

namespace Rtp.Model.Master
{
    public class Permission : IAggregateRoot 
    {
        public string Table { set; get; }
        
        public string CanCreateCondition { set; get; }
        
        public string CanUpdateCondition { set; get; }
        
        public string CanDeleteCondition { set; get; }
    }
}
