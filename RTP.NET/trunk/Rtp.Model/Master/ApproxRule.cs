﻿using System;
using Rtp.Infrastructure;
using Rtp.Infrastructure.UIAttributes;
using System.Linq;

namespace Rtp.Model.Master
{
    [GridSettings(GridType.ListView)]
    public class ApproxRule : IAggregateRoot, IHasAuditInformation
    {
        public int RuleId { get; set; }

        [GridField("Диапазон")]
        public int RangeNum { get; set; }

        public ThermoPair ThermoPair { get; set; }

        [GridField("Смещение")]
        public float Offset { get; set; }

        [GridField("Коэф-т")]
        public float K { get; set; }

        [GridField("Порог")]
        public int UFrom { get; set; }

        DateTime IHasAuditInformation.dtCreated { get; set; }

        DateTime IHasAuditInformation.dtModified { get; set; }

        private int _uTo = -1;
        public int UTo
        {
            get
            {
                if (_uTo < 0)
                {
                    var nextRange = ThermoPair.ApproxRules.ElementAtOrDefault(RangeNum + 1);

                    _uTo = nextRange != null ? nextRange.UFrom : Math.Max(int.MaxValue, UFrom);
                }
                return _uTo;
            }
        }

        public bool ValueIsInside(double  value)
        {
            return value >= UFrom && value <= UTo;
        }

        internal double Calculate(double preValue)
        {
            //Main linearization formula
            return Offset + (preValue - UFrom) * K;	
        }

    }
}
