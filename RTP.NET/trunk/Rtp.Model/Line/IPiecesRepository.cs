﻿
using System.Collections.Generic;

namespace Rtp.Model.Line
{
    public interface IPiecesRepository : IRepository<Piece, int>
    {
        void RecalculateCompleted(int pieceId);
        void RecalculateCompleted(System.TimeSpan period);
        IList<Piece> FindAll(string rawSql);
    }
}
