﻿
using System.Collections.Generic;
using Rtp.Infrastructure;

namespace Rtp.Model.Line
{
    public class AxisInfo
    {
        public string Units { get; set; }

        public float LowLimit { get; set; }
        public float HighLimit { get; set; }

        public static AxisInfo Default
        { 
            get { return new AxisInfo {HighLimit = 100, LowLimit = 0, Units = string.Empty}; }
        }
    }

    public class ModuleChart : IAggregateRoot
    {
        public virtual int Id { get; set; }
        public string Title { get; set; }
        public string Alias { get; set; }
        public int DisplayOrder { get; set; }

        public AxisInfo PrimaryYAxis { get; set; }
        public AxisInfo SecondaryYAxis { get; set; }

        public Module Module { get; set; }

        private ICollection<ChartCurve> _curves;
        virtual public ICollection<ChartCurve> Curves
        {
            get { return _curves; }
            set { _curves = value; }
        }

        public static ModuleChart Default
        {
            get
            {
                return new ModuleChart
                           {
                               Alias = "temperatures",
                               Title = "Новый",
                               Id = -1,
                               PrimaryYAxis = AxisInfo.Default,
                               SecondaryYAxis = AxisInfo.Default,
                               Curves = new List<ChartCurve>()
                           };

            }

            //public override bool Equals(object other)
            //{
            //    if (this == other) return true;

            //    ModuleChart chart = other as ModuleChart;
            //    if (chart == null) return false; // null or not a chart

            //    return Id == chart.Id && chart.Module.Id == Module.Id;
            //}

            //public override int GetHashCode()
            //{
            //    return Id.GetHashCode();
            //}
        }
    }

}
