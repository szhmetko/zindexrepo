﻿using System;
using System.Collections.Generic;
using Rtp.Infrastructure;

namespace Rtp.Model.Line
{
    public class Query : IAggregateRoot
    {
        //public int Id { get; set; }

        public string Key { get; set; }

        public string QueryText { get; set; }

        public string Params { get; set; }

        public IEnumerable<string> ParameterNames
        {
            get
            {
                var ret = new List<string>();

                if(!string.IsNullOrEmpty(Params))
                {
                    ret.AddRange(Params.Split(new []{' ', ','}, StringSplitOptions.RemoveEmptyEntries));
                }

                return ret;
            }
        }

        public string Group { get; set; }

        public string LastExecuted { get; set; }
    }
}
