﻿using Rtp.Infrastructure;

namespace Rtp.Model.Line
{
    public  class Setting : IAggregateRoot
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
