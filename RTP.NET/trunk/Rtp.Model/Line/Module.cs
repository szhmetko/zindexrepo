﻿
using System.Collections.Generic;
using Rtp.Infrastructure;

namespace Rtp.Model.Line
{
    public class Module : IAggregateRoot
    {
        public virtual int Id { get; set; }
        public string Title { get; set; }
        public string Alias { get; set; }
        public string QueryName { get; set; }
        public int DisplayOrder { get; set; }

        private ICollection<ModuleChart> _charts;
        virtual public ICollection<ModuleChart> Charts
        {
            get { return _charts; }
            set { _charts = value; }
        }

        public static Module Default
        {
            get
            {
                return new Module
                    {
                        Alias = "monitor",
                        Id = -1,
                        QueryName = "MONITOR",
                        Title = "Новый",
                        Charts = new List<ModuleChart>()
                    };                
            }
        }

        public ModuleChart AddChart(ModuleChart newChart)
        {
            newChart.Module = this;

            Charts.Add(newChart);

            return newChart;
        }

        public void RemoveChart(ModuleChart chart)
        {
            chart.Module = null;
            Charts.Remove(chart);
        }

        //public override bool Equals(object other)
        //{
        //    if (this == other) return true;

        //    Module module = other as Module;
        //    if (module == null) return false; // null or not a cat

        //    if (Id != module.Id) return false;

        //    return true;
        //}

        //public override int GetHashCode()
        //{
        //    return Id.GetHashCode();
        //}
    }
}
