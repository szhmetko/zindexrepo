﻿
using Rtp.Infrastructure;

namespace Rtp.Model.Line
{
    public class ChartCurve :  IAggregateRoot
    {
        public virtual int Id { get; set; }
        public bool bPrimary { get; set; }
        public Channel Channel{ get; set; }
        public ModuleChart Chart { get; set; }


        //public override bool Equals(object other)
        //{
        //    if (this == other) return true;

        //    ChartCurve curve = other as ChartCurve;
        //    if (curve == null) return false; // null or not a cat

        //    if (Id != curve.Id) return false;

        //    return true;
        //}

        //public override int GetHashCode()
        //{
        //    return Id.GetHashCode();
        //}
    }
}
