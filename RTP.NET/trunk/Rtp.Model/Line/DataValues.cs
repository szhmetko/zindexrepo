﻿using Rtp.Infrastructure;

namespace Rtp.Model.Line
{
    public class DataValues : IAggregateRoot
    {
        public long Id { get; set; }

        public long Uxtime { get; set; }

        public long Meter { get; set; }

        public long PieceId { get; set; }

        public int DataType { get; set; }

        public float Vlin { get; set; }
        
    }
}
