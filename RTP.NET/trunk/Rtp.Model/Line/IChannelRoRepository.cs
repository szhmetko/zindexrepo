﻿
namespace Rtp.Model.Line
{
    public interface IChannelRoRepository : IReadOnlyRepository<Channel, int>
    { }
}
