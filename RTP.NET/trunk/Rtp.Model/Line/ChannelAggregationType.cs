﻿namespace Rtp.Model.Line
{
    public enum ChannelAggregationType
    {
        Avg = 0,
        Min = 1,
        Max = 2,
        Count = 3,
        Sum = 4,
        Unknown
    }
}
