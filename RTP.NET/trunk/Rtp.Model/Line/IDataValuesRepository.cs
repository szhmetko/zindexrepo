﻿using System.Collections;
using System.Collections.Generic;
using Rtp.Model.Data;

namespace Rtp.Model.Line
{
    public interface IDataValuesRepository : IReadOnlyRepository<DataValues, long>
    {
        int BulkInsert(IEnumerable<IRawPacket> packets, IDictionary<int, CalculatableChannel> channels);
        IEnumerable FetchArchiveData(long start, long end, int valuesDesired, string rawSql);
        IEnumerable FetchMonitorData(int mins, int valuesDesired, string rawSql);
        IEnumerable FetchPieceData(int pid, int valuesDesired, string rawSql);
    }
}
