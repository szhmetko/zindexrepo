﻿using System;
using Rtp.Infrastructure;

namespace Rtp.Model.Line
{
    public class Piece : IAggregateRoot
    {
        public int Id { get; set; }

        public int Length { get; set; }

        public DateTime DtOn { get; set; }
        public DateTime DtOff { get; set; }

        public string Description { get; set; }

    }
}
