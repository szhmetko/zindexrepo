﻿
namespace Rtp.Model.Line
{
    public interface IModulesRepository : IRepository<Module, int>
    { }
}
