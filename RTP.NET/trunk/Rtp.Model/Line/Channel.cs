﻿using System;
using Rtp.Infrastructure;
using Rtp.Infrastructure.UIAttributes;

namespace Rtp.Model.Line
{
    public class Channel : IAggregateRoot, IHasAuditInformation, ICloneable
    {
        public int Id { get; set; }

        [GridField]
        public string Title { get; set; }

        public string FieldName { get; set; }

        public float K { get; set; }

        public ChannelAggregationType AggregationType { get; set; }

        public DateTime dtModified { get; set; }
        public DateTime dtCreated { get; set; }

        public string Expression { get; set; }

        object ICloneable.Clone()
        {
            return Clone();
        }

        public Channel Clone()
        {
            return (Channel)MemberwiseClone();
        }

        public int ThermoPairId { get; set; }

        public double CalculateValue(double preValue)
        {
            return preValue*K;
        }

    }
}
