﻿using System;

namespace Rtp.Reporting.PiecesReportView.Model
{
    public class DiameterDeviation
    {
        public double ValueRangeStart { get; set; }
        public Int64 Occurences { get; set; }
        public string Color { set; get; }
    }
}