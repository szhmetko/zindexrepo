﻿
using System;

namespace Rtp.Reporting.PiecesReportView.Model
{
    public class PieceOverall
    {
        public string Probs { get; set; }
        public string RevertedProbs { get; set; }
        public decimal ProbsSum { get; set; }

        public DateTime DtOn { get; set; }
        public DateTime DtOff { get; set; }
        public string OffOnDiff { get; set; }

        public int Length { get; set; }
        public double DEtal { get; set; }
        public double DAvg { get; set; }

        public double VLinAvg { get; set; }
        public double VLinDev { get; set; }

        public double OverRun { get; set; }
        

        public DateTime SysDate { get; set; }
        public string CompanyName { get; set; }


    }
}
