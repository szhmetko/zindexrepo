﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.ServiceLocation;
using Rtp.Infrastructure.Config;
using Rtp.Model;
using Rtp.Model.Line;
using Rtp.Shared;

namespace Rtp.Reporting.PiecesReportView.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// </summary>
    public class PiecesReportViewModel : PieceChildViewModel, IRtpReport
    {
        private readonly IMessageService _messageService;
        //private readonly IPiecesReportViewRepository _repository;
        private readonly IRawSqlRepository _sqlRepository;

        /// <summary>
        /// Initializes a new instance of the PiecesReportViewModel class.
        /// </summary>
        public PiecesReportViewModel(IMessageService messageService
            , IRawSqlRepository sqlRepository)
        {
            _sqlRepository = sqlRepository;
            //_repository = repository;
            _messageService = messageService;

            //LoadedCommand = new RelayCommand(GenerateReport);
            RefreshReportCommand = new RelayCommand(Populate);

            // Set defaults
            Parameters = new ReportParameters
                             {

                                 ModelName = "--",
                                 Operator = string.Empty,
                                 NomVlin = 100.0f,
                                 NomD = 10.0f,
                                 NomDDev = 1.0f,
                                 Density = 3430.0f,
                                 LineTitle = AppConfig.Instance.CurrentLineName
                             };

            //Messenger.Default.Register<PropertyChangedMessage<PieceItemViewModel>>(
            //    this,
            //    message =>
            //    {
            //        var module = message.Sender as ModuleViewModel;
            //        if (module != null && ParentId == module.Model.Id && message.NewValue != null)
            //        {
            //            Parameters.PieceId = message.NewValue.Model.Id;
            //            Parameters.ModelName = message.NewValue.Model.Description;

            //            GenerateReport();
            //        }
            //    });
        }


        public RelayCommand LoadedCommand { get; private set; }
        public RelayCommand RefreshReportCommand { get; private set; }

        public IEnumerable<string> DataSourceNames { protected get; set; }

        private IEnumerable<Query> _dataSourceQueries;
        IEnumerable<Query> DataSourceQueries
        {
            get
            {
                if (_dataSourceQueries == null)
                {
                    var ret = new List<Query>();
                    try
                    {

                        using (var queryRepo = ServiceLocator.Current.GetInstance<IQueriesRepository>())
                        {
                            foreach (var dataSourceName in DataSourceNames)
                            {
                                ret.Add(queryRepo.FindBy(dataSourceName));
                            }
                        }

                        _dataSourceQueries = ret;
                    }
                    catch (Exception exception)
                    {
                        _messageService.ShowError(exception);
                    }
                }



                return _dataSourceQueries;
            }
        }

        public Dictionary<string, IEnumerable> ReportDatasources
        {
            get
            {
                try
                {
                    var ret = new Dictionary<string, IEnumerable>();

                    var paramsAvailable = Parameters.GetParameters();

                    foreach (var dataSourceQuery in DataSourceQueries)
                    {
                        Type t = Assembly.GetCallingAssembly().GetType(
                            string.Format("Rtp.Reporting.PiecesReportView.Model.{0}",
                            dataSourceQuery.Key), false, true);

                        if (t != null)
                        {
                            var reportParameters = new Dictionary<string, object>();

                            foreach (var reportParam in dataSourceQuery.ParameterNames)
                            {
                                if(paramsAvailable.ContainsKey(reportParam))
                                    reportParameters.Add(reportParam, paramsAvailable[reportParam]);
                            }

                            IEnumerable ds = _sqlRepository.QueryEntities(dataSourceQuery.QueryText, reportParameters, t);

                            ret.Add(dataSourceQuery.Key, ds);
                        }
                    }

                    return ret;

                }
                catch (Exception exception)
                {
                    _messageService.ShowError(exception);
                }

                return new Dictionary<string, IEnumerable>();
            }
        }

        #region IRtpReport Members

        public int ParentId { set { }
            protected get { return ParentModule.Model.Id; }
        }

        public Hashtable InputParameters { set; get; }

        #endregion



        #region User Input properties for report parameters

        public class ReportParameters
        {
            public string LineTitle { get; set; }

            public Int32 PieceId { get; set; }

            public string ModelName { get; set; }
            public string Operator { get; set; }
            public float NomVlin { get; set; }

            public float NomD { get; set; }
            public float NomDDev { get; set; }
            public float Density { get; set; }

            public IDictionary<string, object> GetParameters()
            {
                var ret = new Dictionary<string, object>(StringComparer.InvariantCultureIgnoreCase);

                foreach (PropertyInfo fi in GetType().GetProperties())
                {
                    object val = fi.GetValue(this, null);
                    ret.Add(fi.Name, val);
                }
                return ret;

            }
        }

        public ReportParameters Parameters { get; set; }


        #endregion

        protected override PiecesScreenViewMode VisibleMode
        {
            get { return PiecesScreenViewMode.Report; }
        }

        protected override void Populate()
        {
            Parameters.PieceId = SelectedPiece.Model.Id;
            Parameters.ModelName = SelectedPiece.Model.Description;


            RaisePropertyChanged("ReportData");
        }
    }
}