﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms.Integration;
using Microsoft.Reporting.WinForms;
using Rtp.Reporting.PiecesReportView.ViewModel;

namespace Rtp.Reporting.PiecesReportView
{
    /// <summary>
    /// Interaction logic for Report.xaml
    /// </summary>
    public partial class Report : UserControl
    {

        public Report()
        {
            InitializeComponent();
            Unloaded += ReportUnloaded;
            Loaded += ReportLoaded;

        }

        private WindowsFormsHost _whf;

        protected WindowsFormsHost Whf
        {
            get
            {
                if (_whf == null)
                {
                    _whf = CreateWhf();
                }
                return _whf;
            }
        }

        protected ReportViewer ViewerInstance
        {
            get { return Whf.Child as ReportViewer; }
        }

        private WindowsFormsHost CreateWhf()
        {

            _whf = new WindowsFormsHost
            {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                Name = "whf"
            };
            _whf.SetValue(Grid.ColumnProperty, 1);
            _whf.SetValue(Grid.RowProperty, 1);

            var viewerInstance = new ReportViewer { Name = "viewerInstance" };

            _whf.Child = viewerInstance;

            LayoutRoot.Children.Add(_whf);

            return _whf;
        }

        private void DestroyWhf()
        {
            if (_whf != null)
            {
                ViewerInstance.Reset();
                ViewerInstance.LocalReport.Dispose();
                ViewerInstance.Dispose();
                LayoutRoot.Children.Remove(_whf);
                _whf.Dispose();
                _whf = null;
            }
        }

        void ReportUnloaded(object sender, RoutedEventArgs e)
        {
            DestroyWhf();

        }
        void ReportLoaded(object sender, RoutedEventArgs e)
        {
        }


        private void ReportDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {

            var vm = DataContext as PiecesReportViewModel;

            if (vm != null)
            {
                vm.PropertyChanged += vm_PropertyChanged;
                vm.DataSourceNames = new List<string>(DataSourceNames);
            }
        }

        void vm_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "ReportData")
            {
                var vm = DataContext as PiecesReportViewModel;

                if (vm != null)
                {
                    ViewerInstance.Reset();

                    ViewerInstance.LocalReport.Dispose();

                    ReportEmbeddedResource = "Rtp.Reporting.PiecesReportView.Report.rdlc";

                    ReportDatasources = new Dictionary<string, IEnumerable>(vm.ReportDatasources);

                    ReportParameters = new Dictionary<string, object>(vm.Parameters.GetParameters());

                    Refresh();
                }
            }

        }

        #region Implementation of IReportContainer

        string ReportEmbeddedResource
        {
            set { ViewerInstance.LocalReport.ReportEmbeddedResource = value; }
        }

        IDictionary<string, IEnumerable> ReportDatasources
        {
            set
            {
                ViewerInstance.LocalReport.DataSources.Clear();

                foreach (KeyValuePair<string, IEnumerable> enumerable in value)
                {
                    ViewerInstance.LocalReport.DataSources.Add(new ReportDataSource(enumerable.Key, enumerable.Value));
                }
            }
        }

        IDictionary<string, object> ReportParameters
        {
            set
            {
                var @params = new List<ReportParameter>();

                var reportParametersInfo = ViewerInstance.LocalReport.GetParameters();
                var parametrNames = reportParametersInfo.Select(parameterInfo => parameterInfo.Name).ToList();

                foreach (KeyValuePair<string, object> keyValuePair in value)
                {
                    if (parametrNames.Contains(keyValuePair.Key))
                        @params.Add(new ReportParameter(keyValuePair.Key, keyValuePair.Value.ToString(), true));
                }

                ViewerInstance.LocalReport.SetParameters(@params);
            }
        }

        IEnumerable<string> DataSourceNames
        {
            get { return new List<string> { "DiameterDeviation", "PieceOverall" }; }
        }

        void Refresh()
        {
            ViewerInstance.RefreshReport();
        }

        #endregion

    }
}