﻿using Microsoft.Practices.ServiceLocation;
using StructureMap;
using StructureMap.ServiceLocatorAdapter;

namespace Rtp.Spooler
{
    public static class Bootstrapper
    {
        public static void Initialize()
        {
            ObjectFactory.Initialize(cfg =>
                                         {
                                             cfg.UseDefaultStructureMapConfigFile = false;
                                             cfg.IgnoreStructureMapConfig = true;
                                             cfg.AddRegistry<ServiceRegistry>();
                                         });
            ServiceLocator.SetLocatorProvider(() => new StructureMapServiceLocator(ObjectFactory.Container));
        }
    }
}