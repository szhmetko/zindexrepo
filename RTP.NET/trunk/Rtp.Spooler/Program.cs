﻿using System;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Linq;

namespace Rtp.Spooler
{


    internal static class Program
    {
        [DllImport("kernel32.dll")]
        public static extern Boolean AllocConsole();


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        private static void Main(string[] args)
        {
        //TODO: http://structuremap.net/structuremap/ConfiguringStructureMap.htm#section7

            
            bool console = args.Any((s) => s.StartsWith("/console") || s.StartsWith("-c"));

            string lineKey = args.FirstOrDefault((arg) => !arg.StartsWith("-") && !arg.StartsWith("/"));

            var serviceInstance = new RtpSpooler() { ServiceName = lineKey };

            if (args.Length > 0 && console)
            {
                AllocConsole();

                serviceInstance.Start();

                string input = string.Empty;
                Console.Write("Debug Console started. Type 'exit' to stop the application: ");


                // Wait for the user to exit the application
                while (input.ToLower() != "exit")
                {
                    input = Console.ReadLine();

                    if (input == "exit") break;

                    if (input == "stop") serviceInstance.StopService();

                    if (input == "start") serviceInstance.Start();

                    if (input == "restart") { serviceInstance.StopService(); serviceInstance.Start(); }

                }

                // Stop the application.
                serviceInstance.Stop();
            }
            else
            {
                // Initialize and run the service
                var servicesToRun = new ServiceBase[]
                                    {
                                        serviceInstance
                                    };
                ServiceBase.Run(servicesToRun);
            }
        }
    }
}