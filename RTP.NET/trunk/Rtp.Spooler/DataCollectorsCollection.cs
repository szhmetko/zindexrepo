﻿using System;
using System.Collections.Generic;
using System.Timers;
using log4net;
using Rtp.DataCollector;
using Rtp.Infrastructure;
using Rtp.Infrastructure.Exceptions;
using Rtp.Repository;
using Rtp.Repository.Repositories;


namespace Rtp.Spooler
{

    //internal enum State
    //{
    //    Started  =3
    //    ,Stopped  = 2
    //    , Initialized  =1
    //    , NotInitialized =0
    //}

    //internal class DataCollectorsCollection : IDisposable
    //{
    //    protected bool Initialized;
    //    protected bool Started;

    //    private List<DataCollector.DataCollector> _collectors = new List<DataCollector.DataCollector>();

    //    private readonly System.Timers.Timer _initializationTimer = new Timer(1e4);
    //    private readonly System.Timers.Timer _startupTimer = new Timer(1e4);
    //    private readonly ILog _logger = LogManager.GetLogger("Rtp.Spooler");

    //    protected State CurrentState = State.NotInitialized;

    //    public DataCollectorsCollection()
    //    {
    //        _initializationTimer.Elapsed += new ElapsedEventHandler(InitializationTimerElapsed);
    //        _startupTimer.Elapsed += new ElapsedEventHandler(_startupTimer_Elapsed);

    //        Initialize();
    //    }

    //    void _startupTimer_Elapsed(object sender, ElapsedEventArgs e)
    //    {
    //        _startupTimer.Enabled = false;

    //        Start();

    //    }

    //    void InitializationTimerElapsed(object sender, ElapsedEventArgs e)
    //    {
    //        _initializationTimer.Enabled = false;

    //        Initialize();

    //        Start();
    //    }

    //    protected  void Initialize()
    //    {
    //        try
    //        {
    //            _collectors.Clear();

    //            var connInfo = new ConnectionInfo("rtp_master", "sergey", "192.168.1.5", "dedabirAir");
    //            var uow = new NHUnitOfWork(connInfo.ConnectionString);

    //            var lines = new LinesRepository(uow);

    //            foreach (var line in lines.FindAll())
    //            {
    //                _collectors.Add(new DataCollector.DataCollector(line));
    //            }

    //            _logger.Info("Datacollectors created.");

    //            _initializationTimer.Enabled = false;
                
    //            CurrentState = State.Initialized;
                
    //            Initialized = true;
    //        }
    //        catch (Exception exception)
    //        {
    //            _logger.Fatal("Initialization failed! I am waiting for 10 seconds to try again.", exception);

    //            _initializationTimer.Enabled = true;

    //            _collectors.Clear();
            
    //            CurrentState = State.NotInitialized;
    //        }

    //    }

    //    public void Start()
    //    {
    //        if (Initialized)
    //        {
    //            try
    //            {
    //                _collectors.ForEach(collector => collector.Start());

    //                CurrentState = State.Started;

    //                _logger.Info("Datacollectors started.");

    //                Started = true;
    //                _startupTimer.Enabled = false;
    //            }
    //            catch (DataCollectorInitializationException exception)
    //            {
    //                _logger.Fatal("Initialization failed! I am waiting for 10 seconds to try again.", exception.InnerException);
                
    //                _startupTimer.Enabled = true;

    //            }

    //        }
    //    }


    //    public void Stop()
    //    {
    //        if (CurrentState == State.Started)
    //        {
    //            _collectors.ForEach(collector => collector.Stop());

    //            //_collectors.Clear();

    //            CurrentState = State.Stopped;

    //            _logger.Info("Datacollectors stopped.");
    //        }
    //    }

    //    public void Dispose()
    //    {
    //        _initializationTimer.Close();
    //    }
    //}
}