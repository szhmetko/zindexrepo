﻿using System.Configuration;
using Rtp.Infrastructure;
using Rtp.Infrastructure.Config;
using Rtp.Infrastructure.UnitOfWork;
using Rtp.Model.Line;
using Rtp.Model.Master;
using Rtp.Repository;
using Rtp.Repository.Repositories;
using StructureMap.Configuration.DSL;

namespace Rtp.Spooler
{
    internal class ServiceRegistry : Registry
    {
        public ServiceRegistry()
        {
            For<IUnitOfWork>().Use<NHUnitOfWork>().Ctor<ConnectionInfo>("connectionInfo")
                .Is((c) =>
                        {
                            string srv = ConfigurationSettings.AppSettings["MasterHost"];
                            string usr = ConfigurationSettings.AppSettings["MasterUser"];
                            string pwd = ConfigurationSettings.AppSettings["MasterPwd"];
                            string dbn = ConfigurationSettings.AppSettings["MasterDbName"];

                            var masterConnectionInfo = new ConnectionInfo(dbn, usr, srv, pwd);
                            return masterConnectionInfo;
                        }
                );

            For<IConnectionInfo>().Use((a) =>
            {
                return AppConfig.Instance.CurentLineConnection;
            }

                );

            For<IPiecesRepository>().Use<PiecesRepository>();

            For<ILineUnitOfWork>().Use<NHUnitOfWork>();

            For<ILinesRepository>().Use<LinesRepository>();

            For<IThermoPairsRepository>().Use<ThermoPairsRepository>();

        }
    }
}