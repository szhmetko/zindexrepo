﻿using System;
using System.ServiceProcess;
using log4net;
using Microsoft.Practices.ServiceLocation;
using Rtp.Model.Master;

namespace Rtp.Spooler
{
    public partial class RtpSpooler : ServiceBase
    {
        public RtpSpooler()
        {
            InitializeComponent();
        }

        private DataCollector.DataCollector _collector;
        private readonly ILog _logger = LogManager.GetLogger("Rtp.Spooler");

        internal void Start()
        {
            _logger.InfoFormat("Starting for {0}", ServiceName);

            try
            {
                Bootstrapper.Initialize();


                Line currentLineSettings;

                using (var linesRepository = ServiceLocator.Current.GetInstance<ILinesRepository>())
                {
                    currentLineSettings = linesRepository.FindyByAlias(ServiceName);

                    if (currentLineSettings == null)
                        throw new ApplicationException("Unable to fecth line settings by " + ServiceName);

                }

                using (var thermoPairsRepository = ServiceLocator.Current.GetInstance<IThermoPairsRepository>())
                {
                    currentLineSettings.ThermoPairs = thermoPairsRepository.FindAllValid();
                }

                _collector = new DataCollector.DataCollector(currentLineSettings);

                _collector.Start();


            }
            catch (Exception exception)
            {
                _logger.Fatal("Initialization failed!", exception);

                throw;
            }

        }

        internal void StopService()
        {
            _collector.Stop();
        }

        protected override void OnStart(string[] args)
        {
            Start();
        }

        protected override void OnStop()
        {
            StopService();
        }
    }
}