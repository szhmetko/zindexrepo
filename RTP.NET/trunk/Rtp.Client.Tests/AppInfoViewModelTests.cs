﻿using NUnit.Framework;
using Rtp.Client.ViewModel;

namespace Rtp.Client.Tests
{
    public class AppInfoViewModelTests
    {
        [Test]
        public  void CanFetchAssemblyVersion()
        {
            var vm = new AppInfoViewModel();

            Assert.IsFalse(string.IsNullOrEmpty(vm.VersionInfo));

        }


    }
}
